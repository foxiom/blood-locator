<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Format class
 * Help convert between various formats such as XML, JSON, CSV, etc.
 *
 * @author    Phil Sturgeon, Chris Kacerguis, @softwarespot
 * @license   http://www.dbad-license.org/
 */
class Pages {

//================ PAGE CATEGORY=========================
    const page_category_erro_ = 'errors';
    const page_category_admin = 'admin';
    const page_category_public = 'public';
//================ ADMIN PAGE============================
    const page_admin_login = 'admin_login';
    const page_admin_dashboard = 'admin_dashboard';
    const page_admin_registered_user_list = 'admin_registered_user_list';
    const page_admin_un_registered_user_list = 'admin_un_registered_user_list';
    const page_admin_add_edit_registered_user = 'admin_add_edit_registered_user';
    const page_admin_add_edit_un_registered_user = 'admin_add_edit_un_registered_user';
    const page_admin_request_list = 'admin_request_list';
    const page_admin_add_edit_request = 'admin_add_edit_request';
    const page_admin_profile = 'admin_profile';
    const page_admin_add_edit_hospital = 'admin_add_edit_hospital';
    const page_admin_hospital_list = 'admin_hospital_list';
    const page_admin_community_list = 'admin_community_list';
    const page_admin_community_requests = 'admin_community_request';
    const page_admin_add_edit_community = 'admin_add_edit_community';
    const page_admin_import_hospital_csv_file = 'admin_import_hospital_csv_file';
    const page_admin_country_list = 'admin_country_list';
    const page_admin_city_list = 'admin_city_list';
    const page_admin_category_list = 'admin_category_list';
    const page_admin_product_list = 'admin_product_list';
    const page_admin_product_images = 'admin_product_images';
    const page_admin_add_edit_category = 'admin_add_edit_category';
    const page_admin_add_edit_product = 'admin_add_edit_product';
    const page_admin_blog_list = 'admin_blog_list';
    const page_admin_blog_category = 'admin_blog_category';
    const page_admin_add_edit_blog_category = 'admin_add_edit_blog_category';
    const page_admin_add_edit_blog = 'admin_add_edit_blog';
    const page_admin_donation_drive_list = 'admin_donation_drive_list';
    const page_admin_donated_users_list = 'admin_donated_users_list';
    const page_admin_fitness_tips = 'admin_fitness_tips';
    const page_admin_add_edit_fitness_tips = 'admin_add_edit_fitness_tips';
    const page_admin_banners_list = 'admin_banners_list';
    const page_admin_add_edit_banner = 'admin_add_edit_banner';
    const page_admin_add_edit_product_image = 'admin_add_product_image';
}
