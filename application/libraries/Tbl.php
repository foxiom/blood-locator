<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Format class
 * Help convert between various formats such as XML, JSON, CSV, etc.
 *
 * @author    Phil Sturgeon, Chris Kacerguis, @softwarespot
 * @license   http://www.dbad-license.org/
 */
class Tbl {

    const table_auth = 'blood_bank_auth';
    const table_user = 'blood_bank_user';
    const table_request = 'blood_bank_request';
    const table_country = 'blood_bank_country';
    const table_state = 'blood_bank_state';
    const table_device_master = 'blood_bank_device_master';
    const table_hospital = 'blood_bank_hospital';
    const table_chat = 'blood_bank_chat_history';
    const table_community = 'blood_bank_community';
    const table_community_subscription = 'blood_bank_community_subscription';
    const table_donation_history='blood_bank_donation_history';
    const table_community_request = 'blood_bank_community_request';
    const table_category = 'blood_bank_category';
    const table_product = 'blood_bank_product';
    const table_blog = 'blood_bank_blog';
    const table_donation_drive = 'blood_bank_donation_drive';
    const table_donated_users = 'blood_bank_donated_users';
    const table_fitness_tips = 'blood_bank_fitness_tips';
    const table_joined_drive_users = 'blood_bank_drive_joined_users';
    const table_favourites_drive_users = 'blood_bank_favouriite_drive_users';
    const table_blog_category = 'blood_bank_blog_category';
    const table_blog_favourites = 'blood_bank_blog_favourites';
    const table_banners = 'blood_bank_banners';
    const table_hydration = 'blood_bank_hydration';
    const table_hydration_lines = 'blood_bank_hydration_lines';
    const table_cart = 'blood_bank_cart';
    const table_cart_lines = 'blood_bank_cart_lines';
    const table_orders = 'blood_bank_orders';
    const table_order_lines = 'blood_bank_order_lines';
    const table_settings = 'blood_bank_settings';
    const table_user_address = 'blood_bank_user_address';
    const table_hydration_tips = 'blood_bank_hydration_tips';
    const table_product_images = 'blood_bank_product_images';
    const table_notification = 'blood_bank_notifications';
}

