<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AppConst
{

    //    ============= APP CONFIG =========================
    const APPNAME = "BLOOD-LOCATOR";
    const FIREBASE_KEY = 'AAAAeC2LiGU:APA91bElhhhWyINEfN-Jcu0e56_kXk73HsHtU_bVFRMwX5VXPEBRlUgPplsE-YNXhCctYfuu7Z78vZpqdnofvea6r0JiVIZdVq2BibvwO60RkBtewmfcjQGrDM-ZBO8Uhf3x1Nac3J21';
    const ONESIGNAL_APP_ID = '0243e28b-eb99-4026-b80b-a43df8e48886';
    const INDEX = '';
    const LOGO_PATH = 'webres/admin/images/icon.png';
    const TIMEZONE = 'Asia/Kolkata';
    const GOOGLE_MAP_API_KEY = 'AIzaSyCW2WhUnUKJwHizAPnHZwBy99ncKhFJbbk';
    const PLATFORM_WEB = 'WEB';
    const PLATFORM_ANDROID = 'ANDROID';
    const PLATFORM_IOS = 'IOS';
    const PAGINATION_LIMIT = 50;
    const APP_PAGINATION_LIMIT = 10;

    const BLOOD_GROUP_A_PLUS_ID = 5;
    const BLOOD_GROUP_A_MINUS_ID = 10;
    const BLOOD_GROUP_B_PLUS_ID = 15;
    const BLOOD_GROUP_B_MINUS_ID = 20;
    const BLOOD_GROUP_O_PLUS_ID = 25;
    const BLOOD_GROUP_O_MINUS_ID = 30;
    const BLOOD_GROUP_AB_PLUS_ID = 35;
    const BLOOD_GROUP_AB_MINUS_ID = 40;

    const BLOOD_GROUP_A_PLUS = 'A+';
    const BLOOD_GROUP_A_MINUS = 'A-';
    const BLOOD_GROUP_B_PLUS = 'B+';
    const BLOOD_GROUP_B_MINUS = 'B-';
    const BLOOD_GROUP_O_PLUS = 'O+';
    const BLOOD_GROUP_O_MINUS = 'O-';
    const BLOOD_GROUP_AB_PLUS = 'AB+';
    const BLOOD_GROUP_AB_MINUS = 'AB-';

    const GENDER_MALE_ID = 1;
    const GENDER_FEMALE_ID = 2;

    const GENDER_MALE = 'Male';
    const GENDER_FEMALE = 'Female';

    const BLOOD_GROUP_DISTANCE = 10;
    const HOSPITALS_DISTANCE = 50;
    const HOSPITALS_SEARCH_LIMIT = 20;

    const BLOCK = 2;
    const ACTIVE = 1;
    const CHAT_TYPE_REQUEST = 3;
    const CHAT_TYPE_NORMAL = 4;



    const CHAT_PUSH_NOTIFICATION_SENT_STATUS = -1;
    const CHAT_DELIEVRED_STATUS = 0;
    const CHAT_READ_STATUS = 1;

    const API_AUTH_KEY = 'bloodlocator/2021';

    const NOTIFICATION_TYPE_REQUEST = 10;
    const NOTIFICATION_TYPE_CHAT = 20;

    const PREFER_TYPE_O_GROUP = 10;
    const PREFER_TYPE_ALL_GROUP = 20;

    const CHAT_STATUS_PENDING = 10;
    const CHAT_STATUS_ACCEPT = 30;
    const CHAT_STATUS_REJECT = 20;
    const CHAT_STATUS_DONATED = 40;

    const JOINED_TO_COMMUNITY = 1;
    const NOT_JOINED_TO_COMMUNITY = 2;

    const SOURCE_FROM_OUTSIDE_ID = 10;
    const SOURCE_BLOOD_LOCATOR_CALL_ID = 20;
    const SOURCE_BLOOD_LOCATOR_CHAT_ID = 30;
    const SOURCE_BLOOD_LOCATOR_REQUEST_ID = 40;
    //    =============== Order type ======================
    const ORDER_COD = 5;
    const ORDER_PAYED = 10;
    //    =============== image folder roots ================
    const upload_folder_root = 'uploads';
    const upload_folder_hospital = 'uploads/hospital';
    const upload_folder_user = 'uploads/user';
    const upload_folder_community = 'uploads/community';
    const upload_folder_product = 'uploads/product';
    const upload_folder_banner = 'uploads/banner';

    //    =============== USER TYPES ========================
    const USER_TYPE_ADMIN = 10;
    const USER_TYPE_COMMUNITY_ADMIN = 15;
    const USER_TYPE_UNREGISTERED_USER = 20;
    const USER_TYPE_REGISTERED_USER = 30;

    const USER_VERIFIED = 1;
    const USER_NOT_VERIFIED = 0;
    //  ============== Community Request Status =========
    const COMMUNITY_REQUEST_NEW = 1;
    const COMMUNITY_REQUEST_ACCEPT = 2;
    const COMMUNITY_REQUEST_REJECT = 3;
    // ================ Fitness Category ==============
    const WORKOUT_ROUTINE = 7;
    const BEGINNER_ROUTINE = 8;
    // ================ Banner Category ==============
    const STATIC_BANNER = 4;
    const HERO_BANNER = 5;
    const FITNESS_BANNER = 6;
    //    ==============  Hydration =============
    const DAILY_WATER_INTAKE = 2400;
    const ONE_GLASS_WATER = 300;
    //   =============== Order Status =============
    const ORDER_PLACED = 5;
    const ORDER_CHECKOUT = 7;
    //    ============== states of item =========
    const STATUS_ACTIVE = 1;
    const STATUS_BLOCK = 2;
    const STATUS_CLOSED = 3;
    const currenPage_KEY = 'CURRENTPAGE';
    const session_key_user = 'BLOOD_BANK_USER_SESSION_KEY';
    const feedbackmessageKey = 'feedbackMESSAGE';
    const errorcode_success = 0;
    const errorindex_code = 'errorcode';
    const errorindex_message = 'errormessage';
    const errorMessage_db_general_error = "Operaion failed/Data not updated to database";
    const errorMessage_db_general_success = "Operation successful/Data  updated to database";
    const errormessage_success = 'Action successfully completed';
    const res_value = 'values';
    const res_value1 = 'values1';
    const res_value2 = 'values2';
    const res_value3 = 'values3';
    const res_value4 = 'values4';
    //    ============== ERROR MESSGES =======================
    const errormessage_nosession = 'No session found!';
    const errormessage_db_output_success = 'success';
    const errormessage_db_insertion_success = 'Successfully Added/Updated';
    const errormessage_db_updation_success = 'successfully updated';
    const errormessage_db_deletion_success = 'successfully deleted';
    const errormessage_db_request_success = 'request successfully send';
    const errormessage_db_registration_success = 'successfully registered';
    const errormessage_profile_updation_success = 'profile updated successfully';

    const chat_blood_request_msg = 'new blood request';
}
