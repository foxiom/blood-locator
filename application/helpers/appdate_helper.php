<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



if (!function_exists('getDayCountInBetween')) {

    function getDayCountInBetween($date1, $date2) {
        $d2 = strtotime($date2);
        $d1 = strtotime($date1);
        $datediff = $d2 - $d1;
        $dayCount = floor($datediff / (60 * 60 * 24));
        return $dayCount;
    }

}

if (!function_exists('getFirstDayOfMonth')) {

    function getFirstDayOfMonth($date = null) {
        $day = null;
        if ($date != null) {
            //date("Y-m-t", strtotime($day));
        } else {
            $day = date('Y-m-01 00:00:01');
        }
        return $day;
    }

}

if (!function_exists('getLastDayOfMonth')) {

    function getLastDayOfMonth($date = null) {
        $day = null;
        if ($date != null) {
            
        } else {
            $day = date('Y-m-t 23:59:59');
        }
        return date("Y-m-t 23:59:59", strtotime($day));
    }

}
if (!function_exists('appdateformat')) {

    function appdateformat($originalDate) {
        return date("d-m-Y H:i:s", strtotime($originalDate));
    }

}

if (!function_exists('getCurrentDateWithTime')) {

    function getCurrentDateWithTime() {
        date_default_timezone_set('Asia/Kolkata');
        return date('Y-m-d H:i:s');
    }

}

if (!function_exists('getCurrentDate')) {

    function getCurrentDate() {
        date_default_timezone_set('Asia/Kolkata');
        return date('Y-m-d');
    }

}

if (!function_exists('getCurrentTime')) {

    function getCurrentTime() {
        date_default_timezone_set('Asia/Kolkata');
        return date('H:i:s');
    }

}

if (!function_exists('appDateFormat')) {

    function appDateFormat($originalDate) {
        if (isset($originalDate) && trim($originalDate) != '') {
            if ($isTimeAdded) {
                return date("d-m-Y H:i:s", strtotime($originalDate));
            } else {
                return date("d-m-Y", strtotime($originalDate));
            }
        } else {
            return '';
        }
    }

}

if (!function_exists('uiDate2DBdate')) {

    function uiDate2DBdate($originalDate, $isTimeAdded = false) {
        if (isset($originalDate) && trim($originalDate) != '') {
            if ($isTimeAdded) {
                return date("Y-m-d H:i:s", strtotime($originalDate));
            } else {
                return date("Y-m-d", strtotime($originalDate));
            }
        } else {
            return '';
        }
    }

}

if (!function_exists('dbDate2UIdate')) {

    function dbDate2UIdate($originalDate, $isTimeAdded = false) {
        if (isset($originalDate) && trim($originalDate) != '') {
            if ($isTimeAdded) {
                return date("d-m-Y H:i:s", strtotime($originalDate));
            } else {
                return date("d-m-Y", strtotime($originalDate));
            }
        } else {
            return '';
        }
    }

}

/*
  if (!function_exists('appDateDDMMMMMMYYYY')) {

  function appDateDDMMMMMMYYYY($inputDate) {
  return date("d F Y", strtotime($inputDate));
  }

  }

  if (!function_exists('addDateDDMMMYY')) {

  function addDateDDMMMYY($inputDate) {
  return date("d-M-y", strtotime($inputDate));
  }

  } */

//---------------------------

if (!function_exists('dateStrToTimestap_start')) {

    function dateStrToTimestap_start() {
        date_default_timezone_set('Asia/Kolkata');
        return date('Y-m-d H:i:s');
    }

}
if (!function_exists('convertStringtodate')) {

    function convertStringtodate($strDate) {

        $time = strtotime($strDate);

        $dateval = date('Y-m-d H:i:s', $time);
        return $dateval;
    }

}
if (!function_exists('dateStrToTimestap_end')) {

    function dateStrToTimestap_end() {
        date_default_timezone_set('Asia/Kolkata');
        return date('Y-m-d H:i:s');
    }

}

if (!function_exists('currentdateMilsec')) {

    function currentdateMilsec() {
        $milliseconds = round(microtime(true) * 1000);
        return $milliseconds;
    }

}



if (!function_exists('convertStringtodate')) {

    function convertStringtodate($strDate) {
        $time = strtotime($strDate);

        $dateval = date('Y-m-d H:i:s', $time);
        return $dateval;
    }

}


if (!function_exists('isCurrentYear')) {

    function isCurrentYear($Date) {

        $currentYear = date('Y');
        $updatedYear = date('Y', strtotime($Date));
        if ($currentYear == $updatedYear) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}


if (!function_exists('isCurrentMonth')) {

    function isCurrentMonth($Date) {

        $currentMonth = date('m');
        $updatedMonth = date('m', strtotime($Date));
        if ($currentMonth == $updatedMonth) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}


if (!function_exists('getFirstDayByMonthYear')) {

    function getFirstDayByMonthYear($month, $year) {

        $firstDay = $year . '-' . $month . '-01';

        $firstDay = date('Y-m-01', strtotime($firstDay));
        //print_r($firstDay);
        return $firstDay;
    }

}



if (!function_exists('getLastDayByMonthYear')) {

    function getLastDayByMonthYear($month, $year) {

        $lastDay = $year . '-' . $month . '-31';
        $lastDay = date('Y-m-t', strtotime($lastDay));
        return $lastDay;
    }

}

if (!function_exists('getCurrentYear')) {

    function getCurrentYear() {
        date_default_timezone_set(AppConst::TIMEZONE);
        return date('Y');
    }

}
if (!function_exists('getCurrentMonth')) {

    function getCurrentMonth() {
        date_default_timezone_set(AppConst::TIMEZONE);
        return date('m');
    }

}
if (!function_exists('getCurrentDay')) {

    function getCurrentDay() {
        date_default_timezone_set(AppConst::TIMEZONE);
        return date('d');
    }

}
if (!function_exists('getCurrentHour')) {

    function getCurrentHour() {
        date_default_timezone_set(AppConst::TIMEZONE);
        return date('H');
    }

}
if (!function_exists('getCurrentMinute')) {

    function getCurrentMinute() {
        date_default_timezone_set(AppConst::TIMEZONE);
        return date('i');
    }

}
if (!function_exists('getCurrentSecond')) {

    function getCurrentSecond() {
        date_default_timezone_set(AppConst::TIMEZONE);
        return date('s');
    }

}