<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');




if (!function_exists('uploadImage')) {

    function uploadFile($uploadreq,  $uploadfolder = AppConst::upload_folder_root,$fileType = 'img') {
        $uploadFileindex = $uploadreq['uploadfileIndex'];
        $CI = & get_instance();
        $CI->load->library('upload'); // load library 
        $res = array();
        $key = null; 
        $key = ($key == null ? keygens() . uniqid('_id_') : $key . uniqid('_id_'));

        if (isset($uploadreq) && $uploadreq != null) {
            if (!is_dir($uploadfolder)) {
                mkdir($uploadfolder, 0777, TRUE);
            }
            $fileConfig['upload_path'] = $uploadfolder;
            if($fileType === 'img'){
            $fileConfig['max_size'] = (1024 * 4); // 4mb file size
            // image only
            $fileConfig['allowed_types'] = 'gif|jpg|png|jpeg';
            }else{
                $fileConfig['max_size'] = (1024 * 4); // 4mb file size
            // image only
            $fileConfig['allowed_types'] = 'pdf|doc|docx|txt';
            }
           // $fileConfig['max_width'] = (1024 * 4);
           // $fileConfig['max_height'] = (1024 * 4);
            $fileConfig['create_thumb'] = TRUE;
            $fileConfig['overwrite'] = TRUE;
            printv($uploadreq, 'files1111');

            printv($_FILES, 'files');
            
            $uploadfilename = $_FILES[$uploadFileindex]['name'];

            printv('kkkkkkkkkkkkkkkkkkkk');
            $fileinfo = pathinfo($uploadfilename);
            if (isset($fileinfo['extension'])) {
                $ext = $fileinfo['extension'];
                // $fileConfig['file_name'] = $key . '.png'; //. $ext;
                $fileConfig['file_name'] = $key . '_binocular_' . '.' . $ext;
                //printv($fileConfig, '4444444444444444444');
                $CI->upload->initialize($fileConfig);

                if (!$CI->upload->do_upload($uploadFileindex)) {
                    $error = $CI->upload->display_errors();
                    
                    $res[AppConst::errorindex_message] = 'This image not supported';//$error . " Supported file format (gif|jpg|png|jpeg) , Max image size, 4000X4000 and Max Size 3.5 MB";
                    $res[AppConst::errorindex_code] = 100;
                    // printv('3333333333333333333');
                } else {
                    $result['uploadedFileInfo'] = array('upload_data' => $CI->upload->data());
                    //printv($result, 'resul');
                    $filepath = $fileConfig['upload_path'] . '/' . $fileConfig['file_name'];
                    $res[AppConst::res_value] = $filepath;
                    $res[AppConst::errorindex_message] = 'Succcess';
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    // printv('222222222222222222222');
                }
            } else {
                $res[AppConst::errorindex_code] = 11;
                $res[AppConst::errorindex_message] = 'Invalid image';
            }
        } else {
            // printv('11111111111111111111');
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'File data is empty';
        }
        printv($res);

        return $res;
    }

}

if (!function_exists('base64fileupload')) {

    function base64fileupload($base64, $key, $uploadfolder = Cons::profileimage_upload_folder) {
        $res = array();
        if ($base64 != null && $key != null) {
            if (!is_dir($uploadfolder)) {
                mkdir($uploadfolder, 0777, TRUE);
            }

            $uploadfileCompltepath = $uploadfolder . DIRECTORY_SEPARATOR . $key . '.png';
            $ifp = fopen($uploadfileCompltepath, "wb"); // open file with wb mode
            fwrite($ifp, base64_decode($base64));
            fclose($ifp);
            $res[Cons::errorcodeIndex] = Cons::errorcodeSuccess;
            $res[Cons::errormessageIndex] = 'success';
            $res[Cons::resValue] = $uploadfileCompltepath;
        } else {
            $res[Cons::errorcodeIndex] = 10;
            $res[Cons::errormessageIndex] = 'File data is empty';
        }
        return $res;
    }

}

if (!function_exists('uploadImageWithResize')) {

    function uploadImageWithResize($uploadreq, $uploadfolder = AppConst::upload_folder_root) {
        $uploadFileindex = $uploadreq['uploadfileIndex'];
        $CI = & get_instance();
        $CI->load->library('upload'); // load library 
        $res = array();
        $key = null;
        $key = ($key == null ? keygens() . uniqid('_id_') : $key . uniqid('_id_'));

        if (isset($uploadreq) && $uploadreq != null) {
            if (!is_dir($uploadfolder)) {
                mkdir($uploadfolder, 0777, TRUE);
            }
            $fileConfig['upload_path'] = $uploadfolder;
            $fileConfig['allowed_types'] = 'gif|jpg|png|jpeg';
            
            // $fileConfig['max_width'] = (1024 * 4);
            // $fileConfig['max_height'] = (1024 * 4);
            $fileConfig['create_thumb'] = TRUE;
            $fileConfig['overwrite'] = TRUE;

            $uploadfilename = $_FILES[$uploadFileindex]['name'];

            $fileinfo = pathinfo($uploadfilename);
            if (isset($fileinfo['extension'])) {
                $ext = $fileinfo['extension'];
                // $fileConfig['file_name'] = $key . '.png'; //. $ext;
                $fileConfig['file_name'] = $key . '_'.AppConst::APPNAME.'-' . '.' . $ext;
                //printv($fileConfig, '4444444444444444444');
                $CI->upload->initialize($fileConfig);

                if (!$CI->upload->do_upload($uploadFileindex)) {
                    $error = $CI->upload->display_errors();

                    $res[AppConst::errorindex_message] = 'This image not supported'.$error; //$error . " Supported file format (gif|jpg|png|jpeg) , Max image size, 4000X4000 and Max Size 3.5 MB";
                    $res[AppConst::errorindex_code] = 100;
                    // printv('3333333333333333333');
                } else {

                    $image_data = $CI->upload->data();
                    $CI->load->library('image_lib');
                    $w = $image_data['image_width']; // original image's width
                    $h = $image_data['image_height']; // original images's height

                    $n_w = 273; // destination image's width
                    $n_h = 246; // destination image's height

                    $source_ratio = $w / $h;
                    $new_ratio = $n_w / $n_h;

                    if ($source_ratio != $new_ratio) {

                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $fileConfig['upload_path'] . '/' . $fileConfig['file_name'];
                        $config['maintain_ratio'] = FALSE;
                        if ($new_ratio > $source_ratio || (($new_ratio == 1) && ($source_ratio < 1))) {
                            $config['width'] = $w;
                            $config['height'] = round($w / $new_ratio);
                            $config['y_axis'] = round(($h - $config['height']) / 2);
                            $config['x_axis'] = 0;
                        } else {

                            $config['width'] = round($h * $new_ratio);
                            $config['height'] = $h;
                            $size_config['x_axis'] = round(($w - $config['width']) / 2);
                            $size_config['y_axis'] = 0;
                        }

                        $CI->image_lib->initialize($config);
                        $CI->image_lib->crop();
                        $CI->image_lib->clear();
                    }
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $fileConfig['upload_path'] . '/' . $fileConfig['file_name'];
                    $config['new_image'] = $fileConfig['upload_path'] . '/' . $fileConfig['file_name'];
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = $n_w;
                    $config['height'] = $n_h;
                    $config['overwrite'] = TRUE;
                    $CI->image_lib->initialize($config);
                    if (!$CI->image_lib->resize()) {

                        $res[AppConst::errorindex_code] = 11;
                        $res[AppConst::errorindex_message] = $CI->image_lib->display_errors();
                    } else {
                       
                        $filepath = $fileConfig['upload_path'] . '/' . $fileConfig['file_name'];
                        $res[AppConst::res_value] = $filepath;
                        $res[AppConst::errorindex_message] = 'Succcess';
                        $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    }
                }
            } else {
                $res[AppConst::errorindex_code] = 11;
                $res[AppConst::errorindex_message] = 'Invalid image';
            }
        } else {
            // printv('11111111111111111111');
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'File data is empty';
        }
        return $res;
    }
}
    