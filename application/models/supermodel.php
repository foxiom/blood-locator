<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserInfo
 *
 * @author Jaleel
 */
class supermodel extends CI_Model {

    

    public function __construct() {

        $this->load->database();
        $this->load->helper('keygen');
          //$this->load->library("Tbl");
         //$this->load->library(array('login'));
         
    }
    
    
    
    protected function isValidKey($key, $type){
        
        return ((isset($key) && trim($key) != "" && $key != null) );
    }
    
    
   protected function getPaginationCountry($filter = array()){
       
        $config = array();
        $config["base_url"] = base_url() . "countries";
        $per_page = 25;
        $total_row = $this->countryRecordCount($filter);
        $config["total_rows"] = $total_row;
        $config["per_page"] = $per_page;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = $total_row;

        $config['cur_tag_open'] = '&nbsp;<a class="current">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';

        
       
        return $config;
       
   }
   
    protected function getTransactionReport($filter = null) {
        //startDate ,$endDate,ledgerNo,userId 
        $this->db->select('*');
        $this->db->from(Tbl::table_transaction_master . ' as transaction');
        $this->db->join(Tbl::table_account_master . ' as account', ' account.accountNo = transaction.tranLedgerNo ', 'left');
        /*$this->db->join(Tbl::table_customer_master . ' as customer', ' customer.userId = account.userId ', 'left');
        $this->db->join(Tbl::table_address_master . ' as address', ' address.userId = customer.userId ', 'left');
        $this->db->join(Tbl::table_user . ' as user', ' user.userIdpk = customer.userId ', 'left');*/
        $this->db->order_by('transaction.tranAddedDate','desc');
        if(isset($filter['limit'])  && intval($filter['limit']) > 0 ){
            $this->db->limit(intval($filter['limit']));
        }
        if(isset($filter['userId']) && intval($filter['userId']) > 0){
            $this->db->where('userId.userId',$filter['userId']);
        }
        
        if(isset($filter['ledgerNo']) && trim($filter['ledgerNo']) !=''){
            $this->db->where('transaction.tranLedgerNo ',$filter['ledgerNo']);
        }
        
        if(isset($filter['startDate']) && trim($filter['startDate']) !=''){
            $this->db->where('transaction.tranAddedDate >=', trim($filter['startDate'])." 00:00:01");
        
            
        }
        if(isset($filter['endDate']) && trim($filter['endDate']) !=''){
            $this->db->where('transaction.tranAddedDate <=', trim($filter['endDate'])." 23:59:59");
            
        }
        
        //================
        
        $tranlist = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($tranlist) && count($tranlist) > 0 ) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
        $res[AppConst::res_value] = $tranlist;
        
        return $res;
    }
    protected function getTempTransactionReport($filter = null) {
        //startDate ,$endDate,ledgerNo,userId 
        $this->db->select('*');
        $this->db->from(Tbl::table_temp_transaction_master . ' as temp_transaction');
        $this->db->join(Tbl::table_account_master . ' as account', ' account.accountNo = temp_transaction.tempTranLedgerNo ', 'left');
        
        $this->db->order_by('temp_transaction.tempTranAddedDate','desc');
        if(isset($filter['limit'])  && intval($filter['limit']) > 0 ){
            $this->db->limit(intval($filter['limit']));
        }
        if(isset($filter['userId']) && intval($filter['userId']) > 0){
            $this->db->where('userId.userId',$filter['userId']);
        }
        
        if(isset($filter['ledgerNo']) && trim($filter['ledgerNo']) !=''){
            $this->db->where('temp_transaction.tempTranLedgerNo ',$filter['ledgerNo']);
        }
        
        if(isset($filter['startDate']) && trim($filter['startDate']) !=''){
            $this->db->where('temp_transaction.tempTranAddedDate >=', trim($filter['startDate'])." 00:00:01");
        
            
        }
        if(isset($filter['endDate']) && trim($filter['endDate']) !=''){
            $this->db->where('temp_transaction.tempTranAddedDate <=', trim($filter['endDate'])." 23:59:59");
            
        }
        
        //================
        
        $tranlist = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($tranlist) && count($tranlist) > 0 ) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
        $res[AppConst::res_value] = $tranlist;
        
        return $res;
    }
    
    public function sendNotification( $push) {


            $registration_ids = array($push['fcmkey']);


            $ch = curl_init("https://fcm.googleapis.com/fcm/send");
            $header = array('Content-Type: application/json',
                "Authorization: key=AAAA_OxToX0:APA91bHk55f9PT110fYwbBhE7YAko1l9QgYw2AVjxqFGTlaaFRKNhMHmcbk0kp7g5m48f0tO43DYJSsmfYPuTIvgf8U_WXaJwmAN5vuA4EzReJYz8jsXKpfZBNcDKnyvg_jWehaaz2f3");
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $sendData['userId'] = getval($push, 'userId');
            $sendData['type'] = "public";
            $sendData['title'] = $push['title'];
            $sendData['message'] = $push['message'];
            $sendData['messageId'] = getval($push, 'userId');



            $msg['data']['title'] = $push['title'];
            $msg['data']['message'] = $push['message'];
            $msg['data']['userId'] = getval($push, 'userId');
            $msg['data']['type']= AppConst::MESSAGE_TYPE_STATUS_BLOCKED;
            //$msg['data']['regNo'] = getval($push, 'regNo');
            $msg['data']['payload'] = $sendData;


            $fields = array(
                'registration_ids' => $registration_ids,
                'data' => $msg,
            );

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            
            if ($result === FALSE) {
                die('FCM Send Error: ' . curl_error($ch));
            }
            curl_close($ch);

            return $result;
        
    }
  
    
    
    
    
    
    
}