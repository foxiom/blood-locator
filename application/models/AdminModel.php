<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('supermodel.php');

class AdminModel extends supermodel
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("Tbl");
        $this->load->helper(array('fileupload'));
    }

    public function authenticateBy($username, $password, $commingFrom = null)
    {
        $res = array();

        $this->db->select('*');
        $this->db->from(Tbl::table_auth . ' as login');
        $this->db->join(Tbl::table_user . ' as user', ' login.user_id = user.user_id ', 'left');
        $this->db->where('user.user_type', AppConst::USER_TYPE_ADMIN);
        $this->db->where('login.username', $username);
        $this->db->where('login.password', $password);
        $this->db->limit(1);
        $query = $this->db->get();
        lastqry($this->db);
        $resultArray = $query->result_array();
        if ($resultArray != null && isset($resultArray) && count($resultArray) > 0) {

            $userInfo = isset($resultArray[0]) ? $resultArray[0] : null;
            if ($userInfo != null) {
                $userStatus = intval(getval($userInfo, 'status'));

                if ($userStatus == AppConst::STATUS_ACTIVE) {
                    $res[AppConst::errorindex_code] = 0;
                    $res[AppConst::errorindex_message] = "Login success";
                    $res[AppConst::res_value] = $userInfo;
                } else {
                    $res[AppConst::errorindex_code] = 10;
                    $res[AppConst::errorindex_message] = "Your account is blocked.";
                }
            } else {
                $res[AppConst::errorindex_code] = 11;
                $res[AppConst::errorindex_message] = "Some error occured during your login";
            }
        } else {
            $res[AppConst::errorindex_code] = 12;
            $res[AppConst::errorindex_message] = "Invalid username/password.";
        }

        return $res;
    }

    public function getUserById($userId)
    {
        $userid = isset($userId) ? $userId : 0;
        if ($userid > 0) {
            $this->db->select('*');
            $this->db->from(Tbl::table_user . ' as user ');
            $this->db->join(Tbl::table_auth . ' as auth', ' auth.user_id = user.user_id', 'left');
            $this->db->where('user.user_id', $userId);
            $userinfo = $this->db->get()->result_array();
            $res[AppConst::errorindex_code] = (isset($userinfo) && count($userinfo) > 0) ? AppConst::errorcode_success : 10;
            $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No record found.";
            $res[AppConst::res_value] = $userinfo;
            return $res;
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'user could not found..';
            return $res;
        }
    }

    public function blockActiveRequst($filter)
    {
        $this->db->where('request_id', getval($filter, 'request_id'));
        $this->db->update(Tbl::table_request, $filter);
        $insertID = ($this->db->affected_rows() > 0) ? TRUE : 0;
        if ($insertID > 0) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_updation_success : "data updation failed..";
            $res[AppConst::res_value] = $insertID;
            return $res;
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'data updation failed..';
            return $res;
        }
    }

    public function blockActiveHospital($filter)
    {
        $this->db->where('hospital_id', getval($filter, 'hospital_id'));
        $this->db->update(Tbl::table_hospital, $filter);
        $insertID = ($this->db->affected_rows() > 0) ? TRUE : 0;
        if ($insertID > 0) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_updation_success : "data updation failed..";
            $res[AppConst::res_value] = $insertID;
            return $res;
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'data updation failed..';
            return $res;
        }
    }

    public function blockActiveUnRegisterdUser($filter)
    {
        $this->db->where('user_id', getval($filter, 'user_id'));
        $this->db->update(Tbl::table_user, $filter);
        $insertID = ($this->db->affected_rows() > 0) ? TRUE : 0;
        if ($insertID > 0) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_updation_success : "data updation failed..";
            $res[AppConst::res_value] = $insertID;
            return $res;
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'data updation failed..';
            return $res;
        }
    }

    public function changeProfilePassword($password = array())
    {
        $userid = isset($password['user_id']) ? getval($password, 'user_id') : 0;
        if ($userid > 0) {
            $dataToDb['updated_date'] = getCurrentDateWithTime();
            $dataToDb['password'] = getval($password, 'newpassword');
            $this->db->where('user_id', getval($password, 'user_id'));
            $this->db->where('password', getval($password, 'oldpassword'));
            $this->db->update(Tbl::table_auth, $dataToDb);
            $insertID = ($this->db->affected_rows() > 0) ? $userid : 0;
            if ($insertID > 0) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_updation_success : "data updation failed";
                $res[AppConst::res_value] = $insertID;
                return $res;
            } else {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'Data updation failed..';
                return $res;
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'No user id found..';
            return $res;
        }
    }

    public function addEditRegisteredUser($userData = array())
    {
        $userId = isset($userData['user_id']) ? intval(getval($userData, 'user_id')) : 0;
        $dataToUser['first_name'] = getval($userData, 'first_name');
        $dataToUser['last_name'] = getval($userData, 'last_name');
        $dataToUser['dob'] = uiDate2DBdate(getval($userData, 'dob'), FALSE);
        $dataToUser['email'] = getval($userData, 'email');
        $dataToUser['contact_no'] = getval($userData, 'contact_no');
        $dataToUser['blood_group'] = getval($userData, 'blood_group');
        $dataToUser['gender'] = getval($userData, 'gender');
        $dataToUser['address'] = getval($userData, 'address');
        $dataToUser['country'] = getval($userData, 'country');
        $dataToUser['state'] = getval($userData, 'state');
        $dataToUser['home_location_lat'] = getval($userData, 'home_location_lat');
        $dataToUser['home_location_long'] = getval($userData, 'home_location_long');
        $dataToUser['home_location_place'] = getval($userData, 'home_location_place');
        $dataToUser['work_location_lat'] = getval($userData, 'work_location_lat');
        $dataToUser['work_location_long'] = getval($userData, 'work_location_long');
        $dataToUser['work_location_place'] = getval($userData, 'work_location_place');
        $dataToUser['last_date_donation'] = uiDate2DBdate(getval($userData, 'last_date_donation'), FALSE);
        $dataToUser['updated_date'] = getCurrentDateWithTime();
        $userData['uploadfileIndex'] = 'pro_image';
        $imgUploadRes = uploadImageWithResize($userData, AppConst::upload_folder_user);
        if ($imgUploadRes[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $dataToUser['pro_image'] = $imgUploadRes[AppConst::res_value];
        }

        $this->db->trans_begin();
        if ($userId > 0) {
            $this->db->where('user_id', $userId);
            $this->db->update(Tbl::table_user, $dataToUser);
            $userUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($userUpdate == FALSE) {
                $this->db->trans_complete();
                $this->db->trans_rollback();
                $res[AppConst::errorindex_code] = 200;
                $res[AppConst::errorindex_message] = "user updation failed..";
                return $res;
            }
            $dataToLogin['updated_date'] = getCurrentDateWithTime();
            $this->db->where('user_id', $userId);
            $this->db->update(Tbl::table_auth, $dataToLogin);
            $loginUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($loginUpdate == FALSE) {
                $this->db->trans_complete();
                $this->db->trans_rollback();
                $res[AppConst::errorindex_code] = 200;
                $res[AppConst::errorindex_message] = "login updation failed..";
                return $res;
            }
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
            $res[AppConst::res_value] = $userId;
        } else {
            $dataToUser['user_type'] = AppConst::USER_TYPE_REGISTERED_USER;
            $dataToUser['added_date'] = getCurrentDateWithTime();
            $dataToUser['status'] = AppConst::STATUS_ACTIVE;
            $this->db->insert(Tbl::table_user, $dataToUser);
            $userId = $this->db->insert_id();
            if ($userId <= 0) {
                $this->db->trans_complete();
                $this->db->trans_rollback();
                $res[AppConst::errorindex_code] = 100;
                $res[AppConst::errorindex_message] = "user insertion failed.. ";
                return $res;
            }
            $dataToLogin['username'] = getval($userData, 'contact_no');
            $dataToLogin['password'] = '123456x';
            $dataToLogin['user_id'] = $userId;
            $dataToLogin['added_date'] = getCurrentDateWithTime();
            $dataToLogin['updated_date'] = getCurrentDateWithTime();
            $this->db->insert(Tbl::table_auth, $dataToLogin);
            $loginId = $this->db->insert_id();
            if ($loginId <= 0) {
                $this->db->trans_complete();
                $this->db->trans_rollback();
                $res[AppConst::errorindex_code] = 200;
                $res[AppConst::errorindex_message] = "login insertion failed.. ";
                return $res;
            }
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
            $res[AppConst::res_value] = $userId;
        }
        $this->db->trans_complete();
        if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $this->db->trans_commit();
        } else {
            $this->db->trans_rollback();
        }
        return $res;
    }

    public function addEditunRegisteredUser($userData = array())
    {
        $userId = isset($userData['user_id']) ? intval(getval($userData, 'user_id')) : 0;
        $dataToUser['first_name'] = getval($userData, 'first_name');
        $dataToUser['last_name'] = getval($userData, 'last_name');
        $dataToUser['contact_no'] = getval($userData, 'contact_no');
        $dataToUser['blood_group'] = getval($userData, 'blood_group');
        $dataToUser['gender'] = getval($userData, 'gender');
        $dataToUser['address'] = getval($userData, 'address');
        $dataToUser['home_location_lat'] = getval($userData, 'home_location_lat');
        $dataToUser['home_location_long'] = getval($userData, 'home_location_long');
        $dataToUser['home_location_place'] = getval($userData, 'home_location_place');
        $dataToUser['updated_date'] = getCurrentDateWithTime();

        $this->db->trans_begin();
        if ($userId > 0) {
            $this->db->where('user_id', $userId);
            $this->db->update(Tbl::table_user, $dataToUser);
            $userUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($userUpdate == FALSE) {
                $this->db->trans_complete();
                $this->db->trans_rollback();
                $res[AppConst::errorindex_code] = 200;
                $res[AppConst::errorindex_message] = "user updation failed..";
                return $res;
            }
            $dataToLogin['username'] = getval($userData, 'contact_no');
            $dataToLogin['updated_date'] = getCurrentDateWithTime();
            $this->db->where('user_id', $userId);
            $this->db->update(Tbl::table_auth, $dataToLogin);
            $loginUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($loginUpdate == FALSE) {
                $this->db->trans_complete();
                $this->db->trans_rollback();
                $res[AppConst::errorindex_code] = 200;
                $res[AppConst::errorindex_message] = "login updation failed..";
                return $res;
            }
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
            $res[AppConst::res_value] = $userId;
        } else {
            $dataToUser['user_type'] = AppConst::USER_TYPE_UNREGISTERED_USER;
            $dataToUser['added_date'] = getCurrentDateWithTime();
            $dataToUser['status'] = AppConst::STATUS_ACTIVE;
            $this->db->insert(Tbl::table_user, $dataToUser);
            $userId = $this->db->insert_id();
            if ($userId <= 0) {
                $this->db->trans_complete();
                $this->db->trans_rollback();
                $res[AppConst::errorindex_code] = 100;
                $res[AppConst::errorindex_message] = "user insertion failed.. ";
                return $res;
            }
            $dataToLogin['username'] = getval($userData, 'contact_no');
            $dataToLogin['password'] = '123456x';
            $dataToLogin['user_id'] = $userId;
            $dataToLogin['added_date'] = getCurrentDateWithTime();
            $dataToLogin['updated_date'] = getCurrentDateWithTime();
            $this->db->insert(Tbl::table_auth, $dataToLogin);
            $loginId = $this->db->insert_id();
            if ($loginId <= 0) {
                $this->db->trans_complete();
                $this->db->trans_rollback();
                $res[AppConst::errorindex_code] = 200;
                $res[AppConst::errorindex_message] = "login insertion failed.. ";
                return $res;
            }
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
            $res[AppConst::res_value] = $userId;
        }
        $this->db->trans_complete();
        if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $this->db->trans_commit();
        } else {
            $this->db->trans_rollback();
        }
        return $res;
    }

    public function updateProfile($dataToDb)
    {
        $userid = isset($dataToDb['user_id']) ? getval($dataToDb, 'user_id') : 0;
        if ($userid > 0) {
            $dataToDb['updated_date'] = getCurrentDateWithTime();
            $this->db->where('user_id', getval($dataToDb, 'user_id'));
            $this->db->update(Tbl::table_user, $dataToDb);
            $insertID = ($this->db->affected_rows() > 0) ? getval($dataToDb, 'user_id') : 0;
            if ($insertID > 0) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_updation_success : "data updation failed..";
                $res[AppConst::res_value] = $insertID;
                return $res;
            } else {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'Data updation failed..';
                return $res;
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'No user id found..';
            return $res;
        }
    }

    public function checkContactNoAlreadyExistOrNot($contactno)
    {
        $this->db->where('username', $contactno);
        $this->db->or_where('contact_no', $contactno);
        $this->db->from(Tbl::table_user . ' as user ');
        $this->db->join(Tbl::table_auth . ' as login', 'login.user_id = user.user_id', 'left');
        $userList = $this->db->get()->result_array();
        if (count($userList) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getRegisteredUsers($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_user . ' as user ');
        $this->db->join(Tbl::table_auth . ' as login', 'login.user_id = user.user_id', 'left');
        // $this->db->where('user.user_type', AppConst::USER_TYPE_REGISTERED_USER);
        if (isset($filter['user_id']) && intval(getval($filter, 'user_id')) > 0) {
            $this->db->where('user.user_id', getval($filter, 'user_id'));
        }
        if (isset($filter['status']) && intval(getval($filter, 'status')) > 0) {
            $this->db->where('user.status', getval($filter, 'status'));
        }
        if (isset($filter['blood_group']) && intval(getval($filter, 'blood_group')) > 0) {
            $this->db->where('user.blood_group', getval($filter, 'blood_group'));
        }
        if (isset($filter['user_type']) && intval(getval($filter, 'user_type')) > 0) {
            $this->db->where('user.user_type', getval($filter, 'user_type'));
        }

        if (isset($filter['page'])) {
            $this->db->limit(AppConst::PAGINATION_LIMIT, getval($filter, 'page'));
        }
        $this->db->order_by('user.updated_date', 'desc');
        $userlist = $this->db->get()->result_array();

        $res[AppConst::errorindex_code] = (isset($userlist) && count($userlist) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No user record found.";
        $res[AppConst::res_value] = $userlist;
        return $res;
    }

    public function getAllCountry($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_country . ' as country ');
        if (getval($filter, 'country_id') != '') {
            $this->db->where('country_id', getval($filter, 'country_id'));
        }
        if (getval($filter, 'status') != '') {
            $this->db->where('status', getval($filter, 'status'));
        }
        if (isset($filter['page'])) {
            $this->db->limit(AppConst::PAGINATION_LIMIT, getval($filter, 'page'));
        }
        $countrylist = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($countrylist) && count($countrylist) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No user record found.";
        $res[AppConst::res_value] = $countrylist;
        return $res;
    }

    public function getAllState($filter = array())
    {
        $this->db->select('state.*,country.country_name,country.country_code');
        $this->db->from(Tbl::table_state . ' as state ');
        $this->db->join(Tbl::table_country . ' as country', ' country.country_id = state.country_id ', 'left');
        if (isset($filter['country_id']) && intval(getval($filter, 'country_id')) > 0) {
            $this->db->where('state.country_id', getval($filter, 'country_id'));
        }
        if (isset($filter['state_id']) && intval(getval($filter, 'state_id')) > 0) {
            $this->db->where('state.state_id', getval($filter, 'state_id'));
        }
        if (isset($filter['status']) && intval(getval($filter, 'status')) > 0) {
            $this->db->where('state.status', getval($filter, 'status'));
        }
        if (isset($filter['page'])) {
            $this->db->limit(AppConst::PAGINATION_LIMIT, getval($filter, 'page'));
        }
        $citylist = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($citylist) && count($citylist) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No user record found.";
        $res[AppConst::res_value] = $citylist;
        return $res;
    }

    public function addEditRequest($requestData = array())
    {
        $requestId = isset($requestData['request_id']) ? intval(getval($requestData, 'request_id')) : 0;
        $dataToRequest['date_of_need'] = uiDate2DBdate(getval($requestData, 'date_of_need'), FALSE);
        $dataToRequest['no_of_units'] = getval($requestData, 'no_of_units');
        $dataToRequest['donation_address'] = getval($requestData, 'donation_address');
        $dataToRequest['blood_group'] = getval($requestData, 'blood_group');
        $dataToRequest['contact_person'] = getval($requestData, 'contact_person');
        $dataToRequest['contact_no'] = getval($requestData, 'contact_no');
        $dataToRequest['location_lat'] = getval($requestData, 'location_lat');
        $dataToRequest['location_long'] = getval($requestData, 'location_long');
        $dataToRequest['updated_date'] = getCurrentDateWithTime();
        if ($requestId > 0) {
            $this->db->where('request_id', $requestId);
            $this->db->update(Tbl::table_request, $dataToRequest);
            $requestUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($requestUpdate == TRUE) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
                return $res;
            } else {
                $res[AppConst::errorindex_code] = 10;
                $res[AppConst::errorindex_message] = 'updation failed';
                return $res;
            }
        } else {
            $dataToRequest['status'] = AppConst::STATUS_ACTIVE;
            $dataToRequest['added_date'] = getCurrentDateWithTime();
            $this->db->insert(Tbl::table_request, $dataToRequest);
            $requestId = $this->db->insert_id();
            if ($requestId > 0) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
                return $res;
            } else {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'insertion failed';
                return $res;
            }
        }
    }

    public function addEditCategory($requestData = array())
    {
        $categoryId = isset($requestData['category_id']) ? intval(getval($requestData, 'category_id')) : 0;
        $dataToRequest['category_name'] = getval($requestData, 'category_name');
        $dataToRequest['updated_at'] = getCurrentDateWithTime();
        if ($categoryId > 0) {
            $this->db->where('id', $categoryId);
            $this->db->update(Tbl::table_category, $dataToRequest);
            $requestUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($requestUpdate == TRUE) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
                return $res;
            } else {
                $res[AppConst::errorindex_code] = 10;
                $res[AppConst::errorindex_message] = 'updation failed';
                return $res;
            }
        } else {
            $dataToRequest['status'] = AppConst::STATUS_ACTIVE;
            $dataToRequest['created_at'] = getCurrentDateWithTime();
            $this->db->insert(Tbl::table_category, $dataToRequest);
            $categoryId = $this->db->insert_id();
            if ($categoryId > 0) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
                return $res;
            } else {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'insertion failed';
                return $res;
            }
        }
    }

    public function addEditBlogCategory($requestData = array())
    {
        $categoryId = isset($requestData['category_id']) ? intval(getval($requestData, 'category_id')) : 0;
        $dataToRequest['category_name'] = getval($requestData, 'category_name');
        $dataToRequest['updated_at'] = getCurrentDateWithTime();
        if ($categoryId > 0) {
            $this->db->where('id', $categoryId);
            $this->db->update(Tbl::table_blog_category, $dataToRequest);
            $requestUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($requestUpdate == TRUE) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
                return $res;
            } else {
                $res[AppConst::errorindex_code] = 10;
                $res[AppConst::errorindex_message] = 'updation failed';
                return $res;
            }
        } else {
            $dataToRequest['category_status'] = AppConst::STATUS_ACTIVE;
            $dataToRequest['created_at'] = getCurrentDateWithTime();
            $this->db->insert(Tbl::table_blog_category, $dataToRequest);
            $categoryId = $this->db->insert_id();
            if ($categoryId > 0) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
                return $res;
            } else {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'insertion failed';
                return $res;
            }
        }
    }

    public function addEditBanner($requestData = array())
    {
        $bannerId = isset($requestData['banner_id']) ? intval(getval($requestData, 'banner_id')) : 0;
        $dataToRequest['type'] = getval($requestData, 'type');
        $dataToRequest['updated_at'] = getCurrentDateWithTime();
        $requestData['uploadfileIndex'] = 'banner_url';
        $imgUploadRes = uploadImageWithResize($requestData, AppConst::upload_folder_banner);
        if ($imgUploadRes[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $dataToRequest['banner_url'] = $imgUploadRes[AppConst::res_value];
        }
        if ($bannerId > 0) {
            $this->db->where('id', $bannerId);
            $this->db->update(Tbl::table_banners, $dataToRequest);
            $requestUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($requestUpdate == TRUE) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
                return $res;
            } else {
                $res[AppConst::errorindex_code] = 10;
                $res[AppConst::errorindex_message] = 'updation failed';
                return $res;
            }
        } else {
            $dataToRequest['status'] = AppConst::STATUS_ACTIVE;
            $dataToRequest['created_at'] = getCurrentDateWithTime();
            $this->db->insert(Tbl::table_banners, $dataToRequest);
            $bannerId = $this->db->insert_id();
            if ($bannerId > 0) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
                return $res;
            } else {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'insertion failed';
                return $res;
            }
        }
    }

    public function addEditProduct($requestData = array())
    {
        $productId = isset($requestData['product_id']) ? intval(getval($requestData, 'product_id')) : 0;
        $dataToRequest['product_name'] = getval($requestData, 'product_name');
        $dataToRequest['product_details'] = getval($requestData, 'product_details');
        $dataToRequest['regular_price'] = getval($requestData, 'regular_price');
        $dataToRequest['offer_price'] = getval($requestData, 'offer_price');
        $temp1 = $dataToRequest['regular_price'] - $dataToRequest['offer_price'];
        $temp2 = $temp1 / $dataToRequest['regular_price'];
        $temp2 = round($temp2 * 100);
        $dataToRequest['offer_percentage'] = $temp2;
        $dataToRequest['country'] = getval($requestData, 'country');
        $dataToRequest['category'] = getval($requestData, 'category');
        $requestData['uploadfileIndex'] = 'image_url';
        $imgUploadRes = uploadImageWithResize($requestData, AppConst::upload_folder_user);
        if ($imgUploadRes[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $dataToRequest['image_url'] = $imgUploadRes[AppConst::res_value];
        }
        $dataToRequest['updated_at'] = getCurrentDateWithTime();
        if ($productId > 0) {
            $this->db->where('id', $productId);
            $this->db->update(Tbl::table_product, $dataToRequest);
            $requestUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($requestUpdate == TRUE) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
                return $res;
            } else {
                $res[AppConst::errorindex_code] = 10;
                $res[AppConst::errorindex_message] = 'updation failed';
                return $res;
            }
        } else {
            $dataToRequest['product_status'] = AppConst::STATUS_ACTIVE;
            $dataToRequest['created_at'] = getCurrentDateWithTime();
            $this->db->insert(Tbl::table_product, $dataToRequest);
            $productId = $this->db->insert_id();
            if ($productId > 0) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
                return $res;
            } else {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'insertion failed';
                return $res;
            }
        }
    }

    public function addEditProductImage($requestData = array())
    {
        $productId = isset($requestData['product_id']) ? intval(getval($requestData, 'product_id')) : 0;
        $requestData['uploadfileIndex'] = 'image';
        $imgUploadRes = uploadImageWithResize($requestData, AppConst::upload_folder_user);
        if ($imgUploadRes[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $dataToRequest['image'] = $imgUploadRes[AppConst::res_value];
        }
        $dataToRequest['updated_at'] = getCurrentDateWithTime();
        $dataToRequest['product_id'] = $productId;
        $dataToRequest['created_at'] = getCurrentDateWithTime();
        $this->db->insert(Tbl::table_product_images, $dataToRequest);
        $Id = $this->db->insert_id();
        $res['product_id'] = $productId;
        if ($Id > 0) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
            return $res;
        } else {
            $res[AppConst::errorindex_code] = 20;
            $res[AppConst::errorindex_message] = 'insertion failed';
            return $res;
        }
    }

    public function updateProductStatus($productInfo)
    {
        $res = array();
        $product_id = getval($productInfo, 'product_id');
        $status = getval($productInfo, 'status');
        if ($product_id > 0 && $status > 0) {
            $dataToUpdate['product_status'] = $status;
            $dataToUpdate['updated_at'] = getCurrentDateWithTime();
            $this->db->where('id', $product_id);
            $this->db->update(Tbl::table_product, $dataToUpdate);
            $countryUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($countryUpdate == TRUE) {
                if ($status == AppConst::STATUS_BLOCK) {
                    $this->db->where('id', $product_id);
                    $this->db->update(Tbl::table_product, $dataToUpdate);
                    $countryUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
                } else {
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
                }
            } else {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'can not update product status';
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'invalid request';
        }
        return $res;
    }

    public function deleteProductImage($imageId)
    {
        $res = array();
        $this->db->where('id', $imageId);
        $this->db->delete(Tbl::table_product_images);
        $countryUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
        if ($countryUpdate == TRUE) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
        } else {
            $res[AppConst::errorindex_code] = 20;
            $res[AppConst::errorindex_message] = 'can not delete product';
        }
        return $res;
    }

    public function updateCategoryStatus($categoryInfo)
    {
        $res = array();
        $category_id = getval($categoryInfo, 'category_id');
        $status = getval($categoryInfo, 'status');
        if ($category_id > 0 && $status > 0) {
            $dataToUpdate['status'] = $status;
            $dataToUpdate['updated_at'] = getCurrentDateWithTime();
            $this->db->where('id', $category_id);
            $this->db->update(Tbl::table_category, $dataToUpdate);
            $countryUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($countryUpdate == TRUE) {
                if ($status == AppConst::STATUS_BLOCK) {
                    $this->db->where('id', $category_id);
                    $this->db->update(Tbl::table_category, $dataToUpdate);
                    $countryUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
                } else {
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
                }
            } else {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'can not update category status';
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'invalid request';
        }
        return $res;
    }

    public function updateBlogCategoryStatus($categoryInfo)
    {
        $res = array();
        $category_id = getval($categoryInfo, 'category_id');
        $status = getval($categoryInfo, 'status');
        if ($category_id > 0 && $status > 0) {
            $dataToUpdate['category_status'] = $status;
            $dataToUpdate['updated_at'] = getCurrentDateWithTime();
            $this->db->where('id', $category_id);
            $this->db->update(Tbl::table_blog_category, $dataToUpdate);
            $countryUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($countryUpdate == TRUE) {
                if ($status == AppConst::STATUS_BLOCK) {
                    $this->db->where('id', $category_id);
                    $this->db->update(Tbl::table_blog_category, $dataToUpdate);
                    $countryUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
                } else {
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
                }
            } else {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'can not update category status';
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'invalid request';
        }
        return $res;
    }

    public function updateDriveStatus($driveInfo)
    {
        $res = array();
        $drive_id = getval($driveInfo, 'drive_id');
        $status = getval($driveInfo, 'status');
        if ($drive_id > 0 && $status > 0) {
            $dataToUpdate['drive_status'] = $status;
            $dataToUpdate['updated_at'] = getCurrentDateWithTime();
            $this->db->where('id', $drive_id);
            $this->db->update(Tbl::table_donation_drive, $dataToUpdate);
            $countryUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($countryUpdate == TRUE) {
                if ($status == AppConst::STATUS_BLOCK) {
                    $this->db->where('id', $drive_id);
                    $this->db->update(Tbl::table_donation_drive, $dataToUpdate);
                    $countryUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
                } else {
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
                }
            } else {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'can not update drive status';
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'invalid request';
        }
        return $res;
    }

    public function getAllRequests($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_request);
        if (isset($filter['request_id']) && intval(getval($filter, 'request_id')) > 0) {
            $this->db->where('request_id', getval($filter, 'request_id'));
        }
        if (isset($filter['status']) && intval(getval($filter, 'status')) > 0) {
            $this->db->where('status', getval($filter, 'status'));
        }
        if (isset($filter['blood_group']) && intval(getval($filter, 'blood_group')) > 0) {
            $this->db->where('blood_group', getval($filter, 'blood_group'));
        }
        if (isset($filter['page'])) {
            $this->db->limit(AppConst::PAGINATION_LIMIT, getval($filter, 'page'));
        }
        $this->db->order_by('updated_date', 'desc');
        $requestlist = $this->db->get()->result_array();

        $res[AppConst::errorindex_code] = (isset($requestlist) && count($requestlist) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No user record found.";
        $res[AppConst::res_value] = $requestlist;
        return $res;
    }

    public function addEditHospital($dataTomodel)
    {

        $hospitalid = isset($dataTomodel['hospital_id']) ? intval($dataTomodel['hospital_id']) : 0;
        $dataTomodel['uploadfileIndex'] = 'image_url';
        $imgUploadRes = uploadImageWithResize($dataTomodel, AppConst::upload_folder_hospital);
        if ($imgUploadRes[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $dataTodb['image_url'] = $imgUploadRes[AppConst::res_value];
        }
        $dataTodb['hospital_name'] = getval($dataTomodel, 'hospital_name');
        $dataTodb['place'] = getval($dataTomodel, 'place');
        $dataTodb['address'] = getval($dataTomodel, 'address');
        $dataTodb['contact_no'] = getval($dataTomodel, 'contact_no');
        $dataTodb['contact_no2'] = getval($dataTomodel, 'contact_no2');
        $dataTodb['latitude'] = getval($dataTomodel, 'latitude');
        $dataTodb['longitude'] = getval($dataTomodel, 'longitude');
        $dataTodb['state_id'] = getval($dataTomodel, 'state_id');
        $dataTodb['country_id'] = getval($dataTomodel, 'country_id');
        $dataTodb['updated_date'] = getCurrentDateWithTime();
        if ($hospitalid > 0) {
            $this->db->where('hospital_id', $hospitalid);
            $this->db->update(Tbl::table_hospital, $dataTodb);
            $hospitalid = ($this->db->affected_rows() > 0) ? $hospitalid : 0;
        } else {

            $dataTodb['added_date'] = getCurrentDateWithTime();
            $this->db->insert(Tbl::table_hospital, $dataTodb);
            $hospitalid = $this->db->insert_id();
        }
        $res[AppConst::errorindex_code] = ($hospitalid > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ?
            AppConst::errorMessage_db_general_success : AppConst::errorMessage_db_general_error;
        return $res;
    }

    public function getAllHospital($filter = array())
    {

        $this->db->select('*');
        $this->db->from(Tbl::table_hospital . ' as hospital');
        $this->db->join(Tbl::table_state . ' as state', ' state.state_id = hospital.state_id ', 'left');
        $this->db->join(Tbl::table_country . ' as country', ' country.country_id = hospital.country_id ', 'left');
        if (isset($filter['hospital_id']) && intval(getval($filter, 'hospital_id')) > 0) {
            $this->db->where('hospital.hospital_id', getval($filter, 'hospital_id'));
        }
        if (isset($filter['hospital_name']) && trim(getval($filter, 'hospital_name')) != "") {
            $this->db->like('hospital.hospital_name', getval($filter, 'hospital_name'));
        }

        if (isset($filter['page'])) {
            $this->db->limit(AppConst::PAGINATION_LIMIT, getval($filter, 'page'));
        }
        $this->db->order_by('hospital.updated_date', 'desc');
        $hospitallist = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($hospitallist) && count($hospitallist) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No hospital record found.";
        $res[AppConst::res_value] = $hospitallist;
        return $res;
    }

    public function getRegisterdcount($filter = array())
    {
        $this->db->where('user_type', getval($filter, 'user_type'));
        $this->db->from(Tbl::table_user);
        $res = $this->db->count_all_results();

        return $res;
    }

    public function getRequestcount($filter = array())
    {
        $res = $this->db->count_all(Tbl::table_request);
        return $res;
    }

    public function getHospitalcount($filter = array())
    {
        $res = $this->db->count_all(Tbl::table_hospital);
        return $res;
    }

    public function getCommunitycount($filter = array())
    {
        $res = $this->db->count_all(Tbl::table_community);
        return $res;
    }

    public function getCommunityRequestcount($filter = array())
    {
        $res = $this->db->count_all(Tbl::table_community_request);
        return $res;
    }

    public function getCategorycount($filter = array())
    {
        $res = $this->db->count_all(Tbl::table_category);
        return $res;
    }

    public function getBlogCategorycount($filter = array())
    {
        $res = $this->db->count_all(Tbl::table_blog_category);
        return $res;
    }

    public function getBannersCount($filter = array())
    {
        $res = $this->db->count_all(Tbl::table_banners);
        return $res;
    }

    public function getProductcount($filter = array())
    {
        $res = $this->db->count_all(Tbl::table_product);
        return $res;
    }

    public function getBlogcount($filter = array())
    {
        $res = $this->db->count_all(Tbl::table_blog);
        return $res;
    }

    public function getDrivecount($filter = array())
    {
        $res = $this->db->count_all(Tbl::table_donation_drive);
        return $res;
    }

    public function getDonatedUsersCount($filter = array())
    {
        $res = $this->db->count_all(Tbl::table_donated_users);
        return $res;
    }

    public function getFitnessTipsCount($filter = array())
    {
        $res = $this->db->count_all(Tbl::table_fitness_tips);
        return $res;
    }

    public function getStateCount($filter = array())
    {
        $res = $this->db->count_all(Tbl::table_state);
        return $res;
    }

    public function getCountryCount($filter = array())
    {
        $res = $this->db->count_all(Tbl::table_country);
        return $res;
    }

    public function getDashBoardInfo($filter = array())
    {
        $qrysellerDashbord = "SELECT"
            . " (SELECT count(*)   FROM " . Tbl::table_user . "  WHERE user_type = '" . AppConst::USER_TYPE_REGISTERED_USER . "'  ) AS registered_user_count ,"
            . " (SELECT count(*)  FROM " . Tbl::table_user . "  WHERE user_type = '" . AppConst::USER_TYPE_UNREGISTERED_USER . "'  ) AS unregistered_user_count, "
            . " (SELECT count(*)  FROM " . Tbl::table_request . "  WHERE status != '" . AppConst::STATUS_BLOCK . "' ) AS request_count, "
            . " (SELECT count(*)  FROM " . Tbl::table_hospital . "  WHERE hospital_status != '" . AppConst::STATUS_BLOCK . "' ) AS hospital_count ";


        $qry = $this->db->query($qrysellerDashbord);

        $summary = $qry->result_array();


        $res[AppConst::errorindex_code] = (isset($summary) && count($summary) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
        if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $res[AppConst::res_value] = $summary[0];
        }
        return $res;
    }

    public function getAllCommunity($filter = array())
    {

        $this->db->select('*');
        $this->db->from(Tbl::table_community);
        if (isset($filter['page'])) {
            $this->db->limit(AppConst::PAGINATION_LIMIT, getval($filter, 'page'));
        }
        if (isset($filter['community_id']) && intval(getval($filter, 'community_id')) > 0) {
            $this->db->where('community_id', getval($filter, 'community_id'));
        }
        $this->db->order_by('community_updated_date', 'desc');
        $communitylist = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($communitylist) && count($communitylist) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No hospital record found.";
        $res[AppConst::res_value] = $communitylist;
        return $res;
    }

    public function getAllCategory($filter = array())
    {

        $this->db->select('*');
        $this->db->from(Tbl::table_category);
        if (isset($filter['page'])) {
            $this->db->limit(AppConst::PAGINATION_LIMIT, getval($filter, 'page'));
        }
        if (isset($filter['id']) && intval(getval($filter, 'id')) > 0) {
            $this->db->where('id', getval($filter, 'id'));
        }
        if (isset($filter['status']) && intval(getval($filter, 'status')) > 0) {
            $this->db->where('status', getval($filter, 'status'));
        }
        $this->db->order_by('created_at', 'desc');
        $communitylist = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($communitylist) && count($communitylist) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No hospital record found.";
        $res[AppConst::res_value] = $communitylist;
        return $res;
    }

    public function getAllBlogCategory($filter = array())
    {

        $this->db->select('*');
        $this->db->from(Tbl::table_blog_category);
        if (isset($filter['page'])) {
            $this->db->limit(AppConst::PAGINATION_LIMIT, getval($filter, 'page'));
        }
        if (isset($filter['id']) && intval(getval($filter, 'id')) > 0) {
            $this->db->where('id', getval($filter, 'id'));
        }
        if (isset($filter['status']) && intval(getval($filter, 'status')) > 0) {
            $this->db->where('category_status', getval($filter, 'status'));
        }
        $this->db->order_by('created_at', 'desc');
        $communitylist = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($communitylist) && count($communitylist) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No hospital record found.";
        $res[AppConst::res_value] = $communitylist;
        return $res;
    }

    public function getAllBanners($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_banners);
        if (isset($filter['page'])) {
            $this->db->limit(AppConst::PAGINATION_LIMIT, getval($filter, 'page'));
        }
        if (isset($filter['id']) && intval(getval($filter, 'id')) > 0) {
            $this->db->where('id', getval($filter, 'id'));
        }
        $this->db->order_by('created_at', 'desc');
        $bannerslist = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($bannerslist) && count($bannerslist) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No hospital record found.";
        $res[AppConst::res_value] = $bannerslist;
        return $res;
    }

    public function getAllProduct($filter = array())
    {
        $this->db->select('product.*,category.category_name');
        $this->db->from(Tbl::table_product . ' as product');
        $this->db->join(Tbl::table_category . ' as category', 'category.id = product.category', 'left');
        if (isset($filter['page'])) {
            $this->db->limit(AppConst::PAGINATION_LIMIT, getval($filter, 'page'));
        }
        if (isset($filter['id']) && intval(getval($filter, 'id')) > 0) {
            $this->db->where('product.id', getval($filter, 'id'));
        }
        $this->db->order_by('product.created_at', 'desc');
        $communitylist = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($communitylist) && count($communitylist) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No hospital record found.";
        $res[AppConst::res_value] = $communitylist;
        return $res;
    }

    public function getAllProductImages($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_product_images);
        if (isset($filter['page'])) {
            $this->db->limit(AppConst::PAGINATION_LIMIT, getval($filter, 'page'));
        }
        if (isset($filter['product_id']) && intval(getval($filter, 'product_id')) > 0) {
            $this->db->where('product_id', getval($filter, 'product_id'));
        }
        $this->db->order_by('created_at', 'desc');
        $communitylist = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($communitylist) && count($communitylist) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No hospital record found.";
        $res[AppConst::res_value] = $communitylist;
        return $res;
    }

    public function getAllBlog($filter = array())
    {
        $this->db->select('blog.*,category.category_name');
        $this->db->from(Tbl::table_blog . ' as blog');
        $this->db->join(Tbl::table_blog_category . ' as category', 'blog.category = category.id', 'left');
        if (isset($filter['page'])) {
            $this->db->limit(AppConst::PAGINATION_LIMIT, getval($filter, 'page'));
        }
        if (isset($filter['id']) && intval(getval($filter, 'id')) > 0) {
            $this->db->where('blog.id', getval($filter, 'id'));
        }
        $this->db->order_by('blog.created_at', 'desc');
        $bloglist = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($bloglist) && count($bloglist) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No hospital record found.";
        $res[AppConst::res_value] = $bloglist;
        return $res;
    }

    public function addEditBlog($requestData = array())
    {
        $blogId = isset($requestData['blog_id']) ? intval(getval($requestData, 'blog_id')) : 0;
        $dataToRequest['title'] = getval($requestData, 'title');
        $dataToRequest['description'] = getval($requestData, 'description');
        $dataToRequest['youtube_url'] = getval($requestData, 'youtube_url');
        $dataToRequest['category'] = getval($requestData, 'category');
        $requestData['uploadfileIndex'] = 'image_url';
        $imgUploadRes = uploadImageWithResize($requestData, AppConst::upload_folder_community);
        if ($imgUploadRes[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $dataToRequest['image_url'] = $imgUploadRes[AppConst::res_value];
        }
        $dataToRequest['updated_at'] = getCurrentDateWithTime();
        if ($blogId > 0) {
            $this->db->where('id', $blogId);
            $this->db->update(Tbl::table_blog, $dataToRequest);
            $requestUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($requestUpdate == TRUE) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
                return $res;
            } else {
                $res[AppConst::errorindex_code] = 10;
                $res[AppConst::errorindex_message] = 'updation failed';
                return $res;
            }
        } else {
            $dataToRequest['blog_status'] = AppConst::STATUS_ACTIVE;
            $dataToRequest['created_at'] = getCurrentDateWithTime();
            $this->db->insert(Tbl::table_blog, $dataToRequest);
            $blogId = $this->db->insert_id();
            if ($blogId > 0) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
                return $res;
            } else {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'insertion failed';
                return $res;
            }
        }
    }

    public function addEditFitnessTip($requestData = array())
    {
        $tipId = isset($requestData['tip_id']) ? intval(getval($requestData, 'tip_id')) : 0;
        $dataToRequest['title'] = getval($requestData, 'title');
        $dataToRequest['description'] = getval($requestData, 'description');
        $dataToRequest['category_type'] = getval($requestData, 'category_type');
        $dataToRequest['vedio_url'] = getval($requestData, 'vedio_url');
        $dataToRequest['updated_at'] = getCurrentDateWithTime();
        if ($tipId > 0) {
            $this->db->where('id', $tipId);
            $this->db->update(Tbl::table_fitness_tips, $dataToRequest);
            $requestUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($requestUpdate == TRUE) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
                return $res;
            } else {
                $res[AppConst::errorindex_code] = 10;
                $res[AppConst::errorindex_message] = 'updation failed';
                return $res;
            }
        } else {
            $dataToRequest['status'] = AppConst::STATUS_ACTIVE;
            $dataToRequest['created_at'] = getCurrentDateWithTime();
            $this->db->insert(Tbl::table_fitness_tips, $dataToRequest);
            $tipId = $this->db->insert_id();
            if ($tipId > 0) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
                return $res;
            } else {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'insertion failed';
                return $res;
            }
        }
    }

    public function getAllDonationDrive($filter = array())
    {
        $this->db->select('drive.*,community.community_name,user.first_name,user.last_name');
        $this->db->from(Tbl::table_donation_drive . ' as drive');
        $this->db->join(Tbl::table_community . ' as community', 'community.community_id = drive.community', 'left');
        $this->db->join(Tbl::table_user . ' as user', 'user.user_id = drive.created_user_id', 'left');
        if (isset($filter['page'])) {
            $this->db->limit(AppConst::PAGINATION_LIMIT, getval($filter, 'page'));
        }
        if (isset($filter['id']) && intval(getval($filter, 'id')) > 0) {
            $this->db->where('drive.id', getval($filter, 'id'));
        }
        $this->db->order_by('drive.created_at', 'desc');
        $bloglist = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($bloglist) && count($bloglist) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No hospital record found.";
        $res[AppConst::res_value] = $bloglist;
        return $res;
    }

    public function getAllDonatedUsersList($filter = array())
    {
        $this->db->select('donated_users.*,community.community_name,user.first_name,user.last_name');
        $this->db->from(Tbl::table_donated_users . ' as donated_users');
        $this->db->join(Tbl::table_donation_drive . ' as drive', 'drive.id = donated_users.donation_drive_id', 'left');
        $this->db->join(Tbl::table_community . ' as community', 'community.community_id = drive.community', 'left');
        $this->db->join(Tbl::table_user . ' as user', 'user.user_id = drive.created_user_id', 'left');
        if (isset($filter['page'])) {
            $this->db->limit(AppConst::PAGINATION_LIMIT, getval($filter, 'page'));
        }
        if (isset($filter['id']) && intval(getval($filter, 'id')) > 0) {
            $this->db->where('drive.id', getval($filter, 'id'));
        }
        $this->db->order_by('drive.created_at', 'desc');
        $bloglist = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($bloglist) && count($bloglist) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No hospital record found.";
        $res[AppConst::res_value] = $bloglist;
        return $res;
    }
    public function getAllFitnessTips($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_fitness_tips);
        if (isset($filter['page'])) {
            $this->db->limit(AppConst::PAGINATION_LIMIT, getval($filter, 'page'));
        }
        if (isset($filter['id']) && intval(getval($filter, 'id')) > 0) {
            $this->db->where('id', getval($filter, 'id'));
        }
        $this->db->order_by('created_at', 'desc');
        $fitnessList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($fitnessList) && count($fitnessList) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No hospital record found.";
        $res[AppConst::res_value] = $fitnessList;
        return $res;
    }

    public function blockActiveCommunity($communityInfo = array())
    {
        $communityId = intval(getval($communityInfo, 'community_id'));
        $status = intval(getval($communityInfo, 'status'));
        if ($communityId > 0 && $status > 0) {
            $dataToUpdate['community_updated_date'] = getCurrentDateWithTime();
            $dataToUpdate['community_status'] = $status;
            $this->db->where('community_id', $communityId);
            $this->db->update(Tbl::table_community, $dataToUpdate);
            $communityUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($communityUpdate == TRUE) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;

                return $res;
            } else {
                $res[AppConst::errorindex_code] = 10;
                $res[AppConst::errorindex_message] = 'status updation failed..';
                return $res;
            }
        } else {
            $res[AppConst::errorindex_code] = 20;
            $res[AppConst::errorindex_message] = 'invalid request';
            return $res;
        }
    }

    public function updateBlogStatus($blogInfo)
    {
        $res = array();
        $blog_id = getval($blogInfo, 'blog_id');
        $status = getval($blogInfo, 'status');
        if ($blog_id > 0 && $status > 0) {
            $dataToUpdate['blog_status'] = $status;
            $dataToUpdate['updated_at'] = getCurrentDateWithTime();
            $this->db->where('id', $blog_id);
            $this->db->update(Tbl::table_blog, $dataToUpdate);
            $countryUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($countryUpdate == TRUE) {
                if ($status == AppConst::STATUS_BLOCK) {
                    $this->db->where('id', $blog_id);
                    $this->db->update(Tbl::table_blog, $dataToUpdate);
                    $countryUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
                } else {
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
                }
            } else {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'can not update blog status';
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'invalid request';
        }
        return $res;
    }

    public function addEditCommunity($dataTomodel)
    {
        $communityId = isset($dataTomodel['community_id']) ? intval($dataTomodel['community_id']) : 0;
        $dataTomodel['uploadfileIndex'] = 'community_image_url';
        $imgUploadRes = uploadImageWithResize($dataTomodel, AppConst::upload_folder_community);
        if ($imgUploadRes[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $dataTodb['community_image_url'] = $imgUploadRes[AppConst::res_value];
        }

        $dataTodb['community_name'] = getval($dataTomodel, 'community_name');
        $dataTodb['community_country'] = getval($dataTomodel, 'community_country');
        $dataTodb['community_address'] = getval($dataTomodel, 'community_address');
        $dataTodb['date_of_establishment'] = uiDate2DBdate(getval($dataTomodel, 'date_of_establishment'), FALSE);
        $dataTodb['community_lat'] = getval($dataTomodel, 'community_lat');
        $dataTodb['community_long'] = getval($dataTomodel, 'community_long');
        $dataTodb['community_updated_date'] = getCurrentDateWithTime();
        if ($communityId > 0) {
            $this->db->where('community_id', $communityId);
            $this->db->update(Tbl::table_community, $dataTodb);
            $communityId = ($this->db->affected_rows() > 0) ? $communityId : 0;
        } else {
            $dataTodb['community_status'] = AppConst::STATUS_ACTIVE;
            $dataTodb['community_added_date'] = getCurrentDateWithTime();
            $this->db->insert(Tbl::table_community, $dataTodb);
            $communityId = $this->db->insert_id();
        }
        $res[AppConst::errorindex_code] = ($communityId > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ?
            AppConst::errorMessage_db_general_success : AppConst::errorMessage_db_general_error;
        return $res;
    }

    public function getAllCommunityRequests($filter = array())
    {

        $this->db->select('request.*,user.first_name,user.last_name');
        $this->db->from(Tbl::table_community_request . ' as request');
        $this->db->join(Tbl::table_user . ' as user', 'user.user_id = request.user_id', 'left');
        if (isset($filter['page'])) {
            $this->db->limit(AppConst::PAGINATION_LIMIT, getval($filter, 'page'));
        }
        if (isset($filter['id']) && intval(getval($filter, 'id')) > 0) {
            $this->db->where('request.id', getval($filter, 'id'));
        }
        $this->db->order_by('request.updated_at', 'desc');
        $communityRequestlist = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($communityRequestlist) && count($communityRequestlist) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No hospital record found.";
        $res[AppConst::res_value] = $communityRequestlist;
        return $res;
    }

    public function addHospitalCsvFile($dataToModel = array())
    {
        if (count($dataToModel) > 0) {
            $this->db->insert_batch(Tbl::table_hospital, $dataToModel);
            $inserId = $this->db->affected_rows() > 0 ? TRUE : FALSE;
            if ($inserId) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = 'Data Imported successfully';
                return $res;
            } else {
                $res[AppConst::errorindex_code] = 30;
                $res[AppConst::errorindex_message] = 'can not import file';
                return $res;
            }
        } else {
            $res[AppConst::errorindex_code] = 20;
            $res[AppConst::errorindex_message] = 'invalid request';
            return $res;
        }
    }

    public function AddEditCountry($dataToMd = array())
    {
        $countryid = isset($dataToMd['country_id']) ? $dataToMd['country_id'] : 0;
        $dataTodb['country_name'] = getval($dataToMd, 'country_name');
        $dataTodb['country_code'] = getval($dataToMd, 'country_code');

        $dataTodb['updated_date'] = getCurrentDateWithTime();
        if ($countryid > 0) {

            // check Country Name, it is already exist
            $checkCountry = 1;
            $this->db->select('*');
            $this->db->from(Tbl::table_country);
            $this->db->where('country_id', $countryid);
            $countryInfo = $this->db->get()->result_array();
            if (count($countryInfo) > 0) {
                if ($countryInfo[0]['country_name'] != $dataToMd['country_name']) {
                    $this->db->select('*');
                    $this->db->from(Tbl::table_country);
                    $this->db->where('country_name', getval($dataToMd, 'country_name'));
                    $country = $this->db->get()->result_array();
                    if (count($country) > 0) {
                        $checkCountry = 0;
                    }
                }
            }

            if ($checkCountry > 0) {
                $this->db->where('country_id', $countryid);
                $this->db->update(Tbl::table_country, $dataTodb);
                $countryid = ($this->db->affected_rows() > 0) ? $countryid : 0;
                $res[AppConst::errorindex_code] = ($countryid > 0) ? AppConst::errorcode_success : 8;
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ?
                    AppConst::errorMessage_db_general_success : AppConst::errorMessage_db_general_error;
            } else {
                $res[AppConst::errorindex_code] = 502;
                $res[AppConst::errorindex_message] = 'This country already exist';
            }
        } else {

            // check Country Name, it is already exist
            $checkCountry = 1;
            $this->db->select('*');
            $this->db->from(Tbl::table_country);
            $this->db->where('country_name', getval($dataToMd, 'country_name'));
            //$this->db->where('userType', AppConst::USER_TYPE_USER);
            $Country = $this->db->get()->result_array();
            if (count($Country) > 0) {
                $checkCountry = 0;
            }

            if ($checkCountry > 0) {

                $dataTodb['added_date'] = getCurrentDateWithTime();
                $this->db->insert(Tbl::table_country, $dataTodb);
                $countryid = $this->db->insert_id();

                $res[AppConst::errorindex_code] = ($countryid > 0) ? AppConst::errorcode_success : 8;
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ?
                    AppConst::errorMessage_db_general_success : AppConst::errorMessage_db_general_error;
            } else {
                $res[AppConst::errorindex_code] = 502;
                $res[AppConst::errorindex_message] = 'This country already exist';
            }
        }

        return $res;
    }

    public function AddEditCity($dataToMd = array())
    {
        $stateid = isset($dataToMd['state_id']) ? $dataToMd['state_id'] : 0;
        $dataTodb['country_id'] = getval($dataToMd, 'country_id');
        $dataTodb['state_name'] = getval($dataToMd, 'state_name');
        $dataTodb['state_code'] = getval($dataToMd, 'state_code');
        $dataTodb['updated_date'] = getCurrentDateWithTime();

        if ($stateid > 0) {

            // check City , it is already exist
            $checkCity = 1;
            $this->db->select('*');
            $this->db->from(Tbl::table_state);
            $this->db->where('state_id', $stateid);
            $cityInfo = $this->db->get()->result_array();
            if (count($cityInfo) > 0) {
                if ($cityInfo[0]['state_name'] != $dataToMd['state_name'] || getval($cityInfo[0], 'country_id') != getval($dataToMd, 'country_id')) {
                    $this->db->select('*');
                    $this->db->from(Tbl::table_state);
                    $this->db->where('state_name', getval($dataToMd, 'state_name'));
                    $this->db->where('country_id', getval($dataToMd, 'country_id'));
                    $city = $this->db->get()->result_array();
                    if (count($city) > 0) {
                        $checkCity = 0;
                    }
                }
            }

            if ($checkCity > 0) {
                $this->db->where('state_id', $stateid);
                $this->db->update(Tbl::table_state, $dataTodb);
                $cityid = ($this->db->affected_rows() > 0) ? $cityid : 0;

                $res[AppConst::errorindex_code] = ($cityid > 0) ? AppConst::errorcode_success : 8;
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ?
                    AppConst::errorMessage_db_general_success : AppConst::errorMessage_db_general_error;
            } else {
                $res[AppConst::errorindex_code] = 502;
                $res[AppConst::errorindex_message] = 'This state/provenance already exist';
            }
        } else {


            // check City , it is already exist
            $checkCity = 1;
            $this->db->select('*');
            $this->db->from(Tbl::table_state);
            $this->db->where('state_name', getval($dataToMd, 'cityName'));
            $this->db->where('country_id', getval($dataToMd, 'country_id'));
            $City = $this->db->get()->result_array();
            if (count($City) > 0) {
                $checkCity = 0;
            }

            if ($checkCity > 0) {
                $dataTodb['added_date'] = getCurrentDateWithTime();
                $this->db->insert(Tbl::table_state, $dataTodb);
                $cityid = $this->db->insert_id();

                $res[AppConst::errorindex_code] = ($cityid > 0) ? AppConst::errorcode_success : 8;
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ?
                    AppConst::errorMessage_db_general_success : AppConst::errorMessage_db_general_error;
            } else {
                $res[AppConst::errorindex_code] = 502;
                $res[AppConst::errorindex_message] = 'This state/provenance already exist';
            }
        }


        return $res;
    }

    public function updateCountryStatus($countryInfo)
    {
        $res = array();
        $countryId = getval($countryInfo, 'country_id');
        $status = getval($countryInfo, 'status');
        if ($countryId > 0 && $status > 0) {
            $dataToUpdate['status'] = $status;
            $dataToUpdate['updated_date'] = getCurrentDateWithTime();
            $this->db->where('country_id', $countryId);
            $this->db->update(Tbl::table_country, $dataToUpdate);
            $countryUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($countryUpdate == TRUE) {
                if ($status == AppConst::STATUS_BLOCK) {
                    $this->db->where('country_id', $countryId);
                    $this->db->update(Tbl::table_state, $dataToUpdate);
                    $countryUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
                } else {
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
                }
            } else {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'can not update country status';
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'invalid request';
        }
        return $res;
    }

    public function updateFitnessTipStatus($countryInfo)
    {
        $res = array();
        $tip_id = getval($countryInfo, 'id');
        $status = getval($countryInfo, 'status');
        if ($tip_id > 0 && $status > 0) {
            $dataToUpdate['status'] = $status;
            $dataToUpdate['updated_at'] = getCurrentDateWithTime();
            $this->db->where('id', $tip_id);
            $this->db->update(Tbl::table_fitness_tips, $dataToUpdate);
            $countryUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($countryUpdate == TRUE) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
            } else {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'can not update fitness tips status';
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'invalid request';
        }
        return $res;
    }

    public function updateBannerStatus($countryInfo)
    {
        $res = array();
        $tip_id = getval($countryInfo, 'id');
        $status = getval($countryInfo, 'status');
        if ($tip_id > 0 && $status > 0) {
            $dataToUpdate['status'] = $status;
            $dataToUpdate['updated_at'] = getCurrentDateWithTime();
            $this->db->where('id', $tip_id);
            $this->db->update(Tbl::table_banners, $dataToUpdate);
            $countryUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            if ($countryUpdate == TRUE) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
            } else {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'can not update fitness tips status';
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'invalid request';
        }
        return $res;
    }

    public function updateCityStatus($cityInfo)
    {
        $res = array();
        $countryId = getval($cityInfo, 'country_id');
        $status = getval($cityInfo, 'status');
        $state_id = getval($cityInfo, 'state_id');
        if ($countryId > 0 && $status > 0 && $state_id > 0) {
            $dataToUpdate['status'] = $status;
            $dataToUpdate['updated_date'] = getCurrentDateWithTime();

            if ($status == AppConst::STATUS_ACTIVE) {
                $filter['country_id'] = $countryId;
                $countryInfo = $this->getAllCountry($filter);
                if ($countryInfo[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $countryStatus = $countryInfo[AppConst::res_value][0]['status'];
                    if ($countryStatus == AppConst::STATUS_ACTIVE) {
                        $this->db->where('state_id', $state_id);
                        $this->db->update(Tbl::table_state, $dataToUpdate);
                        $cityUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
                        if ($cityUpdate == TRUE) {
                            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                            $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
                        } else {
                            $res[AppConst::errorindex_code] = 50;
                            $res[AppConst::errorindex_message] = 'can not update state/provenance status';
                        }
                    } else if ($countryStatus == AppConst::STATUS_BLOCK) {
                        $res[AppConst::errorindex_code] = 40;
                        $res[AppConst::errorindex_message] = 'sorry, you cant active this state/provenance because its parent country was not active';
                    }
                } else {
                    $res[AppConst::errorindex_code] = 30;
                    $res[AppConst::errorindex_message] = 'invalid request';
                }
            } else if ($status == AppConst::STATUS_BLOCK) {
                $this->db->where('state_id', $state_id);
                $this->db->update(Tbl::table_state, $dataToUpdate);
                $cityUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
                if ($cityUpdate == TRUE) {
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
                } else {
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'can not update state/provenance status';
                }
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'invalid request';
        }
        return $res;
    }
}
