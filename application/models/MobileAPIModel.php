<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('supermodel.php');

class MobileAPIModel extends supermodel
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("Tbl");
    }

    public function adminLoginBy($username, $deviceUniqueId, $deviceNotifyId, $accessKey, $platform)
    {
        $resultArray = array();
        $isGenTocken = FALSE;
        $this->db->trans_begin();
        $this->db->select('*');
        $this->db->from(Tbl::table_auth . ' as auth');
        $this->db->join(Tbl::table_user . ' as user', 'user.user_id = auth.user_id', 'left');
        if (isset($username) && trim($username) != '') {
            $this->db->where('auth.username', $username);
        }
        $this->db->where('user.user_type', AppConst::USER_TYPE_REGISTERED_USER);
        $this->db->limit(1);
        $userInfo = $this->db->get()->result_array();
        if (isset($userInfo) && count($userInfo) > 0) {
            if (getval($userInfo[0], 'status') != AppConst::BLOCK) {
                $userId = getval($userInfo[0], 'user_id');
                $this->db->select('*');
                $this->db->where('user_id', $userId);
                $this->db->where('dev_unique_id', $deviceUniqueId);
                $this->db->from(Tbl::table_device_master);
                $this->db->limit(1);
                $deviceMasterInfo = $this->db->get()->result_array();
                if (isset($deviceMasterInfo) && count($deviceMasterInfo) > 0 && !empty($deviceMasterInfo)) {
                    $userId = getval($userInfo[0], 'user_id');
                    $this->db->where('user_id', $userId);
                    $deviceInfo['platform'] = $platform;
                    $deviceInfo['dev_notify_id'] = $deviceNotifyId;
                    $deviceInfo['dev_access_key'] = uniqid("appAccess_") . round(microtime(true) * 1000);
                    $this->db->update(Tbl::table_device_master, $deviceInfo);
                    $isGenTocken = ($this->db->affected_rows() > 0) ? true : false;
                } else {
                    $userId = getval($userInfo[0], 'user_id');
                    $deviceInfo['user_id'] = $userId;
                    $deviceInfo['platform'] = $platform;
                    $deviceInfo['dev_notify_id'] = $deviceNotifyId;
                    $deviceInfo['dev_unique_id'] = $deviceUniqueId;
                    $deviceInfo['dev_access_key'] = uniqid("appAccess_") . round(microtime(true) * 1000);
                    $deviceInfo['dev_mstatus'] = AppConst::ACTIVE;
                    $this->db->insert(Tbl::table_device_master, $deviceInfo);
                    $isGenTocken = ($this->db->affected_rows() > 0) ? true : false;
                }
                if ($isGenTocken) {
                    $this->db->trans_commit();
                    $auth['username'] = $username;
                    $auth['accessKey'] = getval($deviceInfo, 'dev_access_key');
                    $auth['user_id'] = $userId;
                    $result = $this->checkAuthentication($auth);
                    $resultArray = $result[AppConst::res_value];
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = AppConst::errormessage_success;
                    $res[AppConst::res_value] = $resultArray;
                } else {
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = "Something wrong happend in device info";
                    $resultArray['username'] = $username;
                }
            } else {
                $res[AppConst::errorindex_code] = 30;
                $res[AppConst::errorindex_message] = "This user was blocked";
                $resultArray['username'] = $username;
            }
        } else {
            $isGenTocken = FALSE;
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = "Not registered this mobile no";
            $resultArray['username'] = $username;
        }
        return $res;
    }

    public function checkAuthentication($filter = array())
    {
        $user = array();
        if (isset($filter['username']) && trim($filter['username']) != '') {
            if (isset($filter['user_id']) && intval($filter['user_id']) > 0) {
                $this->db->select('*');
                $this->db->from(Tbl::table_auth . ' as auth');
                $this->db->join(Tbl::table_user . ' as user', 'user.user_id = auth.user_id', 'left');
                $this->db->join(Tbl::table_device_master . ' as deviceMaster', 'deviceMaster.user_id = auth.user_id', 'left');
                if (isset($filter['username']) && trim($filter['username']) != '') {
                    $this->db->where('auth.username', $filter['username']);
                }
                if (isset($filter['user_id']) && intval($filter['user_id']) > 0) {
                    $this->db->where('auth.user_id', $filter['user_id']);
                }
                if (isset($filter['accessKey']) && trim($filter['accessKey']) != '') {
                    $this->db->where('deviceMaster.dev_access_key', $filter['accessKey']);
                }
                $this->db->where('user.user_type', AppConst::USER_TYPE_REGISTERED_USER);
                $this->db->limit(1);
                $user = $this->db->get()->result_array();
                $res[AppConst::errorindex_code] = (isset($user) && count($user) > 0) ? AppConst::errorcode_success : 25000;
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "Already logged in another device";
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $res[AppConst::res_value] = $user[0];
                }
            } else {
                $res[AppConst::errorindex_code] == 2200;
                $res[AppConst::errorindex_message] = 'userid missing';
            }
        } else {
            $res[AppConst::errorindex_code] == 2300;
            $res[AppConst::errorindex_message] = 'username missing';
        }
        return $res;
    }

    private function checkContactNoAlreadyExistOrNot($contactno)
    {
        $this->db->where('username', $contactno);
        $query = $this->db->get(Tbl::table_auth);
        if ($query->num_rows() > 0) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            return $res;
        } else {
            $res[AppConst::errorindex_code] = 8;
            return $res;
        }
    }

    private function checkIsHydrated($user_id, $date)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('date', $date);
        $query = $this->db->get(Tbl::table_hydration);
        if ($query->num_rows() > 0) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
        } else {
            $hydrationMaster['user_id'] = $user_id;
            $hydrationMaster['date'] = $date;
            $hydrationMaster['created_at'] = getCurrentDateWithTime();
            $hydrationMaster['updated_at'] = getCurrentDateWithTime();
            $now = getCurrentTime();
            //$cenvertedTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($now)));
            $hydrationMaster['reminding_time'] = $now;
            $hydrationMaster['status'] = AppConst::STATUS_ACTIVE;
            $hydrationMaster['daily_water_intake_needed'] = AppConst::DAILY_WATER_INTAKE;
            $this->db->insert(Tbl::table_hydration, $hydrationMaster);
            $hydrationId = $this->db->insert_id();
            if ($hydrationId <= 0) {
                $res[AppConst::errorindex_code] = 100;
                $res[AppConst::errorindex_message] = "user insertion failed.. ";
                return $res;
            }
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
        }
        return $res;
    }

    private function checkCartId($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('status', AppConst::STATUS_ACTIVE);
        $query = $this->db->get(Tbl::table_cart);
        if ($query->num_rows() > 0) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
        } else {
            $hydrationMaster['user_id'] = $user_id;
            $hydrationMaster['created_at'] = getCurrentDateWithTime();
            $hydrationMaster['updated_at'] = getCurrentDateWithTime();
            $hydrationMaster['status'] = AppConst::STATUS_ACTIVE;
            $this->db->insert(Tbl::table_cart, $hydrationMaster);
            $cartId = $this->db->insert_id();
            if ($cartId <= 0) {
                $res[AppConst::errorindex_code] = 100;
                $res[AppConst::errorindex_message] = "user insertion failed.. ";
                return $res;
            }
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
        }
        return $res;
    }

    private function checkOrderId($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('order_status', AppConst::ORDER_PLACED);
        $query = $this->db->get(Tbl::table_orders);
        if ($query->num_rows() > 0) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
        } else {
            $res[AppConst::errorindex_code] = 8;
        }
        return $res;
    }

    private function checkProductExistInCart($cart_id, $product_id)
    {
        $this->db->where('cart_id', $cart_id);
        $this->db->where('product_id', $product_id);
        $query = $this->db->get(Tbl::table_cart_lines);
        if ($query->num_rows() > 0) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
        } else {
            $res[AppConst::errorindex_code] = 8;
        }
        return $res;
    }

    private function checkQuantityIsValid($cart_id)
    {
        $this->db->where('cart_id', $cart_id);
        $this->db->where('qty<', 1);
        $query = $this->db->get(Tbl::table_cart_lines);
        if ($query->num_rows() > 0) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
        } else {
            $res[AppConst::errorindex_code] = 8;
        }
        return $res;
    }

    private function getHydrationId($user_id, $date)
    {
        $this->db->select('id');
        $this->db->from(Tbl::table_hydration);
        $this->db->where('user_id', $user_id);
        $this->db->where('date', $date);
        $List = $this->db->get()->result_array();
        $res[AppConst::res_value] = $List[0];
        return $res;
    }

    private function getHydrationLineId($hydraion_id)
    {
        $this->db->select('id');
        $this->db->from(Tbl::table_hydration_lines);
        $this->db->where('hydration_id', $hydraion_id);
        $this->db->order_by('created_at', 'desc');
        $this->db->limit(1);
        $List = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($List) && sizeof($List) > 0) ? AppConst::errorcode_success : 8;
        if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $res[AppConst::res_value] = $List[0];
        }
        return $res;
    }

    private function getCartId($user_id)
    {
        $this->db->select('cart_id');
        $this->db->from(Tbl::table_cart);
        $this->db->where('user_id', $user_id);
        $this->db->where('status', AppConst::STATUS_ACTIVE);
        $List = $this->db->get()->result_array();
        $res[AppConst::res_value] = $List[0];
        return $res;
    }

    private function getOrderId($user_id)
    {
        $this->db->select('order_id');
        $this->db->from(Tbl::table_orders);
        $this->db->where('user_id', $user_id);
        $this->db->where('order_status', AppConst::ORDER_PLACED);
        $List = $this->db->get()->result_array();
        $res[AppConst::res_value] = $List[0];
        return $res;
    }

    public function registerUser($dataToDb)
    {
        $errorMessage = null;
        $customerMaster = array();
        $deviceMater = array();
        $userGroup = array();
        $userId = isset($dataToDb['user_id']) ? intval(getval($dataToDb, 'user_id')) : 0;
        $customerMaster['last_date_donation'] = getval($dataToDb, 'last_donation');
        $customerMaster['work_location_lat'] = getval($dataToDb, 'work_location_lat');
        $customerMaster['work_location_long'] = getval($dataToDb, 'work_location_long');
        $customerMaster['work_location_place'] = getval($dataToDb, 'work_place');
        $customerMaster['receive_notification'] = getval($dataToDb, 'receive_notification');
        if (isset($dataToDb['profile_image']) && getval($dataToDb, 'profile_image') != '') {
            $base64_string = isset($dataToDb['profile_image']) ? getval($dataToDb, 'profile_image') : '';
            $basepath = 'uploads' . DIRECTORY_SEPARATOR;
            $uploadpath = 'profile' . DIRECTORY_SEPARATOR . getCurrentDate() . DIRECTORY_SEPARATOR;
            if (!is_dir($basepath . $uploadpath)) {
                mkdir($basepath . $uploadpath, 0777, TRUE);
            }
            $dbpath = $uploadpath . getCurrentDay() . '_' . getCurrentMonth() . '_' . getCurrentYear() . '_' . getCurrentHour() . '_' . getCurrentMinute() . '_' . getCurrentSecond() . '_' . getval($customerMaster, 'first_name') . '.jpg';
            $output_file = $basepath . $dbpath;
            $ifp = fopen($output_file, "wb");
            fwrite($ifp, base64_decode($base64_string));
            fclose($ifp);
            $customerMaster['pro_image'] = "uploads/" . $dbpath;
        }

        $deviceMater['platform'] = $platform = isset($dataToDb['platform']) ? getval($dataToDb, 'platform') : "ANDROID";
        $deviceMater['dev_unique_id'] = getval($dataToDb, 'deviceUniqueId');
        $deviceMater['dev_notify_id'] = getval($dataToDb, 'deviceNotifyId');
        $deviceMater['dev_access_key'] = uniqid("appAccess_") . round(microtime(true) * 1000);
        $deviceMater['dev_mstatus'] = AppConst::ACTIVE;

        $temp = getval($dataToDb, 'firstname');
        if ($temp != '') {
            $customerMaster['first_name'] = $temp;
            $temp = getval($dataToDb, 'dob');
            if ($temp != '') {
                $customerMaster['dob'] = $temp;
                $temp = getval($dataToDb, 'sex');
                if ($temp != '') {
                    $customerMaster['gender'] = $temp;
                    $temp = getval($dataToDb, 'blood_group');
                    if ($temp != '') {
                        $customerMaster['blood_group'] = $temp;
                        $temp = getval($dataToDb, 'address');
                        if ($temp != '') {
                            $customerMaster['address'] = $temp;
                            $temp = getval($dataToDb, 'country');
                            if ($temp != '') {
                                $customerMaster['country'] = $temp;
                                $temp = getval($dataToDb, 'contactno');
                                if ($temp != '') {
                                    $customerMaster['contact_no'] = $temp;
                                    $temp = getval($dataToDb, 'home_location_lat');
                                    if ($temp != '') {
                                        $customerMaster['home_location_lat'] = $temp;
                                        $temp = getval($dataToDb, 'home_location_long');
                                        if ($temp != '') {
                                            $customerMaster['home_location_long'] = $temp;
                                            $temp = getval($dataToDb, 'home_place');
                                            if ($temp != '') {
                                                $customerMaster['home_location_place'] = $temp;
                                            } else {
                                                $errorMessage = "Home Place is missing";
                                            }
                                        } else {
                                            $errorMessage = "Home location(long) is missing";
                                        }
                                    } else {
                                        $errorMessage = "Home location(lat) is missing";
                                    }
                                } else {
                                    $errorMessage = "Contact number is missing";
                                }
                            } else {
                                $errorMessage = "Country is missing";
                            }
                        } else {
                            $errorMessage = "Address is missing";
                        }
                    } else {
                        $errorMessage = "Blood Group is missing";
                    }
                } else {
                    $errorMessage = "Sex is missing";
                }
            } else {
                $errorMessage = "Date of birth is missing";
            }
        } else {
            $errorMessage = "First Name is missing";
        }
        if ($errorMessage == null || trim($errorMessage) == '') {
            $this->db->trans_begin();
            $checkRes = $this->checkContactNoAlreadyExistOrNot(getval($customerMaster, 'contact_no'));
            if ($checkRes[AppConst::errorindex_code] != AppConst::errorcode_success) {
                $customerMaster['user_type'] = AppConst::USER_TYPE_REGISTERED_USER;
                $customerMaster['added_date'] = getCurrentDateWithTime();
                $customerMaster['updated_date'] = getCurrentDateWithTime();
                $customerMaster['status'] = AppConst::STATUS_ACTIVE;
                $customerMaster['is_verified'] = AppConst::USER_VERIFIED;

                $this->db->insert(Tbl::table_user, $customerMaster);
                $userId = $this->db->insert_id();
                if ($userId <= 0) {
                    $this->db->trans_complete();
                    $this->db->trans_rollback();
                    $res[AppConst::errorindex_code] = 100;
                    $res[AppConst::errorindex_message] = "user insertion failed.. ";
                    return $res;
                }
                $dataToLogin['username'] = getval($customerMaster, 'contact_no');
                $dataToLogin['password'] = '000000';
                $dataToLogin['user_id'] = $userId;
                $dataToLogin['added_date'] = getCurrentDateWithTime();
                $dataToLogin['updated_date'] = getCurrentDateWithTime();
                $this->db->insert(Tbl::table_auth, $dataToLogin);
                $loginId = $this->db->insert_id();
                if ($loginId <= 0) {
                    $this->db->trans_complete();
                    $this->db->trans_rollback();
                    $res[AppConst::errorindex_code] = 200;
                    $res[AppConst::errorindex_message] = "login insertion failed.. ";
                    return $res;
                }
                $deviceMater['user_id'] = $userId;
                $deviceMater['dev_mstatus'] = AppConst::ACTIVE;
                $this->db->insert(Tbl::table_device_master, $deviceMater);
                $deviceID = $this->db->insert_id();
                if ($deviceID <= 0) {
                    $this->db->trans_complete();
                    $this->db->trans_rollback();
                    $res[AppConst::errorindex_code] = 200;
                    $res[AppConst::errorindex_message] = "device details insertion failed.. ";
                    return $res;
                }
                $auth['username'] = getval($customerMaster, 'contact_no');
                $auth['user_id'] = $userId;
                $res = $this->checkAuthentication($auth);
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_registration_success;
            } else {
                $res[AppConst::errorindex_code] = 300;
                $res[AppConst::errorindex_message] = "mobile number already registered";
            }
            $this->db->trans_complete();
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $this->db->trans_commit();
            } else {
                $this->db->trans_rollback();
            }
        } else {
            $res[AppConst::errorindex_code] = 400;
            $res[AppConst::errorindex_message] = $errorMessage;
        }
        return $res;
    }

    public function updateProfile($dataToDb)
    {
        $customerMaster = array();
        $userId = isset($dataToDb['user_id']) ? intval(getval($dataToDb, 'user_id')) : 0;
        $username = isset($dataToDb['username']) ? intval(getval($dataToDb, 'username')) : 0;
        $customerMaster['first_name'] = getval($dataToDb, 'firstname');
        $customerMaster['dob'] = getval($dataToDb, 'dob');
        $customerMaster['gender'] = getval($dataToDb, 'sex');
        $customerMaster['blood_group'] = getval($dataToDb, 'blood_group');
        $customerMaster['address'] = getval($dataToDb, 'address');
        $customerMaster['country'] = getval($dataToDb, 'country');
        //        $customerMaster['contact_no'] = getval($dataToDb, 'contactno');

        if (isset($dataToDb['profile_image']) && getval($dataToDb, 'profile_image') != '') {
            $base64_string = isset($dataToDb['profile_image']) ? getval($dataToDb, 'profile_image') : '';
            $basepath = 'uploads' . DIRECTORY_SEPARATOR;
            $uploadpath = 'profile' . DIRECTORY_SEPARATOR . getCurrentDate() . DIRECTORY_SEPARATOR;
            if (!is_dir($basepath . $uploadpath)) {
                mkdir($basepath . $uploadpath, 0777, TRUE);
            }
            $dbpath = $uploadpath . getCurrentDay() . '_' . getCurrentMonth() . '_' . getCurrentYear() . '_' . getCurrentHour() . '_' . getCurrentMinute() . '_' . getCurrentSecond() . '_' . getval($customerMaster, 'first_name') . '.jpg';
            $output_file = $basepath . $dbpath;
            $ifp = fopen($output_file, "wb");
            fwrite($ifp, base64_decode($base64_string));
            fclose($ifp);
            $customerMaster['pro_image'] = "uploads/" . $dbpath;
        }
        if (getval($dataToDb, 'home_location_lat') != '') {
            $customerMaster['home_location_lat'] = getval($dataToDb, 'home_location_lat');
        }
        if (getval($dataToDb, 'home_location_long') != '') {
            $customerMaster['home_location_long'] = getval($dataToDb, 'home_location_long');
        }
        if (getval($dataToDb, 'home_place') != '') {
            $customerMaster['home_location_place'] = getval($dataToDb, 'home_place');
        }
        if (getval($dataToDb, 'last_donation') != '') {
            $customerMaster['last_date_donation'] = getval($dataToDb, 'last_donation');
        }
        if (getval($dataToDb, 'work_location_lat') != '') {
            $customerMaster['work_location_lat'] = getval($dataToDb, 'work_location_lat');
        }
        if (getval($dataToDb, 'work_location_long') != '') {
            $customerMaster['work_location_long'] = getval($dataToDb, 'work_location_long');
        }
        if (getval($dataToDb, 'work_place') != '') {
            $customerMaster['work_location_place'] = getval($dataToDb, 'work_place');
        }
        $customerMaster['updated_date'] = getCurrentDateWithTime();
        $this->db->trans_begin();
        $this->db->where('user_id', $userId);
        $this->db->update(Tbl::table_user, $customerMaster);
        $userUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
        if ($userUpdate == FALSE) {
            $this->db->trans_complete();
            $this->db->trans_rollback();
            $res[AppConst::errorindex_code] = 200;
            $res[AppConst::errorindex_message] = "user updation failed..";
            return $res;
        }
        $dataToLogin['updated_date'] = getCurrentDateWithTime();
        $this->db->where('user_id', $userId);
        $this->db->update(Tbl::table_auth, $dataToLogin);
        $loginUpdate = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
        if ($loginUpdate == FALSE) {
            $this->db->trans_complete();
            $this->db->trans_rollback();
            $res[AppConst::errorindex_code] = 200;
            $res[AppConst::errorindex_message] = "login updation failed..";
            return $res;
        }
        $auth['username'] = $username;
        $auth['user_id'] = $userId;
        $res = $this->checkAuthentication($auth);
        $res[AppConst::errorindex_message] = AppConst::errormessage_db_updation_success;
        $this->db->trans_complete();
        if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $this->db->trans_commit();
        } else {
            $this->db->trans_rollback();
        }
        return $res;
    }

    public function logout($username, $userId, $accessKey)
    {
        $isLogout = FALSE;
        $auth['username'] = $username;
        $auth['user_id'] = $userId;
        $auth['accessKey'] = $accessKey;
        $userRes = $this->checkAuthentication($auth);
        if ($userRes[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $this->db->where('user_id', $userId);
            $this->db->delete(Tbl::table_device_master);
            $isLogout = ($this->db->affected_rows() > 0) ? true : false;
        }
        if ($isLogout) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            $res[AppConst::errorindex_message] = "Successfully Logout";
        } else {
            $res[AppConst::errorindex_code] = 1;
            $res[AppConst::errorindex_message] = "Logout faild";
        }
        return $res;
    }

    public function userProfile($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_auth . ' as auth');
        $this->db->join(Tbl::table_user . ' as user', 'user.user_id = auth.user_id', 'left');
        $this->db->join(Tbl::table_device_master . ' as deviceMaster', 'deviceMaster.user_id = auth.user_id', 'left');
        if (getval($filter, 'user_id') != '') {
            $this->db->where('auth.user_id', getval($filter, 'user_id'));
        }
        if (getval($filter, 'contact_no') != '') {
            $this->db->where('auth.username', getval($filter, 'contact_no'));
        }
        $userInfo = $this->db->get()->result_array();
        $last_date_donation = $userInfo['last_date_donation'];
        $gender = $userInfo['gender'];
        if ($last_date_donation == null) {
            if ($gender == AppConst::GENDER_FEMALE) {
                $userInfo[0]['countdown_to_donation'] = 120;
            } else {
                $userInfo[0]['countdown_to_donation'] = 90;
            }
        } else {
            $today = getCurrentDate();
            $diff = abs(strtotime($last_date_donation) - strtotime($today));
            $years = floor($diff / (365 * 60 * 60 * 24));
            $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
            $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
            if ($gender == AppConst::GENDER_FEMALE) {
                if ($days > 120) {
                    $userInfo[0]['countdown_to_donation'] = 0;
                } else {
                    $userInfo[0]['countdown_to_donation'] = 120 - $days;
                }
            } else {
                if ($days > 90) {
                    $userInfo[0]['countdown_to_donation'] = 0;
                } else {
                    $userInfo[0]['countdown_to_donation'] = 90 - $days;
                }
            }
        }

        $res[AppConst::errorindex_code] = (isset($userInfo) && sizeof($userInfo) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
        if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $res[AppConst::res_value] = $userInfo[0];
        }
        return $res;
    }


    public function getJoinedCommunityCount($userId)
    {
        $this->db->where('user_id', $userId);
        $this->db->from(Tbl::table_community_subscription);
        $res = $this->db->count_all_results();
        return $res;
    }

    public function searchBloodGroup($filter = array())
    {
        $lat = getval($filter, 'latitude');
        $long = getval($filter, 'longitude');
        $bloodGrp = getval($filter, 'blood_grp');

        //isset($filter['blood_grp'])?$filter['blood_grp']:array();
        $limit = isset($filter['limit']) ? getval($filter, 'limit') : 100;
        $dist = isset($filter['dist']) ? getval($filter, 'dist') : 100;
        $random_array = array(1, 2, 3, 4, 5, 6, 7);
        $random_key = array_rand($random_array, 1);
        //$sort_key = $random_array[$random_key];
        $sort_key = 6;
        $location = NULL;
        if ($bloodGrp != '') {
            $location = 'SELECT *, 3956 * 2 * 
          ASIN(SQRT( POWER(SIN((' . $lat . ' - work_location_lat)*pi()/180/2),2)
          +COS(' . $lat . '*pi()/180 )*COS(work_location_lat*pi()/180)
          *POWER(SIN((' . $long . '-work_location_long)*pi()/180/2),2))) 
          as wdistance,  3956 * 2 * 
          ASIN(SQRT( POWER(SIN((' . $lat . ' - home_location_lat)*pi()/180/2),2)
          +COS(' . $lat . '*pi()/180 )*COS(home_location_lat*pi()/180)
          *POWER(SIN((' . $long . '-home_location_long)*pi()/180/2),2))) 
          as hdistance,DATEDIFF("' . getCurrentDate() . '", last_date_donation) as day_count  FROM ' . Tbl::table_user . ' WHERE 
              
          (work_location_long between (' . $long . '-' . $dist . '/cos(radians(' . $lat . '))*69) 
          and (' . $long . '+' . $dist . '/cos(radians(' . $lat . '))*69) 
          and work_location_lat between (' . $lat . '-(' . $dist . '/69)) 
          and (' . $lat . '+(' . $dist . '/69)))
           OR
          (home_location_long between (' . $long . '-' . $dist . '/cos(radians(' . $lat . '))*69) 
          and (' . $long . '+' . $dist . '/cos(radians(' . $lat . '))*69) 
          and home_location_lat between (' . $lat . '-(' . $dist . '/69)) 
          and (' . $lat . '+(' . $dist . '/69)))
          having (hdistance < ' . $dist . ' or wdistance < ' . $dist . ')  and blood_group = ' . $bloodGrp . '
          and user_id!=' . getval($filter, 'user_id') . ' and day_count > 90 ';
        } else {
            $location = 'SELECT *, 3956 * 2 * 
          ASIN(SQRT( POWER(SIN((' . $lat . ' - work_location_lat)*pi()/180/2),2)
          +COS(' . $lat . '*pi()/180 )*COS(work_location_lat*pi()/180)
          *POWER(SIN((' . $long . '-work_location_long)*pi()/180/2),2))) 
          as wdistance,  3956 * 2 * 
          ASIN(SQRT( POWER(SIN((' . $lat . ' - home_location_lat)*pi()/180/2),2)
          +COS(' . $lat . '*pi()/180 )*COS(home_location_lat*pi()/180)
          *POWER(SIN((' . $long . '-home_location_long)*pi()/180/2),2))) 
          as hdistance, DATEDIFF("' . getCurrentDate() . '", last_date_donation) as day_count   FROM ' . Tbl::table_user . ' WHERE 
          (work_location_long between (' . $long . '-' . $dist . '/cos(radians(' . $lat . '))*69) 
          and (' . $long . '+' . $dist . '/cos(radians(' . $lat . '))*69) 
          and work_location_lat between (' . $lat . '-(' . $dist . '/69)) 
          and (' . $lat . '+(' . $dist . '/69)))
           OR
          (home_location_long between (' . $long . '-' . $dist . '/cos(radians(' . $lat . '))*69) 
          and (' . $long . '+' . $dist . '/cos(radians(' . $lat . '))*69) 
          and home_location_lat between (' . $lat . '-(' . $dist . '/69)) 
          and (' . $lat . '+(' . $dist . '/69)))
          having (hdistance < ' . $dist . ' or wdistance < ' . $dist . ')
          and user_id!=' . getval($filter, 'user_id') . ' and day_count > 90 ';
        }
        switch ($sort_key) {
            case 1:
                $location .= ' ORDER BY first_name asc limit ' . $limit;
                break;
            case 2:
                $location .= ' ORDER BY first_name desc limit ' . $limit;
                break;
            case 3:
                $location .= ' ORDER BY added_date asc limit ' . $limit;
                break;
            case 4:
                $location .= ' ORDER BY added_date desc limit ' . $limit;
                break;
            case 5:
                $location .= ' ORDER BY hdistance, wdistance asc limit ' . $limit;
                break;
            case 6:
                $location .= ' ORDER BY hdistance, wdistance desc limit ' . $limit;
                break;
            case 7:
                $location .= ' ORDER BY day_count desc limit ' . $limit;
                break;
        }

        $qry = $this->db->query($location);

        $arr = $qry->result_array();

        $res[AppConst::errorindex_code] = (isset($arr) && sizeof($arr) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";

        $res[AppConst::res_value] = $arr;
        $res[AppConst::res_value1] = count($arr);

        return $res;
    }

    public function searchBloodGroupForNonRegisteredUser($filter = array())
    {
        $lat = getval($filter, 'latitude');
        $long = getval($filter, 'longitude');
        $bloodGrp = getval($filter, 'blood_grp');

        //isset($filter['blood_grp'])?$filter['blood_grp']:array();
        $limit = isset($filter['limit']) ? getval($filter, 'limit') : 5;
        $dist = isset($filter['dist']) ? getval($filter, 'dist') : 100;
        $random_array = array(1, 2, 3, 4, 5, 6, 7);
        $random_key = array_rand($random_array, 1);
        //$sort_key = $random_array[$random_key];
        $sort_key = 6;
        $location = NULL;
        if ($bloodGrp != '') {
            $location = 'SELECT *, 3956 * 2 * 
          ASIN(SQRT( POWER(SIN((' . $lat . ' - work_location_lat)*pi()/180/2),2)
          +COS(' . $lat . '*pi()/180 )*COS(work_location_lat*pi()/180)
          *POWER(SIN((' . $long . '-work_location_long)*pi()/180/2),2))) 
          as wdistance,  3956 * 2 * 
          ASIN(SQRT( POWER(SIN((' . $lat . ' - home_location_lat)*pi()/180/2),2)
          +COS(' . $lat . '*pi()/180 )*COS(home_location_lat*pi()/180)
          *POWER(SIN((' . $long . '-home_location_long)*pi()/180/2),2))) 
          as hdistance,DATEDIFF("' . getCurrentDate() . '", last_date_donation) as day_count  FROM ' . Tbl::table_user . ' WHERE 
              
          (work_location_long between (' . $long . '-' . $dist . '/cos(radians(' . $lat . '))*69) 
          and (' . $long . '+' . $dist . '/cos(radians(' . $lat . '))*69) 
          and work_location_lat between (' . $lat . '-(' . $dist . '/69)) 
          and (' . $lat . '+(' . $dist . '/69)))
           OR
          (home_location_long between (' . $long . '-' . $dist . '/cos(radians(' . $lat . '))*69) 
          and (' . $long . '+' . $dist . '/cos(radians(' . $lat . '))*69) 
          and home_location_lat between (' . $lat . '-(' . $dist . '/69)) 
          and (' . $lat . '+(' . $dist . '/69)))
          having (hdistance < ' . $dist . ' or wdistance < ' . $dist . ')  and blood_group = ' . $bloodGrp . '
          and day_count > 90 ';
        } else {
            $location = 'SELECT *, 3956 * 2 * 
          ASIN(SQRT( POWER(SIN((' . $lat . ' - work_location_lat)*pi()/180/2),2)
          +COS(' . $lat . '*pi()/180 )*COS(work_location_lat*pi()/180)
          *POWER(SIN((' . $long . '-work_location_long)*pi()/180/2),2))) 
          as wdistance,  3956 * 2 * 
          ASIN(SQRT( POWER(SIN((' . $lat . ' - home_location_lat)*pi()/180/2),2)
          +COS(' . $lat . '*pi()/180 )*COS(home_location_lat*pi()/180)
          *POWER(SIN((' . $long . '-home_location_long)*pi()/180/2),2))) 
          as hdistance, DATEDIFF("' . getCurrentDate() . '", last_date_donation) as day_count   FROM ' . Tbl::table_user . ' WHERE 
          (work_location_long between (' . $long . '-' . $dist . '/cos(radians(' . $lat . '))*69) 
          and (' . $long . '+' . $dist . '/cos(radians(' . $lat . '))*69) 
          and work_location_lat between (' . $lat . '-(' . $dist . '/69)) 
          and (' . $lat . '+(' . $dist . '/69)))
           OR
          (home_location_long between (' . $long . '-' . $dist . '/cos(radians(' . $lat . '))*69) 
          and (' . $long . '+' . $dist . '/cos(radians(' . $lat . '))*69) 
          and home_location_lat between (' . $lat . '-(' . $dist . '/69)) 
          and (' . $lat . '+(' . $dist . '/69)))
          having (hdistance < ' . $dist . ' or wdistance < ' . $dist . ')
          and day_count > 90 ';
        }
        switch ($sort_key) {
            case 1:
                $location .= ' ORDER BY first_name asc limit ' . $limit;
                break;
            case 2:
                $location .= ' ORDER BY first_name desc limit ' . $limit;
                break;
            case 3:
                $location .= ' ORDER BY added_date asc limit ' . $limit;
                break;
            case 4:
                $location .= ' ORDER BY added_date desc limit ' . $limit;
                break;
            case 5:
                $location .= ' ORDER BY hdistance, wdistance asc limit ' . $limit;
                break;
            case 6:
                $location .= ' ORDER BY hdistance, wdistance desc limit ' . $limit;
                break;
            case 7:
                $location .= ' ORDER BY day_count desc limit ' . $limit;
                break;
        }

        $qry = $this->db->query($location);

        $arr = $qry->result_array();

        $res[AppConst::errorindex_code] = (isset($arr) && sizeof($arr) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";

        $res[AppConst::res_value] = $arr;
        $res[AppConst::res_value1] = count($arr);

        return $res;
    }

    public function requestForBlood($dataToDb)
    {
        $errorMessage = null;
        $requestMaster = array();
        $temp = getval($dataToDb, 'patient_name');
        if ($temp != '') {
            $requestMaster['contact_person'] = $temp;
            $temp = getval($dataToDb, 'blood_group');
            if ($temp != '') {
                $requestMaster['blood_group'] = $temp;
                $temp = getval($dataToDb, 'no_of_units');
                if ($temp != '') {
                    $requestMaster['no_of_units'] = $temp;
                    $temp = getval($dataToDb, 'contactno');
                    if ($temp != '') {
                        $requestMaster['contact_no'] = $temp;
                        $temp = getval($dataToDb, 'location_lat');
                        if ($temp != '') {
                            $requestMaster['location_lat'] = $temp;
                            $temp = getval($dataToDb, 'location_long');
                            if ($temp != '') {
                                $requestMaster['location_long'] = $temp;
                                $temp = getval($dataToDb, 'date_of_need');
                                if ($temp != '') {
                                    $requestMaster['date_of_need'] = $temp;
                                    $temp = getval($dataToDb, 'user_id');
                                    if ($temp != '') {
                                        $requestMaster['user_id'] = $temp;
                                        $temp = getval($dataToDb, 'place');
                                        if ($temp != '') {
                                            $requestMaster['place'] = $temp;
                                        } else {
                                            $errorMessage = "Place is missing";
                                        }
                                    } else {
                                        $errorMessage = "User id is missing";
                                    }
                                } else {
                                    $errorMessage = "Date of prefered donation is missing";
                                }
                            } else {
                                $errorMessage = "Location(long) is missing";
                            }
                        } else {
                            $errorMessage = "Location(lat) is missing";
                        }
                    } else {
                        $errorMessage = "Contact number is missing";
                    }
                } else {
                    $errorMessage = "Number of units is missing";
                }
            } else {
                $errorMessage = "Blood group is missing";
            }
        } else {
            $errorMessage = "Patient name is missing";
        }
        if ($errorMessage == null || trim($errorMessage) == '') {
            $requestMaster['added_date'] = getCurrentDateWithTime();
            $requestMaster['updated_date'] = getCurrentDateWithTime();
            $requestMaster['status'] = AppConst::STATUS_ACTIVE;
            $this->db->insert(Tbl::table_request, $requestMaster);
            $reqId = $this->db->insert_id();
            if ($reqId > 0) {
                $msgArray = array();
                $filter['latitude'] = getval($requestMaster, 'location_lat');
                $filter['longitude'] = getval($requestMaster, 'location_long');
                /* if (getval($dataToDb, 'prefer_group') == AppConst::PREFER_TYPE_O_GROUP) {
                  $filter['blood_grp'] = array(AppConst::BLOOD_GROUP_O_MINUS_ID, AppConst::BLOOD_GROUP_O_PLUS_ID, getval($requestMaster, 'blood_group'));
                  } else if (getval($dataToDb, 'prefer_group') == AppConst::PREFER_TYPE_ALL_GROUP) {

                  } else {
                  $filter['blood_grp'] = array(getval($requestMaster, 'blood_group'));
                  }
                 */
                $filter['blood_grp'] = getval($requestMaster, 'blood_group');
                $filter['limit'] = AppConst::HOSPITALS_SEARCH_LIMIT;
                $filter['dist'] = 200;
                $filter['user_id'] = getval($dataToDb, 'user_id');

                $res = $this->searchBloodGroup($filter);
                $usersList = $res[AppConst::res_value];

                $userIds = array();
                $i = 0;
                foreach ($usersList as $uList) {
                    if (getval($uList, 'user_id') != getval($requestMaster, 'user_id')) {
                        $msgArray[$i]['chat_from'] = getval($requestMaster, 'user_id');
                        $msgArray[$i]['chat_to'] = getval($uList, 'user_id');
                        $msgArray[$i]['time'] = getCurrentDateWithTime();
                        $msgArray[$i]['chat_msg'] = AppConst::chat_blood_request_msg;
                        $msgArray[$i]['chat_type'] = AppConst::CHAT_TYPE_REQUEST;
                        $msgArray[$i]['req_id'] = $reqId;
                        $msgArray[$i]['chat_status'] = AppConst::CHAT_STATUS_PENDING;
                        $msgArray[$i]['updated_date'] = getCurrentDateWithTime();
                        array_push($userIds, getval($uList, 'user_id'));
                        $i++;
                    }
                }
                if (count($userIds) > 0) {
                    $this->db->insert_batch(Tbl::table_chat, $msgArray);
                    $chatTable = $this->db->affected_rows() > 0 ? TRUE : FALSE;
                    if ($chatTable) {
                        $deviceFilter['userIds'] = $userIds;
                        $userRes = $this->deviceInfo($deviceFilter);
                        $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                        $res[AppConst::errorindex_message] = AppConst::errormessage_db_request_success;
                        $res[AppConst::res_value] = $reqId;
                        $res[AppConst::res_value1] = $userRes[AppConst::res_value];
                    } else {
                        $res[AppConst::errorindex_code] = 200;
                        $res[AppConst::errorindex_message] = 'request not send';
                        $res[AppConst::res_value] = array();
                    }
                } else {
                    $res[AppConst::errorindex_code] = 600;
                    $res[AppConst::errorindex_message] = 'no user found on this request ';
                    $res[AppConst::res_value] = array();
                }
            } else {
                $res[AppConst::errorindex_code] = 300;
                $res[AppConst::errorindex_message] = 'request processing failed..';
                $res[AppConst::res_value] = array();
            }
        } else {
            $res[AppConst::errorindex_code] = 400;
            $res[AppConst::errorindex_message] = $errorMessage;
        }
        return $res;
    }

    public function requestForCommunity($dataToDb)
    {
        $errorMessage = null;
        $requestMaster = array();
        $temp = getval($dataToDb, 'community_name');
        if ($temp != '') {
            $requestMaster['community_name'] = $temp;
            $temp = getval($dataToDb, 'reason');
            if ($temp != '') {
                $requestMaster['reason'] = $temp;
                $temp = getval($dataToDb, 'place');
                if ($temp != '') {
                    $requestMaster['place'] = $temp;
                    $temp = getval($dataToDb, 'latitude');
                    if ($temp != '') {
                        $requestMaster['latitude'] = $temp;
                        $temp = getval($dataToDb, 'longitude');
                        if ($temp != '') {
                            $requestMaster['longitude'] = $temp;
                            $temp = getval($dataToDb, 'user_id');
                            if ($temp != '') {
                                $requestMaster['user_id'] = $temp;
                                $temp = getval($dataToDb, 'country');
                                if ($temp != '') {
                                    $requestMaster['country'] = $temp;
                                    $temp = getval($dataToDb, 'address');
                                    if ($temp != '') {
                                        $requestMaster['address'] = $temp;
                                    } else {
                                        $errorMessage = "Address is missing";
                                    }
                                } else {
                                    $errorMessage = "Country is missing";
                                }
                            } else {
                                $errorMessage = "User Id is missing";
                            }
                        } else {
                            $errorMessage = "Longitude is missing";
                        }
                    } else {
                        $errorMessage = "Latitude is missing";
                    }
                } else {
                    $errorMessage = "place is missing";
                }
            } else {
                $errorMessage = "Reason is missing";
            }
        } else {
            $errorMessage = "Community name is missing";
        }
        if ($errorMessage == null || trim($errorMessage) == '') {
            $dataToDb['uploadfileIndex'] = 'community_image';
            $imgUploadRes = uploadImageWithResize($dataToDb, AppConst::upload_folder_community);
            if ($imgUploadRes[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $requestMaster['community_image_url'] = $imgUploadRes[AppConst::res_value];
            }
            $requestMaster['created_at'] = getCurrentDateWithTime();
            $requestMaster['updated_at'] = getCurrentDateWithTime();
            $requestMaster['request_status'] = AppConst::COMMUNITY_REQUEST_NEW;
            $this->db->insert(Tbl::table_community_request, $requestMaster);
            $reqId = $this->db->insert_id();
            if ($reqId > 0) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = 'Request successfully send';
            } else {
                $res[AppConst::errorindex_code] = 300;
                $res[AppConst::errorindex_message] = 'request processing failed..';
                $res[AppConst::res_value] = array();
            }
        } else {
            $res[AppConst::errorindex_code] = 400;
            $res[AppConst::errorindex_message] = $errorMessage;
        }
        return $res;
    }

    private function checkUserExistInDrive($user_id, $drive_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('drive_id', $drive_id);
        $query = $this->db->get(Tbl::table_joined_drive_users);
        if ($query->num_rows() > 0) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            return $res;
        } else {
            $res[AppConst::errorindex_code] = 8;
            return $res;
        }
    }

    private function checkUserExistInCommunity($user_id, $community_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('community_id', $community_id);
        $query = $this->db->get(Tbl::table_community_subscription);
        if ($query->num_rows() > 0) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            return $res;
        } else {
            $res[AppConst::errorindex_code] = 8;
            return $res;
        }
    }

    public function joinToDrive($dataToDb)
    {

        $errorMessage = null;
        $requestMaster = array();
        $temp = getval($dataToDb, 'user_id');
        if ($temp != '') {
            $requestMaster['user_id'] = $temp;
            $temp = getval($dataToDb, 'drive_id');
            if ($temp != '') {
                $requestMaster['drive_id'] = $temp;
            } else {
                $errorMessage = "drive_id is missing";
            }
        } else {
            $errorMessage = "user_id is missing";
        }
        if ($errorMessage == null || trim($errorMessage) == '') {
            $res = $this->checkUserExistInDrive($requestMaster['user_id'], $requestMaster['drive_id']);
            if ($res[AppConst::errorindex_code] != AppConst::errorcode_success) {
                $requestMaster['created_at'] = getCurrentDateWithTime();
                $requestMaster['updated_at'] = getCurrentDateWithTime();
                $this->db->insert(Tbl::table_joined_drive_users, $requestMaster);
                $reqId = $this->db->insert_id();
                if ($reqId > 0) {
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = 'Joined donation drive successfully';
                } else {
                    $res[AppConst::errorindex_code] = 300;
                    $res[AppConst::errorindex_message] = 'Joining donation drive processing failed..';
                    $res[AppConst::res_value] = array();
                }
            } else {
                $res[AppConst::errorindex_code] = 403;
                $res[AppConst::errorindex_message] = 'User already joined donation drive';
            }
        } else {
            $res[AppConst::errorindex_code] = 400;
            $res[AppConst::errorindex_message] = $errorMessage;
        }
        return $res;
    }

    public function WithdrawFromDrive($dataToDb)
    {
        $errorMessage = null;
        $requestMaster = array();
        $temp = getval($dataToDb, 'user_id');
        if ($temp != '') {
            $requestMaster['user_id'] = $temp;
            $temp = getval($dataToDb, 'drive_id');
            if ($temp != '') {
                $requestMaster['drive_id'] = $temp;
            } else {
                $errorMessage = "drive_id is missing";
            }
        } else {
            $errorMessage = "user_id is missing";
        }
        if ($errorMessage == null || trim($errorMessage) == '') {
            $this->db->where('user_id', $requestMaster['user_id']);
            $this->db->where('drive_id', $requestMaster['drive_id']);
            $this->db->delete(Tbl::table_joined_drive_users);
            $subid = ($this->db->affected_rows() > 0) ? true : false;
            if ($subid == false) {
                $this->db->trans_complete();
                $this->db->trans_rollback();
                $res[AppConst::errorindex_code] = 200;
                $res[AppConst::errorindex_message] = "can not leave.. ";
                return $res;
            } else {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = 'Removed successfully';
            }
        } else {
            $res[AppConst::errorindex_code] = 400;
            $res[AppConst::errorindex_message] = $errorMessage;
        }
        return $res;
    }

    private function checkUserAddedDriveAsFavorite($user_id, $drive_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('drive_id', $drive_id);
        $query = $this->db->get(Tbl::table_favourites_drive_users);
        if ($query->num_rows() > 0) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            return $res;
        } else {
            $res[AppConst::errorindex_code] = 8;
            return $res;
        }
    }

    public function addDriveToFavorites($dataToDb)
    {

        $errorMessage = null;
        $requestMaster = array();
        $temp = getval($dataToDb, 'user_id');
        if ($temp != '') {
            $requestMaster['user_id'] = $temp;
            $temp = getval($dataToDb, 'drive_id');
            if ($temp != '') {
                $requestMaster['drive_id'] = $temp;
            } else {
                $errorMessage = "drive_id is missing";
            }
        } else {
            $errorMessage = "user_id is missing";
        }
        if ($errorMessage == null || trim($errorMessage) == '') {
            $res = $this->checkUserAddedDriveAsFavorite($requestMaster['user_id'], $requestMaster['drive_id']);
            if ($res[AppConst::errorindex_code] != AppConst::errorcode_success) {
                $requestMaster['created_at'] = getCurrentDateWithTime();
                $requestMaster['updated_at'] = getCurrentDateWithTime();
                $this->db->insert(Tbl::table_favourites_drive_users, $requestMaster);
                $reqId = $this->db->insert_id();
                if ($reqId > 0) {
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = 'Donation drive is added to your favourites';
                } else {
                    $res[AppConst::errorindex_code] = 300;
                    $res[AppConst::errorindex_message] = 'Add to favourites process is failed..';
                    $res[AppConst::res_value] = array();
                }
            } else {
                $res[AppConst::errorindex_code] = 403;
                $res[AppConst::errorindex_message] = 'User already added to favourites';
            }
        } else {
            $res[AppConst::errorindex_code] = 400;
            $res[AppConst::errorindex_message] = $errorMessage;
        }
        return $res;
    }
    public function WithdrawDriveFromFavourities($dataToDb)
    {
        $errorMessage = null;
        $requestMaster = array();
        $temp = getval($dataToDb, 'user_id');
        if ($temp != '') {
            $requestMaster['user_id'] = $temp;
            $temp = getval($dataToDb, 'drive_id');
            if ($temp != '') {
                $requestMaster['drive_id'] = $temp;
            } else {
                $errorMessage = "drive_id is missing";
            }
        } else {
            $errorMessage = "user_id is missing";
        }
        if ($errorMessage == null || trim($errorMessage) == '') {
            $this->db->where('user_id', $requestMaster['user_id']);
            $this->db->where('drive_id', $requestMaster['drive_id']);
            $this->db->delete(Tbl::table_favourites_drive_users);
            $subid = ($this->db->affected_rows() > 0) ? true : false;
            if ($subid == false) {
                $this->db->trans_complete();
                $this->db->trans_rollback();
                $res[AppConst::errorindex_code] = 200;
                $res[AppConst::errorindex_message] = "can not leave.. ";
                return $res;
            } else {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = 'Removed successfully';
            }
        } else {
            $res[AppConst::errorindex_code] = 400;
            $res[AppConst::errorindex_message] = $errorMessage;
        }
        return $res;
    }
    private function checkUserAddedBlogAsFavorite($user_id, $blog_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('blog_id', $blog_id);
        $query = $this->db->get(Tbl::table_blog_favourites);
        if ($query->num_rows() > 0) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            return $res;
        } else {
            $res[AppConst::errorindex_code] = 8;
            return $res;
        }
    }
    public function addBlogToFavorites($dataToDb)
    {

        $errorMessage = null;
        $requestMaster = array();
        $temp = getval($dataToDb, 'user_id');
        if ($temp != '') {
            $requestMaster['user_id'] = $temp;
            $temp = getval($dataToDb, 'blog_id');
            if ($temp != '') {
                $requestMaster['blog_id'] = $temp;
            } else {
                $errorMessage = "blog_id is missing";
            }
        } else {
            $errorMessage = "user_id is missing";
        }
        if ($errorMessage == null || trim($errorMessage) == '') {
            $res = $this->checkUserAddedBlogAsFavorite($requestMaster['user_id'], $requestMaster['blog_id']);
            if ($res[AppConst::errorindex_code] != AppConst::errorcode_success) {
                $requestMaster['created_at'] = getCurrentDateWithTime();
                $requestMaster['updated_at'] = getCurrentDateWithTime();
                $this->db->insert(Tbl::table_blog_favourites, $requestMaster);
                $reqId = $this->db->insert_id();
                if ($reqId > 0) {
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = 'Blog is added to your favourites';
                } else {
                    $res[AppConst::errorindex_code] = 300;
                    $res[AppConst::errorindex_message] = 'Add to favourites process is failed..';
                    $res[AppConst::res_value] = array();
                }
            } else {
                $res[AppConst::errorindex_code] = 403;
                $res[AppConst::errorindex_message] = 'User already added to favourites';
            }
        } else {
            $res[AppConst::errorindex_code] = 400;
            $res[AppConst::errorindex_message] = $errorMessage;
        }
        return $res;
    }
    public function WithdrawBlogFromFavourities($dataToDb)
    {
        $errorMessage = null;
        $requestMaster = array();
        $temp = getval($dataToDb, 'user_id');
        if ($temp != '') {
            $requestMaster['user_id'] = $temp;
            $temp = getval($dataToDb, 'blog_id');
            if ($temp != '') {
                $requestMaster['blog_id'] = $temp;
            } else {
                $errorMessage = "blog_id is missing";
            }
        } else {
            $errorMessage = "user_id is missing";
        }
        if ($errorMessage == null || trim($errorMessage) == '') {
            $this->db->where('user_id', $requestMaster['user_id']);
            $this->db->where('blog_id', $requestMaster['blog_id']);
            $this->db->delete(Tbl::table_blog_favourites);
            $subid = ($this->db->affected_rows() > 0) ? true : false;
            if ($subid == false) {
                $this->db->trans_complete();
                $this->db->trans_rollback();
                $res[AppConst::errorindex_code] = 200;
                $res[AppConst::errorindex_message] = "can not leave.. ";
                return $res;
            } else {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = 'Removed successfully';
            }
        } else {
            $res[AppConst::errorindex_code] = 400;
            $res[AppConst::errorindex_message] = $errorMessage;
        }
        return $res;
    }


    public function searchHospitals($filter = array())
    {
        $lat = getval($filter, 'latitude');
        $long = getval($filter, 'longitude');
        $hospital_name = getval($filter, 'hospital_name');
        $dist = AppConst::HOSPITALS_DISTANCE;
        $location = null;
        if ($hospital_name != '') {
            $location = 'SELECT *, 3956 * 2 * 
          ASIN(SQRT( POWER(SIN((' . $lat . ' -latitude)*pi()/180/2),2)
          +COS(' . $lat . '*pi()/180 )*COS(latitude*pi()/180)
          *POWER(SIN((' . $long . '-longitude)*pi()/180/2),2))) 
          as hdistance  FROM ' . Tbl::table_hospital . ' as hospital LEFT JOIN ' . Tbl::table_state . ' as state ON hospital.state_id =  state.state_id LEFT JOIN ' . Tbl::table_country . ' as country ON country.country_id = hospital.country_id   WHERE 
          longitude between (' . $long . '-' . $dist . '/cos(radians(' . $lat . '))*69) 
          and (' . $long . '+' . $dist . '/cos(radians(' . $lat . '))*69) 
          and latitude between (' . $lat . '-(' . $dist . '/69)) 
          and (' . $lat . '+(' . $dist . '/69))
          having (hdistance < ' . $dist . ') and hospital_name like "%' . $hospital_name . '%" 
          ORDER BY hdistance limit ' . $dist;
        } else {
            $location = 'SELECT *, 3956 * 2 * 
          ASIN(SQRT( POWER(SIN((' . $lat . ' -latitude)*pi()/180/2),2)
          +COS(' . $lat . '*pi()/180 )*COS(latitude*pi()/180)
          *POWER(SIN((' . $long . '-longitude)*pi()/180/2),2))) 
          as hdistance  FROM ' . Tbl::table_hospital . ' as hospital LEFT JOIN ' . Tbl::table_state . ' as state ON hospital.state_id =  state.state_id LEFT JOIN ' . Tbl::table_country . ' as country ON country.country_id = hospital.country_id WHERE 
          longitude between (' . $long . '-' . $dist . '/cos(radians(' . $lat . '))*69) 
          and (' . $long . '+' . $dist . '/cos(radians(' . $lat . '))*69) 
          and latitude between (' . $lat . '-(' . $dist . '/69)) 
          and (' . $lat . '+(' . $dist . '/69))
          having (hdistance < ' . $dist . ')
          ORDER BY hdistance limit ' . $dist;
        }

        $qry = $this->db->query($location);

        $arr = $qry->result_array();

        $res[AppConst::errorindex_code] = (isset($arr) && sizeof($arr) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
        if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $res[AppConst::res_value] = $arr;
            $res[AppConst::res_value1] = count($arr);
        }
        return $res;
    }

    public function activeRequests($filter = array())
    {
        $this->db->select('*,request.blood_group as required_blood_group,request.place as patient_place');
        $this->db->from(Tbl::table_request . ' as request');
        $this->db->join(Tbl::table_user . ' as user', 'user.user_id = request.user_id', 'left');
        $this->db->where('chat.chat_to', getval($filter, 'user_id'));
        $this->db->where('chat.chat_type', AppConst::CHAT_TYPE_REQUEST);
        $this->db->where('request.status !=', AppConst::STATUS_CLOSED);
        //$this->db->where('request.date_of_need>=', getval($filter, 'today_date'));
        $requestInfo = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($requestInfo) && sizeof($requestInfo) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";

        $res[AppConst::res_value] = $requestInfo;

        return $res;
    }

    public function deviceInfo($filter = array())
    {
        $this->db->select('device.dev_notify_id');
        $this->db->from(Tbl::table_device_master . ' as device');
        //$this->db->join(Tbl::table_user . ' as user', 'user.user_id = device.user_id', 'left');
        if (isset($filter['userIds']) && count($filter['userIds']) > 0) {
            $this->db->where_in('device.user_id ', $filter['userIds']);
        }
        if (getval($filter, 'user_id') != '') {
            $this->db->where_in('device.user_id ', $filter['user_id']);
        }
        $deviceList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($deviceList) && sizeof($deviceList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
        $res[AppConst::res_value] = $deviceList;
        return $res;
    }

    public function getCommunityList($filter = array())
    {
        //$this->db->select('*,GROUP_CONCAT(count(subscription.subscription_id) SEPARATOR ",") as subscription_count');
        $this->db->select('*');
        $this->db->from(Tbl::table_community . ' as community');
        if (trim(getval($filter, 'community_name')) != '') {
            $this->db->like('community.community_name', getval($filter, 'community_name'));
        }
        if (intval(getval($filter, 'community_id')) > 0) {
            $this->db->where('community.community_id', getval($filter, 'community_id'));
        }
        if (intval(getval($filter, 'status')) > 0) {
            $this->db->where('community.community_status', getval($filter, 'status'));
        }
        //$this->db->join(Tbl::table_community_subscription . ' as subscription', 'subscription.community_id = community.community_id', 'left outer');
        $this->db->order_by('community.subscription_count', 'desc');
        $communityList = $this->db->get()->result_array();
        $i = 0;
        foreach ($communityList as $dList) {
            $res1 = $this->checkUserExistInCommunity(getval($filter, 'user_id'), getval($dList, 'community_id'));
            if ($res1[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $communityList[$i]['is_joined'] = AppConst::STATUS_ACTIVE;
            } else {
                $communityList[$i]['is_joined'] = AppConst::STATUS_BLOCK;
            }
            $i++;
        }
        $res[AppConst::errorindex_code] = (isset($communityList) && sizeof($communityList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No communities found.";
        $res[AppConst::res_value] = $communityList;
        return $res;
    }

    public function getUsersList($filter = array())
    {
        //$this->db->select('*,GROUP_CONCAT(count(subscription.subscription_id) SEPARATOR ",") as subscription_count');
        $this->db->select('*');
        $this->db->from(Tbl::table_user . ' as user');
        if (trim(getval($filter, 'name')) != '') {
            $this->db->like('user.first_name', getval($filter, 'name'));
        }
        if (intval(getval($filter, 'blood_group')) > 0) {
            $this->db->where('user.blood_group', getval($filter, 'blood_group'));
        }
        //$this->db->join(Tbl::table_community_subscription . ' as subscription', 'subscription.community_id = community.community_id', 'left outer');
        //$this->db->order_by('community.subscription_count', 'desc');
        $communityList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($communityList) && sizeof($communityList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No communities found.";
        $res[AppConst::res_value] = $communityList;
        return $res;
    }

    public function getNotifications($filter = array())
    {
        //$this->db->select('*,GROUP_CONCAT(count(subscription.subscription_id) SEPARATOR ",") as subscription_count');
        $this->db->select('*');
        $this->db->from(Tbl::table_notification);
        if (intval(getval($filter, 'user_id')) > 0) {
            $this->db->where('user_id', getval($filter, 'user_id'));
        }
        //$this->db->join(Tbl::table_community_subscription . ' as subscription', 'subscription.community_id = community.community_id', 'left outer');
        //$this->db->order_by('community.subscription_count', 'desc');
        $communityList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($communityList) && sizeof($communityList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No Notifications found.";
        $res[AppConst::res_value] = $communityList;
        return $res;
    }

    public function searchNearByDonors($filter = array())
    {
        $lat = getval($filter, 'latitude');
        $long = getval($filter, 'longitude');
        $limit = isset($filter['limit']) ? getval($filter, 'limit') : 100;
        $dist = isset($filter['dist']) ? getval($filter, 'dist') : 100;
        $location = 'SELECT *, 3956 * 2 * 
          ASIN(SQRT( POWER(SIN((' . $lat . ' - work_location_lat)*pi()/180/2),2)
          +COS(' . $lat . '*pi()/180 )*COS(work_location_lat*pi()/180)
          *POWER(SIN((' . $long . '-work_location_long)*pi()/180/2),2))) 
          as wdistance,  3956 * 2 * 
          ASIN(SQRT( POWER(SIN((' . $lat . ' - home_location_lat)*pi()/180/2),2)
          +COS(' . $lat . '*pi()/180 )*COS(home_location_lat*pi()/180)
          *POWER(SIN((' . $long . '-home_location_long)*pi()/180/2),2))) 
          as hdistance  FROM ' . Tbl::table_user . ' WHERE 
          work_location_long between (' . $long . '-' . $dist . '/cos(radians(' . $lat . '))*69) 
          and (' . $long . '+' . $dist . '/cos(radians(' . $lat . '))*69) 
          and work_location_lat between (' . $lat . '-(' . $dist . '/69)) 
          and (' . $lat . '+(' . $dist . '/69))
           OR
          home_location_long between (' . $long . '-' . $dist . '/cos(radians(' . $lat . '))*69) 
          and (' . $long . '+' . $dist . '/cos(radians(' . $lat . '))*69) 
          and home_location_lat between (' . $lat . '-(' . $dist . '/69)) 
          and (' . $lat . '+(' . $dist . '/69))
          having (hdistance < ' . $dist . ' or wdistance < ' . $dist . ')
          ORDER BY hdistance, wdistance limit ' . $limit;

        $qry = $this->db->query($location);

        $arr = $qry->result_array();

        $res[AppConst::errorindex_code] = (isset($arr) && sizeof($arr) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";

        $res[AppConst::res_value] = $arr;
        $res[AppConst::res_value1] = count($arr);

        return $res;
    }

    public function userChatList($filter = array())
    {
        if (getval($filter, 'request_id') != '') {
            $query = 'SELECT from_user.first_name AS sender_first_name,from_user.last_name AS sender_last_name,to_user.first_name AS reciever_first_name,to_user.last_name AS reciever_last_name,chat.chat_msg,chat.time,chat.req_id,chat.chat_from,chat.chat_to  FROM ' . Tbl::table_chat . ' AS `chat` LEFT JOIN ' . Tbl::table_request . ' AS `request` ON `request`.`request_id` = `chat`.`req_id`' .
                'LEFT JOIN ' . Tbl::table_user . ' AS `from_user` ON `from_user`.`user_id` = `chat`.`chat_from`  LEFT JOIN ' . Tbl::table_user . ' AS `to_user` ON `to_user`.`user_id` = `chat`.`chat_to`  WHERE  `chat`.`req_id` = ' . $filter['request_id'] .
                ' AND (`chat`.`chat_from` =' . $filter['from_id'] . ' AND `chat`.`chat_to` =' . $filter['to_id'] . ' ) OR (`chat`.`chat_from` = ' . $filter['to_id'] . ' AND `chat`.`chat_to` =' . $filter['from_id'] . ' )' .
                ' AND `chat`.`chat_type` =' . $filter['chat_type'] . ' ORDER BY `chat`.`time` ASC' .
                ' LIMIT ' . $filter['limit'];
        } else {
            $query = 'SELECT from_user.first_name AS sender_first_name,from_user.last_name AS sender_last_name,to_user.first_name AS reciever_first_name,to_user.last_name AS reciever_last_name,chat.chat_msg,chat.time,chat.req_id,chat.chat_from,chat.chat_to  FROM ' . Tbl::table_chat . ' AS `chat` ' .
                'LEFT JOIN ' . Tbl::table_user . ' AS `from_user` ON `from_user`.`user_id` = `chat`.`chat_from`  LEFT JOIN ' . Tbl::table_user . ' AS `to_user` ON `to_user`.`user_id` = `chat`.`chat_to`  WHERE ' .
                '  (`chat`.`chat_from` =' . $filter['from_id'] . ' AND `chat`.`chat_to` =' . $filter['to_id'] . ' ) OR (`chat`.`chat_from` = ' . $filter['to_id'] . ' AND `chat`.`chat_to` =' . $filter['from_id'] . ' )' .
                ' AND `chat`.`chat_type` =' . $filter['chat_type'] . ' ORDER BY `chat`.`time` ASC' .
                ' LIMIT ' . $filter['limit'];
        }
        $qry = $this->db->query($query);

        $chatList = $qry->result_array();

        $res[AppConst::errorindex_code] = (isset($chatList) && sizeof($chatList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
        if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $dataToUpdate['chat_read_status'] = AppConst::CHAT_READ_STATUS;
            $dataToUpdate['updated_date'] = getCurrentDateWithTime();

            $this->db->where('chat_to', getval($filter, 'from_id'));
            $this->db->where('chat_from', getval($filter, 'to_id'));
            $this->db->update(Tbl::table_chat, $dataToUpdate);
        }
        $res[AppConst::res_value] = $chatList;

        return $res;
    }

    public function addUserChat($chatInfo = array())
    {
        $dataToChat = array();
        $errorMessage = null;
        $temp = getval($chatInfo, 'chat_from');
        if ($temp != '') {
            $dataToChat['chat_from'] = $temp;
            $temp = getval($chatInfo, 'chat_to');
            if ($temp != '') {
                $dataToChat['chat_to'] = $temp;
                $temp = getval($chatInfo, 'chat_msg');
                if ($temp != '') {
                    $dataToChat['chat_msg'] = $temp;
                    $temp = getval($chatInfo, 'request_id');
                    if ($temp != '') {
                        $dataToChat['req_id'] = $temp;
                    } else {
                        $errorMessage = "Request Id is missing";
                    }
                } else {
                    $errorMessage = "Message is missing";
                }
            } else {
                $errorMessage = "Reciever Id is missing";
            }
        } else {
            $errorMessage = "Sender Id is missing";
        }
        if ($errorMessage == null || trim($errorMessage) == '') {
            $chat_room_id = 0;
            if (getval($dataToChat, 'chat_from') > getval($dataToChat, 'chat_to')) {
                $chat_room_id = getval($dataToChat, 'chat_from') . getval($dataToChat, 'chat_to');
            } else {
                $chat_room_id = getval($dataToChat, 'chat_to') . getval($dataToChat, 'chat_from');
            }
            $dataToChat['time'] = getCurrentDateWithTime();
            $dataToChat['chat_type'] = AppConst::CHAT_TYPE_NORMAL;
            $dataToChat['chat_status'] = AppConst::STATUS_ACTIVE;
            $dataToChat['updated_date'] = getCurrentDateWithTime();
            $dataToChat['chat_room_id'] = $chat_room_id;
            $dataToChat['chat_read_status'] = AppConst::CHAT_PUSH_NOTIFICATION_SENT_STATUS;
            $this->db->insert(Tbl::table_chat, $dataToChat);
            $chatId = $this->db->insert_id();
            if ($chatId <= 0) {
                $res[AppConst::errorindex_code] = 100;
                $res[AppConst::errorindex_message] = "can not add chat.. ";
            } else {
                $filter['user_id'] = getval($dataToChat, 'chat_to');
                $userRes = $this->deviceInfo($filter);

                $dataToChat['chat_id'] = $chatId;
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = 'successfully sent';
                $res[AppConst::res_value] = $dataToChat;
                if ($userRes[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $res[AppConst::res_value1] = $userRes[AppConst::res_value];
                }
                $userFilter['user_id'] = getval($chatInfo, 'chat_from');
                $senderRes = $this->userProfile($userFilter);
                if ($senderRes[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $res[AppConst::res_value2] = $senderRes[AppConst::res_value]['first_name'] . ' ' . $senderRes[AppConst::res_value]['last_name'];
                }
            }
        } else {
            $res[AppConst::errorindex_code] = 400;
            $res[AppConst::errorindex_message] = $errorMessage;
        }
        return $res;
    }

    public function getSubscribedCommunityList($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_community_subscription . ' as subscription');
        $this->db->join(Tbl::table_community . ' as community', 'community.community_id = subscription.community_id', 'left');

        if (intval(getval($filter, 'user_id')) > 0) {
            $this->db->where('subscription.user_id', getval($filter, 'user_id'));
        }
        if (intval(getval($filter, 'status')) > 0) {
            $this->db->where('community.community_status', getval($filter, 'status'));
        }
        if (intval(getval($filter, 'community_id')) > 0) {
            $this->db->where('subscription.community_id', getval($filter, 'community_id'));
        }
        $this->db->order_by('subscription.subscription_updated_date', 'desc');
        $subscriptionList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($subscriptionList) && sizeof($subscriptionList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";

        $res[AppConst::res_value] = $subscriptionList;

        return $res;
    }

    public function getAdminCommunityList($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_community_request . ' as request');
        $this->db->join(Tbl::table_community . ' as community', 'community.community_id = request.community_id', 'left');

        if (intval(getval($filter, 'user_id')) > 0) {
            $this->db->where('request.user_id', getval($filter, 'user_id'));
        }
        if (intval(getval($filter, 'status')) > 0) {
            $this->db->where('community.community_status', getval($filter, 'status'));
        }
        if (intval(getval($filter, 'community_id')) > 0) {
            $this->db->where('request.community_id', getval($filter, 'community_id'));
        }
        $this->db->limit(1);
        $subscriptionList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($subscriptionList) && sizeof($subscriptionList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";

        $res[AppConst::res_value] = $subscriptionList;

        return $res;
    }
    private function checkUserIsCommunityAdmin($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('request_status', AppConst::COMMUNITY_REQUEST_ACCEPT);
        $query = $this->db->get(Tbl::table_community_request);
        if ($query->num_rows() > 0) {
            $this->db->select('community_id');
            $this->db->from(Tbl::table_community_request);
            $this->db->where('user_id', $user_id);
            $List = $this->db->get()->result_array();
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            $res[AppConst::res_value] = $List[0];
            return $res;
        } else {
            $res[AppConst::errorindex_code] = 8;
            return $res;
        }
    }
    public function getSubscribedUsersList($filter = array())
    {
        $res = $this->checkUserIsCommunityAdmin(getval($filter, 'user_id'));
        if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $communityId = getval($res[AppConst::res_value], 'community_id');
            $this->db->select('user.*');
            $this->db->from(Tbl::table_community_subscription . ' as subscription');
            $this->db->join(Tbl::table_user . ' as user', 'user.user_id = subscription.user_id');
            $this->db->where('subscription.community_id', $communityId);
            $this->db->order_by('subscription.subscription_updated_date', 'desc');
            $subscriptionList = $this->db->get()->result_array();
            $res[AppConst::errorindex_code] = (isset($subscriptionList) && sizeof($subscriptionList) > 0) ? AppConst::errorcode_success : 8;
            $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";

            $res[AppConst::res_value] = $subscriptionList;
        } else {
            $res[AppConst::errorindex_code] = 8;
            $res[AppConst::errorindex_message] = 'User is not an community admin';
        }

        return $res;
    }
    public function removeUserFromMyCommunity($filter = array())
    {
        $res = $this->checkUserIsCommunityAdmin(getval($filter, 'user_id'));
        if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $this->db->trans_begin();
            $communityId = getval($res[AppConst::res_value], 'community_id');
            $this->db->where('user_id', getval($filter, 'member_user_id'));
            $this->db->where('community_id', $communityId);
            $this->db->delete(Tbl::table_community_subscription);
            $isdeleted = ($this->db->affected_rows() > 0) ? true : false;
            if ($isdeleted) {
                $this->db->set('subscription_count', 'subscription_count-1', FALSE);
                $this->db->set('community_updated_date', "'" . getCurrentDateWithTime() . "'", FALSE);
                $this->db->where('community_id', $communityId);
                $this->db->update(Tbl::table_community);
                $communityUpdate = ($this->db->affected_rows() > 0) ? true : false;
                if ($communityUpdate == FALSE) {
                    $this->db->trans_complete();
                    $this->db->trans_rollback();
                    $res[AppConst::errorindex_code] = 300;
                    $res[AppConst::errorindex_message] = "can not update subscription count.. ";
                    return $res;
                } else {
                    $this->db->trans_complete();
                    $this->db->trans_commit();
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = "Successfully removed";
                }
            } else {
                $this->db->trans_complete();
                $this->db->trans_rollback();
                $res[AppConst::errorindex_code] = 1;
                $res[AppConst::errorindex_message] = "Remove failed";
            }
        } else {
            $res[AppConst::errorindex_code] = 8;
            $res[AppConst::errorindex_message] = 'User is not an community admin';
        }

        return $res;
    }
    public function addUserFromMyCommunity($filter = array())
    {
        $res = $this->checkUserIsCommunityAdmin(getval($filter, 'user_id'));
        if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $communityId = getval($res[AppConst::res_value], 'community_id');
            $filter['user_id'] = getval($filter, 'member_user_id');
            $filter['community_id'] = $communityId;
            $subRes = $this->getSubscribedCommunityList($filter);
            if ($subRes[AppConst::errorindex_code] != AppConst::errorcode_success) {
                $this->db->trans_begin();
                $dataToSub['user_id'] = getval($filter, 'member_user_id');
                $dataToSub['community_id'] = $communityId;
                $dataToSub['subscription_added_date'] = getCurrentDateWithTime();
                $dataToSub['subscription_updated_date'] = getCurrentDateWithTime();
                $dataToSub['subscription_status'] = AppConst::STATUS_ACTIVE;
                $this->db->insert(Tbl::table_community_subscription, $dataToSub);
                $isAdded = $this->db->insert_id();
                if ($isAdded > 0) {
                    $this->db->set('subscription_count', 'subscription_count+1', false);
                    $this->db->set('community_updated_date', "'" . getCurrentDateWithTime() . "'", false);
                    $this->db->where('community_id', $communityId);
                    $this->db->update(Tbl::table_community);
                    $communityUpdate = ($this->db->affected_rows() > 0) ? true : false;
                    if ($communityUpdate == false) {
                        $this->db->trans_complete();
                        $this->db->trans_rollback();
                        $res[AppConst::errorindex_code] = 300;
                        $res[AppConst::errorindex_message] = "can not update subscription count.. ";
                        return $res;
                    } else {
                        $this->db->trans_complete();
                        $this->db->trans_commit();
                        $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                        $res[AppConst::errorindex_message] = "Successfully added";
                    }
                } else {
                    $this->db->trans_complete();
                    $this->db->trans_rollback();
                    $res[AppConst::errorindex_code] = 1;
                    $res[AppConst::errorindex_message] = "Adding failed";
                }
            } else {
                $res[AppConst::errorindex_code] = 300;
                $res[AppConst::errorindex_message] = 'User is already joined to community';
            }
        } else {
            $res[AppConst::errorindex_code] = 8;
            $res[AppConst::errorindex_message] = 'User is not an community admin';
        }

        return $res;
    }
    public function getFeaturedProductList($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_product);
        $this->db->where('category', 1);
        if (getval($filter, 'country') != "") {
            $this->db->where('country', getval($filter, 'country'));
        }
        $this->db->order_by('created_at', 'desc');
        $productList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($subscriptproductListiproductListonList) && sizeof($productList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
        $res[AppConst::res_value] = $productList;
        return $res;
    }
    public function getAllProductList($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_product);
        $this->db->where('status', AppConst::STATUS_ACTIVE);
        if (getval($filter, 'country') != "") {
            $this->db->where('country', getval($filter, 'country'));
        }
        if (getval($filter, 'product_name') != "") {
            $this->db->like('product_name', getval($filter, 'product_name'));
        }
        $this->db->order_by('created_at', 'desc');
        $productList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($productList) && sizeof($productList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";

        $res[AppConst::res_value] = $productList;

        return $res;
    }
    public function getTrendingProductList($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_product);
        $this->db->where('category', 2);
        if (getval($filter, 'country') != "") {
            $this->db->where('country', getval($filter, 'country'));
        }
        $productList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($productList) && sizeof($productList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
        $res[AppConst::res_value] = $productList;

        return $res;
    }
    public function getTodaysHydration($filter = array())
    {
        $check = $this->checkIsHydrated(getval($filter, 'user_id'), getCurrentDate());
        if ($check[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $this->db->select('*');
            $this->db->from(Tbl::table_hydration);
            $this->db->where('user_id', getval($filter, 'user_id'));
            $this->db->where('date', getCurrentDate());
            $subscriptionList = $this->db->get()->result_array();
            $res[AppConst::errorindex_code] = (isset($subscriptionList) && sizeof($subscriptionList) > 0) ? AppConst::errorcode_success : 8;
            $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
            $percentage = 0;
            $percentage = ($subscriptionList[0]['cunsumed_water'] / AppConst::DAILY_WATER_INTAKE) * 100;
            $last_time = date("H:i:s", strtotime($subscriptionList[0]['updated_at']));
            $subscriptionList[0]['last_time_drink'] = $last_time;
            $subscriptionList[0]['percentage'] = $percentage;
            $res[AppConst::res_value] = $subscriptionList;
            return $res;
        } else {
            $res[AppConst::errorindex_code] = 409;
            $res[AppConst::errorindex_message] = "Can't view";
            return $res;
        }
    }
    private function last_id($user_id)
    {
        $this->db->select('id');
        $this->db->from(Tbl::table_hydration_lines);
        $List = $this->db->get();
        $row = $List->last_row();
        if ($row) {
            $nextId = (int)$row->id + 1;
        } else {
            $nextId = 1;
        }
        return $nextId;
    }
    public function waterConsumed($filter = array())
    {
        $check = $this->checkIsHydrated(getval($filter, 'user_id'), getCurrentDate());
        if ($check[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $this->db->trans_begin();
            $hydration = $this->getHydrationId(getval($filter, 'user_id'), getCurrentDate());
            $hydrationId = $hydration[AppConst::res_value]['id'];
            $hydrationMaster['consumed_ml'] = AppConst::ONE_GLASS_WATER;
            $hydrationMaster['created_at'] = getCurrentDateWithTime();
            $hydrationMaster['hydration_id'] = $hydrationId;
            //$cenvertedTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($now)));
            $this->db->insert(Tbl::table_hydration_lines, $hydrationMaster);
            $isAdded = $this->db->insert_id();
            if ($isAdded <= 0) {
                $this->db->trans_complete();
                $this->db->trans_rollback();
                $res[AppConst::errorindex_code] = 100;
                $res[AppConst::errorindex_message] = "user insertion failed.. ";
                return $res;
            } else {
                $now = getCurrentTime();
                $remindingTime = date('Y-m-d H:i:s', strtotime('+3 hours', strtotime($now)));
                //$this->db->set('community_updated_date', "'" . getCurrentDateWithTime() . "'", FALSE);
                $consumption = $this->getCurrentConsumption($hydrationId);
                $data['reminding_time'] = $remindingTime;
                $data['cunsumed_water'] = $consumption + AppConst::ONE_GLASS_WATER;
                $this->db->where('id', $hydrationId);
                $this->db->update(Tbl::table_hydration, $data);
                $anyUpdate = $this->db->affected_rows() > 0 ? TRUE : FALSE;
                if ($anyUpdate == FALSE) {
                    $this->db->trans_complete();
                    $this->db->trans_rollback();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'can not update';
                    return $res;
                } else {
                    $this->db->trans_complete();
                    $this->db->trans_commit();
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = 'Successfully updated';
                    return $res;
                }
            }
        } else {
            $res[AppConst::errorindex_code] = 409;
            $res[AppConst::errorindex_message] = "Can't view";
            return $res;
        }
    }
    public function createDonationDrive($dataToDb = array())
    {
        $errorMessage = null;
        $temp = getval($dataToDb, 'title');
        if ($temp != '') {
            $requestMaster['title'] = $temp;
            $temp = getval($dataToDb, 'event_date');
            if ($temp != '') {
                $requestMaster['event_date'] = $temp;
                $temp = getval($dataToDb, 'description');
                if ($temp != '') {
                    $requestMaster['description'] = $temp;
                    $temp = getval($dataToDb, 'banner_image');
                    if ($temp != '') {
                        $base64_string = isset($dataToDb['banner_image']) ? getval($dataToDb, 'banner_image') : '';
                        $basepath = 'uploads' . DIRECTORY_SEPARATOR;
                        $uploadpath = 'drive_banner' . DIRECTORY_SEPARATOR . getCurrentDate() . DIRECTORY_SEPARATOR;
                        if (!is_dir($basepath . $uploadpath)) {
                            mkdir($basepath . $uploadpath, 0777, TRUE);
                        }
                        $dbpath = $uploadpath . getCurrentDay() . '_' . getCurrentMonth() . '_' . getCurrentYear() . '_' . getCurrentHour() . '_' . getCurrentMinute() . '_' . getCurrentSecond() . '.jpg';
                        $output_file = $basepath . $dbpath;
                        $ifp = fopen($output_file, "wb");
                        fwrite($ifp, base64_decode($base64_string));
                        fclose($ifp);
                        $requestMaster['banner_image'] = "uploads/" . $dbpath;
                        $temp = getval($dataToDb, 'latitude');
                        if ($temp != '') {
                            $requestMaster['latitude'] = $temp;
                            $temp = getval($dataToDb, 'longitude');
                            if ($temp != '') {
                                $requestMaster['longitude'] = $temp;
                                $temp = getval($dataToDb, 'place');
                                if ($temp != '') {
                                    $requestMaster['place'] = $temp;
                                    $temp = getval($dataToDb, 'community');
                                    if ($temp != '') {
                                        $requestMaster['community'] = $temp;
                                        $temp = getval($dataToDb, 'user_id');
                                        if ($temp != '') {
                                            $requestMaster['created_user_id'] = $temp;
                                        } else {
                                            $errorMessage = "user_id is missing";
                                        }
                                    } else {
                                        $errorMessage = "community is missing";
                                    }
                                } else {
                                    $errorMessage = "place is missing";
                                }
                            } else {
                                $errorMessage = "longitude is missing";
                            }
                        } else {
                            $errorMessage = "latitude is missing";
                        }
                    } else {
                        $errorMessage = "banner_image is missing";
                    }
                } else {
                    $errorMessage = "description is missing";
                }
            } else {
                $errorMessage = "event_date is missing";
            }
        } else {
            $errorMessage = "title is missing";
        }
        if ($errorMessage == null || trim($errorMessage) == '') {
            $requestMaster['drive_status'] = AppConst::STATUS_ACTIVE;
            $requestMaster['created_at'] = getCurrentDateWithTime();
            $requestMaster['updated_at'] = getCurrentDateWithTime();
            $this->db->insert(Tbl::table_donation_drive, $requestMaster);
            $reqId = $this->db->insert_id();
            if ($reqId > 0) {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = 'Donation drive created successfully';
            } else {
                $res[AppConst::errorindex_code] = 300;
                $res[AppConst::errorindex_message] = 'Creating drive processing failed..';
                $res[AppConst::res_value] = array();
            }
        } else {
            $res[AppConst::errorindex_code] = 400;
            $res[AppConst::errorindex_message] = $errorMessage;
        }
        return $res;
    }
    public function removeConsumed($filter = array())
    {
        $check = $this->checkIsHydrated(getval($filter, 'user_id'), getCurrentDate());
        if ($check[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $this->db->trans_begin();
            $hydration = $this->getHydrationId(getval($filter, 'user_id'), getCurrentDate());
            $hydrationId = $hydration[AppConst::res_value]['id'];
            $hydration_line = $this->getHydrationLineId($hydrationId);
            if ($hydration_line[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $this->db->where('id', $hydration_line[AppConst::res_value]['id']);
                $this->db->delete(Tbl::table_hydration_lines);
                $delId = ($this->db->affected_rows() > 0) ? true : false;
                if ($delId == false) {
                    $this->db->trans_complete();
                    $this->db->trans_rollback();
                    $res[AppConst::errorindex_code] = 200;
                    $res[AppConst::errorindex_message] = "can not delete.. ";
                    return $res;
                } else {
                    $now = getCurrentTime();
                    $remindingTime = date('Y-m-d H:i:s', strtotime('+3 hours', strtotime($now)));
                    //$this->db->set('community_updated_date', "'" . getCurrentDateWithTime() . "'", FALSE);
                    $consumption = $this->getCurrentConsumption($hydrationId);
                    $data['reminding_time'] = $remindingTime;
                    $data['cunsumed_water'] = $consumption - AppConst::ONE_GLASS_WATER;
                    $this->db->where('id', $hydrationId);
                    $this->db->update(Tbl::table_hydration, $data);
                    $anyUpdate = $this->db->affected_rows() > 0 ? TRUE : FALSE;
                    if ($anyUpdate == FALSE) {
                        $this->db->trans_complete();
                        $this->db->trans_rollback();
                        $res[AppConst::errorindex_code] = 20;
                        $res[AppConst::errorindex_message] = 'can not update';
                        return $res;
                    } else {
                        $this->db->trans_complete();
                        $this->db->trans_commit();
                        $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                        $res[AppConst::errorindex_message] = 'Successfully updated';
                        return $res;
                    }
                }
            } else {
                $res[AppConst::errorindex_code] = 410;
                $res[AppConst::errorindex_message] = "Not hydrated today";
                return $res;
            }
        } else {
            $res[AppConst::errorindex_code] = 409;
            $res[AppConst::errorindex_message] = "Can't view";
            return $res;
        }
    }
    public function todaysHydrationRecords($filter = array())
    {
        $check = $this->checkIsHydrated(getval($filter, 'user_id'), getCurrentDate());
        if ($check[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $this->db->trans_begin();
            $hydration = $this->getHydrationId(getval($filter, 'user_id'), getCurrentDate());
            $hydrationId = $hydration[AppConst::res_value]['id'];
            $this->db->select("*");
            $this->db->from(Tbl::table_hydration_lines);
            $this->db->where('hydration_id', $hydrationId);
            $subscriptionList = $this->db->get()->result_array();
            $res[AppConst::errorindex_code] = (isset($subscriptionList) && sizeof($subscriptionList) > 0) ? AppConst::errorcode_success : 8;
            $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
            $res[AppConst::res_value] = $subscriptionList;

            return $res;
        } else {
            $res[AppConst::errorindex_code] = 409;
            $res[AppConst::errorindex_message] = "Can't view";
            return $res;
        }
    }
    public function viewRemiderTime($filter = array())
    {
        $check = $this->checkIsHydrated(getval($filter, 'user_id'), getCurrentDate());
        if ($check[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $this->db->trans_begin();
            $hydration = $this->getHydrationId(getval($filter, 'user_id'), getCurrentDate());
            $hydrationId = $hydration[AppConst::res_value]['id'];
            $this->db->select("reminding_time");
            $this->db->from(Tbl::table_hydration);
            $this->db->where('id', $hydrationId);
            $subscriptionList = $this->db->get()->result_array();
            $res[AppConst::errorindex_code] = (isset($subscriptionList) && sizeof($subscriptionList) > 0) ? AppConst::errorcode_success : 8;
            $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
            $res[AppConst::res_value] = $subscriptionList;

            return $res;
        } else {
            $res[AppConst::errorindex_code] = 409;
            $res[AppConst::errorindex_message] = "Can't view";
            return $res;
        }
    }
    public function editRemiderTime($filter = array())
    {
        $check = $this->checkIsHydrated(getval($filter, 'user_id'), getCurrentDate());
        if ($check[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $hydration = $this->getHydrationId(getval($filter, 'user_id'), getCurrentDate());
            $hydrationId = $hydration[AppConst::res_value]['id'];
            $data_to_update['updated_at'] = getCurrentDateWithTime();
            $data_to_update['reminding_time'] = getval($filter, 'reminding_time');
            $this->db->where('id', $hydrationId);
            $this->db->update(Tbl::table_hydration, $data_to_update);
            $anyUpdate = $this->db->affected_rows() > 0 ? TRUE : FALSE;
            if ($anyUpdate == FALSE) {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'can not update';
                return $res;
            } else {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = 'Successfully updated';
                return $res;
            }
        } else {
            $res[AppConst::errorindex_code] = 409;
            $res[AppConst::errorindex_message] = "Can't view";
            return $res;
        }
    }
    public function getCartDetails($filter = array())
    {
        $check = $this->checkCartId(getval($filter, 'user_id'));
        if ($check[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $cart = $this->getCartId(getval($filter, 'user_id'));
            $cartId = $cart[AppConst::res_value]['cart_id'];
            $this->db->select("*");
            $this->db->from(Tbl::table_cart);
            $this->db->where('cart_id', $cartId);
            $cartDetails = $this->db->get()->result_array();
            $this->db->select('line.*,product.image_url,product.offer_percentage,product.product_name,product.regular_price');
            $this->db->from(Tbl::table_cart_lines . ' as line');
            $this->db->join(Tbl::table_product . ' as product', 'product.id = line.product_id', 'left');
            $this->db->where('line.cart_id', $cartId);
            $cartLineDetails = $this->db->get()->result_array();
            if (count($cartLineDetails) > 0) {
                $cartDetails[0]['delivery_charge'] = 0;
                $cartDetails[0]['sub_total'] = 0;
                for ($i = 0; $i < count($cartLineDetails); $i++) {
                    $cartDetails[0]['sub_total'] =  $cartDetails[0]['sub_total'] + $cartLineDetails[$i]['amount'];
                }
                $cartDetails[0]['total_amount'] = $cartDetails[0]['sub_total'] + $cartDetails[0]['delivery_charge'];
            } else {
                $cartDetails[0]['sub_total'] = 0;
                $cartDetails[0]['delivery_charge'] = 0;
                $cartDetails[0]['total_amount'] =  0;
            }
            $cartDetails[0]['cart_details'] = $cartLineDetails;
            $res[AppConst::errorindex_code] = (isset($cartDetails) && sizeof($cartDetails) > 0) ? AppConst::errorcode_success : 8;
            $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
            $res[AppConst::res_value] = $cartDetails[0];

            return $res;
        } else {
            $res[AppConst::errorindex_code] = 409;
            $res[AppConst::errorindex_message] = "Can't view";
            return $res;
        }
    }
    public function checkOutOrder($filter = array())
    {
        $cartDetails = array();
        $check = $this->checkOrderId(getval($filter, 'user_id'));
        if ($check[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $order = $this->getOrderId(getval($filter, 'user_id'));
            $orderId = $order[AppConst::res_value]['order_id'];
            $this->db->select('*');
            $this->db->from(Tbl::table_order_lines);
            $this->db->where('order_id', $orderId);
            $cartLineDetails = $this->db->get()->result_array();
            if (count($cartLineDetails) > 0) {
                $delivery_charge = 0;
                $sub_total = 0;
                for ($i = 0; $i < count($cartLineDetails); $i++) {
                    $sub_total =  $sub_total + $cartLineDetails[$i]['amount'];
                }
                $total_amount = $sub_total + $delivery_charge;
            } else {
                $delivery_charge = 0;
                $sub_total = 0;
                $total_amount = 0;
            }
            $data_to_update['order_type'] = getval($filter, 'order_type');
            $data_to_update['payment_id'] = getval($filter, 'payment_id');
            $data_to_update['razorpay_order_id'] = getval($filter, 'razorpay_order_id');
            $data_to_update['order_status'] = AppConst::ORDER_CHECKOUT;
            $data_to_update['updated_at'] = getCurrentDateWithTime();
            $data_to_update['sub_total'] = $sub_total;
            $data_to_update['delivery_charge'] = $delivery_charge;
            $data_to_update['total_amount'] = $total_amount;
            $this->db->where('order_id', $orderId);
            $this->db->update(Tbl::table_orders, $data_to_update);
            $anyUpdate = $this->db->affected_rows() > 0 ? TRUE : FALSE;
            if ($anyUpdate == FALSE) {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'can not update';
                return $res;
            } else {
                $cart = $this->getCartId(getval($filter, 'user_id'));
                $cartId = $cart[AppConst::res_value]['cart_id'];
                $this->changeStatus(AppConst::STATUS_BLOCK, $cartId);
                $this->db->select("*");
                $this->db->from(Tbl::table_orders);
                $this->db->where('order_id', $orderId);
                $cartDetails = $this->db->get()->result_array();
            }
            $res[AppConst::errorindex_code] = (isset($cartDetails) && sizeof($cartDetails) > 0) ? AppConst::errorcode_success : 8;
            $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
            $res[AppConst::res_value] = $cartDetails[0];

            return $res;
        } else {
            $res[AppConst::errorindex_code] = 409;
            $res[AppConst::errorindex_message] = "Order is not placed";
            return $res;
        }
    }
    public function addToCart($filter = array())
    {
        $check = $this->checkCartId(getval($filter, 'user_id'));
        if ($check[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $cart = $this->getCartId(getval($filter, 'user_id'));
            $cartId = $cart[AppConst::res_value]['cart_id'];
            $addTocart['cart_id'] = $cartId;
            $addTocart['product_id'] = getval($filter, 'product_id');
            $addTocart['qty'] = getval($filter, 'qty');
            $addTocart['unit_price'] = getval($filter, 'unit_price');
            $isPresent = $this->checkProductExistInCart($addTocart['cart_id'], $addTocart['product_id']);
            if ($isPresent[AppConst::errorindex_code] != AppConst::errorcode_success) {
                if ($addTocart['qty'] > 0) {
                    $addTocart['amount'] = $addTocart['unit_price'] * $addTocart['qty'];
                    $addTocart['created_at'] = getCurrentDateWithTime();
                    $addTocart['updated_at'] = getCurrentDateWithTime();
                    $this->db->insert(Tbl::table_cart_lines, $addTocart);
                    $isAdded = $this->db->insert_id();
                    if ($isAdded <= 0) {
                        $res[AppConst::errorindex_code] = 100;
                        $res[AppConst::errorindex_message] = "user insertion failed.. ";
                        return $res;
                    } else {
                        $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                        $res[AppConst::errorindex_message] = "Successfully added";
                        return $res;
                    }
                } else {
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = "Successfully added";
                    return $res;
                }
            } else {
                // $this->db->set('qty', 'qty + ' . (int) $addTocart['qty'], FALSE);
                // $this->db->set('amount', 'amount + ' . (int)($addTocart['qty'] * $addTocart['unit_price']), FALSE);
                if ($addTocart['qty'] > 0) {
                    $this->db->set('qty', (int) $addTocart['qty']);
                    $this->db->set('amount', (int)($addTocart['qty'] * $addTocart['unit_price']));
                    $this->db->set('updated_at', getCurrentDateWithTime());
                    $this->db->where('cart_id', $addTocart['cart_id']);
                    $this->db->where('product_id', $addTocart['product_id']);
                    $this->db->update(Tbl::table_cart_lines);
                    $isUpdate = ($this->db->affected_rows() > 0) ? true : false;
                    if ($isUpdate <= 0) {
                        $res[AppConst::errorindex_code] = 100;
                        $res[AppConst::errorindex_message] = "user insertion failed.. ";
                        return $res;
                    } else {
                        $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                        $res[AppConst::errorindex_message] = "Successfully added";
                        return $res;
                    }
                } else {
                    $this->db->where('cart_id', $cartId);
                    $this->db->where('product_id', $addTocart['product_id']);
                    $this->db->delete(Tbl::table_cart_lines);
                    $subid = ($this->db->affected_rows() > 0) ? true : false;
                    if ($subid == false) {
                        $res[AppConst::errorindex_code] = 200;
                        $res[AppConst::errorindex_message] = "can not delete.. ";
                        return $res;
                    } else {
                        $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                        $res[AppConst::errorindex_message] = "Removed successfully";
                        return $res;
                    }
                }
            }
        } else {
            $res[AppConst::errorindex_code] = 409;
            $res[AppConst::errorindex_message] = "Can't view";
            return $res;
        }
    }

    public function incrementProductQtyCart($filter = array())
    {
        $check = $this->checkCartId(getval($filter, 'user_id'));
        if ($check[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $cart = $this->getCartId(getval($filter, 'user_id'));
            $cartId = $cart[AppConst::res_value]['cart_id'];
            $addTocart['cart_id'] = $cartId;
            $addTocart['product_id'] = getval($filter, 'product_id');
            $addTocart['qty'] = getval($filter, 'qty');
            $addTocart['unit_price'] = getval($filter, 'unit_price');
            $isPresent = $this->checkProductExistInCart($addTocart['cart_id'], $addTocart['product_id']);
            if ($isPresent[AppConst::errorindex_code] != AppConst::errorcode_success) {
                $addTocart['amount'] = $addTocart['unit_price'] * $addTocart['qty'];
                $addTocart['created_at'] = getCurrentDateWithTime();
                $addTocart['updated_at'] = getCurrentDateWithTime();
                $this->db->insert(Tbl::table_cart_lines, $addTocart);
                $isAdded = $this->db->insert_id();
                if ($isAdded <= 0) {
                    $res[AppConst::errorindex_code] = 100;
                    $res[AppConst::errorindex_message] = "user insertion failed.. ";
                    return $res;
                } else {
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = "Successfully added";
                    return $res;
                }
            } else {
                $this->db->set('qty', 'qty+1', FALSE);
                $this->db->set('amount', 'amount + ' . (int)($addTocart['unit_price']), FALSE);
                $this->db->set('updated_at', getCurrentDateWithTime());
                $this->db->where('cart_id', $addTocart['cart_id']);
                $this->db->where('product_id', $addTocart['product_id']);
                $this->db->update(Tbl::table_cart_lines);
                $isUpdate = ($this->db->affected_rows() > 0) ? true : false;
                if ($isUpdate <= 0) {
                    $res[AppConst::errorindex_code] = 100;
                    $res[AppConst::errorindex_message] = "user insertion failed.. ";
                    return $res;
                } else {
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = "Successfully added";
                    return $res;
                }
            }
        } else {
            $res[AppConst::errorindex_code] = 409;
            $res[AppConst::errorindex_message] = "Can't view";
            return $res;
        }
    }

    public function decrementProductQtyCart($filter = array())
    {
        $check = $this->checkCartId(getval($filter, 'user_id'));
        if ($check[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $cart = $this->getCartId(getval($filter, 'user_id'));
            $cartId = $cart[AppConst::res_value]['cart_id'];
            $addTocart['cart_id'] = $cartId;
            $addTocart['product_id'] = getval($filter, 'product_id');
            $addTocart['qty'] = getval($filter, 'qty');
            $addTocart['unit_price'] = getval($filter, 'unit_price');
            $isPresent = $this->checkProductExistInCart($addTocart['cart_id'], $addTocart['product_id']);
            if ($isPresent[AppConst::errorindex_code] != AppConst::errorcode_success) {
                $addTocart['amount'] = $addTocart['unit_price'] * $addTocart['qty'];
                $addTocart['created_at'] = getCurrentDateWithTime();
                $addTocart['updated_at'] = getCurrentDateWithTime();
                $this->db->insert(Tbl::table_cart_lines, $addTocart);
                $isAdded = $this->db->insert_id();
                if ($isAdded <= 0) {
                    $res[AppConst::errorindex_code] = 100;
                    $res[AppConst::errorindex_message] = "user insertion failed.. ";
                    return $res;
                } else {
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = "Successfully added";
                    return $res;
                }
            } else {
                $this->db->set('qty', 'qty-1', FALSE);
                $this->db->set('amount', 'amount - ' . (int)($addTocart['unit_price']), FALSE);
                $this->db->set('updated_at', getCurrentDateWithTime());
                $this->db->where('cart_id', $addTocart['cart_id']);
                $this->db->where('product_id', $addTocart['product_id']);
                $this->db->update(Tbl::table_cart_lines);
                $isUpdate = ($this->db->affected_rows() > 0) ? true : false;
                if ($isUpdate <= 0) {
                    $res[AppConst::errorindex_code] = 100;
                    $res[AppConst::errorindex_message] = "user insertion failed.. ";
                    return $res;
                } else {
                    $isQty = $this->checkQuantityIsValid($addTocart['cart_id']);
                    if ($isQty[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $this->db->where('cart_id', $addTocart['cart_id']);
                        $this->db->where('qty<', 1);
                        $this->db->delete(Tbl::table_cart_lines);
                        $subid = ($this->db->affected_rows() > 0) ? true : false;
                        if ($subid == false) {
                            $res[AppConst::errorindex_code] = 200;
                            $res[AppConst::errorindex_message] = "can not delete.. ";
                            return $res;
                        } else {
                            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                            $res[AppConst::errorindex_message] = "Removed added";
                            return $res;
                        }
                    } else {
                        $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                        $res[AppConst::errorindex_message] = "Removed added";
                        return $res;
                    }
                }
            }
        } else {
            $res[AppConst::errorindex_code] = 409;
            $res[AppConst::errorindex_message] = "Can't view";
            return $res;
        }
    }


    public function placeOrder($filter = array())
    {
        $check = $this->checkCartId(getval($filter, 'user_id'));
        if ($check[AppConst::errorindex_code] == AppConst::errorcode_success) {
            $cart = $this->getCartId(getval($filter, 'user_id'));
            $cartId = $cart[AppConst::res_value]['cart_id'];
            $this->db->select('value');
            $this->db->from(Tbl::table_settings);
            $this->db->where('key', 1);
            $settings = $this->db->get()->result_array();
            $this->db->trans_begin();
            $order['user_id'] = getval($filter, 'user_id');
            $order['order_id'] = $settings[0]['value'];
            $order['created_at'] = getCurrentDateWithTime();
            $order['updated_at'] = getCurrentDateWithTime();
            $order['order_status'] = AppConst::ORDER_PLACED;
            $this->db->insert(Tbl::table_orders, $order);
            $Id = $this->db->insert_id();
            if ($Id <= 0) {
                $this->db->trans_complete();
                $this->db->trans_rollback();
                $res[AppConst::errorindex_code] = 100;
                $res[AppConst::errorindex_message] = "user insertion failed.. ";
                return $res;
            } else {
                $this->db->select("product_id,unit_price,qty,amount");
                $this->db->from(Tbl::table_cart_lines);
                $this->db->where('cart_id', $cartId);
                $cartList = $this->db->get()->result_array();
                if (count($cartList) > 0) {
                    $i = 0;
                    foreach ($cartList as $List) {
                        $orderLines[$i]['order_id'] = $order['order_id'];
                        $orderLines[$i]['product_id'] = getval($List, 'product_id');
                        $orderLines[$i]['unit_price'] = getval($List, 'unit_price');
                        $orderLines[$i]['qty'] = getval($List, 'qty');
                        $orderLines[$i]['amount'] = getval($List, 'amount');
                        $orderLines[$i]['created_at'] = getCurrentDateWithTime();
                        $orderLines[$i]['updated_at'] = getCurrentDateWithTime();
                        $i++;
                    }
                    $this->db->insert_batch(Tbl::table_order_lines, $orderLines);
                    $chatTable = $this->db->affected_rows() > 0 ? true : false;
                    if ($chatTable <= 0) {
                        $this->db->trans_complete();
                        $this->db->trans_rollback();
                        $res[AppConst::errorindex_code] = 100;
                        $res[AppConst::errorindex_message] = "user insertion failed.. ";
                        return $res;
                    } else {
                        $this->db->set('value', 'value+1', FALSE);
                        $this->db->where('key', 1);
                        $this->db->update(Tbl::table_settings);
                        $isUpdate = ($this->db->affected_rows() > 0) ? true : false;
                        if ($isUpdate == FALSE) {
                            $this->db->trans_complete();
                            $this->db->trans_rollback();
                            $res[AppConst::errorindex_code] = 300;
                            $res[AppConst::errorindex_message] = "can not update subscription count.. ";
                            return $res;
                        } else {
                            $this->db->trans_complete();
                            $this->db->trans_commit();
                            $address = $this->viewUserAddress(getval($filter, 'user_id'));
                            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                            $res[AppConst::errorindex_message] = "Successfully placed order";
                            return $res;
                        }
                    }
                } else {
                    $res[AppConst::errorindex_code] = 409;
                    $res[AppConst::errorindex_message] = "No products in the cart";
                    return $res;
                }
            }
        } else {
            $res[AppConst::errorindex_code] = 409;
            $res[AppConst::errorindex_message] = "Can't view";
            return $res;
        }
    }
    private function changeStatus($status, $cartId)
    {
        $data_to_update['status'] = $status;
        $this->db->where('cart_id', $cartId);
        $this->db->update(Tbl::table_cart, $data_to_update);
        $isUpdate = ($this->db->affected_rows() > 0) ? true : false;
        if ($isUpdate == FALSE) {
            $res[AppConst::errorindex_code] = 300;
            $res[AppConst::errorindex_message] = "can not update ";
            return $res;
        } else {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            $res[AppConst::errorindex_message] = "Successfully placed order";
            return $res;
        }
    }

    private function viewUserAddress($userId)
    {
        $this->db->where('user_id', $userId);
        $query = $this->db->get(Tbl::table_user_address);
        if ($query->num_rows() > 0) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            return $res;
        } else {
            $this->db->select('user_id,first_name as fullname,contact_no as phone,address as  street_address,country,state,place as landmark');
            $this->db->from(Tbl::table_user);
            $this->db->where('user_id', $userId);
            $List = $this->db->get()->result_array();
            $List[0]['created_at'] = getCurrentDateWithTime();
            $List[0]['updated_at'] = getCurrentDateWithTime();
            $this->db->insert(Tbl::table_user_address, $List[0]);
            $Id = $this->db->insert_id();
            if ($Id <= 0) {
                $res[AppConst::errorindex_code] = 100;
                $res[AppConst::errorindex_message] = "Address insertion failed.. ";
                return $res;
            } else {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = "Address insertion sucess";
                return $res;
            }
        }
    }
    public function getUserAddress($filter = array())
    {
        $this->viewUserAddress(getval($filter, 'user_id'));
        $this->db->select('*');
        $this->db->from(Tbl::table_user_address);
        if (isset($filter['user_id']) && intval(getval($filter, 'user_id')) > 0) {
            $this->db->where('user_id', getval($filter, 'user_id'));
        }
        $subscriptionList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($subscriptionList) && sizeof($subscriptionList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
        $res[AppConst::res_value] = $subscriptionList;

        return $res;
    }

    public function getMyOrders($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_orders);
        $this->db->where('user_id', getval($filter, 'user_id'));
        $this->db->where('order_status', AppConst::ORDER_CHECKOUT);
        $ordersList = $this->db->get()->result_array();
        if (count($ordersList) > 0) {
            for ($i = 0; $i < count($ordersList); $i++) {
                $this->db->select('*');
                $this->db->from(Tbl::table_order_lines);
                $this->db->where('order_id', getval($ordersList[$i], 'order_id'));
                $ordersLineList = $this->db->get()->result_array();
                $ordersList[$i]['order_details'] = $ordersLineList;
            }
        }
        $res[AppConst::errorindex_code] = (isset($ordersList) && sizeof($ordersList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
        $res[AppConst::res_value] = $ordersList;

        return $res;
    }

    public function updateDeliveryAddress($filter = array())
    {
        $temp = "";
        $userId = getval($filter, 'user_id');
        if ($userId == null) {
            $res[AppConst::errorindex_code] = 410;
            $res[AppConst::errorindex_message] = 'User is missing';
            return $res;
        }
        $temp = getval($filter, 'fullname');
        if ($temp != "") {
            $dataToDb['fullname'] = $temp;
        }
        $temp = getval($filter, 'phone');
        if ($temp != "") {
            $dataToDb['phone'] = $temp;
        }
        $temp = getval($filter, 'pincode');
        if ($temp != "") {
            $dataToDb['pincode'] = $temp;
        }
        $temp = getval($filter, 'district');
        if ($temp != "") {
            $dataToDb['district'] = $temp;
        }
        $temp = getval($filter, 'state');
        if ($temp != "") {
            $dataToDb['state'] = $temp;
        }
        $temp =  getval($filter, 'country');
        if ($temp != "") {
            $dataToDb['country'] = $temp;
        }
        $temp =  getval($filter, 'landmark');
        if ($temp != "") {
            $dataToDb['landmark'] = $temp;
        }
        $temp =  getval($filter, 'building_name');
        if ($temp != "") {
            $dataToDb['building_name'] = $temp;
        }
        $temp =  getval($filter, 'street_address');
        if ($temp != "") {
            $dataToDb['street_address'] = $temp;
        }
        $temp =  getval($filter, 'city');
        if ($temp != "") {
            $dataToDb['city'] = $temp;
        }
        $dataToDb['updated_at'] =  getCurrentDateWithTime();
        $this->db->where('user_id', $userId);
        $this->db->update(Tbl::table_user_address, $dataToDb);
        $isUpdate = ($this->db->affected_rows() > 0) ? true : false;
        if ($isUpdate == FALSE) {
            $res[AppConst::errorindex_code] = 300;
            $res[AppConst::errorindex_message] = "can not update";
            return $res;
        } else {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            $res[AppConst::errorindex_message] = "Successfully updated";
            return $res;
        }
    }

    private function getCurrentConsumption($hydration_id)
    {
        $this->db->select('cunsumed_water');
        $this->db->from(Tbl::table_hydration);
        $this->db->where('id', $hydration_id);
        $List = $this->db->get()->result_array();
        return $List[0]['cunsumed_water'];
    }
    public function getProductDetail($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_product);
        if (isset($filter['id']) && intval(getval($filter, 'id')) > 0) {
            $this->db->where('id', getval($filter, 'id'));
        }
        if (getval($filter, 'country') != "") {
            $this->db->where('country', getval($filter, 'country'));
        }
        $productList = $this->db->get()->result_array();
        $i = 0;
        foreach ($productList as $List) {
            $this->db->select("id,image");
            $this->db->from(Tbl::table_product_images);
            $this->db->where('product_id', getval($List, 'id'));
            $images = $this->db->get()->result_array();
            $productList[$i]['images'] = $images;
            $i++;
        }
        $res[AppConst::errorindex_code] = (isset($productList) && sizeof($productList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
        $res[AppConst::res_value] = $productList;

        return $res;
    }
    public function getNotifyIdsForDonationDrive($community_id)
    {
        $this->db->select('dev_notify_id');
        $this->db->from(Tbl::table_device_master . ' as device');
        $this->db->join(Tbl::table_community_subscription . ' as subscription', 'subscription.user_id = device.user_id');
        $this->db->where('subscription.community_id', $community_id);
        $subscriptionList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($subscriptionList) && sizeof($subscriptionList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
        $res[AppConst::res_value] = $subscriptionList;
    }
    public function getNotifyIdsForUsers()
    {
        $this->db->select('dev_notify_id');
        $this->db->from(Tbl::table_device_master . ' as device');
        $this->db->join(Tbl::table_user . ' as user', 'user.user_id = device.user_id');
        $this->db->where('user.receive_notification', AppConst::STATUS_ACTIVE);
        $subscriptionList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($subscriptionList) && sizeof($subscriptionList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
        $res[AppConst::res_value] = $subscriptionList;
    }
    public function getAllCountries($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_country);
        $this->db->where('status', AppConst::STATUS_ACTIVE);
        $subscriptionList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($subscriptionList) && sizeof($subscriptionList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
        $res[AppConst::res_value] = $subscriptionList;

        return $res;
    }
    public function getAllFitnessTips($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_fitness_tips);

        if (isset($filter['category_type']) && intval(getval($filter, 'category_type')) > 0) {
            $this->db->where('category_type', getval($filter, 'category_type'));
        }
        $this->db->order_by('created_at', 'desc');
        $fitnessList = $this->db->get()->result_array();
        $i = 0;
        $prefix = "watch?v=";
        foreach ($fitnessList as $dList) {
            if ($dList['vedio_url'] != null) {
                if (strpos($dList['vedio_url'], $prefix) !== false) {
                    $index = strpos($dList['vedio_url'], $prefix) + strlen($prefix);
                    $fitnessList[$i]['vedio_id'] = substr($dList['vedio_url'], $index);
                } else {
                    $fitnessList[$i]['vedio_id'] = '';
                }
            } else {
                $fitnessList[$i]['vedio_id'] = '';
            }
            $i++;
        }
        $res[AppConst::errorindex_code] = (isset($fitnessList) && count($fitnessList) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No Fitness tips found.";
        $res[AppConst::res_value] = $fitnessList;
        return $res;
    }
    public function getAllBanners($filter = array())
    {
        $this->db->select('id,banner_url');
        $this->db->from(Tbl::table_banners);

        if (isset($filter['type']) && intval(getval($filter, 'type')) > 0) {
            $this->db->where('type', getval($filter, 'type'));
        }
        $this->db->order_by('created_at', 'desc');
        $fitnessList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($fitnessList) && count($fitnessList) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No Banners record found.";
        $res[AppConst::res_value] = $fitnessList;
        return $res;
    }
    public function getTodaysHydrationTips($filter = array())
    {
        $this->db->select('id,tip');
        $this->db->from(Tbl::table_hydration_tips);

        if (isset($filter['status']) && intval(getval($filter, 'status')) > 0) {
            $this->db->where('status', getval($filter, 'status'));
        }
        $this->db->order_by('rand()');
        $this->db->limit(1);
        $fitnessList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($fitnessList) && count($fitnessList) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No Banners record found.";
        $res[AppConst::res_value] = $fitnessList;
        return $res;
    }
    public function getAllBlogs($filter = array())
    {
        $this->db->select('blog.*,category.category_name');
        $this->db->from(Tbl::table_blog . ' as blog');
        $this->db->join(Tbl::table_blog_category . ' as category', 'category.id = blog.category', 'left');
        if (isset($filter['blog_id']) && intval(getval($filter, 'blog_id')) > 0) {
            $this->db->where('blog.id', getval($filter, 'blog_id'));
        }
        if (isset($filter['page'])) {
            $this->db->limit(AppConst::APP_PAGINATION_LIMIT, getval($filter, 'page'));
        }
        $this->db->order_by('blog.created_at', 'desc');
        $blogList = $this->db->get()->result_array();
        //$i = 0;
        // foreach ($blogList as $dList) {
        //     $res2 = $this->checkUserAddedBlogAsFavorite(getval($filter, 'user_id'), getval($dList, 'id'));
        //     if ($res2[AppConst::errorindex_code] == AppConst::errorcode_success) {
        //         $blogList[$i]['is_favourite'] = AppConst::STATUS_ACTIVE;
        //     } else {
        //         $blogList[$i]['is_favourite'] = AppConst::STATUS_BLOCK;
        //     }
        //     $i++;
        // }
        $res[AppConst::errorindex_code] = (isset($blogList) && sizeof($blogList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
        $res[AppConst::res_value] = $blogList;

        return $res;
    }
    public function getAllDonationDrive($filter = array())
    {
        $this->db->select('drive.*,drive.id as drive_id,community.community_name,community.community_image_url as community_logo');
        $this->db->from(Tbl::table_donation_drive . ' as drive');
        $this->db->join(Tbl::table_community . ' as community', 'community.community_id = drive.community', 'left');
        if (isset($filter['page'])) {
            $this->db->limit(AppConst::APP_PAGINATION_LIMIT, getval($filter, 'page'));
        }
        // if (isset($filter['id']) && intval(getval($filter, 'id')) > 0) {
        //     $this->db->where('drive.id', getval($filter, 'id'));
        // }
        if (isset($filter['status']) && getval($filter, 'status') != "") {
            $this->db->where('drive.drive_status', getval($filter, 'status'));
        }
        $this->db->where('drive.event_date>=', getCurrentDate());
        $this->db->order_by('drive.created_at', 'desc');
        $doonationList = $this->db->get()->result_array();
        $i = 0;
        foreach ($doonationList as $dList) {
            $res1 = $this->checkUserExistInDrive(getval($filter, 'user_id'), getval($dList, 'drive_id'));
            if ($res1[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $doonationList[$i]['joined_drive'] = AppConst::STATUS_ACTIVE;
            } else {
                $doonationList[$i]['joined_drive'] = AppConst::STATUS_BLOCK;
            }
            // $res2 = $this->checkUserAddedDriveAsFavorite(getval($filter, 'user_id'), getval($dList, 'drive_id'));
            // if ($res2[AppConst::errorindex_code] == AppConst::errorcode_success) {
            //     $doonationList[$i]['is_favourite'] = AppConst::STATUS_ACTIVE;
            // } else {
            //     $doonationList[$i]['is_favourite'] = AppConst::STATUS_BLOCK;
            // }
            $i++;
        }
        $res[AppConst::errorindex_code] = (isset($doonationList) && count($doonationList) > 0) ? AppConst::errorcode_success : 10;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_db_output_success : "No donation drive record found.";
        $res[AppConst::res_value] = $doonationList;
        return $res;
    }
    public function nearbyCommunityMembersList($filter = array())
    {
        $lat = getval($filter, 'latitude');
        $long = getval($filter, 'longitude');
        $community_id = getval($filter, 'community_id');
        $location = null;
        if ($lat != '' && $long != '') {
            $location = 'SELECT *, 3956 * 2 * 
          ASIN(SQRT( POWER(SIN((' . $lat . ' -user.home_location_lat)*pi()/180/2),2)
          +COS(' . $lat . '*pi()/180 )*COS(user.home_location_lat*pi()/180)
          *POWER(SIN((' . $long . '-user.home_location_long)*pi()/180/2),2))) 
          as hdistance  FROM ' . Tbl::table_community_subscription . ' as subscription  LEFT JOIN ' . Tbl::table_user . ' as user ON user.user_id = subscription.user_id WHERE 
          subscription.community_id=' . $community_id . '
          ORDER BY hdistance asc ';
        } else {
            $location = 'SELECT * FROM ' . Tbl::table_community_subscription . ' as subscription  LEFT JOIN ' . Tbl::table_user . ' as user ON user.user_id = subscription.user_id WHERE 
          subscription.community_id=' . $community_id . '
          ORDER BY  subscription.subscription_updated_date desc ';
        }
        $qry = $this->db->query($location);

        $arr = $qry->result_array();

        $res[AppConst::errorindex_code] = (isset($arr) && sizeof($arr) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";
        $res[AppConst::res_value] = $arr;


        return $res;
    }

    public function updateRequestStatus($requestInfo = array())
    {
        $userId = getval($requestInfo, 'user_id');
        $status = getval($requestInfo, 'status');
        $requestId = getval($requestInfo, 'request_id');
        $res = array();
        if (intval($userId) > 0 && intval($requestId) > 0 && intval($status) > 0) {
            $requestUpdate['chat_status'] = $status;
            $requestUpdate['updated_date'] = getCurrentDateWithTime();
            $this->db->where('req_id', $requestId);
            $this->db->where('chat_type', AppConst::CHAT_TYPE_REQUEST);
            $this->db->where('chat_to', $userId);
            $this->db->update(Tbl::table_chat, $requestUpdate);
            $chatUpdate = $this->db->affected_rows() > 0 ? TRUE : FALSE;
            if ($chatUpdate == FALSE) {
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'can not update status';
            } else {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = 'Successfully updated';
                $res[AppConst::res_value] = $requestId;
            }
        } else {
            $res[AppConst::errorindex_code] = 100;
            $res[AppConst::errorindex_message] = 'invalid request';
        }
        return $res;
    }
    

    public function searchCommunity($filter = array())
    {
        $lat = getval($filter, 'latitude');
        $long = getval($filter, 'longitude');
        $community_name = getval($filter, 'community_name');
        $limit = isset($filter['limit']) ? getval($filter, 'limit') : 100;
        $status = isset($filter['status']) ? getval($filter, 'status') : AppConst::STATUS_ACTIVE;
        $dist = isset($filter['dist']) ? getval($filter, 'dist') : 100;
        $location = null;
        if ($community_name != '') {
            $location = 'SELECT *, 3956 * 2 * 
          ASIN(SQRT( POWER(SIN((' . $lat . ' - community_lat)*pi()/180/2),2)
          +COS(' . $lat . '*pi()/180 )*COS(community_lat*pi()/180)
          *POWER(SIN((' . $long . '-community_long)*pi()/180/2),2))) 
          as wdistance  FROM ' . Tbl::table_community . ' WHERE 
          community_long between (' . $long . '-' . $dist . '/cos(radians(' . $lat . '))*69) 
          and (' . $long . '+' . $dist . '/cos(radians(' . $lat . '))*69) 
          and community_lat between (' . $lat . '-(' . $dist . '/69)) 
          and (' . $lat . '+(' . $dist . '/69))
          having (wdistance < ' . $dist . ') and community_status = ' . $status . ' and community_name like "%' . $community_name . '%" 
          ORDER BY wdistance limit ' . $limit;
        } else {
            $location = 'SELECT *, 3956 * 2 * 
          ASIN(SQRT( POWER(SIN((' . $lat . ' - community_lat)*pi()/180/2),2)
          +COS(' . $lat . '*pi()/180 )*COS(community_lat*pi()/180)
          *POWER(SIN((' . $long . '-community_long)*pi()/180/2),2))) 
          as wdistance  FROM ' . Tbl::table_community . ' WHERE 
          community_long between (' . $long . '-' . $dist . '/cos(radians(' . $lat . '))*69) 
          and (' . $long . '+' . $dist . '/cos(radians(' . $lat . '))*69) 
          and community_lat between (' . $lat . '-(' . $dist . '/69)) 
          and (' . $lat . '+(' . $dist . '/69))
          having (wdistance < ' . $dist . ') and community_status = ' . $status . ' 
          ORDER BY wdistance limit ' . $limit;
        }

        $qry = $this->db->query($location);

        $arr = $qry->result_array();
        /* for ($i = 0; $i < count($arr); $i++) {
          $user_count = 0;
          $community_id = getval($arr[$i], 'community_id');
          $this->db->from(Tbl::table_community_subscription);
          $this->db->where('community_id', $community_id);
          $user_count = $this->db->count_all_results();
          $arr[$i]['subscription_count'] = $user_count;
          } */
        $res[AppConst::errorindex_code] = (isset($arr) && sizeof($arr) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";

        $res[AppConst::res_value] = $arr;
        $res[AppConst::res_value1] = count($arr);

        return $res;
    }

    public function getRequestList($filter = array())
    {
        $this->db->select('request.*,user.first_name as sender_first_name,user.last_name as sender_last_name');
        $this->db->from(Tbl::table_request . ' as request');
        $this->db->join(Tbl::table_user . ' as user', 'user.user_id = request.user_id', 'left');
        if (intval(getval($filter, 'user_id')) > 0) {
            $this->db->where('request.user_id', getval($filter, 'user_id'));
        }
        if (intval(getval($filter, 'request_id')) > 0) {
            $this->db->where('request.request_id', getval($filter, 'request_id'));
        }
        $this->db->order_by('request.updated_date', 'desc');
        $requestList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($requestList) && sizeof($requestList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No request found.";

        $res[AppConst::res_value] = $requestList;

        return $res;
    }

    public function addSubscriptionToCommunity($subscriptionInfo = array())
    {
        $communityId = intval(getval($subscriptionInfo, 'community_id'));
        $user_id = intval(getval($subscriptionInfo, 'user_id'));
        $res = array();
        if ($user_id > 0 && $communityId > 0) {
            $filter['user_id'] = $user_id;
            $filter['community_id'] = $communityId;
            $subRes = $this->getSubscribedCommunityList($filter);
            if ($subRes[AppConst::errorindex_code] != AppConst::errorcode_success) {
                $this->db->trans_begin();
                $dataToSub['user_id'] = $user_id;
                $dataToSub['community_id'] = $communityId;
                $dataToSub['subscription_added_date'] = getCurrentDateWithTime();
                $dataToSub['subscription_updated_date'] = getCurrentDateWithTime();
                $dataToSub['subscription_status'] = AppConst::STATUS_ACTIVE;
                $this->db->insert(Tbl::table_community_subscription, $dataToSub);
                $subid = $this->db->insert_id();
                if ($subid <= 0) {
                    $this->db->trans_complete();
                    $this->db->trans_rollback();
                    $res[AppConst::errorindex_code] = 200;
                    $res[AppConst::errorindex_message] = "can not join.. ";
                    return $res;
                }

                $this->db->set('subscription_count', 'subscription_count+1', FALSE);
                $this->db->set('community_updated_date', "'" . getCurrentDateWithTime() . "'", FALSE);
                $this->db->where('community_id', $communityId);
                $this->db->update(Tbl::table_community);
                $communityUpdate = ($this->db->affected_rows() > 0) ? true : false;
                if ($communityUpdate == FALSE) {
                    $this->db->trans_complete();
                    $this->db->trans_rollback();
                    $res[AppConst::errorindex_code] = 300;
                    $res[AppConst::errorindex_message] = "can not update subscription count.. ";
                    return $res;
                }

                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = 'successfully joined';
                $res[AppConst::res_value] = $subid;


                $this->db->trans_complete();
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->db->trans_commit();
                } else {
                    $this->db->trans_rollback();
                }
            } else {
                $res[AppConst::errorindex_code] = 300;
                $res[AppConst::errorindex_message] = "already joined to this community ";
            }
        } else {
            $res[AppConst::errorindex_code] = 100;
            $res[AppConst::errorindex_message] = 'invalid request';
        }
        return $res;
    }

    public function leaveFromCommunity($userInfo = array())
    {
        $communityId = intval(getval($userInfo, 'community_id'));
        $user_id = intval(getval($userInfo, 'user_id'));
        $res = array();
        if ($user_id > 0 && $communityId > 0) {
            $this->db->trans_begin();
            $this->db->where('user_id', $user_id);
            $this->db->where('community_id', $communityId);
            $this->db->delete(Tbl::table_community_subscription);
            $subid = ($this->db->affected_rows() > 0) ? true : false;
            if ($subid == false) {
                $this->db->trans_complete();
                $this->db->trans_rollback();
                $res[AppConst::errorindex_code] = 200;
                $res[AppConst::errorindex_message] = "can not leave.. ";
                return $res;
            }
            $this->db->set('subscription_count', 'subscription_count-1', FALSE);
            $this->db->set('community_updated_date', "'" . getCurrentDateWithTime() . "'", FALSE);
            $this->db->where('community_id', $communityId);
            $this->db->update(Tbl::table_community);
            $communityUpdate = ($this->db->affected_rows() > 0) ? true : false;
            if ($communityUpdate == FALSE) {
                $this->db->trans_complete();
                $this->db->trans_rollback();
                $res[AppConst::errorindex_code] = 300;
                $res[AppConst::errorindex_message] = "can not update subscription count.. ";
                return $res;
            }
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            $res[AppConst::errorindex_message] = 'successfully updated';
            $res[AppConst::res_value] = $communityId;
            $this->db->trans_complete();
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $this->db->trans_commit();
            } else {
                $this->db->trans_rollback();
            }
        } else {
            $res[AppConst::errorindex_code] = 100;
            $res[AppConst::errorindex_message] = 'invalid request';
        }
        return $res;
    }

    public function getDonationHistory($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_donation_history . ' as donation');
        $this->db->join(Tbl::table_user . ' as user', 'user.user_id = donation.user_id', 'left');

        if (intval(getval($filter, 'user_id')) > 0) {
            $this->db->where('donation.user_id', getval($filter, 'user_id'));
        }
        if (getval($filter, 'donated_date') != '') {
            $this->db->where('donation.donated_date', getval($filter, 'donated_date'));
        }
        $this->db->order_by('donation.donated_date', 'desc');
        $donationlist = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($donationlist) && sizeof($donationlist) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No donation history found.";

        $res[AppConst::res_value] = $donationlist;

        return $res;
    }

    public function getRequestNotifiedUsers($filter = array())
    {
        $this->db->select('*,request.place as hospital_address');
        $this->db->from(Tbl::table_chat . ' as chat');
        $this->db->join(Tbl::table_request . ' as request', 'request.request_id = chat.req_id', 'left');
        $this->db->join(Tbl::table_user . ' as user', 'user.user_id = chat.chat_to', 'left');

        if (intval(getval($filter, 'from_id')) > 0) {
            $this->db->where('chat.chat_from', getval($filter, 'from_id'));
        }
        if (intval(getval($filter, 'request_id')) > 0) {
            $this->db->where('chat.req_id', getval($filter, 'request_id'));
        }
        if (intval(getval($filter, 'chat_type')) > 0) {
            $this->db->where('chat.chat_type', getval($filter, 'chat_type'));
        }
        if (intval(getval($filter, 'chat_status')) > 0) {
            $this->db->where('chat.chat_status', getval($filter, 'chat_status'));
        }
        $this->db->order_by('chat.chat_status', 'desc');
        $this->db->order_by('chat.updated_date', 'desc');
        $notifiedlist = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($notifiedlist) && sizeof($notifiedlist) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No notification found.";

        $res[AppConst::res_value] = $notifiedlist;

        return $res;
    }

    public function getChatInfo($filter = array())
    {

        $this->db->select('request.*,from_user.first_name as sender_first_name,from_user.last_name as sender_last_name ,to_user.first_name as reciever_first_name,to_user.last_name as reciever_last_name,from_user.home_location_lat AS sender_location_lat,from_user.home_location_long AS sender_location_long,to_user.home_location_lat AS reciever_location_lat,to_user.home_location_long AS reciever_location_long');
        $this->db->from(Tbl::table_chat . ' as chat');
        $this->db->join(Tbl::table_request . ' as request', 'request.request_id = chat.req_id', 'left');
        $this->db->join(Tbl::table_user . ' as from_user', 'from_user.user_id = chat.chat_from', 'left');
        $this->db->join(Tbl::table_user . ' as to_user', 'to_user.user_id = chat.chat_to', 'left');

        if (intval(getval($filter, 'request_id')) > 0) {
            $this->db->where('chat.req_id', getval($filter, 'request_id'));
        }
        if (intval(getval($filter, 'chat_type')) > 0) {
            $this->db->where('chat.chat_type', getval($filter, 'chat_type'));
        }
        if (intval(getval($filter, 'from_id')) > 0) {
            $this->db->where("(chat.chat_from = '" . getval($filter, 'from_id') . "' || chat.chat_to = '" . getval($filter, 'from_id') . "')");
        }
        $this->db->order_by('chat.updated_date', 'desc');
        $notifiedlist = $this->db->get()->result_array();
        //print_r($this->db->last_query());die();
        $res[AppConst::errorindex_code] = (isset($notifiedlist) && sizeof($notifiedlist) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";

        $res[AppConst::res_value] = $notifiedlist;

        return $res;
    }

    public function getChatSummary($filter = array())
    {
        $res = array();
        if (getval($filter, 'from_id') > 0 && getval($filter, 'to_id') > 0) {
            $senderFilter['user_id'] = getval($filter, 'from_id');
            $senderRes = $this->userProfile($senderFilter);
            if ($senderRes[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $reciverFilter['user_id'] = getval($filter, 'to_id');
                $reciverRes = $this->userProfile($reciverFilter);
                if ($reciverRes[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $chatSummary['sender_first_name'] = $senderRes[AppConst::res_value]['first_name'];
                    $chatSummary['sender_last_name'] = $senderRes[AppConst::res_value]['last_name'];
                    $chatSummary['reciever_first_name'] = $reciverRes[AppConst::res_value]['first_name'];
                    $chatSummary['reciever_last_name'] = $reciverRes[AppConst::res_value]['last_name'];
                    $chatSummary['sender_location_lat'] = $senderRes[AppConst::res_value]['home_location_lat'];
                    $chatSummary['sender_location_long'] = $senderRes[AppConst::res_value]['home_location_long'];
                    $chatSummary['reciever_location_lat'] = $reciverRes[AppConst::res_value]['home_location_lat'];
                    $chatSummary['reciever_location_long'] = $reciverRes[AppConst::res_value]['home_location_long'];
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::errorindex_message] = AppConst::errormessage_success;
                    $res[AppConst::res_value] = $chatSummary;
                } else {
                    $res[AppConst::errorindex_code] = 100;
                    $res[AppConst::errorindex_message] = 'invalid request';
                }
            } else {
                $res[AppConst::errorindex_code] = 200;
                $res[AppConst::errorindex_message] = 'invalid request';
            }
        } else {
            $res[AppConst::errorindex_code] = 300;
            $res[AppConst::errorindex_message] = 'invalid request';
        }
        return $res;
    }

    public function getCommunityMemberesList($filter = array())
    {
        $this->db->select('*');
        $this->db->from(Tbl::table_community_subscription . ' as subscription');
        $this->db->join(Tbl::table_user . ' as user', 'user.user_id = subscription.user_id', 'left');

        if (intval(getval($filter, 'user_id')) > 0) {
            $this->db->where('subscription.user_id', getval($filter, 'user_id'));
        }
        if (getval($filter, 'name') != "") {
            $this->db->like('user.first_name', getval($filter, 'name'));
        }
        if (intval(getval($filter, 'blood_group')) > 0) {
            $this->db->where('user.blood_group', getval($filter, 'blood_group'));
        }
        if (intval(getval($filter, 'status')) > 0) {
            $this->db->where('community.community_status', getval($filter, 'status'));
        }
        if (intval(getval($filter, 'community_id')) > 0) {
            $this->db->where('subscription.community_id', getval($filter, 'community_id'));
        }
        $this->db->order_by('subscription.subscription_updated_date', 'desc');
        $subscriptionList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($subscriptionList) && sizeof($subscriptionList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";

        $res[AppConst::res_value] = $subscriptionList;

        return $res;
    }

    public function closeRequest($requestInfo)
    {
        $requestId = getval($requestInfo, 'request_id');
        $res = array();
        if ($requestId > 0) {
            $dataToUpdate['updated_date'] = getCurrentDateWithTime();
            $dataToUpdate['status'] = AppConst::STATUS_CLOSED;
            $this->db->where('request_id', $requestId);
            $this->db->update(Tbl::table_request, $dataToUpdate);
            $requestUpdate = ($this->db->affected_rows() > 0) ? true : false;
            if ($requestUpdate == false) {
                $res[AppConst::errorindex_code] = 200;
                $res[AppConst::errorindex_message] = "can not close request.. ";
            } else {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = 'successfully request closed';
                $res[AppConst::res_value] = $requestId;
            }
        } else {
            $res[AppConst::errorindex_code] = 100;
            $res[AppConst::errorindex_message] = 'request id is missing';
        }
        return $res;
    }

    public function getRecentChat($filter = array())
    {
        $query = 'SELECT * FROM '
            . '(SELECT chat_id,chat_room_id,from_user.first_name AS sender_first_name,from_user.last_name AS sender_last_name,from_user.pro_image AS sender_image,from_user.blood_group AS sender_blood_group,to_user.first_name AS reciever_first_name,to_user.last_name AS reciever_last_name,to_user.pro_image AS reciever_image,to_user.blood_group AS reciever_blood_group,chat.chat_msg,chat.time,chat.chat_from,chat.chat_to  FROM ' . Tbl::table_chat . '  AS `chat` LEFT JOIN ' . Tbl::table_user . ' AS `from_user` ON `from_user`.`user_id` = `chat`.`chat_from`  LEFT JOIN  ' . Tbl::table_user . '  AS `to_user` ON `to_user`.`user_id` = `chat`.`chat_to` WHERE chat.time IN( SELECT MAX(time) FROM ' . Tbl::table_chat . ' GROUP BY chat_room_id ) and `chat`.`chat_type` = ' . $filter['chat_type'] . ' and (`chat`.`chat_from` = ' . $filter['from_id'] . ' OR `chat`.`chat_to` = ' . $filter['from_id'] . ') ORDER BY `chat`.`time` desc LIMIT ' . $filter['limit'] . ' ) AS t1 '
            . ' LEFT JOIN '
            . ' (SELECT COUNT(*) AS unread_message_count,chat_room_id FROM ' . Tbl::table_chat . '  AS `chat`  WHERE  `chat`.`chat_to` = ' . $filter['from_id'] . '  AND `chat`.`chat_type` = ' . $filter['chat_type'] . '  AND `chat`.`chat_read_status` != ' . AppConst::CHAT_READ_STATUS . ' GROUP BY `chat`.`chat_room_id`  LIMIT ' . $filter['limit'] . ' ) AS t2 '
            . ' ON t1.chat_room_id = t2.chat_room_id ';

        $qry = $this->db->query($query);

        $chatList = $qry->result_array();

        $res[AppConst::errorindex_code] = (isset($chatList) && sizeof($chatList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";

        $res[AppConst::res_value] = $chatList;

        return $res;
    }

    public function updateChatReadStatus($chatInfo)
    {
        if (is_array($chatInfo) && count($chatInfo) > 0) {
            $dataToUpdate['chat_read_status'] = AppConst::CHAT_READ_STATUS;
            $dataToUpdate['updated_date'] = getCurrentDateWithTime();

            $this->db->where_in('chat_id', $chatInfo);
            $this->db->update(Tbl::table_chat, $dataToUpdate);
            $chatUpdate = ($this->db->affected_rows() > 0) ? true : false;
            if ($chatUpdate == false) {
                $res[AppConst::errorindex_code] = 200;
                $res[AppConst::errorindex_message] = "can not update chat read status.. ";
                return $res;
            } else {
                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = 'successfully read status updated';
                $res[AppConst::res_value] = $chatUpdate;
                return $res;
            }
        } else {
            $res[AppConst::errorindex_code] = 100;
            $res[AppConst::errorindex_message] = 'chat id is missing';
            return $res;
        }
    }

    public function getUnreadChatCount($filter = array())
    {
        $this->db->select('count(*) as unread_message_count');
        $this->db->from(Tbl::table_chat . ' as chat');
        $this->db->where('chat.chat_to', getval($filter, 'user_id'));
        $this->db->where('chat.chat_type', AppConst::CHAT_TYPE_NORMAL);
        $this->db->where('chat.chat_read_status !=', AppConst::CHAT_READ_STATUS);
        $this->db->order_by('chat.time', 'desc');
        $chatList = $this->db->get()->result_array();
        $res[AppConst::errorindex_code] = (isset($chatList) && sizeof($chatList) > 0) ? AppConst::errorcode_success : 8;
        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_code] == AppConst::errorcode_success ? AppConst::errormessage_success : "No record found.";

        $res[AppConst::res_value] = $chatList;

        return $res;
    }

    public function addDonationHistory($dataToDb)
    {
        $errorMessage = null;
        $donationInfo = array();
        $temp = getval($dataToDb, 'patient_name');
        if ($temp != '') {
            $donationInfo['patient_name'] = $temp;

            $temp = getval($dataToDb, 'hospital_address');
            if ($temp != '') {
                $donationInfo['hospital_address'] = $temp;
                $temp = getval($dataToDb, 'donated_date');
                if ($temp != '') {
                    $donationInfo['donated_date'] = uiDate2DBdate($temp, FALSE);
                    $temp = getval($dataToDb, 'source');
                    if ($temp != '') {
                        $donationInfo['source'] = $temp;
                        $temp = getval($dataToDb, 'donor_id');
                        if ($temp != '') {

                            $donationInfo['user_id'] = $temp;
                            $donationInfo['hospital_name'] = getval($dataToDb, 'hospital_name');
                            $donationInfo['patient_address'] = getval($dataToDb, 'patient_address');
                            $donationInfo['donation_updated_date'] = getCurrentDateWithTime();
                        } else {
                            $errorMessage = "donor id is missing";
                        }
                    } else {
                        $errorMessage = "Source is missing";
                    }
                } else {
                    $errorMessage = "Donated date is missing";
                }
            } else {
                $errorMessage = "Hospital address is missing";
            }
        } else {
            $errorMessage = "Patient name is missing";
        }
        if ($errorMessage == null || trim($errorMessage) == '') {

            $filter['user_id'] = getval($donationInfo, 'user_id');
            $filter['donated_date'] = getval($donationInfo, 'donated_date');
            $donationRes = $this->getDonationHistory($filter);
            if ($donationRes[AppConst::errorindex_code] != AppConst::errorcode_success) {
                $this->db->trans_begin();
                $donationInfo['donation_added_date'] = getCurrentDateWithTime();
                $donationInfo['donation_status'] = AppConst::STATUS_ACTIVE;
                $this->db->insert(Tbl::table_donation_history, $donationInfo);
                $historyId = $this->db->insert_id();
                if ($historyId <= 0) {
                    $this->db->trans_complete();
                    $this->db->trans_rollback();
                    $res[AppConst::errorindex_code] = 300;
                    $res[AppConst::errorindex_message] = 'can not add donation history..';
                    return $res;
                }

                $userUpdate['last_date_donation'] = getval($donationInfo, 'donated_date');
                $userUpdate['updated_date'] = getCurrentDateWithTime();
                $this->db->where('user_id', getval($donationInfo, 'user_id'));
                $this->db->update(Tbl::table_user, $userUpdate);
                $updateuserId = $this->db->affected_rows() > 0 ? TRUE : FALSE;

                if ($updateuserId == FALSE) {
                    $this->db->trans_complete();
                    $this->db->trans_rollback();
                    $res[AppConst::errorindex_code] = 200;
                    $res[AppConst::errorindex_message] = 'can not update user profile';
                    return $res;
                }
                if (getval($donationInfo, 'source') == AppConst::SOURCE_BLOOD_LOCATOR_REQUEST_ID) {
                    $requestUpdate['chat_status'] = AppConst::CHAT_STATUS_DONATED;
                    $requestUpdate['updated_date'] = getCurrentDateWithTime();
                    $this->db->where('chat_to', getval($donationInfo, 'user_id'));
                    $this->db->where('req_id', getval($dataToDb, 'request_id'));
                    $this->db->update(Tbl::table_chat, $requestUpdate);
                    $chatId = $this->db->affected_rows() > 0 ? TRUE : FALSE;
                    if ($chatId == FALSE) {
                        $this->db->trans_complete();
                        $this->db->trans_rollback();
                        $res[AppConst::errorindex_code] = 700;
                        $res[AppConst::errorindex_message] = 'can not update request status';
                        return $res;
                    }
                }

                $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                $res[AppConst::errorindex_message] = AppConst::errormessage_db_insertion_success;
                $res[AppConst::res_value] = $historyId;

                $this->db->trans_complete();
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->db->trans_commit();
                } else {
                    $this->db->trans_rollback();
                }
            } else {
                $res[AppConst::errorindex_code] = 500;
                $res[AppConst::errorindex_message] = 'already added in this date..';
            }
        } else {
            $res[AppConst::errorindex_code] = 400;
            $res[AppConst::errorindex_message] = $errorMessage;
        }
        return $res;
    }
}
