<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SuperCtrl extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', "url", 'apputil', 'appdate'));
        $this->load->library(array('AppConst', 'Pages', 'session', 'pagination','form_validation'));
         $this->load->model("AdminModel");
    }

    protected function getAdminSession($ifNoSessionMoveToLogin = true,  $errorMessage = AppConst::errormessage_nosession) {
        $session = $this->session->userdata(AppConst::session_key_user);
        
        if ($session == null) {
            if ($ifNoSessionMoveToLogin == true) {
                $this->login($errorMessage,FALSE);
            } else {
                $this->login("");
            }
        }
        
        return $session;
    }

    protected function isPost($requestinput) {
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            return false;
        } else if ($this->input->server('REQUEST_METHOD') == 'POST') {
            return true;
        }
    }

   

    protected function login($message = AppConst::errormessage_nosession, $isSuccess = false) {
        $this->session->unset_userdata(AppConst::session_key_user);
        //$this->session->sess_destroy();
        $datatopage = array();
        if ($isSuccess && $message != NULL) {
            $datatopage['data'][AppConst::feedbackmessageKey] = '<b><font color="green">' . $message . '</font></b>';
        } else {
            $datatopage['data'][AppConst::feedbackmessageKey] = '<b><font color="red">' . $message . '</font></b>';
        }
        
        $this->load->view(Pages::page_admin_login, $datatopage);
        
    }

    public function setActiveMenu($menuName = null) {
        if ($menuName != null && trim($menuName) != "") {
            $this->session->set_userdata(AppConst::currenPage_KEY, $menuName);
        }
    }

    protected function setFeedbackmessage($message, $issuccess, $key = AppConst::feedbackmessageKey) {
        if ($message != null && trim($message) != "") {
            if ($issuccess == true) {
                $message = "<font color='green'><b>" . $message . '</b></font>';
            } else {
                $message = "<font color='red'><b>" . $message . '</b></font>';
            }
            $this->session->set_userdata($key, $message);
        }
    }

    protected function setSession($profile) {
        $this->session->set_userdata(AppConst::session_key_user, $profile);
        return true;
    }
    

    public function adminPageLoadBy($category, $page, $data = array()) {
        $session = $this->getAdminSession(true);
        if($session != null){
            $data['data']['session'] = $session;
            printv($category . '/pages' . DIRECTORY_SEPARATOR . $category . DIRECTORY_SEPARATOR . $page, 'Path', '#800080');
            $this->load->pageLoadBy($category, 'pages' . DIRECTORY_SEPARATOR . $category . DIRECTORY_SEPARATOR . $page, $data);
        }
    }
    protected function pagination($url = null, $total_count = 0, $limit = AppConst::PAGINATION_LIMIT) {
        $res = array();
        if ($url != null && $total_count > 0 && $limit > 0) {
            $config = array();
            $config["base_url"] = base_url() . $url;
            $config["total_rows"] = $total_count;
            $config["per_page"] = $limit;
            $config['use_page_numbers'] = TRUE;
            $config['num_links'] = $limit;
            $config['cur_tag_open'] = '&nbsp;<a class="current">';
            $config['cur_tag_close'] = '</a>';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Previous';
            $this->pagination->initialize($config);
            $page = $this->uri->segment(3);
            if (($page == NULL) || intval($page) > 0) {
                if (intval($page) > 1) {
                    $page = ($this->uri->segment(3) - 1);
                    $page *= $limit;
                } else {
                    $page = 0;
                }

                $res[AppConst::res_value] = $page;
            }
            $str_links = $this->pagination->create_links();
            $data["links"] = explode('&nbsp;', $str_links);
            $res[AppConst::res_value1] = $data['links'];
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
        } else {
            $res[AppConst::errorindex_code] = 8;
            $res[AppConst::errorindex_message] = "invalid pagination request";
        }
        return $res;
    }
    
    

}
