<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once('SuperCtrl.php');

class AdminCtrl extends SuperCtrl
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('excel');
    }

    public function index()
    {

        $this->login();
    }

    public function adminLogIn()
    {
        if ($this->isPost($this->input)) {
            $this->form_validation->set_rules('password_of', 'Password', 'required');
            $this->form_validation->set_rules('username_of', 'Username', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view(Pages::page_admin_login, $this->input->post());
            } else {
                $username = $this->input->post("username_of");
                $password = $this->input->post("password_of");
                $res = $this->AdminModel->authenticateBy($username, $password, "WEB");
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success && isset($res[AppConst::res_value]['user_id'])) {
                    $isSessionOk = $this->setSession($res[AppConst::res_value]);
                    if ($isSessionOk) {
                        redirect(AppConst::INDEX . 'admin');
                    } else {
                        $this->goToErrorPage("some value missing in login... can not go head.");
                    }
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                    $this->login($res[AppConst::errorindex_message]);
                }
            }
        } else {
            $this->login("");
        }
    }

    public function termsConditions()
    {
        $this->load->view('terms_and_conditions');
    }


    public function adminLogOut()
    {
        if ($this->isPost($this->input)) {
            //$this->session->unset_userdata('USER_SESSION_FLUEUP');
            $this->login("Successfully logged out.", true);
        }
    }

    public function adminChangeProfilePassword()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage['data']['session'] = $session;
            if ($this->isPost($this->input)) {
                $password['oldpassword'] = $this->input->post('oldpassword');
                $password['newpassword'] = $this->input->post('newpassword');
                $password['confirmpassword'] = $this->input->post('confirmpassword');
                $password['user_id'] = $this->input->post('user_id');
                if ($password['newpassword'] != $password['confirmpassword']) {
                    $this->setFeedbackmessage("Password Mismatch", FALSE);
                    redirect('admin/profile?user_id=' . getval($password, 'user_id'));
                } else if ($session['password'] != $password['oldpassword']) {

                    $this->setFeedbackmessage("Invalid old Password", FALSE);
                    redirect('admin/profile?user_id=' . getval($password, 'user_id'));
                } else {
                    $res = $this->AdminModel->changeProfilePassword($password);
                    if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                    } else {
                        $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                    }
                    redirect('admin/profile?user_id=' . getval($password, 'user_id'));
                }
            }
        }
    }

    public function ProfileCtrl()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            if ($this->isPost($this->input)) {
                $dataToModel = $this->input->post();
                $res = $this->AdminModel->updateProfile($dataToModel);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
                redirect('admin/profile?user_id=' . getval($dataToModel, 'user_id'));
            }
            $userID = $this->input->get('user_id');
            $userID = isset($userID) ? intval($userID) : 0;
            if ($userID > 0) {
                $res = $this->AdminModel->getUserById($userID);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataToPage['data']['user-info'] = $res[AppConst::res_value][0];
                }
            } else {
                $this->setFeedbackmessage("Sorry, you are not a valid user", FALSE);
            }
            $this->setActiveMenu(Pages::page_admin_profile);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_profile, $dataToPage);
        }
    }

    public function adminDashBoard()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $res = $this->AdminModel->getDashBoardInfo($filter = array());
            if ($res[AppConst::errorindex_code] == AppConst::errorindex_code) {
                $dataToPage['data']['dashboard'] = $res[AppConst::res_value];
            }
            $this->setActiveMenu(Pages::page_admin_dashboard);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_dashboard, $dataToPage);
        }
    }

    public function registeredUserList()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $filter = $this->input->post();
            }
            $countFilter['user_type'] = AppConst::USER_TYPE_REGISTERED_USER;
            $total_count = $this->AdminModel->getRegisterdcount($countFilter);
            $res = $this->pagination('admin/registered-user-list', $total_count, AppConst::PAGINATION_LIMIT);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['page'] = $res[AppConst::res_value];
                $dataTopage['data']['ROLLNO'] = $res[AppConst::res_value] + 1;
                $dataTopage['data']['links'] = $res[AppConst::res_value1];
                $dataTopage['data']['filter'] = $filter;
                $filter['user_type'] = AppConst::USER_TYPE_REGISTERED_USER;
                $res = $this->AdminModel->getRegisteredUsers($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['user-list'] = $res[AppConst::res_value];
                }
            } else {
                $filter['user_type'] = AppConst::USER_TYPE_REGISTERED_USER;
                $res = $this->AdminModel->getRegisteredUsers($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['user-list'] = $res[AppConst::res_value];
                }
            }
            $dataTopage['data']['filter'] = $filter;
            $this->setActiveMenu(Pages::page_admin_registered_user_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_registered_user_list, $dataTopage);
        }
    }

    public function unRegisteredUserList()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $filter = $this->input->post();
            }
            $countFilter['user_type'] = AppConst::USER_TYPE_UNREGISTERED_USER;
            $total_count = $this->AdminModel->getRegisterdcount($countFilter);

            $res = $this->pagination('admin/unregistered-user-list', $total_count, AppConst::PAGINATION_LIMIT);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['page'] = $res[AppConst::res_value];
                $dataToPage['data']['ROLLNO'] = $res[AppConst::res_value] + 1;
                $dataToPage['data']['links'] = $res[AppConst::res_value1];
                $filter['user_type'] = AppConst::USER_TYPE_UNREGISTERED_USER;
                $res = $this->AdminModel->getRegisteredUsers($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataToPage['data']['user-list'] = $res[AppConst::res_value];
                }
            } else {
                $filter['user_type'] = AppConst::USER_TYPE_UNREGISTERED_USER;
                $res = $this->AdminModel->getRegisteredUsers($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataToPage['data']['user-list'] = $res[AppConst::res_value];
                }
            }

            $dataToPage['data']['filter'] = $filter;
            $this->setActiveMenu(Pages::page_admin_un_registered_user_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_un_registered_user_list, $dataToPage);
        }
    }

    public function addEditRegisteredUser()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            if ($this->isPost($this->input)) {
                $userData = $this->input->post();
                $res = $this->AdminModel->addEditRegisteredUser($userData);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
                redirect('admin/registered-user-list');
            }
            $userId = $this->input->get('user_id');
            $userId = isset($userId) ? intval($userId) : 0;
            if ($userId > 0) {
                $filter['user_id'] = $userId;
                $res = $this->AdminModel->getRegisteredUsers($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataToPage['data']['user-info'] = $res[AppConst::res_value][0];
                }
            }
            $countryFilter['status'] = AppConst::STATUS_ACTIVE;
            $res = $this->AdminModel->getAllCountry($countryFilter);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $dataToPage['data']['country-info'] = $res[AppConst::res_value];
            }
            $this->setActiveMenu(Pages::page_admin_registered_user_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_add_edit_registered_user, $dataToPage);
        }
    }

    public function addEditUnRegisteredUser()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            if ($this->isPost($this->input)) {
                $userData = $this->input->post();
                $res = $this->AdminModel->addEditunRegisteredUser($userData);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
                redirect('admin/unregistered-user-list');
            }
            $userId = $this->input->get('user_id');
            $userId = isset($userId) ? intval($userId) : 0;
            if ($userId > 0) {
                $filter['user_id'] = $userId;
                $res = $this->AdminModel->getRegisteredUsers($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataToPage['data']['user-info'] = $res[AppConst::res_value][0];
                }
            }
            $countryFilter['status'] = AppConst::STATUS_ACTIVE;
            $res = $this->AdminModel->getAllCountry($countryFilter);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $dataToPage['data']['country-info'] = $res[AppConst::res_value];
            }
            $this->setActiveMenu(Pages::page_admin_un_registered_user_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_add_edit_un_registered_user, $dataToPage);
        }
    }

    public function get_states()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $countryId = $this->input->get('country_id');
            $countryId = isset($countryId) ? intval($countryId) : 0;
            if ($countryId > 0) {
                $filter['country_id'] = $countryId;
                $filter['status'] = AppConst::STATUS_ACTIVE;
                $res = $this->AdminModel->getAllState($filter);
            }
        }
        echo json_encode($res);
    }

    public function adminCheckContactNoAlreadyExistOrNot()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            if ($this->isPost($this->input)) {
                $contactno = $this->input->post('contactno');
                $res = $this->AdminModel->checkContactNoAlreadyExistOrNot($contactno);
                echo json_encode($res);
            }
        }
    }

    public function requestList()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $filter = $this->input->post();
            }
            $total_count = $this->AdminModel->getRequestcount();
            $res = $this->pagination('admin/request-list', $total_count, AppConst::PAGINATION_LIMIT);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['page'] = $res[AppConst::res_value];
                $dataTopage['data']['ROLLNO'] = $res[AppConst::res_value] + 1;
                $dataTopage['data']['links'] = $res[AppConst::res_value1];
                $filter['user_type'] = AppConst::USER_TYPE_REGISTERED_USER;
                $res = $this->AdminModel->getAllRequests($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['request-list'] = $res[AppConst::res_value];
                }
            } else {
                $res = $this->AdminModel->getAllRequests($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['request-list'] = $res[AppConst::res_value];
                }
            }

            $dataTopage['data']['filter'] = $filter;
            $this->setActiveMenu(Pages::page_admin_request_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_request_list, $dataTopage);
        }
    }

    public function UpdateUserStatus()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {

            $res = array();
            $userID = $this->input->get('user_id');
            $status = $this->input->get('status');

            if ($userID > 0 && $status != '') {
                $filter['request_id'] = $userID;
                $filter['status'] = $status;
                $filter['updated_date'] = getCurrentDateWithTime();
                $res = $this->AdminModel->blockActiveRequst($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
            }
            echo json_encode($res);
        }
    }

    public function unregisterdUpdateUserStatus()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {

            $res = array();
            $userID = $this->input->get('user_id');
            $status = $this->input->get('status');

            if ($userID > 0 && $status != '') {
                $filter['user_id'] = $userID;
                $filter['status'] = $status;
                $filter['updated_date'] = getCurrentDateWithTime();
                $res = $this->AdminModel->blockActiveUnRegisterdUser($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
            }
            echo json_encode($res);
        }
    }

    public function addEditRequest()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            if ($this->isPost($this->input)) {
                $requestData = $this->input->post();
                $res = $this->AdminModel->addEditRequest($requestData);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
                redirect('admin/request-list');
            }
            $requestId = $this->input->get('request_id');
            $requestId = isset($requestId) ? intval($requestId) : 0;
            if ($requestId > 0) {
                $filter['request_id'] = $requestId;
                $res = $this->AdminModel->getAllRequests($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataToPage['data']['request-info'] = $res[AppConst::res_value][0];
                }
            }
            $this->setActiveMenu(Pages::page_admin_request_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_add_edit_request, $dataToPage);
        }
    }

    public function addEditHospital()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();

            if ($this->isPost($this->input)) {
                $addhospital = $this->input->post();
                $res = $this->AdminModel->addEditHospital($addhospital);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
                redirect('admin/hospital-list');
            }
            $hospitalId = $this->input->get('hospital_id');
            $hospitalId = isset($hospitalId) ? intval($hospitalId) : 0;
            if ($hospitalId > 0) {
                $filter['hospital_id'] = $hospitalId;
                $res = $this->AdminModel->getAllHospital($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataToPage['data']['hospital-info'] = $res[AppConst::res_value][0];
                }
            }
            $countryFilter['status'] = AppConst::STATUS_ACTIVE;
            $res = $this->AdminModel->getAllCountry($countryFilter);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $dataToPage['data']['country-info'] = $res[AppConst::res_value];
            }
            $this->setActiveMenu(Pages::page_admin_hospital_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_add_edit_hospital, $dataToPage);
        }
    }

    public function hospitalList()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $filter = $this->input->post();
            }
            $total_count = $this->AdminModel->getHospitalcount();
            $res = $this->pagination('admin/hospital-list', $total_count, AppConst::PAGINATION_LIMIT);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['page'] = $res[AppConst::res_value];
                $dataTopage['data']['ROLLNO'] = $res[AppConst::res_value] + 1;
                $dataTopage['data']['links'] = $res[AppConst::res_value1];
                $res = $this->AdminModel->getAllHospital($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['hospital-list'] = $res[AppConst::res_value];
                }
            } else {
                $res = $this->AdminModel->getAllHospital($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['hospital-list'] = $res[AppConst::res_value];
                }
            }

            $dataTopage['data']['filter'] = $filter;
            $this->setActiveMenu(Pages::page_admin_hospital_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_hospital_list, $dataTopage);
        }
    }

    public function UpdatehospitalStatus()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {

            $res = array();
            $hospitaID = $this->input->get('hospital_id');
            $status = $this->input->get('status');

            if ($hospitaID > 0 && $status != '') {
                $filter['hospital_id'] = $hospitaID;
                $filter['hospital_status'] = $status;
                $filter['updated_date'] = getCurrentDateWithTime();
                $res = $this->AdminModel->blockActiveHospital($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
            }
            echo json_encode($res);
        }
    }

    public function communityList()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $filter = $this->input->post();
            }
            $total_count = $this->AdminModel->getCommunitycount();
            $res = $this->pagination('admin/community-list', $total_count, AppConst::PAGINATION_LIMIT);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['page'] = $res[AppConst::res_value];
                $dataTopage['data']['ROLLNO'] = $res[AppConst::res_value] + 1;
                $dataTopage['data']['links'] = $res[AppConst::res_value1];
                $res = $this->AdminModel->getAllCommunity($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['community-list'] = $res[AppConst::res_value];
                }
            } else {
                $res = $this->AdminModel->getAllCommunity($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['community-list'] = $res[AppConst::res_value];
                }
            }

            $dataTopage['data']['filter'] = $filter;
            $this->setActiveMenu(Pages::page_admin_community_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_community_list, $dataTopage);
        }
    }

    public function marketplaceCategory()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $filter = $this->input->post();
            }
            $total_count = $this->AdminModel->getCategorycount();
            $res = $this->pagination('admin/marketplace-category', $total_count, AppConst::PAGINATION_LIMIT);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['page'] = $res[AppConst::res_value];
                $dataTopage['data']['ROLLNO'] = $res[AppConst::res_value] + 1;
                $dataTopage['data']['links'] = $res[AppConst::res_value1];
                $res = $this->AdminModel->getAllCategory($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['category-list'] = $res[AppConst::res_value];
                }
            } else {
                $res = $this->AdminModel->getAllCategory($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['category-list'] = $res[AppConst::res_value];
                }
            }

            $dataTopage['data']['filter'] = $filter;
            $this->setActiveMenu(Pages::page_admin_category_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_category_list, $dataTopage);
        }
    }

    public function marketplaceProduct()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $filter = $this->input->post();
            }
            $total_count = $this->AdminModel->getProductcount();
            $res = $this->pagination('admin/marketplace-product', $total_count, AppConst::PAGINATION_LIMIT);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['page'] = $res[AppConst::res_value];
                $dataTopage['data']['ROLLNO'] = $res[AppConst::res_value] + 1;
                $dataTopage['data']['links'] = $res[AppConst::res_value1];
                $res = $this->AdminModel->getAllProduct($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['product-list'] = $res[AppConst::res_value];
                }
            } else {
                $res = $this->AdminModel->getAllProduct($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['product-list'] = $res[AppConst::res_value];
                }
            }

            $dataTopage['data']['filter'] = $filter;
            $this->setActiveMenu(Pages::page_admin_product_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_product_list, $dataTopage);
        }
    }

    public function addEditProduct()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $requestData = $this->input->post();
                $res = $this->AdminModel->addEditProduct($requestData);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
                redirect('admin/marketplace-product');
            }
            $productId = $this->input->get('product_id');
            $productId = isset($productId) ? intval($productId) : 0;
            if ($productId > 0) {
                $filter['id'] = $productId;
                $res = $this->AdminModel->getAllProduct($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataToPage['data']['product-info'] = $res[AppConst::res_value][0];
                }
            }
            $countryFilter['status'] = AppConst::STATUS_ACTIVE;
            $res = $this->AdminModel->getAllCountry($countryFilter);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $dataToPage['data']['country-info'] = $res[AppConst::res_value];
            }
            $categoryFilter['status'] = AppConst::STATUS_ACTIVE;
            $res = $this->AdminModel->getAllCategory($categoryFilter);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $dataToPage['data']['category-info'] = $res[AppConst::res_value];
            }
            $this->setActiveMenu(Pages::page_admin_product_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_add_edit_product, $dataToPage);
        }
    }

    public function productImages()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $filter = $this->input->post();
            }
            $productId = $this->input->get('product_id');
            $productId = isset($productId) ? intval($productId) : 0;
            $filter['product_id'] = $productId;
            $total_count = $this->AdminModel->getProductcount();
            $res = $this->pagination('admin/product-images', $total_count, AppConst::PAGINATION_LIMIT);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['page'] = $res[AppConst::res_value];
                $dataTopage['data']['ROLLNO'] = $res[AppConst::res_value] + 1;
                $dataTopage['data']['links'] = $res[AppConst::res_value1];
                $res = $this->AdminModel->getAllProductImages($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['product-images'] = $res[AppConst::res_value];
                }
            } else {
                $res = $this->AdminModel->getAllProductImages($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['product-images'] = $res[AppConst::res_value];
                }
            }
            $dataTopage['data']['product-id'] = $productId;
            $dataTopage['data']['filter'] = $filter;
            $this->setActiveMenu(Pages::page_admin_product_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_product_images, $dataTopage);
        }
    }

    public function addEditProductImage()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $requestData = $this->input->post();
                $res = $this->AdminModel->addEditProductImage($requestData);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
                redirect('admin/product-images?product_id='.$res['product_id'].'');
            }
            
            $productId = $this->input->get('product_id');
            $productId = isset($productId) ? intval($productId) : 0;
            if ($productId > 0) {
                $filter['product_id'] = $productId;
                $res = $this->AdminModel->getAllProductImages($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataToPage['data']['image-info'] = $res[AppConst::res_value][0];
                }
            }
            $dataToPage['data']['image-info']['product_id'] = $productId;
            $this->setActiveMenu(Pages::page_admin_product_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_add_edit_product_image, $dataToPage);
        }
    }

    public function updateProductStatus()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {

            $res = array();
            $categoryid = $this->input->get('product_id');
            $categoryid = isset($categoryid) ? intval($categoryid) : 0;
            $status = $this->input->get('status');
            $status = isset($status) ? intval($status) : 0;

            if ($categoryid > 0 && $status > 0) {
                $filter['product_id'] = $categoryid;
                $filter['status'] = $status;
                $res = $this->AdminModel->updateProductStatus($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
            }
            echo json_encode($res);
        }
    }

    public function deleteProductImage()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {

            $res = array();
            $imageId = $this->input->get('image_id');
            $imageId = isset($imageId) ? intval($imageId) : 0;

            if ($imageId > 0) {
                $res = $this->AdminModel->deleteProductImage($imageId);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
            }
            echo json_encode($res);
        }
    }

    public function addEditCategory()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            if ($this->isPost($this->input)) {
                $requestData = $this->input->post();
                $res = $this->AdminModel->addEditCategory($requestData);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
                redirect('admin/marketplace-category');
            }
            $categoryId = $this->input->get('category_id');
            $categoryId = isset($categoryId) ? intval($categoryId) : 0;
            if ($categoryId > 0) {
                $filter['id'] = $categoryId;
                $res = $this->AdminModel->getAllCategory($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataToPage['data']['category-info'] = $res[AppConst::res_value][0];
                }
            }
            $this->setActiveMenu(Pages::page_admin_category_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_add_edit_category, $dataToPage);
        }
    }

    public function addEditBlogCategory()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            if ($this->isPost($this->input)) {
                $requestData = $this->input->post();
                $res = $this->AdminModel->addEditBlogCategory($requestData);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
                redirect('admin/blog-category');
            }
            $categoryId = $this->input->get('category_id');
            $categoryId = isset($categoryId) ? intval($categoryId) : 0;
            if ($categoryId > 0) {
                $filter['id'] = $categoryId;
                $res = $this->AdminModel->getAllBlogCategory($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataToPage['data']['blog-category'] = $res[AppConst::res_value][0];
                }
            }
            $this->setActiveMenu(Pages::page_admin_blog_category);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_add_edit_blog_category, $dataToPage);
        }
    }

    public function addEditBanner()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            if ($this->isPost($this->input)) {
                $requestData = $this->input->post();
                $res = $this->AdminModel->addEditBanner($requestData);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
                redirect('admin/banners-list');
            }
            $bannerId = $this->input->get('banner_id');
            $bannerId = isset($bannerId) ? intval($bannerId) : 0;
            if ($bannerId > 0) {
                $filter['id'] = $bannerId;
                $res = $this->AdminModel->getAllBanners($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataToPage['data']['banner-details'] = $res[AppConst::res_value][0];
                }
            }
            $this->setActiveMenu(Pages::page_admin_banners_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_add_edit_banner, $dataToPage);
        }
    }

    public function updateCategoryStatus()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {

            $res = array();
            $categoryid = $this->input->get('category_id');
            $categoryid = isset($categoryid) ? intval($categoryid) : 0;
            $status = $this->input->get('status');
            $status = isset($status) ? intval($status) : 0;

            if ($categoryid > 0 && $status > 0) {
                $filter['category_id'] = $categoryid;
                $filter['status'] = $status;
                $res = $this->AdminModel->updateCategoryStatus($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
            }
            echo json_encode($res);
        }
    }

    public function updateBlogCategoryStatus()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {

            $res = array();
            $categoryid = $this->input->get('category_id');
            $categoryid = isset($categoryid) ? intval($categoryid) : 0;
            $status = $this->input->get('status');
            $status = isset($status) ? intval($status) : 0;

            if ($categoryid > 0 && $status > 0) {
                $filter['category_id'] = $categoryid;
                $filter['status'] = $status;
                $res = $this->AdminModel->updateBlogCategoryStatus($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
            }
            echo json_encode($res);
        }
    }

    public function blogList()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $filter = $this->input->post();
            }
            $total_count = $this->AdminModel->getBlogcount();
            $res = $this->pagination('admin/blog-list', $total_count, AppConst::PAGINATION_LIMIT);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['page'] = $res[AppConst::res_value];
                $dataTopage['data']['ROLLNO'] = $res[AppConst::res_value] + 1;
                $dataTopage['data']['links'] = $res[AppConst::res_value1];
                $res = $this->AdminModel->getAllBlog($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['blog-list'] = $res[AppConst::res_value];
                }
            } else {
                $res = $this->AdminModel->getAllBlog($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['blog-list'] = $res[AppConst::res_value];
                }
            }

            $dataTopage['data']['filter'] = $filter;
            $this->setActiveMenu(Pages::page_admin_blog_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_blog_list, $dataTopage);
        }
    }

    public function addEditBlog()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            if ($this->isPost($this->input)) {
                $requestData = $this->input->post();
                $res = $this->AdminModel->addEditBlog($requestData);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
                redirect('admin/blog-list');
            }
            $blogId = $this->input->get('blog_id');
            $blogId = isset($blogId) ? intval($blogId) : 0;
            if ($blogId > 0) {
                $filter['id'] = $blogId;
                $res = $this->AdminModel->getAllBlog($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataToPage['data']['blog-info'] = $res[AppConst::res_value][0];
                }
            }
            $categoryFilter['status'] = AppConst::STATUS_ACTIVE;
            $res = $this->AdminModel->getAllBlogCategory($categoryFilter);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $dataToPage['data']['category-info'] = $res[AppConst::res_value];
            }
            $this->setActiveMenu(Pages::page_admin_blog_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_add_edit_blog, $dataToPage);
        }
    }
    public function blogCategory()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $filter = $this->input->post();
            }
            $total_count = $this->AdminModel->getBlogCategorycount();
            $res = $this->pagination('admin/blog-category', $total_count, AppConst::PAGINATION_LIMIT);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['page'] = $res[AppConst::res_value];
                $dataTopage['data']['ROLLNO'] = $res[AppConst::res_value] + 1;
                $dataTopage['data']['links'] = $res[AppConst::res_value1];
                $res = $this->AdminModel->getAllBlogCategory($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['blog-category'] = $res[AppConst::res_value];
                }
            } else {
                $res = $this->AdminModel->getAllBlogCategory($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['blog-category'] = $res[AppConst::res_value];
                }
            }

            $dataTopage['data']['filter'] = $filter;
            $this->setActiveMenu(Pages::page_admin_blog_category);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_blog_category, $dataTopage);
        }
    }

    public function addEditFitnessTip()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            if ($this->isPost($this->input)) {
                $requestData = $this->input->post();
                $res = $this->AdminModel->addEditFitnessTip($requestData);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
                redirect('admin/fitness-tips');
            }
            $tip_id = $this->input->get('tip_id');
            $tip_id = isset($tip_id) ? intval($tip_id) : 0;
            if ($tip_id > 0) {
                $filter['id'] = $tip_id;
                $res = $this->AdminModel->getAllFitnessTips($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataToPage['data']['fitness-data'] = $res[AppConst::res_value][0];
                }
            }
            $this->setActiveMenu(Pages::page_admin_fitness_tips);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_add_edit_fitness_tips, $dataToPage);
        }
    }

    public function updateBlogStatus()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {

            $res = array();
            $communityId = $this->input->get('blog_id');
            $status = $this->input->get('status');

            if ($communityId > 0 && $status != '') {
                $filter['blog_id'] = $communityId;
                $filter['status'] = $status;
                $res = $this->AdminModel->updateBlogStatus($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
            }
            echo json_encode($res);
        }
    }

    public function donationDriveList()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $filter = $this->input->post();
            }
            $total_count = $this->AdminModel->getDrivecount();
            $res = $this->pagination('admin/donation-drive-list', $total_count, AppConst::PAGINATION_LIMIT);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['page'] = $res[AppConst::res_value];
                $dataTopage['data']['ROLLNO'] = $res[AppConst::res_value] + 1;
                $dataTopage['data']['links'] = $res[AppConst::res_value1];
                $res = $this->AdminModel->getAllDonationDrive($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['drive-list'] = $res[AppConst::res_value];
                }
            } else {
                $res = $this->AdminModel->getAllDonationDrive($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['drive-list'] = $res[AppConst::res_value];
                }
            }

            $dataTopage['data']['filter'] = $filter;
            $this->setActiveMenu(Pages::page_admin_donation_drive_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_donation_drive_list, $dataTopage);
        }
    }

    public function viewDonatedUsers()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $filter = $this->input->post();
            }
            $total_count = $this->AdminModel->getDonatedUsersCount();
            $res = $this->pagination('admin/view-donated_users', $total_count, AppConst::PAGINATION_LIMIT);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['page'] = $res[AppConst::res_value];
                $dataTopage['data']['ROLLNO'] = $res[AppConst::res_value] + 1;
                $dataTopage['data']['links'] = $res[AppConst::res_value1];
                $res = $this->AdminModel->getAllDonatedUsersList($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['donated-users-list'] = $res[AppConst::res_value];
                }
            } else {
                $res = $this->AdminModel->getAllDonatedUsersList($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['donated-users-list'] = $res[AppConst::res_value];
                }
            }

            $dataTopage['data']['filter'] = $filter;
            $this->setActiveMenu(Pages::page_admin_donated_users_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_donated_users_list, $dataTopage);
        }
    }

    public function fitnessTips()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $filter = $this->input->post();
            }
            $total_count = $this->AdminModel->getFitnessTipsCount();
            $res = $this->pagination('admin/fitness-tips', $total_count, AppConst::PAGINATION_LIMIT);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['page'] = $res[AppConst::res_value];
                $dataTopage['data']['ROLLNO'] = $res[AppConst::res_value] + 1;
                $dataTopage['data']['links'] = $res[AppConst::res_value1];
                $res = $this->AdminModel->getAllFitnessTips($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['fitness-tips-list'] = $res[AppConst::res_value];
                }
            } else {
                $res = $this->AdminModel->getAllFitnessTips($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['fitness-tips-list'] = $res[AppConst::res_value];
                }
            }

            $dataTopage['data']['filter'] = $filter;
            $this->setActiveMenu(Pages::page_admin_fitness_tips);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_fitness_tips, $dataTopage);
        }
    }

    public function updateDriveStatus()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {

            $res = array();
            $categoryid = $this->input->get('drive_id');
            $categoryid = isset($categoryid) ? intval($categoryid) : 0;
            $status = $this->input->get('status');
            $status = isset($status) ? intval($status) : 0;

            if ($categoryid > 0 && $status > 0) {
                $filter['drive_id'] = $categoryid;
                $filter['status'] = $status;
                $res = $this->AdminModel->updateDriveStatus($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
            }
            echo json_encode($res);
        }
    }


    public function communityRequest()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $filter = $this->input->post();
            }
            $total_count = $this->AdminModel->getCommunityRequestcount();
            $res = $this->pagination('admin/community-request', $total_count, AppConst::PAGINATION_LIMIT);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['page'] = $res[AppConst::res_value];
                $dataTopage['data']['ROLLNO'] = $res[AppConst::res_value] + 1;
                $dataTopage['data']['links'] = $res[AppConst::res_value1];
                $res = $this->AdminModel->getAllCommunityRequests($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['community-request'] = $res[AppConst::res_value];
                }
            } else {
                $res = $this->AdminModel->getAllCommunityRequests($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['community-request'] = $res[AppConst::res_value];
                }
            }

            $dataTopage['data']['filter'] = $filter;
            $this->setActiveMenu(Pages::page_admin_community_requests);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_community_requests, $dataTopage);
        }
    }

    public function updateCommunityStatus()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {

            $res = array();
            $communityId = $this->input->get('community_id');
            $status = $this->input->get('status');

            if ($communityId > 0 && $status != '') {
                $filter['community_id'] = $communityId;
                $filter['status'] = $status;
                $res = $this->AdminModel->blockActiveCommunity($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
            }
            echo json_encode($res);
        }
    }

    public function addEditCommunity()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();

            if ($this->isPost($this->input)) {
                $addCommunity = $this->input->post();
                $res = $this->AdminModel->addEditCommunity($addCommunity);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
                redirect('admin/community-list');
            }
            $communityId = $this->input->get('community_id');
            $communityId = isset($communityId) ? intval($communityId) : 0;
            if ($communityId > 0) {
                $filter['community_id'] = $communityId;
                $res = $this->AdminModel->getAllCommunity($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataToPage['data']['community-info'] = $res[AppConst::res_value][0];
                }
            }
            $countryFilter['status'] = AppConst::STATUS_ACTIVE;
            $res = $this->AdminModel->getAllCountry($countryFilter);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $dataToPage['data']['country-info'] = $res[AppConst::res_value];
            }
            $this->setActiveMenu(Pages::page_admin_community_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_add_edit_community, $dataToPage);
        }
    }

    public function importHospitalFile()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();

            if ($this->isPost($this->input)) {
                $dataToDb = array();

                if (isset($_FILES["file"]["name"])) {
                    $path = $_FILES["file"]["tmp_name"];
                    $object = PHPExcel_IOFactory::load($path);
                    foreach ($object->getWorksheetIterator() as $worksheet) {
                        $highestRow = $worksheet->getHighestRow();
                        $highestColumn = $worksheet->getHighestColumn();
                        for ($row = 2; $row <= $highestRow; $row++) {
                            $hospital_name = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            $place = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $address = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                            $contactno = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                            $contactno2 = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                            $latitude = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                            $longitude = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                            $image_url = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                            $country_id = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                            $state_id = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                            $added_date = ($worksheet->getCellByColumnAndRow(10, $row)->getValue() != '') ? $worksheet->getCellByColumnAndRow(10, $row)->getValue() : getCurrentDateWithTime();
                            $updated_date = ($worksheet->getCellByColumnAndRow(11, $row)->getValue() != '') ? $worksheet->getCellByColumnAndRow(11, $row)->getValue() : getCurrentDateWithTime();
                            $hospital_status = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                            $dataToDb[] = array(
                                'hospital_name' => $hospital_name,
                                'place' => $place,
                                'address' => $address,
                                'contact_no' => $contactno,
                                'contact_no2' => $contactno2,
                                'latitude' => $latitude,
                                'longitude' => $longitude,
                                'image_url' => $image_url,
                                'country_id' => $country_id,
                                'state_id' => $state_id,
                                'added_date' => $added_date,
                                'updated_date' => $updated_date,
                                'hospital_status' => $hospital_status
                            );
                        }
                    }
                    $res = $this->AdminModel->addHospitalCsvFile($dataToDb);
                    if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                        redirect('admin/hospital-list');
                    } else {
                        $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                        redirect('admin/import-hospital-file');
                    }
                }
            }

            $this->setActiveMenu(Pages::page_admin_hospital_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_import_hospital_csv_file, $dataToPage);
        }
    }

    public function countryList()
    {


        $session = $this->getAdminSession(true);
        if ($session != null) {
            $dataToPage = array();
            $filter = array();
            $total_count = 0;
            if ($this->isPost($this->input)) {
                $country = $this->input->post();
                //print_r($country);die();
                $res = $this->AdminModel->AddEditCountry($country);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage('Successfully added/updated', TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
                redirect(AppConst::INDEX . 'admin/country-list');
            } else {

                $total_count = $this->AdminModel->getCountryCount();
                $res = $this->pagination('admin/country-list', $total_count, AppConst::PAGINATION_LIMIT);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $filter['page'] = $res[AppConst::res_value];
                    $dataToPage['data']['ROLLNO'] = $res[AppConst::res_value] + 1;
                    $dataToPage['data']['links'] = $res[AppConst::res_value1];
                    $res = $this->AdminModel->getAllCountry($filter);
                    if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $dataToPage['data']['country-list'] = $res[AppConst::res_value];
                    }
                } else {
                    $res = $this->AdminModel->getAllCountry($filter);
                    if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $dataToPage['data']['country-list'] = $res[AppConst::res_value];
                    }
                }
            }

            $dataToPage['data']['filter'] = $filter;
            $this->setActiveMenu(Pages::page_admin_country_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_country_list, $dataToPage);
        }
    }

    public function getCountry()
    {
        $session = $this->getAdminSession();
        if ($session != null) {
            $countryid = $this->input->get('id');

            $countryid = isset($countryid) ? intval($countryid) : 0;

            if ($countryid > 0) {
                $filter['country_id'] = $countryid;
                $res = $this->AdminModel->getAllCountry($filter);

                echo json_encode($res);
            }
        }
    }

    public function cityList()
    {

        $session = $this->getAdminSession(true);
        if ($session != null) {
            $total_count = 0;
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $city = $this->input->post();
                $res = $this->AdminModel->AddEditCity($city);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage('Successfully added/updated', TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
                redirect(AppConst::INDEX . 'admin/city-list');
            } else {

                $total_count = $this->AdminModel->getStateCount();
                $res = $this->pagination('admin/city-list', $total_count, AppConst::PAGINATION_LIMIT);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $filter['page'] = $res[AppConst::res_value];
                    $dataToPage['data']['ROLLNO'] = $res[AppConst::res_value] + 1;
                    $dataToPage['data']['links'] = $res[AppConst::res_value1];
                    $res = $this->AdminModel->getAllState($filter);
                    if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $dataToPage['data']['city-list'] = $res[AppConst::res_value];
                    }
                } else {
                    $res = $this->AdminModel->getAllState($filter);
                    if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $dataToPage['data']['city-list'] = $res[AppConst::res_value];
                    }
                }

                $res = $this->AdminModel->getAllCountry(array());
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataToPage['data']['country-list'] = $res[AppConst::res_value];
                }
            }

            $dataToPage['data']['filter'] = $filter;
            $this->setActiveMenu(Pages::page_admin_city_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_city_list, $dataToPage);
        }
    }

    public function bannersList()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {
            $dataToPage = array();
            $filter = array();
            if ($this->isPost($this->input)) {
                $filter = $this->input->post();
            }
            $total_count = $this->AdminModel->getBannersCount();
            $res = $this->pagination('admin/hero-banners', $total_count, AppConst::PAGINATION_LIMIT);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['page'] = $res[AppConst::res_value];
                $dataTopage['data']['ROLLNO'] = $res[AppConst::res_value] + 1;
                $dataTopage['data']['links'] = $res[AppConst::res_value1];
                $res = $this->AdminModel->getAllBanners($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['banners-list'] = $res[AppConst::res_value];
                }
            } else {
                $res = $this->AdminModel->getAllBanners($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $dataTopage['data']['banners-list'] = $res[AppConst::res_value];
                }
            }

            $dataTopage['data']['filter'] = $filter;
            $this->setActiveMenu(Pages::page_admin_banners_list);
            $this->adminPageLoadBy(Pages::page_category_admin, Pages::page_admin_banners_list, $dataTopage);
        }
    }

    public function updateBannerStatus()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {

            $res = array();
            $tipId = $this->input->get('Banner_id');
            $tipId = isset($tipId) ? intval($tipId) : 0;
            $status = $this->input->get('status');
            $status = isset($status) ? intval($status) : 0;

            if ($tipId > 0 && $status > 0) {
                $filter['id'] = $tipId;
                $filter['status'] = $status;
                $res = $this->AdminModel->updateBannerStatus($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
            }
            echo json_encode($res);
        }
    }

    public function getCity()
    {
        $session = $this->getAdminSession();
        if ($session != null) {
            $cityid = $this->input->get('id');

            $cityid = isset($cityid) ? intval($cityid) : 0;

            if ($cityid > 0) {
                $filter['state_id'] = $cityid;
                $res = $this->AdminModel->getAllState($filter);

                echo json_encode($res);
            }
        }
    }

    public function updateCountryStatus()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {

            $res = array();
            $countryid = $this->input->get('country_id');
            $countryid = isset($countryid) ? intval($countryid) : 0;
            $status = $this->input->get('status');
            $status = isset($status) ? intval($status) : 0;

            if ($countryid > 0 && $status > 0) {
                $filter['country_id'] = $countryid;
                $filter['status'] = $status;
                $res = $this->AdminModel->updateCountryStatus($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
            }
            echo json_encode($res);
        }
    }

    public function updateFitnessTipStatus()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {

            $res = array();
            $tipId = $this->input->get('fitnessTip_id');
            $tipId = isset($tipId) ? intval($tipId) : 0;
            $status = $this->input->get('status');
            $status = isset($status) ? intval($status) : 0;

            if ($tipId > 0 && $status > 0) {
                $filter['id'] = $tipId;
                $filter['status'] = $status;
                $res = $this->AdminModel->updateFitnessTipStatus($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
            }
            echo json_encode($res);
        }
    }

    public function updateCityStatus()
    {
        $session = $this->getAdminSession(true);
        if ($session != NULL) {

            $res = array();
            $countryid = $this->input->get('country_id');
            $countryid = isset($countryid) ? intval($countryid) : 0;
            $cityid = $this->input->get('city_id');
            $cityid = isset($cityid) ? intval($cityid) : 0;
            $status = $this->input->get('status');
            $status = isset($status) ? intval($status) : 0;

            if ($countryid > 0 && $status > 0 && $cityid > 0) {
                $filter['country_id'] = $countryid;
                $filter['status'] = $status;
                $filter['state_id'] = $cityid;
                $res = $this->AdminModel->updateCityStatus($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], TRUE);
                } else {
                    $this->setFeedbackmessage($res[AppConst::errorindex_message], FALSE);
                }
            }
            echo json_encode($res);
        }
    }
}
