<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once('Apiservice.php');

class GeneralApis extends Apiservice
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'apputil', "appdate"));
        $this->load->library(array('AppConst', 'Tbl'));
        $this->load->model('MobileAPIModel');
        $this->load->model('AdminModel');
    }

    public function admin_auth_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json'], 'username');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '') {
            $deviceUniqueId = getval($inputJson['json'], 'deviceUniqueId');
            $deviceNotifyId = getval($inputJson['json'], 'deviceNotifyId');
            $accessKey = getval($inputJson['json'], 'accessKey');
            $platform = isset($inputJson['json']['platform']) ? $inputJson['json']['platform'] : "ANDROID";
            $res = $this->MobileAPIModel->adminLoginBy($username, $deviceUniqueId, $deviceNotifyId, $accessKey, $platform);
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid Request';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function user_logout_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $acceskey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '' && $acceskey != '') {
            $res = $this->MobileAPIModel->logout($username, $userId, $acceskey);
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid Request';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function user_registration_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $apiKey = getval($inputJson['json']['auth'], 'api_auth_key');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $apiKey == AppConst::API_AUTH_KEY) {
            $filterMaster = $inputJson['json']['request'];
            if ($filterMaster != '' && !empty($filterMaster)) {
                $res = $this->MobileAPIModel->registerUser($filterMaster);
            } else {
                $res = array();
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'Invalid request or request was null';
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function user_update_profile_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '' && $accessKey != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filterMaster = $inputJson['json']['request'];
                $filterMaster['user_id'] = getval($auth, 'user_id');
                $filterMaster['username'] = getval($auth, 'username');
                $res = $this->MobileAPIModel->updateProfile($filterMaster);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function user_profile_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '' && $accessKey != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filterMaster['user_id'] = getval($auth, 'user_id');
                $res = $this->MobileAPIModel->userProfile($filterMaster);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $res1 = $this->MobileAPIModel->getDonationHistory($filterMaster);
                    $res[AppConst::res_value1] = $res1[AppConst::res_value];
                    $res[AppConst::res_value]['units_donated'] = count($res1[AppConst::res_value]);
                    $res[AppConst::res_value]['joined_commnities_count'] = $this->MobileAPIModel->getJoinedCommunityCount(getval($auth, 'user_id'));
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }


    public function search_blood_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $apiKey = getval($inputJson['json']['auth'], 'api_auth_key');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $apiKey == AppConst::API_AUTH_KEY) {
            $filterMaster['latitude'] = getval($inputJson['json']['request'], 'latitude');
            $filterMaster['longitude'] = getval($inputJson['json']['request'], 'longitude');
            $filterMaster['blood_grp'] = getval($inputJson['json']['request'], 'blood_grp');
            $filterMaster['dist'] = AppConst::BLOOD_GROUP_DISTANCE;
            $filterMaster['user_id'] = isset($inputJson['json']['request']['user_id']) ? getval($inputJson['json']['request'], 'user_id') : 0;

            $res = $this->MobileAPIModel->searchBloodGroup($filterMaster);
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }

        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function quick_search_blood_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $apiKey = getval($inputJson['json']['auth'], 'api_auth_key');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $apiKey == AppConst::API_AUTH_KEY) {
            $filterMaster['latitude'] = getval($inputJson['json']['request'], 'latitude');
            $filterMaster['longitude'] = getval($inputJson['json']['request'], 'longitude');
            $filterMaster['blood_grp'] = getval($inputJson['json']['request'], 'blood_grp');
            $filterMaster['dist'] = AppConst::BLOOD_GROUP_DISTANCE;

            $res = $this->MobileAPIModel->searchBloodGroupForNonRegisteredUser($filterMaster);
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }

        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function add_user_chat_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filterMaster['chat_from'] = getval($inputJson['json']['request'], 'chat_from');
                $filterMaster['chat_to'] = getval($inputJson['json']['request'], 'chat_to');
                $filterMaster['chat_msg'] = getval($inputJson['json']['request'], 'chat_msg');
                $filterMaster['request_id'] = getval($inputJson['json']['request'], 'request_id');

                if (getval($filterMaster, 'chat_from') != '' && getval($filterMaster, 'chat_to') != '' && getval($filterMaster, 'chat_msg') != '' && getval($filterMaster, 'request_id') != '') {
                    $res = $this->MobileAPIModel->addUserChat($filterMaster);
                    if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $sender_name = isset($res[AppConst::res_value2]) ? $res[AppConst::res_value2] : '';
                        $fcmArray = $res[AppConst::res_value1];
                        $fcmkeys = array();
                        for ($i = 0; $i < count($fcmArray); $i++) {
                            array_push($fcmkeys, getval($fcmArray[$i], 'dev_notify_id'));
                        }
                        $push['fcmkey'] = $fcmkeys;
                        $push['requestId'] = getval($filterMaster, 'request_id');
                        $push['chat_from'] = getval($filterMaster, 'chat_from');
                        $push['chat_to'] = getval($filterMaster, 'chat_to');
                        $push['chatId'] = $res[AppConst::res_value]['chat_id'];
                        $push['time'] = $res[AppConst::res_value]['time'];
                        $push['sender_name'] = $sender_name;
                        $push['title'] = 'New Message From ' . $sender_name;
                        $push['message'] = getval($filterMaster, 'chat_msg');
                        $push['read_status'] = AppConst::CHAT_PUSH_NOTIFICATION_SENT_STATUS;
                        $this->sendNotification(AppConst::NOTIFICATION_TYPE_CHAT, $push);
                    }
                } else {
                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request or request was null';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function send_notification_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filterMaster['title'] = getval($inputJson['json']['request'], 'title');
                $filterMaster['body'] = getval($inputJson['json']['request'], 'body');

                if (getval($filterMaster, 'title') != '' && getval($filterMaster, 'body') != '') {
                    // $res = $this->MobileAPIModel->addUserChat($filterMaster);
                    // if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    //     $sender_name = isset($res[AppConst::res_value2]) ? $res[AppConst::res_value2] : '';
                    //     $fcmArray = $res[AppConst::res_value1];
                        $fcmkeys = array('96894bb8-36c9-48e0-b13d-54acc1ca60f4');
                        // for ($i = 0; $i < count($fcmArray); $i++) {
                        //     array_push($fcmkeys, getval($fcmArray[$i], 'dev_notify_id'));
                        // }
                        $push['fcmkey'] = $fcmkeys;
                        $push['title'] = getval($filterMaster, 'title');
                        $push['body'] = getval($filterMaster, 'body');
                        $push['url'] = getval($filterMaster, 'url');
                        $push['image_url'] = getval($filterMaster, 'image_url');
                        $push['order_id'] = getval($filterMaster, 'order_id');
                        $push['community_id'] = getval($filterMaster, 'community_id');
                        $push['article_id'] = getval($filterMaster, 'article_id');
                        $push['user_id'] = getval($filterMaster, 'user_id');
                        $this->sendNotificationOneSignal($push);
                    // }
                } else {
                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request or request was null';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    
    public function user_blood_request_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filterMaster = $inputJson['json']['request'];
                $filterMaster['user_id'] = getval($auth, 'user_id');
                if ($filterMaster != '' && !empty($filterMaster)) {
                    $res = $this->MobileAPIModel->requestForBlood($filterMaster);
                    if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $fcmArray = $res[AppConst::res_value1];
                        $fcmkeys = array();
                        for ($i = 0; $i < count($fcmArray); $i++) {
                            if (getval($fcmArray[$i], 'dev_notify_id') != '') {
                                array_push($fcmkeys, getval($fcmArray[$i], 'dev_notify_id'));
                            }
                        }
                        $push['fcmkey'] = $fcmkeys;
                        $push['requestId'] = $res[AppConst::res_value];
                        $push['senderId'] = $userId;

                        $push['title'] = 'New blood request';
                        $push['message'] = 'Blood Locator';
                        $this->sendNotification(AppConst::NOTIFICATION_TYPE_REQUEST, $push);
                    }
                } else {
                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request or request was null';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function user_community_request_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filterMaster = $inputJson['json']['request'];
                $filterMaster['user_id'] = getval($auth, 'user_id');
                if ($filterMaster != '' && !empty($filterMaster)) {
                    $res = $this->MobileAPIModel->requestForCommunity($filterMaster);
                    if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                    } else {
                        $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                    }
                } else {
                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request or request was null';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function get_active_featured_products_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['user_id'] = $userId;
                $filter['status'] = AppConst::STATUS_ACTIVE;
                $filter['country'] = getval($inputJson['json']['filter'], 'country');
                $res = $this->MobileAPIModel->getFeaturedProductList($filter);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function get_all_products_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter = $inputJson['json']['request'];
                $filter['user_id'] = $userId;
                $filter['status'] = AppConst::STATUS_ACTIVE;
                $filter['country'] = getval($inputJson['json']['filter'], 'country');
                $res = $this->MobileAPIModel->getAllProductList($filter);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function get_active_trending_products_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['user_id'] = $userId;
                $filter['status'] = AppConst::STATUS_ACTIVE;
                $filter['country'] = getval($inputJson['json']['filter'], 'country');
                $res = $this->MobileAPIModel->getTrendingProductList($filter);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function get_hydration_status_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['user_id'] = $userId;
                $res = $this->MobileAPIModel->getTodaysHydration($filter);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function add_hydration_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['user_id'] = $userId;
                $res = $this->MobileAPIModel->waterConsumed($filter);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function remove_hydration_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['user_id'] = $userId;
                $res = $this->MobileAPIModel->removeConsumed($filter);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function todays_hydration_records_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['user_id'] = $userId;
                $res = $this->MobileAPIModel->todaysHydrationRecords($filter);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function view_reminder_time_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['user_id'] = $userId;
                $res = $this->MobileAPIModel->viewRemiderTime($filter);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function edit_reminder_time_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter = $inputJson['json']['request'];
                $filter['user_id'] = $userId;
                $res = $this->MobileAPIModel->editRemiderTime($filter);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function get_product_details_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        $productId = getval($inputJson['json']['filter'], 'product_id');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            if ($productId != '') {
                $auth['username'] = $username;
                $auth['user_id'] = $userId;
                $auth['accessKey'] = $accessKey;
                $res = $this->MobileAPIModel->checkAuthentication($auth);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $filter['user_id'] = $userId;
                    $filter['id'] = $productId;
                    $filter['status'] = AppConst::STATUS_ACTIVE;
                    $filter['country'] = getval($inputJson['json']['filter'], 'country');
                    $res = $this->MobileAPIModel->getProductDetail($filter);
                } else {
                    $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                    $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                }
            } else {
                $res[AppConst::errorindex_code] = 422;
                $res[AppConst::errorindex_message] = "product id is missing!";
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function get_active_donation_drive_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter = $inputJson['json']['request'];
                $filter['user_id'] = $userId;
                $filter['status'] = AppConst::STATUS_ACTIVE;
                $res = $this->MobileAPIModel->getAllDonationDrive($filter);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function create_donation_drive_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $deviceNotifyIds = array();
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter = $inputJson['json']['request'];
                $filter['user_id'] = $userId;
                $res = $this->MobileAPIModel->createDonationDrive($filter);
                if($res[AppConst::errorindex_code] == AppConst::errorcode_success){
                    $res1 = $this->MobileAPIModel->getNotifyIdsForDonationDrive(getval($inputJson['json']['request'], 'community'));
                    if($res1[AppConst::errorindex_code] == AppConst::errorcode_success){
                        array_push($deviceNotifyIds,$res1[AppConst::res_value]['dev_notify_id']);
                    }
                    $res2 = $this->MobileAPIModel->getNotifyIdsForUsers();
                    if($res2[AppConst::errorindex_code] == AppConst::errorcode_success){
                        array_push($deviceNotifyIds,$res2[AppConst::res_value]['dev_notify_id']);
                    }
                    $fcmkeys = array_unique($deviceNotifyIds);
                    $push['fcmkey'] = $fcmkeys;
                    $push['title'] = 'Blood donation drive created';
                    $push['body'] = 'Blood donation drive is scheduled on '.getval($inputJson['json']['request'], 'event_date').' at '.getval($inputJson['json']['request'], 'place').' , we are looking for your participation'.getval($inputJson['json']['request'], 'description'). '';
                    $push['url'] = '';
                    $push['image_url'] = '';
                    $push['order_id'] = '';
                    $push['community_id'] = '';
                    $push['article_id'] = '';
                    $push['user_id'] = '';
                    $this->sendNotificationOneSignal($push);
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function join_donation_drive_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filterMaster = $inputJson['json']['request'];
                $filterMaster['user_id'] = getval($auth, 'user_id');
                if ($filterMaster != '' && !empty($filterMaster)) {
                    $res = $this->MobileAPIModel->joinToDrive($filterMaster);
                    if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                    } else {
                        $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                    }
                } else {
                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request or request was null';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function withdraw_donation_drive_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filterMaster = $inputJson['json']['request'];
                $filterMaster['user_id'] = getval($auth, 'user_id');
                if ($filterMaster != '' && !empty($filterMaster)) {
                    $res = $this->MobileAPIModel->WithdrawFromDrive($filterMaster);
                    if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                    } else {
                        $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                    }
                } else {
                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request or request was null';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function add_drive_to_favourite_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filterMaster = $inputJson['json']['request'];
                $filterMaster['user_id'] = getval($auth, 'user_id');
                if ($filterMaster != '' && !empty($filterMaster)) {
                    $res = $this->MobileAPIModel->addDriveToFavorites($filterMaster);
                    if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                    } else {
                        $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                    }
                } else {
                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request or request was null';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function withdraw_drive_from_favourites_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filterMaster = $inputJson['json']['request'];
                $filterMaster['user_id'] = getval($auth, 'user_id');
                if ($filterMaster != '' && !empty($filterMaster)) {
                    $res = $this->MobileAPIModel->WithdrawDriveFromFavourities($filterMaster);
                    if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                    } else {
                        $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                    }
                } else {
                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request or request was null';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function get_all_blogs_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter = $inputJson['json']['request'];
                $filter['user_id'] = $userId;
                $filter['status'] = AppConst::STATUS_ACTIVE;
                $res = $this->MobileAPIModel->getAllBlogs($filter);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function add_blog_to_favourite_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filterMaster = $inputJson['json']['request'];
                $filterMaster['user_id'] = getval($auth, 'user_id');
                if ($filterMaster != '' && !empty($filterMaster)) {
                    $res = $this->MobileAPIModel->addBlogToFavorites($filterMaster);
                    if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                    } else {
                        $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                    }
                } else {
                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request or request was null';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function withdraw_blog_from_favourites_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filterMaster = $inputJson['json']['request'];
                $filterMaster['user_id'] = getval($auth, 'user_id');
                if ($filterMaster != '' && !empty($filterMaster)) {
                    $res = $this->MobileAPIModel->WithdrawBlogFromFavourities($filterMaster);
                    if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                    } else {
                        $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                        $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                    }
                } else {
                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request or request was null';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function search_hospital_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filterMaster['latitude'] = getval($inputJson['json']['request'], 'latitude');
                $filterMaster['longitude'] = getval($inputJson['json']['request'], 'longitude');
                $filterMaster['hospital_name'] = getval($inputJson['json']['request'], 'hospital_name');
                if (getval($filterMaster, 'latitude') != '' && getval($filterMaster, 'longitude') != '') {
                    $res = $this->MobileAPIModel->searchHospitals($filterMaster);
                } else if (getval($filterMaster, 'hospital_name') != '') {
                    $res = $this->AdminModel->getAllHospital($filterMaster);
                } else {
                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request or request was null';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function user_active_requests_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filterMaster['user_id'] = getval($auth, 'user_id');
                //$filterMaster['today_date'] = getCurrentDate();
                $res = $this->MobileAPIModel->activeRequests($filterMaster);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function get_my_communities_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {

                $filter['user_id'] = $userId;
                $filter['status'] = AppConst::STATUS_ACTIVE;
                $res = $this->MobileAPIModel->getSubscribedCommunityList($filter);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function get_my_community_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {

                $filter['user_id'] = $userId;
                $filter['status'] = AppConst::STATUS_ACTIVE;
                $res = $this->MobileAPIModel->getAdminCommunityList($filter);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function get_users_in_my_communities_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {

                $filter['user_id'] = $userId;
                $filter['status'] = AppConst::STATUS_ACTIVE;
                $res = $this->MobileAPIModel->getSubscribedUsersList($filter);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function remove_user_in_my_communities_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter = $inputJson['json']['request'];
                $filter['user_id'] = $userId;
                $filter['status'] = AppConst::STATUS_ACTIVE;
                $res = $this->MobileAPIModel->removeUserFromMyCommunity($filter);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function add_user_in_my_communities_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter = $inputJson['json']['request'];
                $filter['user_id'] = $userId;
                $filter['status'] = AppConst::STATUS_ACTIVE;
                $res = $this->MobileAPIModel->addUserFromMyCommunity($filter);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function get_user_chat_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $fromId = getval($inputJson['json']['request'], 'from_id');
                $toId = getval($inputJson['json']['request'], 'to_id');
                $requestId = getval($inputJson['json']['request'], 'request_id');
                $limit = getval($inputJson['json']['request'], 'limit');
                if ($fromId != '' && $toId != '' && $requestId > 0) {
                    $filter['from_id'] = $fromId;
                    $filter['to_id'] = $toId;
                    $filter['request_id'] = $requestId;
                    $filter['limit'] = $limit;
                    $filter['chat_type'] = AppConst::CHAT_TYPE_NORMAL;
                    $chatFilter['request_id'] = $requestId;
                    $chatFilter['from_id'] = $fromId;
                    $res = $this->MobileAPIModel->getChatInfo($chatFilter);
                    if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $reqfilter['request_id'] = $requestId;
                        $reqfilter['chat_status'] = AppConst::CHAT_STATUS_ACCEPT;
                        $res1 = $this->MobileAPIModel->getRequestNotifiedUsers($reqfilter);
                        $res[AppConst::res_value][0]['request_accept_count'] = count($res1[AppConst::res_value]);
                        $res[AppConst::res_value] = $res[AppConst::res_value][0];
                        $res2 = $this->MobileAPIModel->userChatList($filter);
                        $res[AppConst::res_value1] = $res2[AppConst::res_value];
                    } else {
                        $res = array();
                        $res[AppConst::errorindex_code] = 40;
                        $res[AppConst::errorindex_message] = 'no record found ';
                    }
                } else if ($fromId != '' && $toId != '') {
                    $filter['from_id'] = $fromId;
                    $filter['to_id'] = $toId;
                    $filter['limit'] = $limit;
                    $filter['chat_type'] = AppConst::CHAT_TYPE_NORMAL;
                    $chatFilter['to_id'] = $toId;
                    $chatFilter['from_id'] = $fromId;
                    $res = $this->MobileAPIModel->getChatSummary($chatFilter);
                    if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $res1 = $this->MobileAPIModel->userChatList($filter);
                        $res[AppConst::res_value1] = $res1[AppConst::res_value];
                    }
                } else {

                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request ';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function get_communities_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $apiKey = getval($inputJson['json']['auth'], 'api_auth_key');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $apiKey == AppConst::API_AUTH_KEY) {
            $filter = $inputJson['json']['request'];
            $filter['status'] = AppConst::STATUS_ACTIVE;
            $filter['user_id'] = getval($inputJson['json']['auth'], 'userId');
            $res = $this->MobileAPIModel->getCommunityList($filter);
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function get_all_notifications_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            // if ($productId != '') {
                $auth['username'] = $username;
                $auth['user_id'] = $userId;
                $auth['accessKey'] = $accessKey;
                $res = $this->MobileAPIModel->checkAuthentication($auth);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $filter['user_id'] = $userId;
                    //$filter['country'] = getval($inputJson['json']['filter'], 'country');
                    $res = $this->MobileAPIModel->getNotifications($filter);
                } else {
                    $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                    $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                }
            // } else {
            //     $res[AppConst::errorindex_code] = 422;
            //     $res[AppConst::errorindex_message] = "product id is missing!";
            // }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function get_all_users_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $apiKey = getval($inputJson['json']['auth'], 'api_auth_key');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $apiKey == AppConst::API_AUTH_KEY) {
            $filter = $inputJson['json']['request'];
            $filter['status'] = AppConst::STATUS_ACTIVE;
            $filter['user_id'] = getval($inputJson['json']['auth'], 'userId');
            $res = $this->MobileAPIModel->getUsersList($filter);
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function get_countries_get()
    {
        $res = $this->MobileAPIModel->getAllCountries();
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function get_beginner_routine_get()
    {
        $filter['category_type'] = AppConst::BEGINNER_ROUTINE;
        $res = $this->MobileAPIModel->getAllFitnessTips($filter);
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function get_workout_routine_get()
    {
        $filter['category_type'] = AppConst::WORKOUT_ROUTINE;
        $res = $this->MobileAPIModel->getAllFitnessTips($filter);
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function get_fitness_tip_banners_get()
    {
        $filter['type'] = AppConst::FITNESS_BANNER;
        $res = $this->MobileAPIModel->getAllBanners($filter);
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function get_hero_banners_get()
    {
        $filter['type'] = AppConst::HERO_BANNER;
        $res = $this->MobileAPIModel->getAllBanners($filter);
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function get_static_banner_get()
    {
        $filter['type'] = AppConst::STATIC_BANNER;
        $res = $this->MobileAPIModel->getAllBanners($filter);
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function get_hydration_tips_get()
    {
        $filter['status'] = AppConst::STATUS_ACTIVE;
        $res = $this->MobileAPIModel->getTodaysHydrationTips($filter);
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function get_cart_details_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            // if ($productId != '') {
                $auth['username'] = $username;
                $auth['user_id'] = $userId;
                $auth['accessKey'] = $accessKey;
                $res = $this->MobileAPIModel->checkAuthentication($auth);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $filter['user_id'] = $userId;
                    $filter['status'] = AppConst::STATUS_ACTIVE;
                    //$filter['country'] = getval($inputJson['json']['filter'], 'country');
                    $res = $this->MobileAPIModel->getCartDetails($filter);
                } else {
                    $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                    $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                }
            // } else {
            //     $res[AppConst::errorindex_code] = 422;
            //     $res[AppConst::errorindex_message] = "product id is missing!";
            // }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function checkout_order_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            // if ($productId != '') {
                $auth['username'] = $username;
                $auth['user_id'] = $userId;
                $auth['accessKey'] = $accessKey;
                $res = $this->MobileAPIModel->checkAuthentication($auth);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $filter['user_id'] = $userId;
                    $filter['status'] = AppConst::STATUS_ACTIVE;
                    //$filter['country'] = getval($inputJson['json']['filter'], 'country');
                    $res = $this->MobileAPIModel->checkOutOrder($filter);
                } else {
                    $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                    $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                }
            // } else {
            //     $res[AppConst::errorindex_code] = 422;
            //     $res[AppConst::errorindex_message] = "product id is missing!";
            // }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function get_address_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            // if ($productId != '') {
                $auth['username'] = $username;
                $auth['user_id'] = $userId;
                $auth['accessKey'] = $accessKey;
                $res = $this->MobileAPIModel->checkAuthentication($auth);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $filter['user_id'] = $userId;
                    $filter['status'] = AppConst::STATUS_ACTIVE;
                    //$filter['country'] = getval($inputJson['json']['filter'], 'country');
                    $res = $this->MobileAPIModel->getUserAddress($filter);
                } else {
                    $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                    $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                }
            // } else {
            //     $res[AppConst::errorindex_code] = 422;
            //     $res[AppConst::errorindex_message] = "product id is missing!";
            // }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function get_all_my_orders_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            // if ($productId != '') {
                $auth['username'] = $username;
                $auth['user_id'] = $userId;
                $auth['accessKey'] = $accessKey;
                $res = $this->MobileAPIModel->checkAuthentication($auth);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $filter['user_id'] = $userId;
                    $filter['status'] = AppConst::STATUS_ACTIVE;
                    //$filter['country'] = getval($inputJson['json']['filter'], 'country');
                    $res = $this->MobileAPIModel->getMyOrders($filter);
                } else {
                    $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                    $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                }
            // } else {
            //     $res[AppConst::errorindex_code] = 422;
            //     $res[AppConst::errorindex_message] = "product id is missing!";
            // }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function update_delivery_address_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            // if ($productId != '') {
                $auth['username'] = $username;
                $auth['user_id'] = $userId;
                $auth['accessKey'] = $accessKey;
                $res = $this->MobileAPIModel->checkAuthentication($auth);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $filter = $inputJson['json']['request'];
                    $filter['user_id'] = $userId;
                    $res = $this->MobileAPIModel->updateDeliveryAddress($filter);
                } else {
                    $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                    $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                }
            // } else {
            //     $res[AppConst::errorindex_code] = 422;
            //     $res[AppConst::errorindex_message] = "product id is missing!";
            // }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    public function test_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            // if ($productId != '') {
                $auth['username'] = $username;
                $auth['user_id'] = $userId;
                $auth['accessKey'] = $accessKey;
                $res = $this->MobileAPIModel->checkAuthentication($auth);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $filter['user_id'] = $userId;
                    $filter['status'] = AppConst::STATUS_ACTIVE;
                    //$filter['country'] = getval($inputJson['json']['filter'], 'country');
                    $res = $this->MobileAPIModel->viewUserAddress($userId);
                } else {
                    $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                    $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                }
            // } else {
            //     $res[AppConst::errorindex_code] = 422;
            //     $res[AppConst::errorindex_message] = "product id is missing!";
            // }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function add_to_cart_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        $productId = getval($inputJson['json']['request'], 'product_id');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            if ($productId != '') {
                $auth['username'] = $username;
                $auth['user_id'] = $userId;
                $auth['accessKey'] = $accessKey;
                $res = $this->MobileAPIModel->checkAuthentication($auth);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $filter = $inputJson['json']['request'];
                    $filter['user_id'] = $userId;
                    $filter['status'] = AppConst::STATUS_ACTIVE;
                    //$filter['country'] = getval($inputJson['json']['filter'], 'country');
                    $res = $this->MobileAPIModel->addToCart($filter);
                    $res1 = $this->MobileAPIModel->getCartDetails($filter);
                    $res[AppConst::res_value] = $res1[AppConst::res_value];
                } else {
                    $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                    $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                }
            } else {
                $res[AppConst::errorindex_code] = 422;
                $res[AppConst::errorindex_message] = "product id is missing!";
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function increment_product_qty_to_cart_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        $productId = getval($inputJson['json']['request'], 'product_id');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            if ($productId != '') {
                $auth['username'] = $username;
                $auth['user_id'] = $userId;
                $auth['accessKey'] = $accessKey;
                $res = $this->MobileAPIModel->checkAuthentication($auth);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $filter = $inputJson['json']['request'];
                    $filter['user_id'] = $userId;
                    $filter['status'] = AppConst::STATUS_ACTIVE;
                    //$filter['country'] = getval($inputJson['json']['filter'], 'country');
                    $res = $this->MobileAPIModel->incrementProductQtyCart($filter);
                    $res1 = $this->MobileAPIModel->getCartDetails($filter);
                    $res[AppConst::res_value] = $res1[AppConst::res_value];
                } else {
                    $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                    $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                }
            } else {
                $res[AppConst::errorindex_code] = 422;
                $res[AppConst::errorindex_message] = "product id is missing!";
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function decrement_product_qty_to_cart_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        $productId = getval($inputJson['json']['request'], 'product_id');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            if ($productId != '') {
                $auth['username'] = $username;
                $auth['user_id'] = $userId;
                $auth['accessKey'] = $accessKey;
                $res = $this->MobileAPIModel->checkAuthentication($auth);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $filter = $inputJson['json']['request'];
                    $filter['user_id'] = $userId;
                    $filter['status'] = AppConst::STATUS_ACTIVE;
                    //$filter['country'] = getval($inputJson['json']['filter'], 'country');
                    $res = $this->MobileAPIModel->decrementProductQtyCart($filter);
                    $res1 = $this->MobileAPIModel->getCartDetails($filter);
                    $res[AppConst::res_value] = $res1[AppConst::res_value];
                } else {
                    $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                    $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                }
            } else {
                $res[AppConst::errorindex_code] = 422;
                $res[AppConst::errorindex_message] = "product id is missing!";
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function place_order_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
                $auth['username'] = $username;
                $auth['user_id'] = $userId;
                $auth['accessKey'] = $accessKey;
                $res = $this->MobileAPIModel->checkAuthentication($auth);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    // $filter = $inputJson['json']['request'];
                    $filter['user_id'] = $userId;
                    $filter['status'] = AppConst::STATUS_ACTIVE;
                    $res = $this->MobileAPIModel->placeOrder($filter);
                } else {
                    $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                    $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
                }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request/authentication request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }


    public function update_request_status_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $requestId = getval($inputJson['json']['request'], 'request_id');
                $status = getval($inputJson['json']['request'], 'status');
                if ($userId != '' && $status != '' && $requestId != '') {
                    $filter['user_id'] = $userId;
                    $filter['request_id'] = $requestId;
                    $filter['status'] = $status;
                    $res = $this->MobileAPIModel->updateRequestStatus($filter);
                } else {
                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request ';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function search_community_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filter['community_name'] = getval($inputJson['json']['request'], 'community_name');
                $filter['latitude'] = getval($inputJson['json']['request'], 'latitude');
                $filter['longitude'] = getval($inputJson['json']['request'], 'longitude');
                $filter['status'] = AppConst::STATUS_ACTIVE;
                if ($filter['latitude'] != '' && $filter['longitude']) {
                    $res = $this->MobileAPIModel->searchCommunity($filter);
                } else {

                    $res = $this->MobileAPIModel->getCommunityList($filter);
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function my_requests_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filterMaster['user_id'] = $userId;
                //$filterMaster['today_date'] = getCurrentDate();
                $res = $this->MobileAPIModel->getRequestList($filterMaster);
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function get_community_info_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $apiKey = getval($inputJson['json']['auth'], 'api_auth_key');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $apiKey == AppConst::API_AUTH_KEY) {

            $community_id = getval($inputJson['json']['request'], 'community_id');
            $user_id = getval($inputJson['json']['request'], 'user_id');
            $lat = getval($inputJson['json']['request'], 'latitude');
            $long = getval($inputJson['json']['request'], 'longitude');
            if ($community_id != '') {
                $filter['community_id'] = $community_id;
                $res = $this->MobileAPIModel->getCommunityList($filter);
                if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $communityInfo = $res[AppConst::res_value][0];
                    $filter['latitude'] = $lat;
                    $filter['longitude'] = $long;
                    $res1 = $this->MobileAPIModel->nearbyCommunityMembersList($filter);
                    if ($res1[AppConst::errorindex_code] == AppConst::errorcode_success) {
                        $res[AppConst::res_value1] = $res1[AppConst::res_value];
                    }
                    if ($user_id != '') {
                        $filter['user_id'] = $user_id;
                        $res2 = $this->MobileAPIModel->getCommunityMemberesList($filter);
                        if ($res2[AppConst::errorindex_code] == AppConst::errorcode_success) {
                            $communityInfo['is_joined'] = AppConst::JOINED_TO_COMMUNITY;
                        } else {
                            $communityInfo['is_joined'] = AppConst::NOT_JOINED_TO_COMMUNITY;
                        }
                    }
                    $res[AppConst::res_value] = $communityInfo;
                } else {
                    $res[AppConst::errorindex_code] = 30;
                    $res[AppConst::errorindex_message] = 'invalid request';
                }
            } else {
                $res = array();
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'Invalid request ';
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }

        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function join_to_community_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $community_id = getval($inputJson['json']['request'], 'community_id');
                if ($community_id != '') {
                    $filter['community_id'] = $community_id;
                    $filter['user_id'] = $userId;
                    $res = $this->MobileAPIModel->addSubscriptionToCommunity($filter);
                } else {
                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request ';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function leave_from_community_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $community_id = getval($inputJson['json']['request'], 'community_id');
                if ($community_id != '') {
                    $filter['community_id'] = $community_id;
                    $filter['user_id'] = $userId;
                    $res = $this->MobileAPIModel->leaveFromCommunity($filter);
                } else {
                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request ';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function get_request_notified_user_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $fromId = $userId;
                $requestId = getval($inputJson['json']['request'], 'request_id');

                if ($fromId != '' && $requestId != '') {
                    $filter['from_id'] = $fromId;
                    $filter['request_id'] = $requestId;
                    $filter['chat_type'] = AppConst::CHAT_TYPE_REQUEST;
                    $res = $this->MobileAPIModel->getRequestNotifiedUsers($filter);
                } else {
                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request ';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function contact_no_already_exist_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $apiKey = getval($inputJson['json']['auth'], 'api_auth_key');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $apiKey == AppConst::API_AUTH_KEY) {
            $contact_no = getval($inputJson['json']['request'], 'contact_no');
            if ($contact_no != '') {
                $filter['contact_no'] = $contact_no;
                $res = $this->MobileAPIModel->userProfile($filter);
                if ($res[AppConst::errorindex_code] != AppConst::errorcode_success) {
                    $res[AppConst::errorindex_message] = 'this mobile no not exist';
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                }
            } else {
                $res = array();
                $res[AppConst::errorindex_code] = 20;
                $res[AppConst::errorindex_message] = 'contact no missing';
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function close_request_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $requestId = getval($inputJson['json']['request'], 'request_id');

                if ($requestId != '') {

                    $filter['request_id'] = $requestId;
                    $res = $this->MobileAPIModel->closeRequest($filter);
                } else {
                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request ';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function get_recent_chat_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $fromId = getval($inputJson['json']['request'], 'from_id');
                $limit = getval($inputJson['json']['request'], 'limit');
                if ($fromId != '') {
                    $filter['from_id'] = $fromId;
                    $filter['limit'] = $limit;
                    $filter['chat_type'] = AppConst::CHAT_TYPE_NORMAL;
                    $res = $this->MobileAPIModel->getRecentChat($filter);
                } else {

                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request ';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function get_count_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $res = array();
                $filterMaster['user_id'] = getval($auth, 'user_id');
                //$filterMaster['today_date'] = getCurrentDate();
                $res1 = $this->MobileAPIModel->activeRequests($filterMaster);
                if ($res1[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $res[AppConst::errorindex_code] = AppConst::errorcode_success;
                    $res[AppConst::res_value]['active_request_count'] = count($res1[AppConst::res_value]);
                } else {
                    $res[AppConst::errorindex_code] = 50;
                    $res[AppConst::res_value]['active_request_count'] = count($res1[AppConst::res_value]);
                }
                $res2 = $this->MobileAPIModel->getUnreadChatCount($filterMaster);
                if ($res2[AppConst::errorindex_code] == AppConst::errorcode_success) {
                    $res[AppConst::res_value]['unread_message_count'] = $res2[AppConst::res_value][0]['unread_message_count'];
                } else {
                    $res[AppConst::res_value]['unread_message_count'] = 0;
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function update_chat_read_status_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $chat_ids = $inputJson['json']['request']['chat_ids'];
                if (is_array($chat_ids) && count($chat_ids) > 0) {

                    $res = $this->MobileAPIModel->updateChatReadStatus($chat_ids);
                } else {

                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request ';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }

    public function add_donor_history_post()
    {
        $req = json_decode(file_get_contents('php://input'), TRUE);
        $inputJson = $this->decodeRequest($req);
        $res = $inputJson;
        $username = getval($inputJson['json']['auth'], 'username');
        $userId = getval($inputJson['json']['auth'], 'userId');
        $accessKey = getval($inputJson['json']['auth'], 'accesskey');
        if ($inputJson[AppConst::errorindex_code] == AppConst::errorcode_success && $username != '' && $userId != '') {
            $auth['username'] = $username;
            $auth['user_id'] = $userId;
            $auth['accessKey'] = $accessKey;
            $res = $this->MobileAPIModel->checkAuthentication($auth);
            if ($res[AppConst::errorindex_code] == AppConst::errorcode_success) {
                $filterMaster = $inputJson['json']['request'];
                if ($filterMaster != '') {

                    $res = $this->MobileAPIModel->addDonationHistory($filterMaster);
                } else {

                    $res = array();
                    $res[AppConst::errorindex_code] = 20;
                    $res[AppConst::errorindex_message] = 'Invalid request ';
                }
            } else {
                $res[AppConst::errorindex_code] = $res[AppConst::errorindex_code];
                $res[AppConst::errorindex_message] = $res[AppConst::errorindex_message];
            }
        } else {
            $res[AppConst::errorindex_code] = 10;
            $res[AppConst::errorindex_message] = 'Invalid request or auth request missing';
        }
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
}
