<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Apiservice extends REST_Controller
{

    const requestValueIndex = 'inputs';
    const requestModeIndex = 'modecode';
    const requestModeJson = 100;
    const requestModeString = 200;
    const requestModeBase64 = 300;
    const requestModeEncription = 400;

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        //$this->load->library('encryption');
    }

    protected function encodeResponse($val = null)
    {
        if ($val != null && is_array($val)) {

            $val['CRP'] = "0";
        }
        return $val;
    }

    protected function decodeRequest($req = null)
    {
        $res = array();
        if (isset($req) && $req != null && is_array($req) && isset($req[Apiservice::requestValueIndex])) {
            $res[AppConst::errorindex_code] = AppConst::errorcode_success;
            $res[AppConst::errorindex_message] = AppConst::errormessage_success;

            $crp = isset($req[Apiservice::requestModeIndex]) ? intval($req[Apiservice::requestModeIndex]) : 0;
            if ($crp == Apiservice::requestModeJson) {

                $res['json'] = $req[Apiservice::requestValueIndex];

                if (!$this->isValidInput($res['json'])) {
                    $res[AppConst::errorindex_code] = 1000;
                    $res[AppConst::errorindex_message] = "Valid data not found in request: InJS:1000";
                    unset($res['json']);
                }
            } else if ($crp == Apiservice::requestModeString) {
                $jsonStr = $req[Apiservice::requestModeIndex];
                $res['json'] = json_decode($jsonStr, TRUE);

                if (!$this->isValidInput($res['json'])) {
                    $res[AppConst::errorindex_code] = 1001;
                    $res[AppConst::errorindex_message] = "Valid data not found in request: InSTR:1001";
                    unset($res['json']);
                }
            } else if ($crp == Apiservice::requestModeBase64) {

                $jsonStr = base64_decode($req[Apiservice::requestValueIndex]);
                $res['json'] = json_decode($jsonStr, TRUE);
                if (!$this->isValidInput($res['json'])) {
                    $res[AppConst::errorindex_code] = 1002;
                    $res[AppConst::errorindex_message] = "Valid data not found in request: InB6:1002";
                    unset($res['json']);
                }
            } else if ($crp == Apiservice::requestModeEncription) {

                $jsonStr = base64_decode($req[Apiservice::requestValueIndex]);
                $res['json'] = json_decode($jsonStr, TRUE);
                if (!$this->isValidInput($res['json'])) {
                    $res[AppConst::errorindex_code] = 1003;
                    $res[AppConst::errorindex_message] = "Valid data not found in request: InENX:1003";
                    unset($res['json']);
                }
            } else {
                $res = array();
                $res[AppConst::errorindex_code] = 1004;
                $res[AppConst::errorindex_message] = "Invalid request : UNKCD:1004";
            }
        } else {
            $res[AppConst::errorindex_code] = 999;
            $res[AppConst::errorindex_message] = "Invalid request : IFOUT:999";
        }
        return $res;
    }

    private function isValidInput($reqInputs)
    {
        if (isset($reqInputs) && $reqInputs != null && $reqInputs != NULL && is_array($reqInputs)) {
            return true;
        } else {
            return false;
        }
    }

    public function sendNotificationOneSignal($push)
    {
        

        $sendData['title'] = getval($push, 'title');
        $sendData['body'] = getval($push, 'body');
        $sendData['url'] = getval($push, 'url');
        $sendData['image_url'] = getval($push, 'image_url');
        $sendData['order_id'] = getval($push, 'order_id');
        $sendData['community_id'] = getval($push, 'community_id');
        $sendData['article_id'] = getval($push, 'article_id');
        $sendData['user_id'] = getval($push, 'user_id');
        $registration_ids = $push['fcmkey'];

        $content = array(
            "en" => $sendData
        );

        // print_r($content);
        // die();

        $fields = array(
            'app_id' => AppConst::ONESIGNAL_APP_ID,
            'include_player_ids' => $registration_ids,
            'data' => $sendData,
            'headings' => array(
                "en" => $sendData['title']
                ),
            'contents' => array(
                "en" => $sendData['body']
                ),
        );

        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);


        $return["allresponses"] = $response;
        $return = json_encode($return);

        return $return;
    }

    public function sendNotification($type, $push)
    {
        if ($type == AppConst::NOTIFICATION_TYPE_REQUEST) {
            $registration_ids = $push['fcmkey'];
            $ch = curl_init("https://fcm.googleapis.com/fcm/send");
            $header = array(
                'Content-Type: application/json',
                "Authorization: key=" . AppConst::FIREBASE_KEY . ""
            );
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $sendData['requestId'] = getval($push, 'requestId');
            $sendData['chat_from'] = getval($push, 'senderId');
            $sendData['senderId'] = getval($push, 'senderId');
            $sendData['type'] = "public";
            $sendData['title'] = $push['title'];
            $sendData['message'] = $push['message'];
            $sendData['messageType'] = AppConst::NOTIFICATION_TYPE_REQUEST;


            $msg['data']['title'] = $push['title'];
            $msg['data']['message'] = $push['message'];
            $msg['data']['messageType'] = AppConst::NOTIFICATION_TYPE_REQUEST;
            $msg['data']['requestId'] = getval($push, 'requestId');
            $msg['data']['chat_from'] = getval($push, 'senderId');
            $msg['data']['senderId'] = getval($push, 'senderId');
            $msg['data']['payload'] = $sendData;


            $fields = array(
                'registration_ids' => $registration_ids,
                'data' => $msg,
            );

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            if ($result === FALSE) {
                die('FCM Send Error: ' . curl_error($ch));
            }
            curl_close($ch);

            return $result;
        } else if ($type == AppConst::NOTIFICATION_TYPE_CHAT) {
            $registration_ids = $push['fcmkey'];
            $ch = curl_init("https://fcm.googleapis.com/fcm/send");
            $header = array(
                'Content-Type: application/json',
                "Authorization: key=" . AppConst::FIREBASE_KEY . ""
            );
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $sendData['requestId'] = getval($push, 'requestId');
            $sendData['chat_from'] = getval($push, 'chat_from');
            $sendData['chat_to'] = getval($push, 'chat_to');
            $sendData['chatId'] = getval($push, 'chatId');
            $sendData['time'] = getval($push, 'time');
            $sendData['sender_name'] = getval($push, 'sender_name');
            $sendData['type'] = "public";
            $sendData['title'] = $push['title'];
            $sendData['message'] = $push['message'];
            $sendData['messageType'] = AppConst::NOTIFICATION_TYPE_CHAT;
            $sendData['read_status'] = $push['read_status'];


            $msg['data']['title'] = $push['title'];
            $msg['data']['message'] = $push['message'];
            $msg['data']['messageType'] = AppConst::NOTIFICATION_TYPE_CHAT;
            $msg['data']['requestId'] = getval($push, 'requestId');
            $msg['data']['chat_from'] = getval($push, 'chat_from');
            $msg['data']['chat_to'] = getval($push, 'chat_to');
            $msg['data']['chatId'] = getval($push, 'chatId');
            $msg['data']['time'] = getval($push, 'time');
            $msg['data']['sender_name'] = getval($push, 'sender_name');
            $msg['data']['read_status'] = getval($push, 'read_status');
            $msg['data']['payload'] = $sendData;


            $fields = array(
                'registration_ids' => $registration_ids,
                'data' => $msg,
            );

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            if ($result === FALSE) {
                die('FCM Send Error: ' . curl_error($ch));
            }
            curl_close($ch);

            return $result;
        }
    }
}
