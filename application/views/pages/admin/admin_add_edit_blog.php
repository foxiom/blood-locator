<?php
$blogInfo = isset($data['blog-info']) ? $data['blog-info'] : array();
$categoryList = isset($data['category-info']) ? $data['category-info'] : array();
?>
<link rel="stylesheet" href="<?= base_url('webres/admin/css/jquery-ui.css')?>">
<script src="<?= base_url('webres/admin/js/jquery-ui.js')?>"></script>
<section class="content-header">BLOG
    <h1>
        
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Blog List</a></li>
        <li class="active">Add Blog</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        ADD/EDIT BLOG
                    </div>
                    <div class="box-body">
                        <?php
                        $formAttributes = array('id' => 'form', 'data-parsley-validate' => '', 'class' => 'form', 'enctype' => 'multipart/form-data');
                        echo form_open_multipart(AppConst::INDEX . 'admin/add-blog', $formAttributes);
                        ?>
                        <input type="hidden" name="blog_id" value="<?= getval($blogInfo, 'id') ?>"/>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Blog Title <span class="required-color">*</span></label>
                                    <input type="text" name="title" autocomplete="off" class="form-control"  required=""   value="<?= getval($blogInfo, 'title'); ?>" placeholder="Blog Title" >
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Category<span class="required-color">*</span></label>
                                    <?php $categoryId = getval($blogInfo, 'category'); ?>
                                    <select class="form-control select2" style="width:100%" name="category" required="">
                                        <option value="">-Select-</option>
                                        <?php foreach ($categoryList as $category) { ?>
                                            <option value="<?= getval($category, 'id') ?>" <?= ($categoryId == getval($category, 'id')) ? 'selected=""' : '' ?>><?= getval($category, 'category_name') ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Image </label>
                                    <input type="file" name="image_url" class="form-control">
                                    <?php if (getval($blogInfo, 'image_url') != '') { ?>
                                        <a target="_blank" href="<?= base_url(getval($blogInfo, 'image_url')) ?>">View Image</a>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label >Description <span class="required-color">*</span></label>
                                    <textarea name="description" autocomplete="off" class="form-control"  required=""  placeholder="Description" ><?= getval($blogInfo, 'description'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Youtube Url <span class="required-color">*</span></label>
                                    <input type="text" name="youtube_url" autocomplete="off" class="form-control"  required=""   value="<?= getval($blogInfo, 'youtube_url'); ?>" placeholder="Youtube Url" >
                                </div>
                            </div>
                        </div>
                       
                        <hr>
                    </div>
                    <div class="box-footer">
                        <button type="submit" id="btn_save" class="btn btn-microsoft pull-right btn_submit">&nbsp;&nbsp;&nbsp; Save &nbsp;&nbsp;&nbsp;</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


