<?php
$productList = isset($data['product-list']) ? $data['product-list'] : array();
$filter = isset($data['filter']) ? $data['filter'] : array();
$links = isset($data['links']) ? $data['links'] : array();
$no = isset($data['ROLLNO']) ? $data['ROLLNO'] : 1;
?>

<section class="content-header">
    <h1>
        PRODUCT MANAGEMENT
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">product List</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="pull-right" style="padding-top: 4px;padding-right: 8px;">
                        <a class="btn btn-primary pull-right" href="<?php echo base_url(AppConst::INDEX . 'admin/add-product'); ?>" title="New product"><i class="fa fa-plus"></i> New product</a>
                    </div>
                    <div class="panel-heading">
                        PRODUCT LIST
                        <span><?= getFeedbackMessage() ?></span>
                    </div>
                    <div class="panel-body">
                        <table width="" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="10%">Product Name</th>
                                    <th width="10%">Product Image</th>
                                    <th width="15%">Product Details</th>
                                    <th width="10%">Price Details</th>
                                    <th width="10%">Created On</th>
                                    <th width="10%">Status</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = $no;
                                if (count($productList) > 0) {
                                    foreach ($productList as $product) {
                                ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= getval($product, 'product_name') ?></td>
                                            <?php $imageUrl = getval($product, 'image_url'); ?>
                                            <td><?php if ($imageUrl != '') { ?>
                                                    <img src="<?= base_url($imageUrl) ?>" width="80px" height="80px">
                                                <?php } ?>
                                            </td>
                                            <td><?= getval($product, 'product_details') ?><br>
                                                <label>Country: <?= getval($product, 'country') ?></label><br>
                                                <label>Category: <?= getval($product, 'category_name') ?></label>
                                            </td>
                                            <td><label>Regular Price:</label> <?= getval($product, 'regular_price') ?><br>
                                                <label>Offer Price:</label> <?= getval($product, 'offer_price') ?><br>
                                                <label>Offer Percentage:</label> <?= getval($product, 'offer_percentage') ?>%
                                            </td>
                                            <td><?= dbDate2UIdate(getval($product, 'created_at'), FALSE) ?></td>
                                            <?php $status = getval($product, 'product_status'); ?>
                                            <td><?php if ($status == AppConst::STATUS_ACTIVE) { ?>
                                                    <label class="label bg-green"> Active</label>
                                                <?php } else if ($status == AppConst::STATUS_BLOCK) { ?>
                                                    <label class="label bg-red">Blocked</label>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <a class="btn bg-blue" title="Edit <?= getval($product, 'product_name') ?>" href="<?= base_url('admin/add-product?product_id=' . getval($product, 'id')) ?>"><i class="fa fa-edit"></i>Edit</a>
                                                <?php if ($status == AppConst::STATUS_ACTIVE) { ?>
                                                    <a class="btn bg-red" title="Block <?= getval($product, 'product_name') ?> " onclick="updateproductStatus('<?= getval($product, 'id') ?>','<?= AppConst::STATUS_BLOCK ?>')">Block</a>
                                                <?php } else if ($status == AppConst::STATUS_BLOCK) { ?>
                                                    <a class="btn bg-green" title=" Active <?= getval($product, 'product_name') ?> " onclick="updateproductStatus('<?= getval($product, 'id') ?>','<?= AppConst::STATUS_ACTIVE ?>')">Active</a>
                                                <?php } ?>
                                                <a class="btn bg-yellow" title="Edit <?= getval($product, 'product_name') ?>" href="<?= base_url('admin/product-images?product_id=' . getval($product, 'id')) ?>"><i class="fa fa-image"></i>Images</a>
                                            </td>

                                        <?php
                                        $i++;
                                    }
                                        ?>
                                        </tr>
                                    <?php } else { ?>
                                        <tr>
                                            <td colspan="8" class="text-center">No product Found</td>
                                        </tr>
                                    <?php } ?>
                            <tbody>
                        </table>
                        <ul class=" pagination pull-right">

                            <!-- Show pagination links -->
                            <?php foreach ($links as $link) { ?>
                                <li><?= $link ?></li>
                            <?php }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function updateproductStatus(product_id, status) {
        if (confirm("Are you sure ?")) {
            $.getJSON("<?= base_url(AppConst::INDEX . 'admin/update-product-status?product_id=') ?>" + product_id + "&status=" + status,
                function(res) {
                    window.location.reload();
                });
        }
    }
</script>