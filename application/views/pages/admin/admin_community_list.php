<?php
$communityList = isset($data['community-list']) ? $data['community-list'] : array();
$filter = isset($data['filter']) ? $data['filter'] : array();
$links = isset($data['links']) ? $data['links'] : array();
$no = isset($data['ROLLNO']) ? $data['ROLLNO'] : 1;
?>

<section class="content-header">
    <h1>
        COMMUNITY MANAGEMENT
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Community List</li>
    </ol>
</section>
<?php /*
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        SEARCH COMMUNITY
                        <span> <?= getFeedbackMessage() ?></span>
                    </div>
                    <div class="panel-body">
                        <?php
                        echo form_open_multipart(AppConst::INDEX . 'admin/community-list');
                        ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label >Blood Group</label>
                                    <?php $blood_group = getval($filter, 'blood_group'); ?>
                                    <select name="blood_group"  class="form-control">
                                        <option value="">-Select-</option>
                                        <option value="<?= AppConst::BLOOD_GROUP_A_PLUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_A_PLUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_A_PLUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_A_MINUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_A_MINUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_A_MINUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_B_PLUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_B_PLUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_B_PLUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_B_MINUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_B_MINUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_B_MINUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_O_PLUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_O_PLUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_O_PLUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_O_MINUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_O_MINUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_O_MINUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_AB_PLUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_AB_PLUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_AB_PLUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_AB_MINUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_AB_MINUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_AB_MINUS ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group" style="padding-top: 25px;">
                                    <button type="submit" class="btn btn-microsoft">&nbsp;&nbsp;&nbsp; Search &nbsp;&nbsp;&nbsp;</button>
                                    <a href="<?= base_url('admin/community-list'); ?>" class="btn btn-danger">&nbsp;&nbsp;&nbsp;&nbsp; Reset &nbsp;&nbsp;&nbsp;&nbsp;</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section> 
*/?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="pull-right" style="padding-top: 4px;padding-right: 8px;">
                         <a class="btn btn-primary pull-right" href="<?php echo base_url(AppConst::INDEX . 'admin/add-community'); ?>" title="New Community"><i class="fa fa-plus"></i> New Community</a> 
                    </div>
                    <div class="panel-heading">
                        COMMUNITY LIST
                        <span><?= getFeedbackMessage() ?></span>
                    </div>
                    <div class="panel-body">
                        <table width="" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="10%">Community Name</th>
                                    <th width="10%">Adress</th>
                                    <th width="20%">Date of Establishment</th>
                                    <th width="7%">Image</th>
                                    <th width="7%">Status</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = $no;
                                if (count($communityList) > 0) {
                                    foreach ($communityList as $community) {
                                        ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= getval($community, 'community_name') ?></td>
                                            <td><?= getval($community, 'community_address')?></td>
                                            <td><?= dbDate2UIdate(getval($community, 'date_of_establishment'),FALSE) ?></td>
                                            <?php $imageUrl = getval($community, 'community_image_url'); ?>
                                            <td><?php if($imageUrl!=''){ ?>
                                                <img src="<?= base_url($imageUrl)?>" width="80px" height="80px">
                                            <?php } ?>
                                            </td>
                                            <?php $status = getval($community, 'community_status'); ?>
                                            <td><?php if ($status == AppConst::STATUS_ACTIVE) { ?>
                                                    <label class="label bg-green"> Active</label>
                                                <?php } else if ($status == AppConst::STATUS_BLOCK) { ?>
                                                    <label class="label bg-red">Blocked</label>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <a class="btn bg-blue" title="Edit <?= getval($community, 'community_name')?>" href="<?= base_url('admin/add-community?community_id='. getval($community, 'community_id'))?>"><i class="fa fa-edit"></i>Edit</a>
                                                <?php if($status == AppConst::STATUS_ACTIVE){ ?>
                                                <a class="btn bg-red" title="Block <?= getval($community, 'community_name')?> " onclick="updateCommunityStatus('<?= getval($community, 'community_id')?>','<?= AppConst::STATUS_BLOCK?>')">Block</a>
                                                <?php }else if($status == AppConst::STATUS_BLOCK){ ?>
                                                <a class="btn bg-green" title=" Active <?= getval($community, 'community_name')?> " onclick="updateCommunityStatus('<?= getval($community, 'community_id')?>','<?= AppConst::STATUS_ACTIVE?>')">Active</a>
                                                <?php } ?>
                                            </td>

                                            <?php
                                            $i++;
                                        }
                                        ?>
                                    </tr>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="8" class="text-center">No Community Found</td>
                                    </tr>
                                <?php } ?>
                            <tbody>
                        </table>
                        <ul class=" pagination pull-right">

                            <!-- Show pagination links -->
                            <?php foreach ($links as $link) { ?>
                                <li><?= $link ?></li>
                                <?php }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function updateCommunityStatus(community_id, status)
    {
        if (confirm("Are you sure ?"))
        {
            $.getJSON("<?= base_url(AppConst::INDEX . 'admin/update-community-status?community_id=') ?>" + community_id + "&status=" + status,
                    function (res) {
                        window.location.reload();
                    });
        }
    }
</script>