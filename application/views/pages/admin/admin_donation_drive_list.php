<?php
$driveList = isset($data['drive-list']) ? $data['drive-list'] : array();
$filter = isset($data['filter']) ? $data['filter'] : array();
$links = isset($data['links']) ? $data['links'] : array();
$no = isset($data['ROLLNO']) ? $data['ROLLNO'] : 1;
?>

<section class="content-header">
    <h1>
    DONATION DRIVE MANAGEMENT
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Drive List</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <!-- <div class="pull-right" style="padding-top: 4px;padding-right: 8px;">
                         <a class="btn btn-primary pull-right" href="<?php echo base_url(AppConst::INDEX . 'admin/add-drive'); ?>" title="New drive"><i class="fa fa-plus"></i> New Drive</a> 
                    </div> -->
                    <div class="panel-heading">
                        DRIVE LIST
                        <span><?= getFeedbackMessage() ?></span>
                    </div>
                    <div class="panel-body">
                        <table width="" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="10%">Drive Title</th>
                                    <!-- <th width="10%">Logo</th> -->
                                    <th width="10%">Date</th>
                                    <th width="20%">Description</th>
                                    <th width="10%">Details</th>
                                    <th width="10%">Created On</th>
                                    <th width="10%">Status</th>
                                    <th width="7%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = $no;
                                if (count($driveList) > 0) {
                                    foreach ($driveList as $drive) {
                                        ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= getval($drive, 'title') ?></td>
                                            <!-- <?php $imageUrl = getval($drive, 'image_url'); ?>
                                            <td><?php if ($imageUrl != '') { ?>
                                                    <img src="<?= base_url($imageUrl) ?>" width="80px" height="80px">
                                                <?php } ?>
                                            </td> -->
                                            <td><?= dbDate2UIdate(getval($drive, 'event_date'),FALSE) ?></td>
                                            <td><?= getval($drive, 'description') ?></td>
                                            <?php $imageUrl = getval($drive, 'banner_image'); ?>
                                            <td><label>Community Name:</label> <?= getval($drive, 'community_name') ?><br>
                                            <label>Place:</label> <?= getval($drive, 'place') ?><br>
                                            <label>Created By:</label> <?= getval($drive, 'first_name')."".getval($drive, 'last_name')  ?><br>
                                            <!-- <label>Community Name:</label><?= getval($drive, 'community_name') ?><br> -->
                                            <?php if ($imageUrl != '') { ?>
                                                    <img src="<?= base_url($imageUrl) ?>" width="160px" height="80px">
                                                <?php } ?>
                                            </td>
                                            <td><?= dbDate2UIdate(getval($drive, 'created_at'),FALSE) ?></td>
                                            <?php $status = getval($drive, 'drive_status'); ?>
                                            <td><?php if ($status == AppConst::STATUS_ACTIVE) { ?>
                                                    <label class="label bg-green"> Active</label>
                                                <?php } else if ($status == AppConst::STATUS_BLOCK) { ?>
                                                    <label class="label bg-red">Blocked</label>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <!-- <a class="btn bg-blue" title="Edit <?= getval($drive, 'drive_name')?>" href="<?= base_url('admin/add-drive?drive_id='. getval($drive, 'id'))?>"><i class="fa fa-edit"></i>Edit</a> -->
                                                <?php if($status == AppConst::STATUS_ACTIVE){ ?>
                                                <a class="btn bg-red" title="Block <?= getval($drive, 'drive_name')?> " onclick="updateDriveStatus('<?= getval($drive, 'id')?>','<?= AppConst::STATUS_BLOCK?>')">Block</a>
                                                <?php }else if($status == AppConst::STATUS_BLOCK){ ?>
                                                <a class="btn bg-green" title=" Active <?= getval($drive, 'drive_name')?> " onclick="updateDriveStatus('<?= getval($drive, 'id')?>','<?= AppConst::STATUS_ACTIVE?>')">Active</a>
                                                <?php } ?>
                                                <a class="btn bg-yellow" style="margin-top: 2px;" title="view" href="<?= base_url('admin/view-donated_users?drive_id='. getval($drive, 'id'))?>">Donated users</a>
                                            </td>

                                            <?php
                                            $i++;
                                        }
                                        ?>
                                    </tr>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="8" class="text-center">No Drive Found</td>
                                    </tr>
                                <?php } ?>
                            <tbody>
                        </table>
                        <ul class=" pagination pull-right">

                            <!-- Show pagination links -->
                            <?php foreach ($links as $link) { ?>
                                <li><?= $link ?></li>
                                <?php }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function updateDriveStatus(drive_id, status)
    {
        if (confirm("Are you sure ?"))
        {
            $.getJSON("<?= base_url(AppConst::INDEX . 'admin/update-drive-status?drive_id=') ?>" + drive_id + "&status=" + status,
                    function (res) {
                        window.location.reload();
                    });
        }
    }
</script>