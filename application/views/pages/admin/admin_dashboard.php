<?php
$dashboard = isset($data['dashboard']) ? $data['dashboard'] : array();
$session = isset($data['session']) ? $data['session'] : array();
?>

<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= getval($dashboard, 'registered_user_count')?></h3>
                    <p>Total Registered Users</p>
                </div>
                <a href="<?= base_url('admin/registered-user-list')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?= getval($dashboard, 'unregistered_user_count')?></h3>
                    <p>Total Unregistered Users</p>
                </div>
                <a href="<?= base_url('admin/unregistered-user-list')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?= getval($dashboard, 'request_count')?></h3>
                    <p>Total Requests</p>
                </div>
                <a href="<?= base_url('admin/request-list')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?= getval($dashboard, 'hospital_count')?></h3>
                    <p>Total Hospitals</p>
                </div>
                <a href="<?= base_url('admin/hospital-list')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
</section>






