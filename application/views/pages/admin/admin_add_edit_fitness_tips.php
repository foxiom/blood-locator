<?php
$fitnessData = isset($data['fitness-data']) ? $data['fitness-data'] : array();

?>
<link rel="stylesheet" href="<?= base_url('webres/admin/css/jquery-ui.css')?>">
<script src="<?= base_url('webres/admin/js/jquery-ui.js')?>"></script>
<section class="content-header">FITNESS TIPS
    <h1>
        
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Fitness List</a></li>
        <li class="active">Add Fitness Tip</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        ADD/EDIT FITNESS TIP
                    </div>
                    <div class="box-body">
                        <?php
                        $formAttributes = array('id' => 'form', 'data-parsley-validate' => '', 'class' => 'form', 'enctype' => 'multipart/form-data');
                        echo form_open_multipart(AppConst::INDEX . 'admin/add-fitness-tip', $formAttributes);
                        ?>
                        <input type="hidden" name="tip_id" value="<?= getval($fitnessData, 'id') ?>"/>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Title <span class="required-color">*</span></label>
                                    <input type="text" name="title" autocomplete="off" class="form-control"  required=""   value="<?= getval($fitnessData, 'title'); ?>" placeholder="Title" >
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label >Description <span class="required-color">*</span></label>
                                    <textarea name="description" autocomplete="off" class="form-control"  required=""  placeholder="Description" ><?= getval($fitnessData, 'description'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Category Type</label>
                                    <select class="form-control select2" style="width:100%" name="category_type" required="">
                                        <option value="">-Select-</option>
                                            <option value="<?= AppConst::BEGINNER_ROUTINE ?>" <?= ( AppConst::BEGINNER_ROUTINE == getval($fitnessData, 'category_type')) ? 'selected=""' : '' ?>>Beginner Routine</option>
                                            <option value="<?= AppConst::WORKOUT_ROUTINE ?>" <?= (AppConst::WORKOUT_ROUTINE == getval($fitnessData, 'category_type')) ? 'selected=""' : '' ?>>Workout Routine</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label>Vedio Url <span class="required-color">*</span></label>
                                    <input type="text" name="vedio_url" autocomplete="off" class="form-control"  required=""   value="<?= getval($fitnessData, 'vedio_url'); ?>" placeholder="Vedio Url" >
                                </div>
                            </div>
                        </div>
                       
                        <hr>
                    </div>
                    <div class="box-footer">
                        <button type="submit" id="btn_save" class="btn btn-microsoft pull-right btn_submit">&nbsp;&nbsp;&nbsp; Save &nbsp;&nbsp;&nbsp;</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


