<script src="<?= base_url('webres/admin/plugins/jQuery/jquery-3.1.1.min.js') ?>"></script>
<?php
$userInfo = isset($data['user-info']) ? $data['user-info'] : array();
?>

<section class="content-header">
    <h1>
        PROFILE MANAGEMENT
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a class="active">Profile</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        PROFILE DETAILS
                        <span><?= getFeedbackMessage() ?></span>
                    </div>
                    <div class="box-body">
                        <?= form_open_multipart('admin/updateprofile'); ?>
                        <input type="hidden" name="user_id" value="<?= getval($userInfo, 'user_id') ?>">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-info" style="width: 100px; cursor: default;">Username</button>
                                        </div>
                                        <input type="text" name="username" autocomplete="off" disabled="" value="<?= getval($userInfo, 'username'); ?>" class="form-control" required="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-info" style="width: 100px; cursor: default;">Firstname</button>
                                        </div>
                                        <input type="text" id="firstname" name="first_name" autocomplete="off" value="<?= getval($userInfo, 'first_name'); ?>" class="form-control" required="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-info" style="width: 100px; cursor: default;">Lastname</button>
                                        </div>
                                        <input type="text" id="lastname" name="last_name" autocomplete="off" value="<?= getval($userInfo, 'last_name'); ?>" class="form-control" required="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-info" style="width: 100px; cursor: default;">Email</button>
                                        </div>
                                        <input type="text" id="email" name="email" autocomplete="off" value="<?= getval($userInfo, 'email'); ?>" class="form-control" required="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-info" style="width: 100px; cursor: default;">Contact No</button>
                                        </div>
                                        <input type="text" id="contactno" name="contact_no" autocomplete="off" value="<?= getval($userInfo, 'contact_no'); ?>" class="form-control" required="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="box-footer pull-right">
                                    <button type="submit" id="profile" class="btn btn-microsoft">&nbsp;&nbsp;&nbsp; Save &nbsp;&nbsp;&nbsp;</button>
                                </div>
                                </form>
                                <div class="form-group">
                                    <a onclick="showPasswordSection()" style="cursor: pointer;"> Change Password ?</a>
                                </div>
                            </div>
                        </div>
                        <div id="changepassord" style="display: none">
                            <?= form_open_multipart('admin/changepassword'); ?>
                            <input type="hidden" name="user_id" value="<?= getval($userInfo, 'user_id') ?>">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-info" style="width: 125px; cursor: default;">Old Password</button>
                                            </div>
                                            <input type="text" name="oldpassword" autocomplete="off" class="form-control" required="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-info" style="width: 125px; cursor: default;">New Password</button>
                                            </div>
                                            <input type="text" name="newpassword" autocomplete="off"class="form-control" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-info" style="width: 135px; cursor: default;">Confirm Password</button>
                                            </div>
                                            <input type="text" name="confirmpassword" autocomplete="off" class="form-control" required="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="box-footer pull-right">
                                        <button type="submit" class="btn btn-microsoft">&nbsp;&nbsp;&nbsp; Save &nbsp;&nbsp;&nbsp;</button>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    // hide and show
    function showPasswordSection() {
        var show = document.getElementById("changepassord");
        var profile = document.getElementById('profile');
        var firstname = document.getElementById('firstname');
        var lastname = document.getElementById('lastname');
        var email = document.getElementById('email');
        var contactno = document.getElementById('contactno');
        if (show.style.display === "none") {
            show.style.display = "block";
            profile.style.display = "none";
            firstname.disabled = "true";
            lastname.disabled = "true";
            email.disabled = "true";
            contactno.disabled = "true";
        } else {
            show.style.display = "none";
            profile.style.display = "block";
            firstname.disabled = "";
            lastname.disabled = "";
            email.disabled = "";
            contactno.disabled = "";
        }
    }

    // check new and confirm password same
    function checkPassword(form) {
        newpassword = form.newpassword.value;
        confirmpassword = form.confirmpassword.value;
        if (newpassword != confirmpassword) {
            alert("\nPassword did not match: Please try again...")
            return false;
        }
    }
</script>