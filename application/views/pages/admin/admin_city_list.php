<?php
$session = isset($data['session']) ? $data['session'] : '';
$filter = isset($data['filter']) ? $data['filter'] : array();
$countrylist = isset($data['country-list']) ? $data['country-list'] : array();
$links = isset($data['links']) ? $data['links'] : array();
$no = isset($data['ROLLNO']) ? $data['ROLLNO'] : 1;
?>

<section class="content-header">
    <h1>
        STATE/PROVENANCE MANAGEMENT
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">State/Provenance List</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <header class="panel-heading">
                        <div class="row">
                            <div class="col-sm-8">
                                <h4> <i class="fa fa-list-ul"></i>  
                                    List of States/Provenances
                                </h4>
                                <span><?= getFeedbackMessage() ?></span>
                            </div>
                            <div class="col-sm-4 text-right">
                                <a href="javascript:;" data-toggle="modal" onclick="clearForm()" data-target="#modal-form" class="btn btn-success "> <i class="fa fa-plus"> </i> Add State/Provenance</a> 

                            </div>
                        </div>
                    </header>
                    <?php
                    $citylist = isset($data['city-list']) ? $data['city-list'] : array();
                    ?>
                    <div class="table-responsive panel-body">
                        <table class="table table-bordered table-striped m-b-none" id="datatables">
                            <thead>
                                <tr>
                                    <th width="10%">No.</th>
                                    <th width="20%">State/Provenance Name</th>
                                    <th width="15%">State/Provenance Code & Id</th>
                                    <th width="20%">Country Name</th>
                                    <th width="10%">Country Code</th>
                                    <th width="10%">Status</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = $no;

                                foreach ($citylist as $city) {
                                    ?>

                                    <tr>
                                        <td>
                                            <?=
                                            $i;
                                            $i++
                                            ?>
                                        </td>

                                        <td>
                                            <?= getval($city, 'state_name') ?>

                                        </td>
                                        <td>
                                            <?= getval($city, 'state_code').' - ('. getval($city, 'state_id').')' ?>

                                        </td>
                                        <td>
                                            <?= getval($city, 'country_name') ?>


                                        </td>
                                        <td>
                                            <?= getval($city, 'country_code').' - ('. getval($city, 'country_id').')' ?>

                                        </td>
                                        <td>
                                            <?php if(getval($city, 'status') == AppConst::STATUS_ACTIVE){ ?>
                                            <label class="label bg-green-gradient">Active</label>
                                            <?php }else if(getval($city, 'status') == AppConst::STATUS_BLOCK){ ?>
                                            <label class="label bg-red-gradient">Blocked</label>
                                            <?php } ?>
                                        </td>
                                        <td><a  onclick="editRow('<?= getval($city, 'state_id') ?>')" class="btn bg-orange btn-xs" > 
                                                <i class="glyphicon glyphicon-edit" ></i> 
                                            </a>
                                            <?php if(getval($city, 'status') == AppConst::STATUS_ACTIVE){ ?>
                                            <a class="btn btn-danger" title="Block <?= getval($city, 'state_name')?>" onclick="updateCityStatus('<?= getval($city, 'state_id')?>','<?= getval($city, 'country_id')?>','<?= AppConst::STATUS_BLOCK?>')">Block</a>
                                            <?php }else if(getval($city, 'status') == AppConst::STATUS_BLOCK){ ?>
                                            <a class="btn btn-success" title="Active <?= getval($city, 'state_name')?>" onclick="updateCityStatus('<?= getval($city, 'state_id')?>','<?= getval($city, 'country_id')?>','<?= AppConst::STATUS_ACTIVE?>')">Active</a>
                                            <?php } ?>
                                        </td>



                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                        <ul class=" pagination pull-right">

                            <!-- Show pagination links -->
                            <?php foreach ($links as $link) { ?>
                                <li><?= $link ?></li>
                            <?php }
                            ?>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal-form" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title" >State/Provenance</label>                
            </div>
            <?php
            $formAttributes = array('id' => 'form', 'data-parsley-validate' => '', 'class' => 'form-horizontal');

            echo form_open(AppConst::INDEX . 'city-list', $formAttributes);
            ?>              

            <input type="hidden" name="state_id" id="id" value="" />

            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label col-xs-4" required >Country Name</label>
                    <div class="col-xs-8">
                        <select class="form-control" id="countryId" name="country_id" required="">
                            <option value="">-Select-</option>
                            <?php foreach ($countrylist as $country) { ?>
                                <option value="<?= getval($country, 'country_id') ?>"><?= getval($country, 'country_name') ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-4" required >State/Provenance Name</label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control" name="state_name" id="name" 
                               required
                               placeholder="State/Provenance Name" autocomplete="off" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-4" required >City/State Code</label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control" name="state_code" id="code" 

                               placeholder="State/Provenance Code" autocomplete="off" />
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" >Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" >Close</button>
            </div>          
            </form>              
        </div>
    </div>
</div>

<script type="text/javascript">
    function updateCityStatus(city_id, country_id,status)
    {
        if (confirm("Are you sure ?"))
        {
            $.getJSON("<?= base_url(AppConst::INDEX . 'admin/update-city-status?city_id=') ?>" + city_id +"&country_id="+country_id+ "&status=" + status,
                    function (res) {
                        window.location.reload();
                    });
        }
    }

    function editRow(id)
    {

        $.getJSON("<?= base_url(AppConst::INDEX . 'get-city?id=') ?>" + id,
                function (row) {

                    if (row.values.length === 1) {

                        $("#id").val(row.values[0].state_id);
                        $("#name").val(row.values[0].state_name);
                        $("#code").val(row.values[0].state_code);
                        $("#countryId").val(row.values[0].country_id);

                        $("#modal-form").modal("show");
                    } else {
                        alert('something went wrong');
                    }

                });
    }


    function clearForm()
    {
        $("#id").val(0);
        $("#name").val('');
        $("#code").val('');
        $("#countryId").val('');
    }
</script>