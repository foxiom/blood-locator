<?php
$requestInfo = isset($data['request-info']) ? $data['request-info'] : array();
?>
<link rel="stylesheet" href="<?= base_url('webres/admin/css/jquery-ui.css')?>">
<script src="<?= base_url('webres/admin/js/jquery-ui.js')?>"></script>
<section class="content-header">
    <h1>
        REQUEST MANAGEMENT
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Request List</a></li>
        <li class="active">Add Request</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        ADD/EDIT REQUEST
                    </div>
                    <div class="box-body">
                        <?php
                        $formAttributes = array('id' => 'form', 'data-parsley-validate' => '', 'class' => 'form', 'enctype' => 'multipart/form-data');
                        echo form_open_multipart(AppConst::INDEX . 'admin/add-request', $formAttributes);
                        ?>
                        <input type="hidden" name="request_id" value="<?= getval($requestInfo, 'request_id') ?>"/>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Date Of Need (DD-MM-YYYY)</label>

                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <?php if (getval($requestInfo, 'date_of_need') != '0000-00-00' && getval($requestInfo, 'date_of_need') != '') { ?>
                                            <input type="text" class="form-control pull-right datepicker1" autocomplete="off" name="date_of_need" value="<?= dbDate2UIdate(getval($requestInfo, 'date_of_need'), FALSE) ?>" required="" >
                                        <?php } else { ?>
                                            <input type="text" class="form-control pull-right datepicker1" autocomplete="off" name="date_of_need" value="" required="" >
                                        <?php } ?>

                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >No Of Units <span class="required-color">*</span></label>
                                    <input type="text"  name="no_of_units" autocomplete="off" class="form-control"  value="<?= getval($requestInfo, 'contact_no') ?>" required data-parsley-trigger="keyup" 
                                           data-parsley-type="number"   placeholder="No Of Units">

                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Donation Address <span class="required-color">*</span></label>
                                    <textarea class="form-control" name="donation_address" required="" placeholder="Donation Location"><?= getval($requestInfo, 'donation_location') ?></textarea>
                                </div>
                            </div>

                        </div>
                        <div class="row">


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Blood Group<span class="required-color">*</span></label>
                                    <?php $blood_group = getval($requestInfo, 'blood_group'); ?>
                                    <select class="form-control" name="blood_group" required=""> 
                                        <option value="">-Select-</option>
                                        <option value="<?= AppConst::BLOOD_GROUP_A_PLUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_A_PLUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_A_PLUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_A_MINUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_A_MINUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_A_MINUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_B_PLUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_B_PLUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_B_PLUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_B_MINUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_B_MINUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_B_MINUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_O_PLUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_O_PLUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_O_PLUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_O_MINUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_O_MINUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_O_MINUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_AB_PLUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_AB_PLUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_AB_PLUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_AB_MINUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_AB_MINUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_AB_MINUS ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Contact Person <span class="required-color">*</span></label>
                                    <input type="text" name="contact_person" autocomplete="off" class="form-control" data-parsley-pattern="^[a-zA-Z\s.\-_]+$" required="" data-parsley-pattern-message="This is not a valid name" data-parsley-trigger="keyup" value="<?= getval($requestInfo, 'contact_person'); ?>" placeholder="Contact Person">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Contact No <span class="required-color">*</span></label>
                                    <input type="text"  name="contact_no" autocomplete="off" class="form-control"  value="<?= getval($requestInfo, 'contact_no') ?>" required data-parsley-trigger="keyup" 
                                           data-parsley-type="number"  data-parsley-minlength="10" data-parsley-maxlength="10" placeholder="Contact No">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="inputSuccess">Location Name</label>
                                    <input type="text"  class="form-control" id="us1-address" placeholder="Enter a location" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="hidden" class="form-control" id="us1-radius" value="3">
                                        <div id="us1" style="width: 100%; height: 400px; position: relative; overflow: hidden;">
                                            <div style="height: 100%; width: 100%; position: absolute; background-color: rgb(229, 227, 223);">
                                                <div class="gm-style" style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0;">
                                                    <div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;) 8 8, default;">
                                                        <div style="position: absolute; left: 0px; top: 0px; z-index: 1; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);">
                                                            <div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;">
                                                                <div style="position: absolute; left: 0px; top: 0px; z-index: 0;">
                                                                    <div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;">
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: 8px; top: 46px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: 264px; top: 46px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: 264px; top: -210px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: 8px; top: -210px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: 8px; top: 302px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: 264px; top: 302px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: -248px; top: 46px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: 520px; top: 46px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: -248px; top: 302px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: 520px; top: -210px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: -248px; top: -210px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: 520px; top: 302px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;">
                                                                <div style="position: absolute; left: 0px; top: 0px; z-index: 30;">
                                                                    <div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;">
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 8px; top: 46px;">
                                                                            <canvas width="256" height="256" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; position: absolute; left: 0px; top: 0px;"></canvas>
                                                                        </div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 264px; top: 46px;">
                                                                            <canvas width="256" height="256" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; position: absolute; left: 0px; top: 0px;"></canvas>
                                                                        </div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 264px; top: -210px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 8px; top: -210px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 8px; top: 302px;">
                                                                            <canvas width="256" height="256" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; position: absolute; left: 0px; top: 0px;"></canvas>
                                                                        </div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 264px; top: 302px;">
                                                                            <canvas width="256" height="256" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; position: absolute; left: 0px; top: 0px;"></canvas>
                                                                        </div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -248px; top: 46px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 520px; top: 46px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -248px; top: 302px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 520px; top: -210px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -248px; top: -210px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 520px; top: 302px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div>
                                                            <div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;">
                                                                <div style="position: absolute; left: 0px; top: 0px; z-index: -1;">
                                                                    <div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;">
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 8px; top: 46px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 264px; top: 46px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 264px; top: -210px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 8px; top: -210px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 8px; top: 302px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 264px; top: 302px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -248px; top: 46px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 520px; top: 46px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -248px; top: 302px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 520px; top: -210px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -248px; top: -210px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 520px; top: 302px;"></div>
                                                                    </div>
                                                                </div>
                                                                <div style="width: 22px; height: 40px; overflow: hidden; position: absolute; left: 264px; top: 160px; z-index: 200;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 22px; height: 40px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                            </div>
                                                            <div style="position: absolute; z-index: 0; left: 0px; top: 0px;">
                                                                <div style="overflow: hidden; width: 550px; height: 400px;"><img src="https://maps.googleapis.com/maps/api/js/StaticMapService.GetMapImage?1m2&amp;1i4258040&amp;2i2979026&amp;2e1&amp;3u15&amp;4m2&amp;1u550&amp;2u400&amp;5m5&amp;1e0&amp;5sen-US&amp;6sus&amp;10b1&amp;12b1&amp;token=24794" style="width: 550px; height: 400px;"></div>
                                                            </div>
                                                            <div style="position: absolute; left: 0px; top: 0px; z-index: 0;">
                                                                <div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;">
                                                                    <div style="position: absolute; left: 264px; top: 46px; transition: opacity 200ms ease-out;">
                                                                        <div style="z-index: 0;"><img src="https://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16634!3i11637!4i256!2m3!1e0!2sm!3i366039322!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=34520" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    </div>
                                                                    <div style="position: absolute; left: 8px; top: 46px; transition: opacity 200ms ease-out;">
                                                                        <div style="z-index: 0;"><img src="https://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16633!3i11637!4i256!2m3!1e0!2sm!3i366039322!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=74241" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    </div>
                                                                    <div style="position: absolute; left: 264px; top: -210px; transition: opacity 200ms ease-out;">
                                                                        <div style="z-index: 0;"><img src="https://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16634!3i11636!4i256!2m3!1e0!2sm!3i366039322!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=129570" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    </div>
                                                                    <div style="position: absolute; left: 264px; top: 302px; transition: opacity 200ms ease-out;">
                                                                        <div style="z-index: 0;"><img src="https://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16634!3i11638!4i256!2m3!1e0!2sm!3i366037870!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=7081" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    </div>
                                                                    <div style="position: absolute; left: 520px; top: 46px; transition: opacity 200ms ease-out;">
                                                                        <div style="z-index: 0;"><img src="https://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16635!3i11637!4i256!2m3!1e0!2sm!3i366037870!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=62410" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    </div>
                                                                    <div style="position: absolute; left: 8px; top: -210px; transition: opacity 200ms ease-out;">
                                                                        <div style="z-index: 0;"><img src="https://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16633!3i11636!4i256!2m3!1e0!2sm!3i366039322!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=38220" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    </div>
                                                                    <div style="position: absolute; left: 8px; top: 302px; transition: opacity 200ms ease-out;">
                                                                        <div style="z-index: 0;"><img src="https://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16633!3i11638!4i256!2m3!1e0!2sm!3i366037870!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=46802" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    </div>
                                                                    <div style="position: absolute; left: 520px; top: -210px; transition: opacity 200ms ease-out;">
                                                                        <div style="z-index: 0;"><img src="https://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16635!3i11636!4i256!2m3!1e0!2sm!3i366037870!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=26389" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    </div>
                                                                    <div style="position: absolute; left: 520px; top: 302px; transition: opacity 200ms ease-out;">
                                                                        <div style="z-index: 0;"><img src="https://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16635!3i11638!4i256!2m3!1e0!2sm!3i366037870!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=98431" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    </div>
                                                                    <div style="position: absolute; left: -248px; top: 46px; transition: opacity 200ms ease-out;">
                                                                        <div style="z-index: 0;"><img src="https://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16632!3i11637!4i256!2m3!1e0!2sm!3i366039322!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=113962" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    </div>
                                                                    <div style="position: absolute; left: -248px; top: 302px; transition: opacity 200ms ease-out;">
                                                                        <div style="z-index: 0;"><img src="https://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16632!3i11638!4i256!2m3!1e0!2sm!3i366034009!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=45327" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    </div>
                                                                    <div style="position: absolute; left: -248px; top: -210px; transition: opacity 200ms ease-out;">
                                                                        <div style="z-index: 0;"><img src="https://maps.google.com/maps/vt?pb=!1m5!1m4!1i15!2i16632!3i11636!4i256!2m3!1e0!2sm!3i366039322!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=77941" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gm-style-pbc" style="position: absolute; left: 0px; top: 0px; z-index: 2; width: 100%; height: 100%; transition-duration: 0.3s; opacity: 0; display: none;">
                                                            <p class="gm-style-pbt">Use two fingers to move the map</p>
                                                        </div>
                                                        <div style="position: absolute; left: 0px; top: 0px; z-index: 3; width: 100%; height: 100%;"></div>
                                                        <div style="position: absolute; left: 0px; top: 0px; z-index: 4; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);">
                                                            <div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div>
                                                            <div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div>
                                                            <div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;">
                                                                <div class="gmnoprint" style="width: 22px; height: 40px; overflow: hidden; position: absolute; opacity: 0.01; left: 264px; top: 160px; z-index: 200;">
                                                                    <img src="https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi.png" draggable="false" usemap="#gmimap2" style="position: absolute; left: 0px; top: 0px; width: 22px; height: 40px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;">
                                                                    <map name="gmimap2" id="gmimap2">
                                                                        <area href="javascript:void(0)" log="miw" coords="8,0,5,1,4,2,3,3,2,4,2,5,1,6,1,7,0,8,0,14,1,15,1,16,2,17,2,18,3,19,3,20,4,21,5,22,5,23,6,24,7,25,7,27,8,28,8,29,9,30,9,33,10,34,10,40,11,40,11,34,12,33,12,30,13,29,13,28,14,27,14,25,15,24,16,23,16,22,17,21,18,20,18,19,19,18,19,17,20,16,20,15,21,14,21,8,20,7,20,6,19,5,19,4,18,3,17,2,16,1,14,1,13,0,8,0" shape="poly" title="Drag Me" style="cursor: pointer;">
                                                                    </map>
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;">
                                                                <div style="z-index: -202; cursor: pointer; display: none;">
                                                                    <div style="width: 30px; height: 27px; overflow: hidden; position: absolute;"><img src="https://maps.gstatic.com/mapfiles/undo_poly.png" draggable="false" style="position: absolute; left: 0px; top: 0px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 90px; height: 27px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;">
                                                        <a target="_blank" href="https://maps.google.com/maps?ll=46.152424,2.74707&amp;z=15&amp;t=m&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3" title="Click to see this area on Google Maps" style="position: static; overflow: visible; float: none; display: inline;">
                                                            <div style="width: 66px; height: 26px; cursor: pointer;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/google4.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div>
                                                        </a>
                                                    </div>
                                                    <div style="background-color: white; padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 125px; top: 110px;">
                                                        <div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div>
                                                        <div style="font-size: 13px;">Map data ©2016 Google</div>
                                                        <div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/mapcnt6.png" draggable="false" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                    </div>
                                                    <div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 166px; bottom: 0px; width: 121px;">
                                                        <div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px;">
                                                            <div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;">
                                                                <div style="width: 1px;"></div>
                                                                <div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div>
                                                            </div>
                                                            <div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Map Data</a><span>Map data ©2016 Google</span></div>
                                                        </div>
                                                    </div>
                                                    <div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;">
                                                        <div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Map data ©2016 Google</div>
                                                    </div>
                                                    <div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; -webkit-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 95px; bottom: 0px;">
                                                        <div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;">
                                                            <div style="width: 1px;"></div>
                                                            <div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div>
                                                        </div>
                                                        <div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/intl/en-US_US/help/terms_maps.html" target="_blank" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Terms of Use</a></div>
                                                    </div>
                                                    <div style="width: 25px; height: 25px; overflow: hidden; display: none; margin: 10px 14px; position: absolute; top: 0px; right: 0px;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/sv9.png" draggable="false" class="gm-fullscreen-control" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 175px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div>
                                                    <div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;">
                                                        <div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;">
                                                            <div style="width: 1px;"></div>
                                                            <div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div>
                                                        </div>
                                                        <div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_new" title="Report errors in the road map or imagery to Google" href="https://www.google.com/maps/@46.1524244,2.7470703,15z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Report a map error</a></div>
                                                    </div>
                                                    <div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" draggable="false" controlwidth="28" controlheight="55" style="margin: 10px; -webkit-user-select: none; position: absolute; bottom: 69px; right: 28px;">
                                                        <div class="gmnoprint" controlwidth="28" controlheight="55" style="position: absolute; left: 0px; top: 0px;">
                                                            <div draggable="false" style="-webkit-user-select: none; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-radius: 2px; cursor: pointer; background-color: rgb(255, 255, 255); width: 28px; height: 55px;">
                                                                <div title="Zoom in" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;">
                                                                    <div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: 0px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div>
                                                                </div>
                                                                <div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; background-color: rgb(230, 230, 230); top: 0px;"></div>
                                                                <div title="Zoom out" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;">
                                                                    <div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: -15px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gmnoprint" controlwidth="28" controlheight="0" style="display: none; position: absolute;">
                                                            <div title="Rotate map 90 degrees" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; cursor: pointer; background-color: rgb(255, 255, 255); display: none;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                            <div class="gm-tilt" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; top: 0px; cursor: pointer; background-color: rgb(255, 255, 255);"><img src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="row">
                                            <div class="col-md-12"><label class="control-label label-warning" >Move red mark on Map to your exact location. Address will change automatically</label></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">


                                        <div class="col-lg-12"> 
                                        </div>
                                        <div class="row">

                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="control-label" for="inputSuccess">Latitude</label>
                                                    <?php
                                                    $lat = getval($requestInfo, 'location_lat');
                                                    ?>
                                                    <input type="text" class="form-control" id="us1-lat" placeholder="Latitude" name="location_lat" 
                                                           value="<?= $lat ?>"  required="" readonly="">
                                                    <span class="help-inline text-danger"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-md-offset-1">
                                                <div class="form-group">
                                                    <label class="control-label" for="inputSuccess">Longitude</label>
                                                    <?php $lon = getval($requestInfo, 'location_long'); ?>
                                                    <input type="text" class="form-control" id="us1-lon"  placeholder="Longitude" name="location_long"
                                                           value="<?= $lon ?>" readonly="" required="">
                                                    <span class="help-inline text-danger"></span>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="clearfix">&nbsp;</div>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" id="btn_save" class="btn btn-microsoft pull-right btn_submit">&nbsp;&nbsp;&nbsp; Save &nbsp;&nbsp;&nbsp;</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(".datepicker1").datepicker({
        dateFormat: "dd-mm-yy",

    });
    $('#us1').locationpicker({
        location: {
            latitude: $("#us1-lat").val(),
            longitude: $("#us1-lon").val()
        },
        radius: 500,
        inputBinding: {
            latitudeInput: $('#us1-lat'),
            longitudeInput: $('#us1-lon'),
            radiusInput: $('#us1-radius'),
            locationNameInput: $('#us1-address')
        },
        enableAutocomplete: true,
        onchanged: function (currentLocation, radius, isMarkerDropped) {


            var url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + currentLocation.latitude + ',' + currentLocation.longitude+'&sensor=false&key=<?= AppConst::GOOGLE_MAP_API_KEY?>';

            $.ajax({
                type: 'GET',
                url: url,
                async: false,
                dataType: 'json',
                success: function (data) {

                }
            });
        },
        oninitialized: function (component) {

            var addressComponents = $(component).locationpicker('map').location.addressComponents;
            updateControls(addressComponents);

        }



    });
</script>


