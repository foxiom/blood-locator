<?php
$session = isset($data['session']) ? $data['session'] : '';
$filter = isset($data['filter']) ? $data['filter'] : array();
$links = isset($data['links']) ? $data['links'] : array();
$no = isset($data['ROLLNO']) ? $data['ROLLNO'] : 1;
?>


<section class="content-header">
    <h1>
        COUNTRY MANAGEMENT
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Country List</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <header class="panel-heading">
                        <div class="row">
                            <div class="col-sm-8">
                                <h4> <i class="fa fa-list-ul"></i>  
                                    List of Countries
                                </h4>
                                <span><?= getFeedbackMessage() ?></span>
                            </div>
                            <div class="col-sm-4 text-right">
                                <a href="javascript:;" data-toggle="modal" onclick="clearForm()" data-target="#modal-form" class="btn btn-success "> <i class="fa fa-plus"> </i> Add Country</a> 

                            </div>
                        </div>
                    </header>
                    <?php $countrylist = isset($data['country-list']) ? $data['country-list'] : array();
                    ?>
                    <div class="table-responsive panel-body">
                        <table class="table table-bordered table-striped m-b-none" id="datatables">
                            <thead>
                                <tr>
                                    <th width="10%">No.</th>
                                    <th width="40%">Country Name</th>
                                    <th width="25%">Country Code & Id</th>
                                    <th width="10%">Status</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = $no;

                                foreach ($countrylist as $country) {
                                    ?>

                                    <tr>
                                        <td>
                                            <?=
                                            $i;
                                            $i++
                                            ?>
                                        </td>

                                        <td>
                                            <?= getval($country, 'country_name') ?>

                                        </td>
                                        <td>
                                            <?= getval($country, 'country_code').' - ('. getval($country, 'country_id').')' ?>

                                        </td>
                                        <td>
                                            <?php if(getval($country, 'status') == AppConst::STATUS_ACTIVE){ ?>
                                            <label class="label bg-green-gradient">Active</label>
                                            <?php }else if(getval($country, 'status') == AppConst::STATUS_BLOCK){ ?>
                                            <label class="label bg-red-gradient">Blocked</label>
                                            <?php } ?>
                                        </td>
                                        <td><a  onclick="editRow('<?= getval($country, 'country_id') ?>')" class="btn bg-orange btn-xs" > 
                                                <i class="glyphicon glyphicon-edit" ></i> 
                                            </a>
                                            <?php if(getval($country, 'status') == AppConst::STATUS_ACTIVE){ ?>
                                            <a class="btn btn-danger" title="Block <?= getval($country, 'country_name')?>" onclick="updateCountryStatus('<?= getval($country, 'country_id')?>','<?= AppConst::STATUS_BLOCK?>')">Block</a>
                                            <?php }else if(getval($country, 'status') == AppConst::STATUS_BLOCK){ ?>
                                            <a class="btn btn-success" title="Active <?= getval($country, 'country_name')?>" onclick="updateCountryStatus('<?= getval($country, 'country_id')?>','<?= AppConst::STATUS_ACTIVE?>')">Active</a>
                                            <?php } ?>
                                        </td>



                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <ul class=" pagination pull-right">

                            <!-- Show pagination links -->
                            <?php foreach ($links as $link) { ?>
                                <li><?= $link ?></li>
                                <?php }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal fade" id="modal-form" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title" >Country</label>                
            </div>
            <?php
            $formAttributes = array('id' => 'form', 'data-parsley-validate' => '', 'class' => 'form-horizontal');

            echo form_open(AppConst::INDEX . 'country-list', $formAttributes);
            ?>              

            <input type="hidden" name="country_id" id="id" value="" />

            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label col-xs-4" required >Country Name</label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control" name="country_name" id="name" 
                               required
                               placeholder="Country Name" autocomplete="off" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-xs-4" required >Country Code</label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control" name="country_code" id="code" 
                               required
                               placeholder="Country Code" autocomplete="off" />
                               <?php /* <input type="text" class="form-control" name="countryCode" id="code" 
                                 required
                                 placeholder="Country Code" autocomplete="off" /> */ ?>
                        <!--<textarea class="form-control" rows="5" name="note"></textarea>-->
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" >Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" >Close</button>
            </div>          
            </form>              
        </div>
    </div>
</div>

<script type="text/javascript">
    
    function updateCountryStatus(country_id, status)
    {
        if (confirm("Are you sure ?"))
        {
            $.getJSON("<?= base_url(AppConst::INDEX . 'admin/update-country-status?country_id=') ?>" + country_id + "&status=" + status,
                    function (res) {
                        window.location.reload();
                    });
        }
    }

    function editRow(id)
    {

        $.getJSON("<?= base_url(AppConst::INDEX . 'get-country?id=') ?>" + id,
                function (row) {

                    if (row.values.length === 1) {

                        $("#id").val(row.values[0].country_id);
                        $("#name").val(row.values[0].country_name);

                        $("#code").val(row.values[0].country_code);
                        $("#modal-form").modal("show");
                    } else {
                        alert('something went wrong');
                    }

                });
    }


    function clearForm()
    {
        $("#id").val(0);
        $("#name").val('');
        $("#code").val('');

    }
</script>
