<?php
$blogList = isset($data['blog-list']) ? $data['blog-list'] : array();
$filter = isset($data['filter']) ? $data['filter'] : array();
$links = isset($data['links']) ? $data['links'] : array();
$no = isset($data['ROLLNO']) ? $data['ROLLNO'] : 1;
?>

<section class="content-header">
    <h1>
    BLOG MANAGEMENT
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Blog List</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="pull-right" style="padding-top: 4px;padding-right: 8px;">
                         <a class="btn btn-primary pull-right" href="<?php echo base_url(AppConst::INDEX . 'admin/add-blog'); ?>" title="New blog"><i class="fa fa-plus"></i> New Blog</a> 
                    </div>
                    <div class="panel-heading">
                        BLOG LIST
                        <span><?= getFeedbackMessage() ?></span>
                    </div>
                    <div class="panel-body">
                        <table width="" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="10%">Blog Title</th>
                                    <th width="10%">Category</th>
                                    <th width="30%">Description</th>
                                    <th width="10%">Blog Details</th>
                                    <th width="10%">Created On</th>
                                    <th width="10%">Status</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = $no;
                                if (count($blogList) > 0) {
                                    foreach ($blogList as $blog) {
                                        ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= getval($blog, 'title') ?></td>
                                            <td><?= getval($blog, 'category_name') ?></td>
                                            <td><?= getval($blog, 'description') ?></td>
                                            <?php $imageUrl = getval($blog, 'image_url'); ?>
                                            <td><?php if ($imageUrl != '') { ?>
                                                    <img src="<?= base_url($imageUrl) ?>" width="80px" height="80px">
                                                <?php } ?><br>
                                                <label>Youtube Url:</label> <a target="_blank" href="<?= getval($blog, 'youtube_url') ?>"> <?= getval($blog, 'youtube_url') ?></a>
                                            </td>
                                            <td><?= dbDate2UIdate(getval($blog, 'created_at'),FALSE) ?></td>
                                            <?php $status = getval($blog, 'blog_status'); ?>
                                            <td><?php if ($status == AppConst::STATUS_ACTIVE) { ?>
                                                    <label class="label bg-green"> Active</label>
                                                <?php } else if ($status == AppConst::STATUS_BLOCK) { ?>
                                                    <label class="label bg-red">Blocked</label>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <a class="btn bg-blue" title="Edit <?= getval($blog, 'blog_name')?>" href="<?= base_url('admin/add-blog?blog_id='. getval($blog, 'id'))?>"><i class="fa fa-edit"></i>Edit</a>
                                                <?php if($status == AppConst::STATUS_ACTIVE){ ?>
                                                <a class="btn bg-red" title="Block <?= getval($blog, 'blog_name')?> " onclick="updateBlogStatus('<?= getval($blog, 'id')?>','<?= AppConst::STATUS_BLOCK?>')">Block</a>
                                                <?php }else if($status == AppConst::STATUS_BLOCK){ ?>
                                                <a class="btn bg-green" title=" Active <?= getval($blog, 'blog_name')?> " onclick="updateBlogStatus('<?= getval($blog, 'id')?>','<?= AppConst::STATUS_ACTIVE?>')">Active</a>
                                                <?php } ?>
                                            </td>

                                            <?php
                                            $i++;
                                        }
                                        ?>
                                    </tr>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="8" class="text-center">No Blog Found</td>
                                    </tr>
                                <?php } ?>
                            <tbody>
                        </table>
                        <ul class=" pagination pull-right">

                            <!-- Show pagination links -->
                            <?php foreach ($links as $link) { ?>
                                <li><?= $link ?></li>
                                <?php }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function updateBlogStatus(blog_id, status)
    {
        if (confirm("Are you sure ?"))
        {
            $.getJSON("<?= base_url(AppConst::INDEX . 'admin/update-blog-status?blog_id=') ?>" + blog_id + "&status=" + status,
                    function (res) {
                        window.location.reload();
                    });
        }
    }
</script>