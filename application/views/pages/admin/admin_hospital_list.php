<?php
$hospitalList = isset($data['hospital-list']) ? $data['hospital-list'] : array();
$filter = isset($data['filter']) ? $data['filter'] : array();
$links = isset($data['links']) ? $data['links'] : array();
$no = isset($data['ROLLNO']) ? $data['ROLLNO'] : 1;
?>

<section class="content-header">
    <h1>
        HOSPITAL MANAGEMENT
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Hospital List</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        SEARCH HOSPITAL
                        <span> <?= getFeedbackMessage() ?></span>
                    </div>
                    <div class="panel-body">
                        <?php
                        echo form_open_multipart(AppConst::INDEX . 'admin/hospital-list');
                        ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Hospital Name <span class="required-color">*</span></label>
                                    <input type="text"  name="hospital_name" autocomplete="off" class="form-control"  value="<?= getval($filter, 'hospital_name') ?>" required data-parsley-trigger="keyup" 
                                           data-parsley-type="name"   placeholder="Hospital Name">

                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group" style="padding-top: 25px;">
                                    <button type="submit" class="btn btn-microsoft">&nbsp;&nbsp;&nbsp; Search &nbsp;&nbsp;&nbsp;</button>
                                    <a href="<?= base_url('admin/hospital-list'); ?>" class="btn btn-danger">&nbsp;&nbsp;&nbsp;&nbsp; Reset &nbsp;&nbsp;&nbsp;&nbsp;</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section> 

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="pull-right" style="padding-top: 4px;padding-right: 8px;">
                        <a class="btn btn-primary pull-right" href="<?php echo base_url(AppConst::INDEX . 'admin/add-hospital'); ?>" title="New Hospital"><i class="fa fa-plus"></i> New Hospital</a>
                        <a class="btn bg-maroon pull-right" href="<?php echo base_url(AppConst::INDEX . 'admin/import-hospital-file'); ?>" title="Import File"><i class="fa fa-plus"></i> Import File</a>
                    </div>
                    <div class="panel-heading">
                        HOSPITAL LIST
                        <span><?= getFeedbackMessage() ?></span>
                    </div>
                    <div class="panel-body">
                        <table width="" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="15%">Hospital Name</th>
                                    <th width="15%">Place</th>
                                    <th width="22%">Address</th>
                                    <th width="20%">Contact Details</th>
                                    <th width="8%">Status</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = $no;
                                if (count($hospitalList) > 0) {
                                    foreach ($hospitalList as $hospital) {
                                        ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= getval($hospital, 'hospital_name') ?></td>
                                            <td><?= getval($hospital, 'place') ?></td>
                                            <td><?= getval($hospital, 'address') ?></td>
                                            <td>Mobile No : <?= getval($hospital, 'contact_no') ?><br>
                                                Mobile No 2 : <?= getval($hospital, 'contact_no2') ?></td>

                                            <?php $status = getval($hospital, 'hospital_status'); ?>
                                            <td><?php if ($status == AppConst::STATUS_ACTIVE) { ?>
                                                    <label class="label bg-green"> Active</label>
                                                <?php } else if ($status == AppConst::STATUS_BLOCK) { ?>
                                                    <label class="label bg-red">Block</label>
                                                <?php } ?>
                                            </td>
                                            <td><a class="btn label-warning btn-xs" style="float: left;display: block; width: 100%;" href="<?= base_url(AppConst::INDEX . 'admin/add-hospital?hospital_id=' . getval($hospital, 'hospital_id')); ?>" title="Edit"><i class="fa fa-edit"></i> Edit</a>
                                                <?php if ($status == AppConst::STATUS_ACTIVE) { ?>
                                                    <button type="button" class="btn btn-danger pull-right btn-xs" onclick="blockActiveHospital('<?= getval($hospital, 'hospital_id'); ?>', '<?= AppConst::STATUS_BLOCK ?>')" style="width: 100%;margin-top: 2px" title="Active <?= getval($hospital, 'hospital_name'); ?>"><i class="fa fa-ban"></i> Block</button>
                                                <?php } else { ?>
                                                    <button type="button" class="btn btn-success pull-right btn-xs" onclick="blockActiveHospital('<?= getval($hospital, 'hospital_id'); ?>', '<?= AppConst::STATUS_ACTIVE ?>')" style="width: 100%;margin-top: 2px" title="Block <?= getval($hospital, 'hospital_name'); ?>"><i class="fa fa-check-square-o"></i>Active </button>
                                                <?php } ?></td>

                                            <?php
                                            $i++;
                                        }
                                        ?>
                                    </tr>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="8" class="text-center">No Hospital Found</td>
                                    </tr>
                                <?php } ?>
                            <tbody>
                        </table>
                        <ul class=" pagination pull-right">

                            <!-- Show pagination links -->
                            <?php foreach ($links as $link) { ?>
                                <li><?= $link ?></li>
                                <?php }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function blockActiveHospital(hospital_id, status)
    {
        if (confirm("Are you sure ?"))
        {
            $.getJSON("<?= base_url(AppConst::INDEX . 'hospital/updatestatus?hospital_id=') ?>" + hospital_id + "&status=" + status,
                    function (res) {
                        window.location.reload();
                    });
        }
    }
</script>