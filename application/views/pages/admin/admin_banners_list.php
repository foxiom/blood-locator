<?php
$bannersList = isset($data['banners-list']) ? $data['banners-list'] : array();
$filter = isset($data['filter']) ? $data['filter'] : array();
$links = isset($data['links']) ? $data['links'] : array();
$no = isset($data['ROLLNO']) ? $data['ROLLNO'] : 1;
?>

<section class="content-header">
    <h1>
        BANNERS LIST MANAGEMENT
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Banners List</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="pull-right" style="padding-top: 4px;padding-right: 8px;">
                        <a class="btn btn-primary pull-right" href="<?php echo base_url(AppConst::INDEX . 'admin/add-banner'); ?>" title="New blog"><i class="fa fa-plus"></i> New </a>
                    </div>
                    <div class="panel-heading">
                        BANNERS LIST
                        <span><?= getFeedbackMessage() ?></span>
                    </div>
                    <div class="panel-body">
                        <table width="" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="10%">Banner</th>
                                    <th width="10%">Type</th>
                                    <th width="10%">Created On</th>
                                    <th width="10%">Status</th>
                                    <th width="7%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = $no;
                                if (count($bannersList) > 0) {
                                    foreach ($bannersList as $banner) {
                                ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <?php $imageUrl = getval($banner, 'banner_url'); ?>
                                            <td><?php if ($imageUrl != '') { ?>
                                                    <img src="<?= base_url($imageUrl) ?>" width="80px" height="80px">
                                                <?php } ?>
                                            </td>
                                            <td><?php if (getval($banner, 'type') == AppConst::HERO_BANNER) { ?>
                                                    Hero Banner
                                                <?php } else if (getval($banner, 'type') == AppConst::FITNESS_BANNER) { ?>
                                                    Fitness Banner
                                                <?php } else if (getval($banner, 'type') == AppConst::STATIC_BANNER) { ?>
                                                    Static Banner
                                                <?php } ?></td>
                                            <td><?= dbDate2UIdate(getval($banner, 'created_at'), FALSE) ?></td>
                                            <?php $status = getval($banner, 'status'); ?>
                                            <td><?php if ($status == AppConst::STATUS_ACTIVE) { ?>
                                                    <label class="label bg-green"> Active</label>
                                                <?php } else if ($status == AppConst::STATUS_BLOCK) { ?>
                                                    <label class="label bg-red">Blocked</label>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <a class="btn bg-blue" style="margin-bottom: 2px;" title="Edit <?= getval($banner, 'title') ?>" href="<?= base_url('admin/add-banner?banner_id=' . getval($banner, 'id')) ?>"><i class="fa fa-edit"></i>Edit</a>
                                                <?php if ($status == AppConst::STATUS_ACTIVE) { ?>
                                                    <a class="btn bg-red" title="Block <?= getval($banner, 'title') ?> " onclick="updateBannersStatus('<?= getval($banner, 'id') ?>','<?= AppConst::STATUS_BLOCK ?>')">Block</a>
                                                <?php } else if ($status == AppConst::STATUS_BLOCK) { ?>
                                                    <a class="btn bg-green" title=" Active <?= getval($banner, 'title') ?> " onclick="updateBannersStatus('<?= getval($banner, 'id') ?>','<?= AppConst::STATUS_ACTIVE ?>')">Active</a>
                                                <?php } ?>
                                            </td>

                                        <?php
                                        $i++;
                                    }
                                        ?>
                                        </tr>
                                    <?php } else { ?>
                                        <tr>
                                            <td colspan="8" class="text-center">No Banner Found</td>
                                        </tr>
                                    <?php } ?>
                            <tbody>
                        </table>
                        <ul class=" pagination pull-right">

                            <!-- Show pagination links -->
                            <?php foreach ($links as $link) { ?>
                                <li><?= $link ?></li>
                            <?php }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function updateBannersStatus(banner_id, status) {
        if (confirm("Are you sure ?")) {
            $.getJSON("<?= base_url(AppConst::INDEX . 'admin/update-banner-status?Banner_id=') ?>" + banner_id + "&status=" + status,
                function(res) {
                    window.location.reload();
                });
        }
    }
</script>