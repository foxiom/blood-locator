<?php
$communityInfo = isset($data['community-info']) ? $data['community-info'] : array();
$countryList = isset($data['country-info']) ? $data['country-info'] : array();
?>
<link rel="stylesheet" href="<?= base_url('webres/admin/css/jquery-ui.css') ?>">
<script src="<?= base_url('webres/admin/js/jquery-ui.js') ?>"></script>
<section class="content-header">
    <h1>
        HOSPITAL MANAGEMENT
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Hospital List</a></li>
        <li class="active">Import File</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        IMPORT FILE
                    </div>
                    <div class="box-body">
                        <?php
                        $formAttributes = array('id' => 'form', 'data-parsley-validate' => '', 'class' => 'form', 'enctype' => 'multipart/form-data');
                        echo form_open_multipart(AppConst::INDEX . 'admin/import-hospital-file', $formAttributes);
                        ?>

                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Import File (.xls,.xlsx)<span class="required-color">*</span></label>
                                    <input type="file" name="file"  required="" accept=".xls, .xlsx"   class="form-control" >


                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" id="btn_save" class="btn btn-microsoft pull-right btn_submit">&nbsp;&nbsp;&nbsp; Import &nbsp;&nbsp;&nbsp;</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


