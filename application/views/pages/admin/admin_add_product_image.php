<?php
$productInfo = isset($data['image-info']) ? $data['image-info'] : array();
?>
<link rel="stylesheet" href="<?= base_url('webres/admin/css/jquery-ui.css') ?>">
<script src="<?= base_url('webres/admin/js/jquery-ui.js') ?>"></script>
<section class="content-header">
    <h1>
        PRODUCT MANAGEMENT
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product List</a></li>
        <li class="active">Add Product Image</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        ADD/EDIT PRODUCT IMAGE
                    </div>
                    <div class="box-body">
                        <?php
                        $formAttributes = array('id' => 'form', 'data-parsley-validate' => '', 'class' => 'form', 'enctype' => 'multipart/form-data');
                        echo form_open_multipart(AppConst::INDEX . 'admin/add-product-image', $formAttributes);
                        ?>
                        <input type="hidden" name="product_id" value="<?= getval($productInfo, 'product_id') ?>" />
                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Image </label>
                                    <input type="file" name="image" class="form-control">
                                    <?php if (getval($productInfo, 'image_url') != '') { ?>
                                        <a target="_blank" href="<?= base_url(getval($productInfo, 'image_url')) ?>">View Image</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" id="btn_save" class="btn btn-microsoft pull-right btn_submit">&nbsp;&nbsp;&nbsp; Save &nbsp;&nbsp;&nbsp;</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>