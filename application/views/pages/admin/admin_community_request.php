<?php
$communityList = isset($data['community-request']) ? $data['community-request'] : array();
$filter = isset($data['filter']) ? $data['filter'] : array();
$links = isset($data['links']) ? $data['links'] : array();
$no = isset($data['ROLLNO']) ? $data['ROLLNO'] : 1;
?>

<section class="content-header">
    <h1>
        COMMUNITY REQUEST
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        COMMUNITY REQUEST
                        <span><?= getFeedbackMessage() ?></span>
                    </div>
                    <div class="panel-body">
                        <table width="" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="10%">Community Name</th>
                                    <th width="10%">Reason</th>
                                    <th width="10%">Place</th>
                                    <th width="20%">Date of Request</th>
                                    <th width="10%">Requested By</th>
                                    <th width="7%">Status</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = $no;
                                if (count($communityList) > 0) {
                                    foreach ($communityList as $community) {
                                ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= getval($community, 'community_name') ?></td>
                                            <td><?= getval($community, 'reason') ?></td>
                                            <td><?= getval($community, 'place') ?></td>
                                            <td><?= dbDate2UIdate(getval($community, 'created_at'), FALSE) ?></td>
                                            <td><?= getval($community, 'first_name')." ".getval($community, 'last_name') ?></td>
                                            <?php $status = getval($community, 'request_status'); ?>
                                            <td><?php if ($status == AppConst::COMMUNITY_REQUEST_NEW) { ?>
                                                    <label class="label bg-green"> New</label>
                                                <?php } else if ($status == AppConst::COMMUNITY_REQUEST_ACCEPT) { ?>
                                                    <label class="label bg-red">Accept</label>
                                                <?php } else if ($status == AppConst::COMMUNITY_REQUEST_REJECT) { ?>
                                                    <label class="label bg-red">Reject</label>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <a class="btn bg-blue" title="Edit <?= getval($community, 'community_name') ?>" href="<?= base_url('admin/add-community?community_id=' . getval($community, 'community_id')) ?>"><i class="fa fa-edit"></i>Edit</a>
                                                <a class="btn bg-red" title="Reject <?= getval($community, 'community_name') ?> " onclick="updateCommunityStatus('<?= getval($community, 'community_id') ?>','<?= AppConst::COMMUNITY_REQUEST_REJECT ?>')">Reject</a>
                                                <a class="btn bg-green" title=" Accept <?= getval($community, 'community_name') ?> " onclick="updateCommunityStatus('<?= getval($community, 'community_id') ?>','<?= AppConst::COMMUNITY_REQUEST_ACCEPT ?>')">Accept</a>

                                            </td>

                                        <?php
                                        $i++;
                                    }
                                        ?>
                                        </tr>
                                    <?php } else { ?>
                                        <tr>
                                            <td colspan="8" class="text-center">No Community Found</td>
                                        </tr>
                                    <?php } ?>
                            <tbody>
                        </table>
                        <ul class=" pagination pull-right">

                            <!-- Show pagination links -->
                            <?php foreach ($links as $link) { ?>
                                <li><?= $link ?></li>
                            <?php }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function updateCommunityStatus(community_id, status) {
        if (confirm("Are you sure ?")) {
            $.getJSON("<?= base_url(AppConst::INDEX . 'admin/update-community-status?community_id=') ?>" + community_id + "&status=" + status,
                function(res) {
                    window.location.reload();
                });
        }
    }
</script>