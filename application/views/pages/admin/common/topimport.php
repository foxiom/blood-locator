<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?= AppConst::APPNAME; ?></title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="<?= base_url('webres/admin/bootstrap/css/bootstrap.min.css') ?>">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?= base_url('webres/admin/fontawesome/css/all.css') ?>">
<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Date Picker -->

<!-- select 2 library -->
<link rel="stylesheet" href="<?= base_url('webres/admin/plugins/select2/select2.min.css') ?>">
<!-- Theme style -->
<link rel="stylesheet" href="<?= base_url('webres/admin/css/AdminLTE.min.css') ?>">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="<?= base_url('webres/admin/css/_all-skins.min.css') ?>">
<!-- parsey form validation -->
<link rel="stylesheet" href="<?= base_url('webres/parsley/parsley.css') ?>">
<!-- cutome style sheet -->
<link rel="stylesheet" href="<?= base_url('webres/admin/bootstrap/css/style.css') ?>">

<script src="<?= base_url('webres/admin/plugins/jQuery/jquery-3.1.1.min.js') ?>"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=<?= AppConst::GOOGLE_MAP_API_KEY?>&libraries=places"></script>

<script src="<?php echo base_url() . 'webres/admin/js/locationpicker.jquery.js' ?>"></script>