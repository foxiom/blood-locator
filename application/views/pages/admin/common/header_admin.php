<?php 
$sessionUserInfo = isset($data['session']) ? $data['session'] : array();
$userName = getval($sessionUserInfo, 'first_name').' '. getval($sessionUserInfo, 'last_name');
$userID = getval($sessionUserInfo, 'user_id');
?>

<a href="#" class="logo" title="<?= AppConst::APPNAME?>">
    <span class="logo-mini"><b><?= AppConst::APPNAME?></b></span>
    <span class="logo-lg"><b><?php echo AppConst::APPNAME ?></b></span>
</a>
<nav class="navbar navbar-static-top">
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="<?= base_url(AppConst::LOGO_PATH); ?>" class="user-image" alt="User Image">
                    <span class="hidden-xs"><?= $userName ?></span>
                </a>
                <ul class="dropdown-menu">
                    <li class="user-header">
                        <img src="<?= base_url(AppConst::LOGO_PATH); ?>" class="img-circle" alt="User Image">
                        <p>
                            <?php echo AppConst::APPNAME ?>
                            <small></small>
                        </p>
                    </li>
                    <li class="user-footer">
                        <?php
                        $formAttributes = array('id' => 'form-logout');
                        echo form_open(AppConst::INDEX . 'admin/logout', $formAttributes);
                        ?>
                        <div class="pull-left">
                            <a href="<?= base_url('admin/profile?user_id=' . $userID); ?>" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="javascript:$('#form-logout').submit();" class="btn btn-default btn-flat">Logout</a>
                        </div>
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
