<?php
$sessionUserInfo = isset($data['session']) ? $data['session'] : array();

$activeMenu = getActiveMenu();
?>
<section class="sidebar">
    <div class="user-panel">
        <div class="pull-left image">
            <img src="<?= base_url(AppConst::LOGO_PATH); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p><?= getval($sessionUserInfo, 'first_name') . ' ' . getval($sessionUserInfo, 'last_name') ?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header" style="background-color: white;">MAIN NAVIGATIONS</li>
        <?php
        $Css = '';
        if (isset($activeMenu)) {
            $Css = $activeMenu == Pages::page_admin_dashboard ? 'active' : '';
        }
        ?>
        <li class="<?php echo $Css ?>">
            <a href="<?php echo base_url(AppConst::INDEX . 'admin'); ?>">
                <i class="fa fa-tachometer-alt"></i> <span>Dashboard</span>
            </a>
        </li>
        <?php
        $MainCss = '';
        if (isset($activeMenu) && ($activeMenu == Pages::page_admin_country_list || $activeMenu == Pages::page_admin_city_list)) {
            $MainCss = 'active';
        }
        ?>
        <li class="treeview <?= $MainCss ?>">
            <a href="#">
                <i class="fa fa-bars"> </i> <span>Master Data</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">

                <?php

                $Css = '';
                if (isset($activeMenu)) {
                    $Css = $activeMenu == Pages::page_admin_country_list ? 'active' : '';
                }
                ?>
                <li class="<?php echo $Css ?>">
                    <a href="<?= base_url(AppConst::INDEX . 'admin/country-list') ?>"> <i class="fa fa-map-marker"> </i> <span class="font-normal">Country Management</span> </a>
                </li>
                <?php

                $Css = '';
                if (isset($activeMenu)) {
                    $Css = $activeMenu == Pages::page_admin_city_list ? 'active' : '';
                }
                ?>
                <li class="<?php echo $Css ?>">
                    <a href="<?= base_url(AppConst::INDEX . 'admin/city-list') ?>"> <i class="fa fa-map-marker"> </i> <span class="font-normal">State/Provenance Management</span> </a>
                </li>

            </ul>
        </li>
        <?php

        $MainCss = '';
        if (isset($activeMenu) && ($activeMenu == Pages::page_admin_community_list || $activeMenu == Pages::page_admin_community_requests)) {
            $MainCss = 'active';
        }
        ?>
        <li class="treeview <?= $MainCss ?>">
            <a href="#">
                <i class="fa fa-users"> </i> <span>Community Management</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">

                <?php

                $Css = '';
                if (isset($activeMenu)) {
                    $Css = $activeMenu == Pages::page_admin_community_list ? 'active' : '';
                }
                ?>
                <li class="<?php echo $Css ?>">
                    <a href="<?= base_url(AppConst::INDEX . 'admin/community-list') ?>"> <i class="fa fa-list"> </i> <span class="font-normal">Community List</span> </a>
                </li>
                <?php

                $Css = '';
                if (isset($activeMenu)) {
                    $Css = $activeMenu == Pages::page_admin_community_requests ? 'active' : '';
                }
                ?>
                <li class="<?php echo $Css ?>">
                    <a href="<?= base_url(AppConst::INDEX . 'admin/community-request') ?>"> <i class="fa fa-rocket"> </i> <span class="font-normal">Community Request</span> </a>
                </li>

            </ul>
        </li>
        <?php
        $Css = '';
        if (isset($activeMenu)) {
            $Css = $activeMenu == Pages::page_admin_un_registered_user_list ? 'active' : '';
        }
        ?>
        <li class="<?php echo $Css ?>">
            <a href="<?php echo base_url(AppConst::INDEX . 'admin/unregistered-user-list'); ?>">
                <i class="fa fa-user"></i> <span>Unregistered Management</span>
            </a>
        </li>
        <?php
        $Css = '';
        if (isset($activeMenu)) {
            $Css = $activeMenu == Pages::page_admin_registered_user_list ? 'active' : '';
        }
        ?>
        <li class="<?php echo $Css ?>">
            <a href="<?php echo base_url(AppConst::INDEX . 'admin/registered-user-list'); ?>">
                <i class="fa fa-users"></i> <span>Registered User Management</span>
            </a>
        </li>

        <?php
        $Css = '';
        if (isset($activeMenu)) {
            $Css = $activeMenu == Pages::page_admin_request_list ? 'active' : '';
        }
        ?>
        <li class="<?php echo $Css ?>">
            <a href="<?php echo base_url(AppConst::INDEX . 'admin/request-list'); ?>">
                <i class="fa fa-list"></i> <span>Request Management</span>
            </a>
        </li>

        <?php
        $Css = '';
        if (isset($activeMenu)) {
            $Css = $activeMenu == Pages::page_admin_hospital_list ? 'active' : '';
        }
        ?>
        <li class="<?php echo $Css ?>">
            <a href="<?php echo base_url(AppConst::INDEX . 'admin/hospital-list'); ?>">
                <i class="fa fa-hospital "></i> <span>Hospital Management</span>
            </a>
        </li>
        <?php

        $MainCss = '';
        if (isset($activeMenu) && ($activeMenu == Pages::page_admin_add_edit_category || $activeMenu == Pages::page_admin_add_edit_product)) {
            $MainCss = 'active';
        }
        ?>
        <li class="treeview <?= $MainCss ?>">
            <a href="#">
                <i class="fa fa-store"> </i> <span>Marketplace</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">

                <?php

                $Css = '';
                if (isset($activeMenu)) {
                    $Css = $activeMenu == Pages::page_admin_add_edit_category ? 'active' : '';
                }
                ?>
                <li class="<?php echo $Css ?>">
                    <a href="<?= base_url(AppConst::INDEX . 'admin/marketplace-category') ?>"> <i class="fa fa-stream"> </i> <span class="font-normal">Category</span> </a>
                </li>
                <?php

                $Css = '';
                if (isset($activeMenu)) {
                    $Css = $activeMenu == Pages::page_admin_add_edit_product ? 'active' : '';
                }
                ?>
                <li class="<?php echo $Css ?>">
                    <a href="<?= base_url(AppConst::INDEX . 'admin/marketplace-product') ?>"> <i class="fab fa-product-hunt"> </i> <span class="font-normal">Products</span> </a>
                </li>

            </ul>
        </li>
        <?php

        $MainCss = '';
        if (isset($activeMenu) && ($activeMenu == Pages::page_admin_blog_list || $activeMenu == Pages::page_admin_blog_category)) {
            $MainCss = 'active';
        }
        ?>
        <li class="treeview <?= $MainCss ?>">
            <a href="#">
                <i class="fa fa-blog"> </i> <span>Blogs</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">

                <?php

                $Css = '';
                if (isset($activeMenu)) {
                    $Css = $activeMenu == Pages::page_admin_blog_category ? 'active' : '';
                }
                ?>
                <li class="<?php echo $Css ?>">
                    <a href="<?= base_url(AppConst::INDEX . 'admin/blog-category') ?>"> <i class="fa fa-stream"> </i> <span class="font-normal">Blog Category</span> </a>
                </li>
                <?php

                $Css = '';
                if (isset($activeMenu)) {
                    $Css = $activeMenu == Pages::page_admin_blog_list ? 'active' : '';
                }
                ?>
                <li class="<?php echo $Css ?>">
                    <a href="<?= base_url(AppConst::INDEX . 'admin/blog-list') ?>"> <i class="fa fa-blog"> </i> <span class="font-normal">Blogs</span> </a>
                </li>

            </ul>
        </li>
        <?php
        $Css = '';
        if (isset($activeMenu)) {
            $Css = $activeMenu == Pages::page_admin_donation_drive_list ? 'active' : '';
        }
        ?>
        <li class="<?php echo $Css ?>">
            <a href="<?php echo base_url(AppConst::INDEX . 'admin/donation-drive-list'); ?>">
                <i class="fa fa-heartbeat"></i> <span>Donation Drive</span>
            </a>
        </li>
        <?php
        $Css = '';
        if (isset($activeMenu)) {
            $Css = $activeMenu == Pages::page_admin_fitness_tips ? 'active' : '';
        }
        ?>
        <li class="<?php echo $Css ?>">
            <a href="<?php echo base_url(AppConst::INDEX . 'admin/fitness-tips'); ?>">
                <i class="fa fa-running"></i> <span>Fitness Tips</span>
            </a>
        </li>
        <?php
        $Css = '';
        if (isset($activeMenu)) {
            $Css = $activeMenu == Pages::page_admin_banners_list ? 'active' : '';
        }
        ?>
        <li class="<?php echo $Css ?>">
            <a href="<?php echo base_url(AppConst::INDEX . 'admin/banners-list'); ?>">
                <i class="fa fa-images"></i> <span>Banners</span>
            </a>
        </li>

    </ul>
</section>