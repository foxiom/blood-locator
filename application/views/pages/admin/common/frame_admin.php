<!DOCTYPE html>
<html lang="en" class="app">
    <head>
        <?= $import_top; ?>
        <?= $import_top_custom; ?>
    </head>
    <body class="skin-blue sidebar-mini wysihtml5-supported" >
        <div class="wrapper">
            <header class="main-header">
                <?= $header; ?>
            </header>
            <aside class="main-sidebar">
                <?= $sidebar; ?>
            </aside>
            <div class="content-wrapper">
                <?= $content; ?>
            </div>
        </div>
        <footer class="main-footer">
            <?= $footer; ?>
        </footer>
        <?= $import_bottom; ?>
        <?= $import_botton_custom; ?>
    </body>
</html>