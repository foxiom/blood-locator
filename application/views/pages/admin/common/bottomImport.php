
<!-- jQuery UI 1.11.4 -->
<script src="<?= base_url('webres/admin/plugins/jQuery/jquery-ui.min.js') ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= base_url('webres/admin/bootstrap/js/bootstrap.min.js') ?>"></script>
<!-- select 2 library -->
<script src="<?= base_url('webres/admin/plugins/select2/select2.full.min.js') ?>"></script>

<!-- FastClick -->
<script src="<?= base_url('webres/admin/plugins/fastclick/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('webres/admin/js/adminlte.min.js') ?>"></script>
<!-- parsey form validation -->
<script src="<?= base_url('webres/parsley/parsley.min.js') ?>"></script>
<script src="<?= base_url('webres/parsley/parsley_file_validation.js') ?>"></script>
<script type="text/javascript">

    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        $('.form').submit(function () {
            
            $(".btn_submit")
                    .html("Please Wait...")
                    .attr('disabled', 'disabled');

            return true;
        });
        

    });
</script>