<?php
$categoryInfo = isset($data['blog-category']) ? $data['blog-category'] : array();

?>
<link rel="stylesheet" href="<?= base_url('webres/admin/css/jquery-ui.css')?>">
<script src="<?= base_url('webres/admin/js/jquery-ui.js')?>"></script>
<section class="content-header">BLOG CATEGORY
    <h1>
        
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">category List</a></li>
        <li class="active">Add Category</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        ADD/EDIT CATEGORY
                    </div>
                    <div class="box-body">
                        <?php
                        $formAttributes = array('id' => 'form', 'data-parsley-validate' => '', 'class' => 'form', 'enctype' => 'multipart/form-data');
                        echo form_open_multipart(AppConst::INDEX . 'admin/add-blog-category', $formAttributes);
                        ?>
                        <input type="hidden" name="category_id" value="<?= getval($categoryInfo, 'id') ?>"/>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Category Name <span class="required-color">*</span></label>
                                    <input type="text" name="category_name" autocomplete="off" class="form-control" data-parsley-pattern="^[a-zA-Z\s.\-_]+$" required="" data-parsley-pattern-message="This is not a valid name" data-parsley-trigger="keyup" value="<?= getval($categoryInfo, 'category_name'); ?>" placeholder="Category Name" >
                                </div>
                            </div>

                        </div>
                       
                        <hr>
                    </div>
                    <div class="box-footer">
                        <button type="submit" id="btn_save" class="btn btn-microsoft pull-right btn_submit">&nbsp;&nbsp;&nbsp; Save &nbsp;&nbsp;&nbsp;</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


