<?php
$productImages = isset($data['product-images']) ? $data['product-images'] : array();
$productId = isset($data['product-id']) ? $data['product-id'] : 0;
$filter = isset($data['filter']) ? $data['filter'] : array();
$links = isset($data['links']) ? $data['links'] : array();
$no = isset($data['ROLLNO']) ? $data['ROLLNO'] : 1;
?>

<section class="content-header">
    <h1>
        PRODUCT MANAGEMENT
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">product List</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="pull-right" style="padding-top: 4px;padding-right: 8px;">
                        <a class="btn btn-primary pull-right" href="<?php echo base_url(AppConst::INDEX . 'admin/add-product-image?product_id=' . $productId) ?>" title="New product"><i class="fa fa-plus"></i> New Image</a>
                    </div>
                    <div class="panel-heading">
                        PRODUCT LIST
                        <span><?= getFeedbackMessage() ?></span>
                    </div>
                    <div class="panel-body">
                        <table width="" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="10%">Image</th>
                                    <th width="10%">Created On</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = $no;
                                if (count($productImages) > 0) {
                                    foreach ($productImages as $product) {
                                ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <?php $imageUrl = getval($product, 'image'); ?>
                                            <td><?php if ($imageUrl != '') { ?>
                                                    <img src="<?= base_url($imageUrl) ?>" width="80px" height="80px">
                                                <?php } ?>
                                            </td>
                                            <td><?= dbDate2UIdate(getval($product, 'created_at'), FALSE) ?></td>
                                            </td>
                                            <td>
                                                <a class="btn bg-blue" title="Edit <?= getval($product, 'product_name') ?>" href="<?= base_url('admin/add-product-image?product_id=' . $productId) ?>"><i class="fa fa-edit"></i>Edit</a>
                                                <a class="btn bg-red" title="Delete" onclick="deleteProductImage('<?= getval($product, 'id') ?>')">Delete</a>
                                            </td>

                                        <?php
                                        $i++;
                                    }
                                        ?>
                                        </tr>
                                    <?php } else { ?>
                                        <tr>
                                            <td colspan="8" class="text-center">No product Found</td>
                                        </tr>
                                    <?php } ?>
                            <tbody>
                        </table>
                        <ul class=" pagination pull-right">

                            <!-- Show pagination links -->
                            <?php foreach ($links as $link) { ?>
                                <li><?= $link ?></li>
                            <?php }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function deleteProductImage(image_id) {
        if (confirm("Are you sure ?")) {
            $.getJSON("<?= base_url(AppConst::INDEX . 'admin/delete-product-image?image_id=') ?>" + image_id,
                function(res) {
                    window.location.reload();
                });
        }
    }
</script>