<?php
$bannerInfo = isset($data['banner-details']) ? $data['banner-details'] : array();
?>
<link rel="stylesheet" href="<?= base_url('webres/admin/css/jquery-ui.css') ?>">
<script src="<?= base_url('webres/admin/js/jquery-ui.js') ?>"></script>
<section class="content-header">BANNER
    <h1>

    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Banner List</a></li>
        <li class="active">Add Banner</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        ADD/EDIT BANNER
                    </div>
                    <div class="box-body">
                        <?php
                        $formAttributes = array('id' => 'form', 'data-parsley-validate' => '', 'class' => 'form', 'enctype' => 'multipart/form-data');
                        echo form_open_multipart(AppConst::INDEX . 'admin/add-banner', $formAttributes);
                        ?>
                        <input type="hidden" name="banner_id" value="<?= getval($bannerInfo, 'id') ?>" />
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Category<span class="required-color">*</span></label>
                                    <select class="form-control select2" style="width:100%" name="type" required="">
                                        <option value="">-Select-</option>
                                        <option value="<?= AppConst::HERO_BANNER ?>" <?= (AppConst::HERO_BANNER == getval($bannerInfo, 'type')) ? 'selected=""' : '' ?>>Hero Banner</option>
                                        <option value="<?= AppConst::FITNESS_BANNER ?>" <?= (AppConst::FITNESS_BANNER == getval($bannerInfo, 'type')) ? 'selected=""' : '' ?>>Fitness Banner</option>
                                        <option value="<?= AppConst::STATIC_BANNER ?>" <?= (AppConst::STATIC_BANNER == getval($bannerInfo, 'type')) ? 'selected=""' : '' ?>>Static Banner</option>
                                    </select>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Image </label>
                                    <input type="file" name="banner_url" class="form-control">
                                    <?php if (getval($bannerInfo, 'banner_url') != '') { ?>
                                        <a target="_blank" href="<?= base_url(getval($bannerInfo, 'banner_url')) ?>">View Image</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <hr>
                    </div>
                    <div class="box-footer">
                        <button type="submit" id="btn_save" class="btn btn-microsoft pull-right btn_submit">&nbsp;&nbsp;&nbsp; Save &nbsp;&nbsp;&nbsp;</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>