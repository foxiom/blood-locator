<?php
$productInfo = isset($data['product-info']) ? $data['product-info'] : array();
$countryList = isset($data['country-info']) ? $data['country-info'] : array();
$categoryList = isset($data['category-info']) ? $data['category-info'] : array();
?>
<link rel="stylesheet" href="<?= base_url('webres/admin/css/jquery-ui.css') ?>">
<script src="<?= base_url('webres/admin/js/jquery-ui.js') ?>"></script>
<section class="content-header">
    <h1>
        PRODUCT MANAGEMENT
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product List</a></li>
        <li class="active">Add Product</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        ADD/EDIT PRODUCT
                    </div>
                    <div class="box-body">
                        <?php
                        $formAttributes = array('id' => 'form', 'data-parsley-validate' => '', 'class' => 'form', 'enctype' => 'multipart/form-data');
                        echo form_open_multipart(AppConst::INDEX . 'admin/add-product', $formAttributes);
                        ?>
                        <input type="hidden" name="product_id" value="<?= getval($productInfo, 'id') ?>" />
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Product Name <span class="required-color">*</span></label>
                                    <input type="text" name="product_name" autocomplete="off" class="form-control" value="<?= getval($productInfo, 'product_name') ?>" required placeholder="Product Name">

                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Product Details <span class="required-color">*</span></label>
                                    <textarea class="form-control" name="product_details" required="" placeholder="Product Details"><?= getval($productInfo, 'product_details') ?></textarea>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>Regular Price <span class="required-color">*</span></label>
                                    <input type="number" class="form-control" name="regular_price" required="" placeholder="Regular Price" value="<?= getval($productInfo, 'regular_price') ?>">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>Offer Price <span class="required-color">*</span></label>
                                    <input type="number" class="form-control" name="offer_price" required="" placeholder="Offer Price" value="<?= getval($productInfo, 'offer_price') ?>">
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Category<span class="required-color">*</span></label>
                                    <?php $categoryId = getval($productInfo, 'category'); ?>
                                    <select class="form-control select2" style="width:100%" name="category" required="">
                                        <option value="">-Select-</option>
                                        <?php foreach ($categoryList as $category) { ?>
                                            <option value="<?= getval($category, 'id') ?>" <?= ($categoryId == getval($category, 'id')) ? 'selected=""' : '' ?>><?= getval($category, 'category_name') ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Country<span class="required-color">*</span></label>
                                    <?php $countryname = getval($productInfo, 'country'); ?>
                                    <select class="form-control select2" style="width:100%" name="country" required="">
                                        <option value="">-Select-</option>
                                        <?php foreach ($countryList as $country) { ?>
                                            <option value="<?= getval($country, 'country_name') ?>" <?= ($countryname == getval($country, 'country_name')) ? 'selected=""' : '' ?>><?= getval($country, 'country_name') ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Image </label>
                                    <input type="file" name="image_url" class="form-control">
                                    <?php if (getval($productInfo, 'image_url') != '') { ?>
                                        <a target="_blank" href="<?= base_url(getval($productInfo, 'image_url')) ?>">View Image</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" id="btn_save" class="btn btn-microsoft pull-right btn_submit">&nbsp;&nbsp;&nbsp; Save &nbsp;&nbsp;&nbsp;</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>