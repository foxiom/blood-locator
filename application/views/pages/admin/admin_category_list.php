<?php
$categoryList = isset($data['category-list']) ? $data['category-list'] : array();
$filter = isset($data['filter']) ? $data['filter'] : array();
$links = isset($data['links']) ? $data['links'] : array();
$no = isset($data['ROLLNO']) ? $data['ROLLNO'] : 1;
?>

<section class="content-header">
    <h1>
    CATEGORY MANAGEMENT
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Category List</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="pull-right" style="padding-top: 4px;padding-right: 8px;">
                         <a class="btn btn-primary pull-right" href="<?php echo base_url(AppConst::INDEX . 'admin/add-category'); ?>" title="New category"><i class="fa fa-plus"></i> New Category</a> 
                    </div>
                    <div class="panel-heading">
                        CATEGORY LIST
                        <span><?= getFeedbackMessage() ?></span>
                    </div>
                    <div class="panel-body">
                        <table width="" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="10%">Category Name</th>
                                    <th width="10%">Created On</th>
                                    <th width="10%">Status</th>
                                    <th width="7%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = $no;
                                if (count($categoryList) > 0) {
                                    foreach ($categoryList as $category) {
                                        ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= getval($category, 'category_name') ?></td>
                                            <td><?= dbDate2UIdate(getval($category, 'created_at'),FALSE) ?></td>
                                            <?php $status = getval($category, 'status'); ?>
                                            <td><?php if ($status == AppConst::STATUS_ACTIVE) { ?>
                                                    <label class="label bg-green"> Active</label>
                                                <?php } else if ($status == AppConst::STATUS_BLOCK) { ?>
                                                    <label class="label bg-red">Blocked</label>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <a class="btn bg-blue" title="Edit <?= getval($category, 'category_name')?>" href="<?= base_url('admin/add-category?category_id='. getval($category, 'id'))?>"><i class="fa fa-edit"></i>Edit</a>
                                                <?php if($status == AppConst::STATUS_ACTIVE){ ?>
                                                <a class="btn bg-red" title="Block <?= getval($category, 'category_name')?> " onclick="updatecategoryStatus('<?= getval($category, 'id')?>','<?= AppConst::STATUS_BLOCK?>')">Block</a>
                                                <?php }else if($status == AppConst::STATUS_BLOCK){ ?>
                                                <a class="btn bg-green" title=" Active <?= getval($category, 'category_name')?> " onclick="updatecategoryStatus('<?= getval($category, 'id')?>','<?= AppConst::STATUS_ACTIVE?>')">Active</a>
                                                <?php } ?>
                                            </td>

                                            <?php
                                            $i++;
                                        }
                                        ?>
                                    </tr>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="8" class="text-center">No category Found</td>
                                    </tr>
                                <?php } ?>
                            <tbody>
                        </table>
                        <ul class=" pagination pull-right">

                            <!-- Show pagination links -->
                            <?php foreach ($links as $link) { ?>
                                <li><?= $link ?></li>
                                <?php }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function updatecategoryStatus(category_id, status)
    {
        if (confirm("Are you sure ?"))
        {
            $.getJSON("<?= base_url(AppConst::INDEX . 'admin/update-category-status?category_id=') ?>" + category_id + "&status=" + status,
                    function (res) {
                        window.location.reload();
                    });
        }
    }
</script>