<?php
$userList = isset($data['user-list']) ? $data['user-list'] : array();
$filter = isset($data['filter']) ? $data['filter'] : array();
$links = isset($data['links']) ? $data['links'] : array();
$no = isset($data['ROLLNO']) ? $data['ROLLNO'] : 1;
?>

<section class="content-header">
    <h1>
        REGISTERED USER MANAGEMENT
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Registered User List</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        SEARCH REGISTERED USER
                        <span> <?= getFeedbackMessage() ?></span>
                    </div>
                    <div class="panel-body">
                        <?php
                        echo form_open_multipart(AppConst::INDEX . 'admin/registered-user-list');
                        ?>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Blood Group</label>
                                    <?php $blood_group = getval($filter, 'blood_group'); ?>
                                    <select name="blood_group" class="form-control">
                                        <option value="">-Select-</option>
                                        <option value="<?= AppConst::BLOOD_GROUP_A_PLUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_A_PLUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_A_PLUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_A_MINUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_A_MINUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_A_MINUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_B_PLUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_B_PLUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_B_PLUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_B_MINUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_B_MINUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_B_MINUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_O_PLUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_O_PLUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_O_PLUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_O_MINUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_O_MINUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_O_MINUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_AB_PLUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_AB_PLUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_AB_PLUS ?></option>
                                        <option value="<?= AppConst::BLOOD_GROUP_AB_MINUS_ID ?>" <?= ($blood_group == AppConst::BLOOD_GROUP_AB_MINUS_ID) ? 'selected=""' : '' ?>><?= AppConst::BLOOD_GROUP_AB_MINUS ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group" style="padding-top: 25px;">
                                    <button type="submit" class="btn btn-microsoft">&nbsp;&nbsp;&nbsp; Search &nbsp;&nbsp;&nbsp;</button>
                                    <a href="<?= base_url('admin/registered-user-list'); ?>" class="btn btn-danger">&nbsp;&nbsp;&nbsp;&nbsp; Reset &nbsp;&nbsp;&nbsp;&nbsp;</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="panel panel-default">
                    <div class="pull-right" style="padding-top: 4px;padding-right: 8px;">
                        <a class="btn btn-primary pull-right" href="<?php echo base_url(AppConst::INDEX . 'admin/add-registered-user'); ?>" title="New Registered User"><i class="fa fa-plus"></i> New Registered User</a>
                    </div>
                    <div class="panel-heading">
                        REGISTERED USER LIST
                        <span><?= getFeedbackMessage() ?></span>
                    </div>
                    <div class="panel-body">
                        <table width="" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="20%">User Info</th>
                                    <th width="10%">Blood Group</th>
                                    <th width="10%">Contact No</th>
                                    <th width="25%">Address</th>
                                    <th width="10%">Username & Password</th>
                                    <th width="10%">Status</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = $no;
                                if (count($userList) > 0) {
                                    foreach ($userList as $user) {
                                ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td>Name : <?= getval($user, 'first_name') . ' ' . getval($user, 'last_name'); ?><br>
                                                Gender : <?php
                                                            if (getval($user, 'gender') == AppConst::GENDER_MALE_ID) {
                                                                echo AppConst::GENDER_MALE;
                                                            } else if (getval($user, 'gender') == AppConst::GENDER_FEMALE_ID) {
                                                                echo AppConst::GENDER_FEMALE;
                                                            }
                                                            ?>

                                            </td>
                                            <?php $blood_group = getval($user, 'blood_group'); ?>
                                            <td><?php
                                                if ($blood_group == AppConst::BLOOD_GROUP_A_PLUS_ID) {
                                                    echo AppConst::BLOOD_GROUP_A_PLUS;
                                                } else if ($blood_group == AppConst::BLOOD_GROUP_A_MINUS_ID) {
                                                    echo AppConst::BLOOD_GROUP_A_MINUS;
                                                } else if ($blood_group == AppConst::BLOOD_GROUP_B_PLUS_ID) {
                                                    echo AppConst::BLOOD_GROUP_B_PLUS;
                                                } else if ($blood_group == AppConst::BLOOD_GROUP_B_MINUS_ID) {
                                                    echo AppConst::BLOOD_GROUP_B_MINUS;
                                                } else if ($blood_group == AppConst::BLOOD_GROUP_O_PLUS_ID) {
                                                    echo AppConst::BLOOD_GROUP_O_PLUS;
                                                } else if ($blood_group == AppConst::BLOOD_GROUP_O_MINUS_ID) {
                                                    echo AppConst::BLOOD_GROUP_O_MINUS;
                                                } else if ($blood_group == AppConst::BLOOD_GROUP_AB_PLUS_ID) {
                                                    echo AppConst::BLOOD_GROUP_AB_PLUS;
                                                } else if ($blood_group == AppConst::BLOOD_GROUP_AB_MINUS_ID) {
                                                    echo AppConst::BLOOD_GROUP_AB_MINUS;
                                                }
                                                ?></td>
                                            <td><?= getval($user, 'contact_no') ?></td>
                                            <td><?= getval($user, 'address') ?></td>
                                            <td><?= getval($user, 'username') ?><br>
                                                <?= getval($user, 'password') ?></td>
                                            <?php $status = getval($user, 'status'); ?>
                                            <td><?php if ($status == AppConst::STATUS_ACTIVE) { ?>
                                                    <label class="label bg-green"> Active</label>
                                                <?php } else if ($status == AppConst::STATUS_BLOCK) { ?>
                                                    <label class="label bg-red">Blocked</label>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <a class="btn label-warning btn-xs" style="float: left;display: block; width: 100%;" href="<?= base_url(AppConst::INDEX . 'admin/add-registered-user?user_id=' . getval($user, 'user_id')); ?>" title="Edit <?= getval($user, 'first_name'); ?>"><i class="fa fa-edit"></i> Edit</a>
                                                <?php /* if (getval($user, 'status') == AppConst::STATUS_ACTIVE) { ?>
                                                  <button type="button" class="btn btn-danger pull-right btn-xs" onclick="blockActiveUser('<?= getval($user, 'user_id'); ?>', '<?= AppConst::STATUS_BLOCK ?>')" style="width: 100%;margin-top: 2px" title="Block <?= getval($user, 'first_name'); ?>"><i class="fa fa-ban"></i> Block</button>
                                                  <?php } else { ?>
                                                  <button type="button" class="btn btn-success pull-right btn-xs" onclick="blockActiveUser('<?= getval($user, 'user_id'); ?>', '<?= AppConst::STATUS_ACTIVE ?>')" style="width: 100%;margin-top: 2px" title="Active <?= getval($user, 'first_name'); ?>"><i class="fa fa-check-square-o"></i> Active</button>
                                                  <?php } */ ?>
                                            </td>
                                        <?php
                                        $i++;
                                    }
                                        ?>
                                        </tr>
                                    <?php } else { ?>
                                        <tr>
                                            <td colspan="8" class="text-center">No Registered Users</td>
                                        </tr>
                                    <?php } ?>
                            <tbody>
                        </table>
                        <ul class=" pagination pull-right">

                            <!-- Show pagination links -->
                            <?php foreach ($links as $link) { ?>
                                <li><?= $link ?></li>
                            <?php }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function blockActiveUser(user_id, status) {
        if (confirm("Are you sure ?")) {
            $.getJSON("<?= base_url(AppConst::INDEX . 'admin/user/updatestatus?user_id=') ?>" + user_id + "&status=" + status,
                function(res) {
                    window.location.reload();
                });
        }
    }
</script>