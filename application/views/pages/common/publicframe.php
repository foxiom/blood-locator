<!doctype html>
<html lang="en">
    <?= $import_top ?> 
        <main id="page">
            
            <?= $header ?>

            <?= $content; ?>

            <?= $footer ?>

        </main>

        <?= $import_bottom ?>

    </body>
</html>