<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="<?= AppConst::APPNAME ?>">
        <title><?php echo AppConst::APPNAME ?></title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url() . 'webres/admin/css/AdminLTE.min.css'; ?>">
        <link rel="stylesheet" href="<?php echo base_url() . 'webres/parsley/parsley.css' ?>">
    </head>
    <body class="hold-transition login-page" style="overflow-y: hidden">
        <div class="login-box" style="margin-top:15px;">
            <div class="login-logo">
                <img src="<?= base_url('webres/admin/images/blood.png'); ?>" height="100px">
            </div>
            <div class="login-box-body">
                <h4>Login Your Account</h4>
                <?php if (isset($data[AppConst::feedbackmessageKey])) { ?>
                    <h5><?php echo $data[AppConst::feedbackmessageKey]; ?></h5>
                    <?php
                }
                ?>
                <hr>
                <?php
                $formAttributes = array('id' => 'loginFrom', 'data-parsley-validate' => '', 'autocomplete' => 'off');
                echo form_open('admin/login', $formAttributes);
                ?>
                <div class="form-group has-feedback">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <input type="text" class="form-control" placeholder="Username"  name="username_of" required="">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    <span class="help-inline text-danger"><?php echo form_error('username_of'); ?></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" required="" name="password_of">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    <span class="help-inline text-danger"><?php echo form_error('password_of'); ?></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                    </div>
                    <div class="col-xs-4">
                        <input type="hidden" id="initscr" name="initscr" required="">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <script src="<?php echo base_url() . 'webres/parsley/parsley.min.js' ?>"></script>
    </body>
</html>