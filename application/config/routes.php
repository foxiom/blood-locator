<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'AdminCtrl';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['admin'] = 'AdminCtrl/adminDashBoard';
$route['admin/login'] = 'AdminCtrl/adminLogIn';
$route['terms_conditions'] = 'AdminCtrl/termsConditions';
$route['admin/logout'] = 'AdminCtrl/adminLogOut';
$route['admin/registered-user-list'] = 'AdminCtrl/registeredUserList';
$route['admin/registered-user-list/(:num)'] = 'AdminCtrl/registeredUserList';
$route['admin/add-registered-user'] = 'AdminCtrl/addEditRegisteredUser';
$route['admin/checkcontactno'] = 'AdminCtrl/adminCheckContactNoAlreadyExistOrNot';
$route['admin/request-list'] = 'AdminCtrl/requestList';
$route['admin/request-list/(:num)'] = 'AdminCtrl/requestList';
$route['admin/add-request'] = 'AdminCtrl/addEditRequest';
$route['admin/profile'] = 'AdminCtrl/ProfileCtrl';
$route['admin/updateprofile'] = 'AdminCtrl/ProfileCtrl';
$route['admin/changepassword'] = 'AdminCtrl/adminChangeProfilePassword';
$route['user/updatestatus'] = 'AdminCtrl/UpdateUserStatus';
$route['admin/getstates'] = 'AdminCtrl/get_states';
$route['admin/unregistered-user-list'] = 'AdminCtrl/unRegisteredUserList';
$route['admin/unregistered-user-list/(:num)'] = 'AdminCtrl/unRegisteredUserList';
$route['admin/add-unregistered-user'] = 'AdminCtrl/addEditUnRegisteredUser';
$route['unregisterd-user/updatestatus'] = 'AdminCtrl/unregisterdUpdateUserStatus';
$route['admin/hospital-list'] = 'AdminCtrl/hospitalList';
$route['admin/hospital-list/(:num)'] = 'AdminCtrl/hospitalList';
$route['admin/add-hospital'] = 'AdminCtrl/addEditHospital';
$route['hospital/updatestatus'] = 'AdminCtrl/UpdatehospitalStatus';
$route['admin/community-list'] = 'AdminCtrl/communityList';
$route['admin/community-list/(:num)'] = 'AdminCtrl/communityList';
$route['admin/add-community'] = 'AdminCtrl/addEditCommunity';
$route['admin/update-community-status'] = 'AdminCtrl/updateCommunityStatus';
$route['admin/community-request'] = 'AdminCtrl/communityRequest';
$route['admin/import-hospital-file'] = 'AdminCtrl/importHospitalFile';
$route['admin/country-list'] = 'AdminCtrl/countryList';
$route['admin/country-list/(:num)'] = 'AdminCtrl/countryList';
$route['get-country'] = 'AdminCtrl/getCountry';
$route['admin/city-list'] = 'AdminCtrl/cityList';
$route['admin/city-list/(:num)'] = 'AdminCtrl/cityList';
$route['get-city'] = 'AdminCtrl/getCity';
$route['admin/update-country-status'] = 'AdminCtrl/updateCountryStatus';
$route['admin/update-city-status'] = 'AdminCtrl/updateCityStatus';
$route['admin/marketplace-category'] = 'AdminCtrl/marketplaceCategory';
$route['admin/marketplace-product'] = 'AdminCtrl/marketplaceProduct';
$route['admin/add-category'] = 'AdminCtrl/addEditCategory';
$route['admin/update-category-status'] = 'AdminCtrl/updateCategoryStatus';
$route['admin/add-product'] = 'AdminCtrl/addEditProduct';
$route['admin/update-product-status'] = 'AdminCtrl/updateProductStatus';
$route['admin/blog-list'] = 'AdminCtrl/blogList';
$route['admin/blog-category'] = 'AdminCtrl/blogCategory';
$route['admin/add-blog-category'] = 'AdminCtrl/addEditBlogCategory';
$route['admin/update-blog-category-status'] = 'AdminCtrl/updateBlogCategoryStatus';
$route['admin/add-blog'] = 'AdminCtrl/addEditBlog';
$route['admin/update-blog-status'] = 'AdminCtrl/updateBlogStatus';
$route['admin/donation-drive-list'] = 'AdminCtrl/donationDriveList';
$route['admin/update-drive-status'] = 'AdminCtrl/updateDriveStatus';
$route['admin/view-donated_users'] = 'AdminCtrl/viewDonatedUsers';
$route['admin/fitness-tips'] = 'AdminCtrl/fitnessTips';
$route['admin/update-fitness-tip-status'] = 'AdminCtrl/updateFitnessTipStatus';
$route['admin/add-fitness-tip'] = 'AdminCtrl/addEditFitnessTip';
$route['admin/banners-list'] = 'AdminCtrl/bannersList';
$route['admin/add-banner'] = 'AdminCtrl/addEditBanner';
$route['admin/update-banner-status'] = 'AdminCtrl/updateBannerStatus';
$route['admin/product-images'] = 'AdminCtrl/productImages';
$route['admin/add-product-image'] = 'AdminCtrl/addEditProductImage';
$route['admin/delete-product-image'] = 'AdminCtrl/deleteProductImage';