-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 03, 2020 at 04:37 AM
-- Server version: 10.3.18-MariaDB-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ppabahos_blood_locator_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `blood_bank_auth`
--

CREATE TABLE `blood_bank_auth` (
  `login_id` int(11) NOT NULL,
  `username` varchar(32) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `added_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blood_bank_auth`
--

INSERT INTO `blood_bank_auth` (`login_id`, `username`, `password`, `user_id`, `added_date`, `updated_date`) VALUES
(1, 'admin@admin.com', '123456x', 1, '2020-03-11 01:01:01', '2020-03-11 01:01:01'),
(2, '9961357429', '123456x', 2, '2020-03-13 11:10:34', '2020-04-03 12:25:20'),
(3, '9685743652', '123456x', 3, '2020-03-17 13:10:45', '2020-03-17 13:11:32'),
(4, '8943707954', '123456x', 4, '2020-03-17 13:13:15', '2020-03-17 13:13:15'),
(5, '9427977329', '123456x', 5, '2020-03-17 13:17:07', '2020-03-17 13:17:07'),
(6, '9544530319', '123456x', 6, '2020-03-17 13:20:00', '2020-03-17 13:20:00'),
(7, '9654152638', '123456x', 7, '2020-03-17 13:22:51', '2020-03-17 13:22:51'),
(8, '8574693210', '123456x', 8, '2020-03-17 13:28:02', '2020-03-17 13:28:02'),
(9, '9652415263', '123456x', 9, '2020-03-17 13:32:53', '2020-03-17 13:33:39'),
(10, '8129941961', '123456x', 10, '2020-03-17 13:36:41', '2020-03-17 13:36:41'),
(11, '9745890453', '123456x', 11, '2020-03-17 13:39:27', '2020-03-17 13:39:27'),
(12, '9895074076', '123456x', 12, '2020-03-17 13:42:14', '2020-03-17 13:42:14'),
(13, '9542156325', '123456x', 13, '2020-03-17 13:46:16', '2020-03-17 13:46:16'),
(14, '8765432123', '123456x', 14, '2020-03-17 16:15:08', '2020-03-17 16:15:08'),
(15, '8765456787', '123456x', 15, '2020-03-17 16:17:40', '2020-03-17 16:17:40'),
(16, '9646562640', '123456x', 16, '2020-03-17 16:31:39', '2020-03-17 16:31:39'),
(17, '6345241111', '123456x', 17, '2020-03-17 16:33:48', '2020-03-17 16:33:48'),
(18, '6543277210', '123456x', 18, '2020-03-17 16:51:30', '2020-03-17 16:51:30'),
(19, '8086345418', '123456x', 19, '2020-03-17 16:57:32', '2020-03-17 16:59:06'),
(20, '8976545678', '123456x', 20, '2020-03-17 17:30:29', '2020-03-17 17:30:29'),
(21, '9567247198', '123456x', 21, '2020-03-17 17:32:46', '2020-03-17 17:32:46'),
(22, '9987988767', '123456x', 22, '2020-03-17 17:35:07', '2020-03-17 17:35:07'),
(23, '9961345467', '123456x', 23, '2020-03-17 17:39:39', '2020-03-17 17:39:39'),
(24, '8976567898', '123456x', 24, '2020-03-17 17:46:55', '2020-03-17 17:46:55'),
(25, '7345654789', '123456x', 25, '2020-03-17 18:18:07', '2020-03-17 18:18:07'),
(26, '7345657890', '123456x', 26, '2020-03-17 18:25:54', '2020-03-17 18:25:54'),
(27, '8976567311', '123456x', 27, '2020-03-17 18:28:22', '2020-03-17 18:28:22'),
(28, '7456321456', '123456x', 28, '2020-03-17 19:24:34', '2020-03-17 19:24:34'),
(29, '9876543257', '123456x', 29, '2020-03-17 19:32:29', '2020-03-17 19:32:29'),
(30, '9767548990', '123456x', 30, '2020-03-17 19:34:20', '2020-03-17 19:34:20'),
(31, '7986578789', '123456x', 31, '2020-03-17 19:37:31', '2020-03-17 19:37:31'),
(32, '9767859457', '123456x', 32, '2020-03-17 19:40:33', '2020-03-17 19:40:33'),
(33, '8847562216', '123456x', 33, '2020-03-17 19:54:29', '2020-03-17 19:56:02'),
(34, '8956412362', '123456x', 34, '2020-03-17 19:58:10', '2020-03-17 19:58:39'),
(35, '8652352125', '123456x', 35, '2020-03-17 20:03:39', '2020-03-17 20:03:39'),
(36, '8564352558', '123456x', 36, '2020-03-17 20:12:28', '2020-03-17 20:12:28'),
(37, '9652415365', '123456x', 37, '2020-03-17 20:19:18', '2020-03-17 20:19:18'),
(38, '8562542396', '123456x', 38, '2020-03-17 20:21:53', '2020-03-17 20:21:53'),
(39, '8542628789', '123456x', 39, '2020-03-17 20:31:14', '2020-03-17 20:31:14'),
(40, '9900253600', '123456x', 40, '2020-03-17 20:33:14', '2020-03-17 20:33:14'),
(41, '9962535478', '123456x', 41, '2020-03-17 20:36:25', '2020-03-17 20:36:25'),
(42, '8856241389', '123456x', 42, '2020-03-17 20:39:07', '2020-03-17 20:39:07'),
(48, '9544530319', '123456x', 48, '2020-04-01 12:16:52', '2020-04-01 12:16:52'),
(49, '9854756389', '123456x', 49, '2020-04-01 14:31:47', '2020-04-01 14:31:47'),
(50, '6282414138', '123456x', 50, '2020-04-01 14:48:05', '2020-04-01 14:48:05'),
(51, '9895512316', '123456x', 51, '2020-04-02 09:17:29', '2020-04-02 09:17:29'),
(53, '9633321061', 'asdfg', 53, '2020-04-02 11:18:04', '2020-04-03 14:01:54'),
(54, '9544530319', 'vhashirv', 54, '2020-04-02 14:27:26', '2020-04-02 14:27:26'),
(55, '6282414138', 'vhashirv', 55, '2020-04-02 15:12:32', '2020-04-02 15:12:32'),
(56, '6282414138', 'vhashirv', 56, '2020-04-02 15:14:57', '2020-04-02 15:14:57'),
(60, '8547534849', '000000', 60, '2020-04-02 18:10:09', '2020-04-02 18:10:09'),
(61, '9562786700', '000000', 61, '2020-04-02 18:34:28', '2020-04-02 18:34:28'),
(62, '9895022243', '000000', 62, '2020-04-02 19:28:03', '2020-04-02 19:28:03'),
(63, '6238472499', '000000', 63, '2020-04-02 19:48:36', '2020-04-02 19:48:36'),
(64, '9946330358', '000000', 64, '2020-04-02 21:29:05', '2020-04-02 21:29:05'),
(65, '9946182222', '000000', 65, '2020-04-02 21:43:32', '2020-04-02 21:43:32'),
(66, '9961357433', '000000', 66, '2020-04-03 11:34:41', '2020-04-03 13:23:23'),
(67, '9961357434', '000000', 67, '2020-04-03 12:37:22', '2020-04-03 12:43:04'),
(68, '8080808080', '000000', 68, '2020-04-03 13:48:02', '2020-04-03 13:48:02'),
(69, '9090909090', '000000', 69, '2020-04-03 14:06:02', '2020-04-03 14:06:02');

-- --------------------------------------------------------

--
-- Table structure for table `blood_bank_chat_history`
--

CREATE TABLE `blood_bank_chat_history` (
  `chat_id` int(11) NOT NULL,
  `chat_from` int(11) DEFAULT NULL,
  `chat_to` int(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `chat_msg` text DEFAULT NULL,
  `chat_type` tinyint(4) DEFAULT NULL,
  `req_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blood_bank_chat_history`
--

INSERT INTO `blood_bank_chat_history` (`chat_id`, `chat_from`, `chat_to`, `time`, `chat_msg`, `chat_type`, `req_id`) VALUES
(1, 2, 53, '2020-04-01 11:14:08', 'new blood request', 3, 4),
(2, 3, 2, '2020-04-04 11:14:08', 'new blood request', 3, 4),
(3, 3, 4, '2020-04-01 11:14:08', 'new blood request', 3, 4),
(4, 2, 5, '2020-04-01 11:14:08', 'new blood request', 3, 4),
(5, 2, 24, '2020-04-01 11:14:08', 'new blood request', 3, 4),
(6, 2, 27, '2020-04-01 11:14:08', 'new blood request', 3, 4),
(7, 6, 38, '2020-04-01 11:14:08', 'new blood request', 3, 4),
(8, 3, 2, '2020-04-01 11:51:18', 'new blood request', 3, 5),
(9, 2, 53, '2020-04-01 11:51:18', 'new blood request', 3, 5),
(10, 4, 3, '2020-04-02 09:47:14', 'new blood request', 3, 6),
(11, 2, 14, '2020-04-02 09:47:14', 'new blood request', 3, 6),
(12, 3, 53, '2020-04-02 09:47:53', 'new blood request', 3, 7),
(13, 2, 3, '2020-04-02 10:01:16', 'new blood request', 3, 8),
(14, 2, 53, '2020-04-02 10:01:16', 'new blood request', 3, 8);

-- --------------------------------------------------------

--
-- Table structure for table `blood_bank_country`
--

CREATE TABLE `blood_bank_country` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(100) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blood_bank_country`
--

INSERT INTO `blood_bank_country` (`country_id`, `country_name`, `added_date`, `updated_date`, `status`) VALUES
(1, 'India', '2020-03-16 00:00:00', '2020-03-16 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `blood_bank_device_master`
--

CREATE TABLE `blood_bank_device_master` (
  `device_mid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `dev_access_key` varchar(255) DEFAULT NULL,
  `dev_unique_id` varchar(255) DEFAULT NULL,
  `dev_notify_id` varchar(255) DEFAULT NULL,
  `dev_mstatus` tinyint(4) DEFAULT NULL,
  `added_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blood_bank_device_master`
--

INSERT INTO `blood_bank_device_master` (`device_mid`, `user_id`, `platform`, `dev_access_key`, `dev_unique_id`, `dev_notify_id`, `dev_mstatus`, `added_date`, `updated_date`) VALUES
(10, 51, 'ANDROID', 'appAccess_5e8561bd8e7ab1585799613584', '', '', 1, NULL, NULL),
(18, 55, 'ANDROID', 'appAccess_5e85de04368c41585831428223', '', '', 1, NULL, NULL),
(19, 59, 'ANDROID', 'appAccess_5e85dbe1be69f1585830881780', 'MKJ45265KH', '12548795522', 1, NULL, NULL),
(22, 60, 'ANDROID', 'appAccess_5e85dddbbe3b61585831387779', '', '', 1, NULL, NULL),
(23, 61, 'ANDROID', 'appAccess_5e85e2dc8a2461585832668566', '', '', 1, NULL, NULL),
(24, 62, 'ANDROID', 'appAccess_5e85ef6b6c08a1585835883443', '', '', 1, NULL, NULL),
(25, 63, 'ANDROID', 'appAccess_5e85f43c78aa51585837116494', '', '', 1, NULL, NULL),
(26, 64, 'ANDROID', 'appAccess_5e860bc9944bb1585843145607', '', '', 1, NULL, NULL),
(27, 65, 'ANDROID', 'appAccess_5e860f2cb30ca1585844012733', '', '', 1, NULL, NULL),
(32, 2, 'ANDROID', 'appAccess_5e86da327ae4e1585895986503', 'MKJ45265KH', '12548795522', 1, NULL, NULL),
(34, 67, 'ANDROID', 'appAccess_5e86e0aac3b321585897642802', 'MKJ45265KH', '12548795522', 1, NULL, NULL),
(35, 68, 'ANDROID', 'appAccess_5e86f13a052951585901882021', '', '', 1, NULL, NULL),
(36, 69, 'ANDROID', 'appAccess_5e86f5724f6c11585902962325', '', '', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blood_bank_hospital`
--

CREATE TABLE `blood_bank_hospital` (
  `hospital_id` int(11) NOT NULL,
  `hospital_name` varchar(100) NOT NULL,
  `place` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `contact_no2` varchar(15) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `image_url` text NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `hospital_status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blood_bank_hospital`
--

INSERT INTO `blood_bank_hospital` (`hospital_id`, `hospital_name`, `place`, `address`, `contact_no`, `contact_no2`, `latitude`, `longitude`, `image_url`, `added_date`, `updated_date`, `hospital_status`) VALUES
(1, 'MERCY HOSPITAL', 'Kodnotty', 'MAIN ROAD, Kondotty, Kerala 673638', '04832712073', '', '11.1456988', '75.96426319999999', 'uploads/hospital/7036711_id_5e85ac3b08727_binocular_.jpg', '2020-03-31 16:08:03', '2020-04-03 12:36:50', 1),
(2, 'Relief Hospital and TRAUMA Center', 'Kondotty', 'Kondotty Bypass Rd, Kondotty, Kerala 673638', '04832712078', '', '11.14343', '75.96573', 'uploads/hospital/5146776_id_5e85abed2ea75_binocular_.png', '2020-03-31 16:09:48', '2020-03-31 16:09:48', 1),
(3, 'Government hospital', 'Kondotty', 'Kondotty, Kerala 673638', '04832710586', '', '11.1516598', '75.9627222', 'uploads/hospital/7036711_id_5e85ac3b08727_binocular_.jpg', '2020-03-31 16:13:15', '2020-04-02 14:47:41', 1),
(4, 'Noufa Hospital', 'vengara', 'State Highway 72, Vettuthodu, Vengara, Kerala 676304', '04942455369', '', '11.052307', '75.9803165', 'uploads/hospital/4890888_id_5e85ac75af20d_binocular_.png', '2020-03-31 16:14:49', '2020-03-31 16:14:49', 1),
(5, 'Al Salama Hospital', 'vengara', 'Vengara Rd, Vettuthodu, Vengara, Kerala 676304', '04942455951', '', '11.051705', '75.9786012', 'uploads/hospital/7036711_id_5e85ac3b08727_binocular_.jpg', '2020-03-31 16:15:41', '2020-03-31 16:15:41', 1),
(6, 'Malabar Hospital', 'Vengara', 'Vengara Achanambalam Road, Kazhukanchina, Oorakam, Kerala 676304', '07034478313', '', '11.0510089', '75.9825348', 'uploads/hospital/7036711_id_5e85ac3b08727_binocular_.jpg', '2020-03-31 16:18:47', '2020-04-02 14:48:15', 1),
(7, 'Lailas Hospital', 'Chemmad', 'Thalappara Road, Chemmad, Kerala 676306', '04942464193', '', '11.0433995', '75.9113431', 'uploads/hospital/7036711_id_5e85ac3b08727_binocular_.jpg', '2020-03-31 16:22:08', '2020-03-31 16:22:08', 1),
(8, 'Government Hospitals', 'Tirurangadi', 'Tirurangadi, Kerala 676306', '04942460372', '', '11.1111052', '75.88736469999999', 'uploads/hospital/4890888_id_5e85ac75af20d_binocular_.png', '2020-03-31 16:24:08', '2020-04-02 14:48:45', 1),
(9, 'Korambayil Hospital & Diagnostic Centre (P) Ltd', 'Manjeri', 'Hospital in Manjeri, Kerala', '04832810000', '', '11.1228549', '76.1258179', 'uploads/hospital/4890888_id_5e85ac75af20d_binocular_.png', '2020-03-31 16:25:41', '2020-03-31 16:25:41', 1),
(10, 'Malabar Hospital', 'Manjeri', 'Melakkam, Manjeri, Kerala 676132', '04832805100', '', '11.1172507', '76.12238889999999', 'uploads/hospital/5146776_id_5e85abed2ea75_binocular_.png', '2020-03-31 16:26:26', '2020-04-02 14:49:13', 1),
(11, 'Prasanthi Hospital', 'Manjeri', 'Opp. Medical College Hospital, Court Rd, Manjeri, Kerala 676121', '04832769500', '', '11.1158917', '76.1186225', 'uploads/hospital/1373303_id_5e85acb64a45b_binocular_.jpg', '2020-03-31 16:29:02', '2020-04-02 14:43:26', 1),
(12, 'Manu Memorial Hospital', 'Manjeri', 'Nilambur - Malappuram Road, Melakkam, Manjeri, Kerala 676123', '04832763022', '', '11.1285396', '76.116294', 'uploads/hospital/4890888_id_5e85ac75af20d_binocular_.png', '2020-03-31 16:30:06', '2020-03-31 16:30:06', 1),
(13, 'Government Medical College', 'Manjeri', 'Hospital Rd, Vellarangal, Manjeri, Kerala 676121', '04832764056', '', '11.1170468', '76.119199', 'uploads/hospital/7036711_id_5e85ac3b08727_binocular_.jpg', '2020-03-31 16:31:03', '2020-03-31 16:31:03', 1),
(14, 'Ernad Hospital', 'Manjeri', 'Opposit Girls High School, Industrial Estate Rd, Karuvambram, Manjeri, Kerala 676121', '04832769040', '', '11.1190042', '76.1179537', 'uploads/hospital/2561138_id_5e85aca455e28_binocular_.jpg', '2020-03-31 16:31:52', '2020-04-02 14:43:08', 1),
(15, 'Pulikkal Hospital', 'Pulikkal', 'Kozhikode - Palakkad Hwy, Pulikkal, Kerala 673637', '04832790407', '', '11.1715485', '75.9226503', 'uploads/hospital/7057629_id_5e85aba37e49c_binocular_.jpg', '2020-03-31 16:32:53', '2020-03-31 16:32:53', 1),
(16, 'BM Hospital', 'Pulikkal', 'Kozhikode - Palakkad Hwy, Pulikkal, Kerala 673637', '08113989888', '', '11.1715694', '75.919652', 'uploads/hospital/4890888_id_5e85ac75af20d_binocular_.png', '2020-03-31 16:33:41', '2020-04-02 14:42:21', 1),
(17, 'Care well Hospital', 'Ramanattukara', 'Care Well Hospital Road, Ramanattukara, Kerala 673633', '04952440253', '', '11.1726109', '75.8676871', 'uploads/hospital/2576258_id_5e85ac51a83bb_binocular_.jpg', '2020-03-31 16:35:10', '2020-04-02 14:41:45', 1),
(18, 'ESI Hospital', 'Farok', 'NH 17, Ramanattukara, Kerala 673631', '04952442122', '', '11.185721', '75.84104870000002', 'uploads/hospital/7036711_id_5e85ac3b08727_binocular_.jpg', '2020-03-31 16:36:05', '2020-04-02 14:41:23', 1),
(19, 'MD Hospital', 'Ramanattukara', 'Palakkad, Ramanattukara, Kerala 673633', '09744378855', '', '11.1787643', '75.86514070000001', 'uploads/hospital/5146776_id_5e85abed2ea75_binocular_.png', '2020-03-31 16:37:32', '2020-04-02 14:40:05', 1),
(20, 'Manassanthi Hospital', 'Ramanattukara', 'kaithakundu-spinningmill road,kaithakundu., Ferokh - Palakkad Highway, Ramanattukara, Kerala 673634', '04832794451', '', '11.17563', '75.89197', 'uploads/hospital/7057629_id_5e85aba37e49c_binocular_.jpg', '2020-03-31 16:38:25', '2020-04-02 14:38:51', 1);

-- --------------------------------------------------------

--
-- Table structure for table `blood_bank_request`
--

CREATE TABLE `blood_bank_request` (
  `request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_of_need` date DEFAULT NULL,
  `no_of_units` tinyint(4) DEFAULT NULL,
  `location_lat` varchar(255) DEFAULT NULL,
  `location_long` varchar(255) DEFAULT NULL,
  `blood_group` tinyint(4) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_no` varchar(15) DEFAULT NULL,
  `place` varchar(255) NOT NULL,
  `added_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blood_bank_request`
--

INSERT INTO `blood_bank_request` (`request_id`, `user_id`, `date_of_need`, `no_of_units`, `location_lat`, `location_long`, `blood_group`, `contact_person`, `contact_no`, `place`, `added_date`, `updated_date`, `status`) VALUES
(4, 2, '2020-05-04', 1, '75.96426319999999', '11.1456988', 25, 'jauhar', '9961357429', '', '2020-04-01 11:14:08', '2020-04-01 11:14:08', 1),
(5, 2, '2020-08-05', 1, '76.1186225', '11.1158917', 5, 'ashiq', '9947526263', '', '2020-04-01 11:51:18', '2020-04-01 11:51:18', 1),
(6, 2, '2020-06-30', 1, '76.1186225', '11.1158917', 5, 'ashiq', '9947526263', '', '2020-04-02 09:47:14', '2020-04-02 09:47:14', 1),
(7, 2, '2020-06-20', 1, '76.1186225', '11.1158917', 5, 'ashiq', '9947526263', '', '2020-04-02 09:47:53', '2020-04-02 09:47:53', 1),
(8, 2, '2020-07-15', 1, '76.1186225', '11.1158917', 5, 'ashiq', '9947526263', '', '2020-04-02 10:01:16', '2020-04-02 10:01:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `blood_bank_state`
--

CREATE TABLE `blood_bank_state` (
  `state_id` int(11) NOT NULL,
  `state_name` varchar(100) NOT NULL,
  `country_id` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blood_bank_state`
--

INSERT INTO `blood_bank_state` (`state_id`, `state_name`, `country_id`, `added_date`, `updated_date`, `status`) VALUES
(1, 'kerala', 1, '2020-03-16 00:00:00', '2020-03-16 00:00:00', 1),
(2, 'Tamil Nadu', 1, '2020-03-16 00:00:00', '2020-03-16 00:00:00', 1),
(3, 'Punjab', 1, '2020-03-16 00:00:00', '2020-03-16 00:00:00', 1),
(4, 'jammuKshmir', 1, '2020-03-16 00:00:00', '2020-03-16 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `blood_bank_user`
--

CREATE TABLE `blood_bank_user` (
  `user_id` int(11) NOT NULL,
  `user_type` tinyint(4) DEFAULT NULL,
  `first_name` varchar(60) DEFAULT NULL,
  `last_name` varchar(60) DEFAULT NULL,
  `email` varchar(96) DEFAULT NULL,
  `contact_no` varchar(15) DEFAULT NULL,
  `blood_group` tinyint(4) DEFAULT NULL,
  `dob` date NOT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `country` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `place` varchar(255) NOT NULL,
  `home_location_lat` varchar(40) DEFAULT NULL,
  `home_location_long` varchar(40) DEFAULT NULL,
  `home_location_place` varchar(255) NOT NULL,
  `work_location_lat` varchar(40) DEFAULT NULL,
  `work_location_long` varchar(40) DEFAULT NULL,
  `work_location_place` varchar(255) NOT NULL,
  `last_date_donation` date DEFAULT NULL,
  `pro_image` text NOT NULL,
  `added_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blood_bank_user`
--

INSERT INTO `blood_bank_user` (`user_id`, `user_type`, `first_name`, `last_name`, `email`, `contact_no`, `blood_group`, `dob`, `gender`, `address`, `country`, `state`, `place`, `home_location_lat`, `home_location_long`, `home_location_place`, `work_location_lat`, `work_location_long`, `work_location_place`, `last_date_donation`, `pro_image`, `added_date`, `updated_date`, `status`) VALUES
(1, 10, 'Super ', 'Admin', 'admin@admin.com', '123456789', 1, '0000-00-00', NULL, NULL, '', '', '', NULL, NULL, '', NULL, NULL, '', NULL, '', '2020-03-11 01:01:01', '2020-03-11 01:01:01', 1),
(2, 30, 'Irshad', 'kuruniyan', 'irshadbinkunharamu@gmial.com', '9961357429', 25, '2020-09-13', 1, 'kuruniyan house po', 'india', 'kerala', '', '75.36524', '96.85726', 'kakkathadam, parambile peedika', '75.625439', '96.255547', 'kadappady', '2020-03-31', 'uploads/profile/2020-03-31/irshad.jpg', '2020-03-13 11:10:34', '2020-03-17 13:14:56', 1),
(3, 30, 'Muhsin', 'Abdulla', 'muhsinabdulla@gmail.com', '9685743652', 5, '2000-07-16', 1, 'thekkedath house, po vengara, pin: 635265,  malappuram', 'India', 'kerala', '', '11.0510089', '75.9825348', '', '11.0447831', '75.9100311', '', '2020-01-02', '', '2020-03-17 13:10:45', '2020-03-17 13:11:32', 1),
(4, 30, 'Ashiq', 'Kuruniyan', 'ashiqkuruniyan7@gmail.com', '8943707954', 25, '1983-07-16', 1, 'KURUNIYAN HOUSE\r\nPARAMBIL PEEDIKA PO', 'India', 'kerala', '', '11.1060436', '75.92865259999999', 'kakkathadam, parambile peedika', '11.0510089', '75.9825348', '', '2019-12-11', 'uploads/profile/2020-03-31/irshad.jpg', '2020-03-17 13:13:15', '2020-03-17 13:13:15', 1),
(5, 30, 'Jazeel', 'k', 'jazeelk@gmail.com', '9427977329', 25, '1999-07-16', 1, 'KURUNIYAN HOUSE\r\nPARAMBIL PEEDIKA PO, MALAPPURAM', 'India', 'kerala', '', '11.1060436', '75.92865259999999', '', '11.012658', '75.995089', '', '2019-08-15', '', '2020-03-17 13:17:07', '2020-03-17 13:17:07', 1),
(6, 30, 'Hashir', 'v', 'peapieng2@gmail.com', '9544530319', 15, '1981-07-16', 1, 'Vellanrakeeri (h), Malappuram po, pin: 985635, malappuram', 'India', 'kerala', '', '11.0509762', '76.0710967', '', '11.0515208', '75.9820113', '', '2020-03-05', '', '2020-03-17 13:20:00', '2020-03-17 13:20:00', 1),
(7, 30, 'Shreedin', 'Radakrishnan', 'shreedhinradhakrishnan@gmail.com', '9654152638', 10, '1999-07-16', 1, 'Maleveettil (h), kondotty (po), pin: 635241, malappuram', 'India', 'kerala', '', '11.1456988', '75.96426319999999', '', '10.9989697', '75.9917634', '', '2020-03-01', '', '2020-03-17 13:22:51', '2020-03-17 13:22:51', 1),
(8, 30, 'Ali', 'Akbar', 'ali09akbar@gmail.com', '8574693210', 35, '1998-07-16', 1, 'Chinakkal (h), peruvallur po, pin: 676317, malappuram', 'India', 'kerala', '', '11.0971445', '75.93537839999999', '', '11.086401', '75.953301', '', '2020-01-22', '', '2020-03-17 13:28:02', '2020-03-17 13:28:02', 1),
(9, 30, 'Sangeeth', 'k dev', 'sangeethkdev@gmail.com', '9652415263', 30, '1981-07-16', 1, 'kattungal (h), chelari (po), malappuram', 'India', 'kerala', '', '11.11203', '75.88434629999999', '', '11.0754766', '75.9845141', '', '2020-02-20', '', '2020-03-17 13:32:53', '2020-03-17 13:33:39', 1),
(10, 30, 'shajahan', 'm', 'engwizzo@gmail.com', '8129941961', 20, '1988-07-16', 1, 'maliyekkal house, velluvampuram po, pin: 562436, malappuram', 'India', 'kerala', '', '11.1336197', '76.0369087', '', '11.0515208', '75.9820113', '', '2019-07-16', '', '2020-03-17 13:36:41', '2020-03-17 13:36:41', 1),
(11, 30, 'Hanshad', 'ek', 'peapieng03@gmail.com', '9745890453', 40, '1979-07-16', 1, 'ek house, kondotty po, pin: 254163', 'India', 'kerala', '', '11.1485451', '75.9580753', '', '11.0515208', '75.9820113', '', '2019-09-10', '', '2020-03-17 13:39:27', '2020-03-17 13:39:27', 1),
(12, 30, 'Abdul jaleel', 'pp', 'jaleelpp@gmail.com', '9895074076', 15, '1998-07-16', 1, 'pp house, vengara po, malappuram', 'India', 'kerala', '', '11.0516419', '75.97635129999999', '', '11.0515208', '75.9820113', '', '2020-01-27', '', '2020-03-17 13:42:14', '2020-03-17 13:42:14', 1),
(13, 30, 'Amina Shirin', 'A', 'aminashirin@gmail.com', '9542156325', 35, '1988-07-16', 2, 'Athrappil house, po kondotty, malappuram', 'India', 'kerala', '', '11.1405932', '75.94888410000001', '', '11.1458734', '75.9642822', '', '2020-02-28', '', '2020-03-17 13:46:16', '2020-03-17 13:46:16', 1),
(14, 30, 'Saalu Sadiq', 'K', 'salusadiq@gmail.com', '8765432123', 5, '1978-07-16', 1, 'Karadan house, po vengara', 'India', 'kerala', '', '11.0774614', '75.9769659', '', '11.0386125', '75.9377733', '', '2019-12-20', '', '2020-03-17 16:15:08', '2020-03-17 16:15:08', 1),
(15, 30, 'Raees', 'Katteeri', 'raeeakatteeri@gmail.com', '8765456787', 20, '1989-07-16', 1, 'Katteeri (house) po parambil peedija', 'India', 'kerala', '', '11.0992224', '75.92006479999999', '', '11.0428879', '75.91279870000001', '', '2020-03-01', '', '2020-03-17 16:17:40', '2020-03-17 16:17:40', 1),
(16, 30, 'Jauhar', 'A', 'jauharxlr@gmail.com', '9646562640', 30, '1975-07-16', 1, 'Athrappil (h) , peruvallur po, malappuram', 'India', 'kerala', '', '11.0971445', '75.93537839999999', '', '11.0404334', '75.9215454', '', '2018-10-09', '', '2020-03-17 16:31:39', '2020-03-17 16:31:39', 1),
(17, 30, 'Fayis', 'Chemban', 'fayischemban@gmail.com', '6345241111', 15, '1989-07-16', 1, 'Chemban (h), parambil peedika po, pin : 676317, malappuram', 'India', 'kerala', '', '11.0991784', '75.9195895', '', '10.9394906', '75.9836796', '', '2019-09-18', '', '2020-03-17 16:33:48', '2020-03-17 16:33:48', 1),
(18, 30, 'Junaid', 'C', 'Junaid756@gmail.com', '6543277210', 35, '1978-07-16', 1, 'Pullat house , po vengara , malappuram', 'India', 'kerala', '', '11.0782836', '75.9768028', '', '10.9989697', '75.9917634', '', '2020-03-01', '', '2020-03-17 16:51:30', '2020-03-17 16:51:30', 1),
(19, 20, 'bilal', 'kurishingal', NULL, '8086345418', 5, '1991-07-16', 1, 'forthkochi ,7th street ,home no 5', '', '', '', '9.9657787', '76.2421147', '', NULL, NULL, '', NULL, '', '2020-03-17 16:57:32', '2020-03-17 16:59:06', 1),
(20, 30, 'Suhail', 'Poongadan', 'Suhailpoongadan@gmail.com', '8976545678', 20, '1980-07-16', 1, 'Poongadan h, kannamankalam po, malappuram', 'India', 'kerala', '', '11.0909102', '75.9811564', '', '11.0419582', '75.9281127', '', '2020-02-20', '', '2020-03-17 17:30:29', '2020-03-17 17:30:29', 1),
(21, 30, 'shahid', 'mnk', 'shahidmnk007@gmail.com', '9567247198', 5, '1994-07-16', 1, 'chudakkadam\r\nkondotty po\r\nkondotty', 'India', 'kerala', '', '11.0357728', '75.9383404', '', '11.0447831', '75.9100311', '', '2020-02-12', '', '2020-03-17 17:32:46', '2020-03-17 17:32:46', 1),
(22, 30, 'Ansar', 'Pulikkal', 'Ansarpulikkal7@gmail.com', '9987988767', 10, '1978-07-16', 1, 'Pulikkal house, kondotty po, malappuram', 'India', 'kerala', '', '11.1776595', '75.9188786', '', '11.1123084', '75.9428674', '', '2020-03-01', '', '2020-03-17 17:35:07', '2020-03-17 17:35:07', 1),
(23, 30, 'Nidhash', 'Ck', 'Nidashneedhu@gmail.com', '9961345467', 40, '1993-07-16', 1, 'Chathambatt kalathil house, parambil peedika po, malappuram', 'India', 'kerala', '', '11.1053037', '75.9329669', '', '11.1093513', '75.9332596', '', '2020-01-25', '', '2020-03-17 17:39:39', '2020-03-17 17:39:39', 1),
(24, 30, 'Saidalavi', 'Ck', 'Saidalavi@gmail.com', '8976567898', 25, '1980-07-16', 1, 'Ck house, po parambil peedika', 'India', 'kerala', '', '11.1144597', '75.9245494', '', '11.1132116', '75.9564843', '', '2020-03-11', '', '2020-03-17 17:46:55', '2020-03-17 17:46:55', 1),
(25, 30, 'Ashraf', 'Edasheri', 'Ashraf@gmail.com', '7345654789', 15, '1990-07-16', 1, 'Edasheri house, kannamankalam po', 'India', 'kerala', '', '11.0704132', '75.9855098', '', '11.0566143', '75.9855098', '', '2020-03-15', '', '2020-03-17 18:18:07', '2020-03-17 18:18:07', 1),
(26, 30, 'Nazer', 'Mattil', 'Nazermattil@gmail.com', '7345657890', 30, '1970-07-16', 1, 'Mattil house, po chenakkal', 'India', 'kerala', '', '11.0396617', '75.9669614', '', '11.1077317', '75.89041069999999', '', '2019-11-14', '', '2020-03-17 18:25:54', '2020-03-17 18:25:54', 1),
(27, 30, 'Narayanan', 'N', 'Narayanann@gmail.con', '8976567311', 25, '1997-07-16', 1, 'Nellimattath house, po chemmad, malappuram', 'India', 'kerala', '', '11.11943', '75.9129349', '', '11.0739181', '75.90154729999999', '', '2019-12-27', '', '2020-03-17 18:28:22', '2020-03-17 18:28:22', 1),
(28, 30, 'Shribil shri', 'M', 'Shribilshri@gmail.com', '7456321456', 35, '1986-07-16', 1, 'Mangalam house, po kondotty', 'India', 'kerala', '', '11.1278443', '75.97410010000002', '', '11.1340267', '75.89523539999999', '', '2020-02-13', '', '2020-03-17 19:24:34', '2020-03-17 19:24:34', 1),
(29, 30, 'Rabeeh', 'Pp', 'Rabeehpp@gmail.com', '9876543257', 30, '1990-07-16', 1, 'Pp house, kannamankalam po, malappuram', 'India', 'kerala', '', '11.0814008', '75.9246414', '', '10.9918935', '75.9566579', '', '2019-09-19', '', '2020-03-17 19:32:29', '2020-03-17 19:32:29', 1),
(30, 30, 'Shahid', 'Ck', 'Shahidmnk@gmail.con', '9767548990', 15, '1988-07-16', 1, 'Chathambatt kalathil house,po peruvallur', 'India', 'kerala', '', '11.0598114', '75.9361549', '', '11.1123084', '75.9428674', '', '2019-12-10', '', '2020-03-17 19:34:20', '2020-03-17 19:34:20', 1),
(31, 30, 'Sahil', 'K', 'Sahilk8909@gmail.com', '7986578789', 5, '1994-07-16', 1, 'Karadan house,po parappanagadi', 'India', 'kerala', '', '11.0539705', '75.8555185', '', '11.0451992', '75.91875340000001', '', '2019-11-14', '', '2020-03-17 19:37:31', '2020-03-17 19:37:31', 1),
(32, 30, 'Sahad', 'M', 'Melemuttedathsahad@gmail.com', '9767859457', 40, '1988-07-16', 1, 'Melemuttedath house, po chelari, malappuram', 'India', 'kerala', '', '11.0704132', '75.9855098', '', '11.1201017', '75.95795249999999', '', '2019-12-12', '', '2020-03-17 19:40:33', '2020-03-17 19:40:33', 1),
(33, 30, 'Fasil', 'K', 'fasilkuruniyan@gmail.com', '8847562216', 15, '1994-07-16', 1, 'kuruniyan h, po vengara', 'India', 'kerala', '', '11.0579322', '75.9867142', '', '11.1287166', '75.9585699', '', '2020-03-05', '', '2020-03-17 19:54:29', '2020-03-17 19:56:02', 1),
(34, 30, 'hari krishnan', 'b', 'harikrishnan@gmail.com', '8956412362', 30, '1988-07-16', 1, 'bala sudinam (h), vengara po, malappuram', 'India', 'kerala', '', '11.0919705', '75.9545035', '', '11.1168114', '76.0151075', '', '2020-01-17', '', '2020-03-17 19:58:10', '2020-03-17 19:58:39', 1),
(35, 30, 'safwan', 'c', 'safwanchundakkadan@gmail.com', '8652352125', 35, '1991-09-05', 1, 'chundakkadan (h), po kondotty', 'India', 'kerala', '', '11.1521573', '75.8963595', '', '11.1355974', '75.9408807', '', '2019-11-13', '', '2020-03-17 20:03:39', '2020-03-17 20:03:39', 1),
(36, 30, 'anwar', 'sadath', 'anwarsadath87@gmail.com', '8564352558', 40, '1982-10-20', 1, 'pulikkal house, po vengara', 'India', 'kerala', '', '11.0516419', '75.97635129999999', '', '11.1003494', '75.90349739999999', '', '2020-01-15', '', '2020-03-17 20:12:28', '2020-03-17 20:12:28', 1),
(37, 30, 'hashik', 'lakkal', 'hashiklakkal@gmail.com', '9652415365', 20, '1996-02-29', 1, 'lakkal house, po kannamankalam', 'India', 'kerala', '', '11.0919705', '75.9545035', '', '11.0413102', '75.9327723', '', '2020-02-28', '', '2020-03-17 20:19:18', '2020-03-17 20:19:18', 1),
(38, 30, 'shameem', 'c', 'shameemhg@gmail.com', '8562542396', 25, '1990-06-18', 1, 'cheruvannur house, po velluvampuram', 'India', 'kerala', '', '11.0512918', '76.06568829999999', '', '11.1339518', '75.942781', '', '2020-02-27', '', '2020-03-17 20:21:53', '2020-03-17 20:21:53', 1),
(39, 30, 'shakkir', 'k', 'shakkir8788@gmail.com', '8542628789', 25, '1995-11-25', 1, 'koyithodi house, po kannamangalam', 'India', 'kerala', '', '11.1091372', '75.9902939', '', '11.0722708', '75.9956673', '', '2020-01-15', '', '2020-03-17 20:31:14', '2020-03-17 20:31:14', 1),
(40, 30, 'jilshad', 'k', 'jilshadjilshu@gmail.com', '9900253600', 10, '1989-12-14', 1, 'koorithodi (h), po kondotty', 'India', 'kerala', '', '10.6790493', '76.19286400000001', '', '11.0711221', '75.8893391', '', '2020-03-06', '', '2020-03-17 20:33:14', '2020-03-17 20:33:14', 1),
(41, 30, 'mansoor', 'l', 'mansoorl@gmail.com', '9962535478', 10, '1992-05-03', 1, 'latheen house, po peruvallur', 'India', 'kerala', '', '11.1228056', '75.93591140000001', '', '11.1337937', '76.03492690000002', '', '2019-11-27', '', '2020-03-17 20:36:25', '2020-03-17 20:36:25', 1),
(42, 30, 'imranka', 'n', 'imrankan@gmail.com', '8856241389', 40, '1980-03-20', 1, 'nalakath house, po malappuram', 'India', 'kerala', '', '11.038779', '76.0771013', '', '10.9999326', '75.9982145', '', '2019-11-25', '', '2020-03-17 20:39:07', '2020-03-17 20:39:07', 1),
(53, 30, 'fuhad', NULL, NULL, '9633321061', 40, '1998-03-26', 1, 'chathanad house, po peruvallur', 'India', '', '', '11.1124205', '75.9339387', 'Unnamed Road, kakkathadam, Peruvallur, Kerala 673647, India', '', '', '', '2020-01-29', '', '2020-04-02 11:18:04', '2020-04-02 11:18:04', 1),
(54, 30, 'hashirr', NULL, NULL, '9544530319', 25, '2020-04-02', 2, 'xhxx xbxnxnxxxn', 'India', '', '', '11.0407775', '76.0697435', '', '', '', '', '2020-04-02', '', '2020-04-02 14:27:26', '2020-04-02 14:27:26', 1),
(55, 30, 'Hashir Muhammed V', NULL, NULL, '6282414138', 15, '2020-04-02', 2, 'xxnxnxnx', 'India', '', '', '11.0394061', '76.0659146', '', '', '', '', '2020-04-02', '', '2020-04-02 15:12:32', '2020-04-02 15:12:32', 1),
(56, 30, 'hashir jxzk', NULL, NULL, '6282414138', 20, '2020-04-02', 1, 'xxnxjxnxxx', 'India', '', '', '11.0394061', '76.0659146', '', '', '', '', '2020-04-02', '', '2020-04-02 15:14:57', '2020-04-02 15:14:57', 1),
(60, 30, 'zhnxbhchc', NULL, NULL, '8547534849', 30, '2020-04-02', 1, 'BBC jxnx\ndbxjxhxjxhx\nhxjjx', 'India', '', 'Unnamed Road, Mylappuram, Down Hill, Malappuram, Kerala 676505, India', '11.0394061', '76.0659146', '', '', '', '', '2020-04-02', '', '2020-04-02 18:10:09', '2020-04-02 18:10:09', 1),
(61, 30, 'ADHNAN OC', NULL, NULL, '9562786700', 5, '1990-02-05', 1, 'kolappuram', 'India', '', 'Irumbuchola Rd, Irumbuchola, Kerala 676311, India', '11.0606834', '75.929086', '', '', '', '', '2019-12-08', '', '2020-04-02 18:34:28', '2020-04-02 18:34:28', 1),
(62, 30, 'fathima shahana.k', NULL, NULL, '9895022243', 5, '2001-02-10', 2, 'shas mahal kunnummal (H) chuzhali moonniyuor south', 'India', '', 'Areethala, Irumbuchola, Kerala 676311, India', '11.0596243', '75.928424', '', '', '', '', '2018-04-02', '', '2020-04-02 19:28:03', '2020-04-02 19:28:03', 1),
(63, 30, 'Raheesa Adhnan', NULL, NULL, '6238472499', 25, '1997-12-01', 2, 'Kolappuram areethala', 'India', '', 'Areethala, Irumbuchola, Kerala 676311, India', '11.0596243', '75.928424', '', '', '', '', '2019-09-13', '', '2020-04-02 19:48:36', '2020-04-02 19:48:36', 1),
(64, 30, 'Sabeena Raheem', NULL, NULL, '9946330358', 15, '1982-02-16', 2, 'karappam veetil house\n Koorkenchery\nThrissur\nkerala', 'India', '', 'Muttichur Rd, Muttichur, Anthikad, Kerala 680641, India', '10.4444235', '76.10250089999998', '', '', '', '', '2020-02-28', '', '2020-04-02 21:29:05', '2020-04-02 21:29:05', 1),
(65, 30, 'Saniyya Raheem', NULL, NULL, '9946182222', 15, '2000-01-28', 2, 'karappam veettil house\nKoorkenchery\nThrissur\nKerala', 'India', '', 'Muttichur Rd, Muttichur, Anthikad, Kerala 680641, India', '10.4444235', '76.10250089999998', '', '', '', '', '2019-10-16', '', '2020-04-02 21:43:32', '2020-04-02 21:43:32', 1),
(66, 30, 'jasnimol', NULL, NULL, '9961357433', 30, '2020-09-13', 1, 'kuruniyan house po', 'India', '', '', '11.1124205', '75.9339387', 'Unnamed Road, kakkathadam, Peruvallur, Kerala 673647, India', '', '', '', '2020-03-31', 'uploads/profile/2020-04-03/03_04_2020_11_34_41_jasn2.jpg', '2020-04-03 11:34:41', '2020-04-03 11:34:41', 1),
(67, 30, 'mehrin', NULL, NULL, '9961357434', 25, '2020-09-13', 1, 'kuruniyan house po', 'india', '', '', '75.36524', '96.85726', 'kakkathadam, parambile peedika', '75.625439', '96.255547', 'kadappady', '2020-03-31', 'uploads/profile/2020-04-03/03_04_2020_12_43_04_mehrin.jpg', '2020-04-03 12:37:22', '2020-04-03 12:37:22', 1),
(68, 30, 'sadath', NULL, NULL, '8080808080', 40, '1978-01-23', 1, 'chakkipparamban house po chelari', 'India', '', '', '11.1124205', '75.9339387', 'Unnamed Road, kakkathadam, Peruvallur, Kerala 673647, India', '', '', '', '2020-02-26', '', '2020-04-03 13:48:02', '2020-04-03 13:48:02', 1),
(69, 30, 'Shahid manzil', NULL, NULL, '9090909090', 5, '2019-12-25', 1, 'katteeri', 'India', '', '', '11.1127768', '75.929086', 'Kuruniyan Rd, Chathrathodi, Peruvallur, Kerala 673647, India', '', '', '', '2020-04-01', '', '2020-04-03 14:06:02', '2020-04-03 14:06:02', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blood_bank_auth`
--
ALTER TABLE `blood_bank_auth`
  ADD PRIMARY KEY (`login_id`);

--
-- Indexes for table `blood_bank_chat_history`
--
ALTER TABLE `blood_bank_chat_history`
  ADD PRIMARY KEY (`chat_id`);

--
-- Indexes for table `blood_bank_country`
--
ALTER TABLE `blood_bank_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `blood_bank_device_master`
--
ALTER TABLE `blood_bank_device_master`
  ADD PRIMARY KEY (`device_mid`);

--
-- Indexes for table `blood_bank_hospital`
--
ALTER TABLE `blood_bank_hospital`
  ADD PRIMARY KEY (`hospital_id`);

--
-- Indexes for table `blood_bank_request`
--
ALTER TABLE `blood_bank_request`
  ADD PRIMARY KEY (`request_id`);

--
-- Indexes for table `blood_bank_state`
--
ALTER TABLE `blood_bank_state`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `blood_bank_user`
--
ALTER TABLE `blood_bank_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blood_bank_auth`
--
ALTER TABLE `blood_bank_auth`
  MODIFY `login_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `blood_bank_chat_history`
--
ALTER TABLE `blood_bank_chat_history`
  MODIFY `chat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `blood_bank_country`
--
ALTER TABLE `blood_bank_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blood_bank_device_master`
--
ALTER TABLE `blood_bank_device_master`
  MODIFY `device_mid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `blood_bank_hospital`
--
ALTER TABLE `blood_bank_hospital`
  MODIFY `hospital_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `blood_bank_request`
--
ALTER TABLE `blood_bank_request`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `blood_bank_state`
--
ALTER TABLE `blood_bank_state`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `blood_bank_user`
--
ALTER TABLE `blood_bank_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
